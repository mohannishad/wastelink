from django.shortcuts import render, redirect
from os import path
from django.utils import translation
from python_graphql_client import GraphqlClient
from entity.views import entity_list
from wastelink.settings import client
from django.utils.translation import gettext as _
from django.utils.translation import get_language,activate,gettext
def indexview(request):
#   not len(dict(request.session))>0

  trans=translate(language='hi')
  if len(dict(request.session))>0:
    # a = request.session['username']
    request.session['offset']=0
    request.session['pgno']=1
    request.session['max_rows']=10
    request.session['search_data']=None
    request.session['entity_type']=None
    request.session['entity_purpose']=None
    request.session['industry']=None
    request.session['sort']=None
    request.session['num']=None
    #==========Start Master Entity Purpose====
    request.session["entity_purpose_offset"]=0
    request.session["entity_purpose_first"]=10
    request.session['entityPurposeCurrentPage']=1
    request.session['entityPurposePageSize']=None
    request.session['entityPurposeSearch']=None
    request.session['entityPurposeSorting']=None
    #==========End Master Entity Purpose====

    #========== Start Master Entity Type =====
    request.session["masterEntityTypeOffset"]=0
    request.session["masterEntityTypeFirst"]=10
    request.session["masterEntityTypeCurrentPage"]=1
    request.session['masterEntityTypePageSize']=None
    request.session['masterEntityTypeSearch']=None
    request.session['masterEntityTypeIsBase']=None
    request.session['masterEntityTypeHasChildren']=None
    request.session['masterEntityTypeSorting']=None


    #========== End Master Entity Type =====
    #========== Start Master Crm Common Status =====
    request.session['masterCrmCommonStatusOffset']=0
    request.session['masterCrmCommonStatusFirst']=10
    request.session["masterCrmCommonStatusCurrentPage"]=1
    request.session["masterCrmCommonStatusSearch"]=None
    request.session["masterCrmCommonStatusSize"]=None
    request.session["masterCrmCommonStatusSorting"]=None
    #========== End Master Crm Common Status =====

    #========== Start Master Crm Common Type =====
    request.session['masterCrmCommonTypeOffset']=0
    request.session['masterCrmCommonTypeFirst']=10
    request.session['masterCrmCommonTypeCurrentPage']=1
    request.session['masterCrmCommonTypeSearch']=None
    request.session['masterCrmCommonTypeSize']=None
    request.session['masterCrmCommonTypeSorting']=None
    #========== Start Master Crm Common Type =====

    #======== Start Master Crm Source===
    request.session['masterCrmSourceOffset']=0
    request.session['masterCrmSourceFirst']=10
    request.session['masterCrmSourceCurrentPage']=1
    request.session['masterCrmSourceSearch']=None
    request.session['masterCrmSourceSize']=None
    request.session['masterCrmSourceSorting']=None

    #======= End Master Crm Source=======
    #======== Start Master Crm Status===
    request.session['masterCrmStatusOffset']=0
    request.session['masterCrmStatusFirst']=10
    request.session['masterCrmStatusCurrentPage']=1
    request.session['masterCrmStatusSearch']=None
    request.session['masterCrmStatusSize']=None
    request.session['masterCrmStatusSorting']=None
    #======= End Master Crm Status=======
    #======== Start Master User Type===
    request.session['masterUserTypeOffset']=0
    request.session['masterUserTypeFirst']=10
    request.session['masterUserTypeCurrentPage']=1
    request.session['masterUserTypeSearch']=None
    request.session['masterUserTypeSize']=None
    request.session['masterUserTypeSorting']=None
    #======= End Master User Type=======
    #======== Start Master User Type===
    request.session['masterPincodeOffset']=0
    request.session['masterPincodeFirst']=10
    request.session['masterPincodeCurrentPage']=1
    request.session['masterPincodeSearch']=None
    request.session['masterPincodeSize']=None
    request.session['masterPincodeSorting']=None
    #======= End Master User Type=======

    #=======start master Industry=========
    request.session['masterIndustryOffset']=0
    request.session['masterIndustryFirst']=10
    request.session['masterIndustryCurrentPage']=1
    request.session['masterIndustrySearch']=None
    request.session['masterIndustryMaxRows']=None
    request.session['masterIndustrySort']=None
    request.session['masterIndustrySize']=None
    #=======end master Industry=========

    #=======Start Master Product Category======
    request.session['masterProductCategoryOffset']=0
    request.session['masterProductCategoryFirst']=10
    request.session['masterProductCategoryCurrentPage']=1
    request.session['masterProdctCategorySearch']=None
    request.session['masterProdctCategoryCode']=None
    request.session['masterProductCategorySort']=None
    request.session['masterProductCategorySize']=None
   
    #=======End Master Product Category========
    #======== Start Master Product SubCategory===
    request.session['masterSubCategoryOffset']=0
    request.session['masterSubCategoryFirst']=10
    request.session['masterSubCategoryCurrentPage']=1
    request.session['masterSubCategorySearch']=None
    request.session['masterSubCategoryCode']=None
    request.session['masterSubCategoryCategory']=None
    request.session['masterSubCategorySize']=None
    request.session['masterSubCategorySorting']=None
    #======= End Master Product SubCategory=======
    #======== Start Master Product Nutrition Type===
    request.session['masterNutritionTypeOffset']=0
    request.session['masterNutritionTypeFirst']=10
    request.session['masterNutritionTypeCurrentPage']=1
    request.session['masterNutritionTypeSearch']=None
    request.session['masterNutritionTypeSize']=None
    request.session['masterNutritionTypeSorting']=None
    #======= End Master Product Nutrition Type=======
    #======== Start Master Product Measure Type===
    request.session['masterMeasureTypeOffset']=0
    request.session['masterMeasureTypeFirst']=10
    request.session['masterMeasureTypeCurrentPage']=1
    request.session['masterMeasureTypeSearch']=None
    request.session['masterMeasureTypeSize']=None
    request.session['masterMeasureTypeSorting']=None
    #======= End Master Product Measure Type=======
    #======== Start Master Product Measure Unit===
    request.session['masterMeasureUnitOffset']=0
    request.session['masterMeasureUnitFirst']=10
    request.session['masterMeasureUnitCurrentPage']=1
    request.session['masterMeasureUnitSearch']=None
    request.session['masterMeasureUnitSize']=None
    request.session['masterMeasureUnitSorting']=None
    #======= End Master Product Measure Unit==
    #======== Start Master Product Next Step===
    request.session['masterNextStepOffset']=0
    request.session['masterNextStepFirst']=10
    request.session['masterNextStepCurrentPage']=1
    request.session['masterNextStepSearch']=None
    request.session['masterNextStepSize']=None
    request.session['masterNextStepSorting']=None
    #======= End Master Product Next Step==
    #======== Start Master Product State===
    request.session['masterStateOffset']=0
    request.session['masterStateFirst']=10
    request.session['masterStateCurrentPage']=1
    request.session['masterStateSearch']=None
    request.session['masterStateSize']=None
    request.session['masterStateSorting']=None
    #======= End Master Product State==
    #======== Start Master Product Taste===
    request.session['masterTasteOffset']=0
    request.session['masterTasteFirst']=10
    request.session['masterTasteCurrentPage']=1
    request.session['masterTasteSearch']=None
    request.session['masterTasteSize']=None
    request.session['masterTasteSorting']=None
    #======= End Master Product Taste==
     #=========== Start Salutation======
    request.session['masterSalutationOffset']=0
    request.session['masterSalutationFirst']=10
    request.session['masterSalutationCurrentPage']=1
    request.session['masterSalutationSearch']=None
    request.session['masterSalutationSize']=None
    request.session['masterSalutationSorting']=None

 #=========== End Saluation========
     #=========== Start Product Packaging======
    request.session['masterProductPackagingOffset']=0
    request.session['masterProductPackagingFirst']=10
    request.session['masterProductPackagingCurrentPage']=1
    request.session['masterProductPackagingSearch']=None
    request.session['masterProductPackagingSize']=None
    request.session['masterProductPackagingSorting']=None

   #=========== End Product Packaging========

 #=========== Start Designation======
    request.session['masterDesignationOffset']=0
    request.session['masterDesignationFirst']=10
    request.session['masterDesignationCurrentPage']=1
    request.session['masterDesignationSearch']=None
    request.session['masterDesignationSize']=None
    request.session['masterDesignationSorting']=None

 #=========== End Designation========
 #=========== Start Transaction Type======
    request.session['masterTransactionTypeOffset']=0
    request.session['masterTransactionTypeFirst']=10
    request.session['masterTransactionTypeCurrentPage']=1
    request.session['masterTransactionTypeSearch']=None
    request.session['masterTransactionTypeSize']=None
    request.session['masterTransactionTypeSorting']=None

 #=========== End Transaction Type========
 #=========== Start Transaction Status======
    request.session['masterTransactionStatusOffset']=0
    request.session['masterTransactionStatusFirst']=10
    request.session['masterTransactionStatusCurrentPage']=1
    request.session['masterTransactionStatusSearch']=None
    request.session['masterTransactionStatusSize']=None
    request.session['masterTransactionStatusSorting']=None

 #=========== End Transaction Status========
 #=========== Start Transaction Doctype======
    request.session['masterTransactionDoctypeOffset']=0
    request.session['masterTransactionDoctypeFirst']=10
    request.session['masterTransactionDoctypeCurrentPage']=1
    request.session['masterTransactionDoctypeSearch']=None
    request.session['masterTransactionDoctypeSize']=None
    request.session['masterTransactionDoctypeSorting']=None

 #=========== End Transaction Doctype========
 #=========== Start Paymentterms======
    request.session['masterPaymentTermsOffset']=0
    request.session['masterPaymentTermsFirst']=10
    request.session['masterPaymentTermsCurrentPage']=1
    request.session['masterPaymentTermsTerm']=None
    request.session['masterPaymentTermsCreditDays']=None
    request.session['masterPaymentTermsPaymentPercent']=None
    request.session['masterPaymentTermsSize']=None
    request.session['masterPaymentTermsSorting']=None

 #=========== End Paymentterms========
    #==========Start User======
    request.session['userOffset']=0
    request.session['userFirst']=10
    request.session['userCurrentPage']=1
    request.session['userSearch']=None
    request.session['userSize']=None
    request.session['userTypeFilter']=None
    request.session['userEntityFilter']=None
    request.session['userisActive']=None
    #==========End User========
    #==========Start Entity======
    request.session['entityBrandOffset']=0
    request.session['entityBrandFirst']=10
    request.session['entityBrandCurrentPage']=1
    request.session['entityBrandSearch']=None
    request.session['entityBrandMaxRows']=None
    request.session['entitySize']=None
    request.session['entityBrandSorting']=None
    #==========End Entity========


   #============Start Entity Address=====
    request.session['entityAddressOffset']=0
    request.session['entityAddressFirst']=10
    request.session['entityAddressCurrentPage']=1
    request.session['entityAddressSearch']=None
    request.session['entityAddressSize']=None
    request.session['entityAddressSorting']=None
   #============End Entity Address=====
   #============Start Entity Pricing=====
    request.session['entityPricingOffset']=0
    request.session['entityPricingFirst']=10
    request.session['entityPricingCurrentPage']=1
    request.session['entityPricingSearch']=None
    request.session['entityPricingSize']=None
    request.session['entityPricingSorting']=None
   #============End Entity Pricing=====
   #============Start Entity Contact=====
    request.session['entityContactOffset']=0
    request.session['entityContactFirst']=10
    request.session['entityContactCurrentPage']=1
    request.session['entityContactSearch']=None
    request.session['entityContactSize']=None
    request.session['entityContactSorting']=None
   #============End Entity Contact=====
   #============Start Category Entity=====
    request.session['categoryEntityOffset']=0
    request.session['categoryEntityFirst']=10
    request.session['categoryEntityCurrentPage']=1
    request.session['categoryEntitySearch']=None
    request.session['categoryEntitySize']=None
    request.session['categoryEntitySorting']=None
   #============End Category Entity=====
   #============Start Parent Entity=====
    request.session['parentEntityOffset']=0
    request.session['parentEntityFirst']=10
    request.session['parentEntityCurrentPage']=1
    request.session['parentEntitySearch']=None
    request.session['parentEntitySize']=None
    request.session['parentEntitySorting']=None
   #============End Parent Entity=====
   #============Start CRM Entity=====
    request.session['crmEntityOffset']=0
    request.session['crmEntityFirst']=10
    request.session['crmEntityCurrentPage']=1
    request.session['crmEntitySearch']=None
    request.session['crmEntitySize']=None
    request.session['crmEntitySorting']=None
   #============End CRM Entity=====


   
 #=========== start Product App ==========
 #=========== Start Product ======
    request.session['productsOffset']=0
    request.session['productsFirst']=10
    request.session['productsCurrentPage']=1
    request.session['productsSearch']=None
    request.session['productsSize']=None
    request.session['productsSorting']=None

 #=========== End Product ========
 #=========== Start Product Unit======
    request.session['productUnitOffset']=0
    request.session['productUnitFirst']=10
    request.session['productUnitCurrentPage']=1
    request.session['productUnitSearch']=None
    request.session['productUnitSize']=None
    request.session['productUnitSorting']=None

 #=========== End Product Unit========
 #=========== Start Product Composition======
    request.session['productCompositionOffset']=0
    request.session['productCompositionFirst']=10
    request.session['productCompositionCurrentPage']=1
    request.session['productCompositionSearch']=None
    request.session['productCompositionSize']=None
    request.session['productCompositionSorting']=None

 #=========== End Product Composition========


 #=========== Start Transaction======
    request.session['transactionsOffset']=0
    request.session['transactionsFirst']=3
    request.session['transactionsCurrentPage']=1
    request.session['transactionsSearch']=None
    request.session['transactionsSize']=None
    request.session['transactionsSorting']=None

 #=========== End Transaction========
 #=========== Start Transaction Bags======
    request.session['transactionbagsOffset']=0
    request.session['transactionbagsFirst']=10
    request.session['transactionbagsCurrentPage']=1
    request.session['transactionbagsSearch']=None
    request.session['transactionbagsSize']=None
    request.session['transactionbagsSorting']=None
    request.session['transactionbagsisAudited']=None

 #=========== End Transaction Bags========
 #=========== Start Transaction Products======
    request.session['transactionproductsOffset']=0
    request.session['transactionproductsFirst']=3
    request.session['transactionproductsCurrentPage']=1
    request.session['transactionproductsSearch']=None
    request.session['transactionproductsSize']=None
    request.session['transactionproductsSorting']=None

 #=========== End Transaction Products========
    query="""
    
        query{
                    users{
             edges{
           node{
               id
              }
              }
             }
               }
    """
    
    userlist = client.execute(query=query)['data']['users']['edges']
    # print('x'*34)
    # print(type(userlist))
    lengths=len(userlist)

   #  query_entity="""
   #              query{
   #            entity{
   #              edges{
   #                node{
   #                  id
   #                }
   #              }
   #            }
   #          }
    
   #  """
    
   #  entitylist=client.execute(query=query_entity)['data']['entity']['edges']
   #  total_data_entity=len(entitylist)
    total_data_entity=12
    
    return render(request,path.join('core','home.html'),{"len":lengths,'total_data_entity':total_data_entity,'trans':trans})
  else:
    return redirect('login')

def translate(language):
  cur_language=get_language()
  try:
    activate(language)
    text=gettext('hello')
  finally:
    activate(cur_language)
  return text