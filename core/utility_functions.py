from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
def page_user(mod, page_number=None):
    u = Paginator(mod, 10)
    page_number = page_number
    try:
        page_obj = u.get_page(page_number)  # returns the desired page object
    except PageNotAnInteger:
        # if page_number is not an integer then assign the first page
        page_obj = u.page(1)
    except EmptyPage:
        # if page is empty then return last page
        page_obj = u.page(u.num_pages)
    return page_obj