from django.urls import path
from .views import ven_create,ven_list,ven_update,ven_delete
urlpatterns = [
  path('create/',ven_create,name='ven_create'),
  path('list/',ven_list,name='ven_list'),
  path('update/',ven_update,name='ven_update'),
  path('delete/',ven_delete,name='ven_delete'),
]
