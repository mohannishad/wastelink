from django.db import models
from django.db import models
from django.utils import timezone as tz
#===Vendor Model====#
class Vendor(models.Model):
  vendor_code = models.CharField(db_index=True, max_length=64)
  vendor_name = models.CharField(db_index=True, max_length=127)
  vendor_address = models.CharField(db_index=True, max_length=256)
  city = models.CharField(db_index=True, max_length=127)
  state = models.CharField(db_index=True, max_length=127)
  contact_number = models.CharField(max_length=64)
  created_by = models.CharField(max_length=64)
  created_on = models.DateTimeField(default=tz.now)
  def __str__(self):
      return self.vendor_name
  class Meta:
    ordering = ['-id']