from django.shortcuts import render
from os import path
#===Vendor Create===#
def ven_create(request):
  return render(request,path.join('Vendor','ven_create.html'))
#===Vendor List===#.
def ven_list(request):
  return render(request,path.join('Vendor','ven_list.html'))
#===Vendor Update===#
def ven_update(request):
  return render(request,path.join('Vendor','ven_update.html'))
#===Vendor Delete===#
def ven_delete(request):
  return render(request,path.join('Vendor','ven_delete.html'))


