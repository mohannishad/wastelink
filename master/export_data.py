import csv
from django.http import HttpResponse
from wastelink.settings import client
def export_entity_purpose(request):
    # Create the HttpResponse object with the appropriate CSV header.

    query = """
    
    query{
              masterEntityPurposes{
                edges{
                  node{
                entityPurpose
                id
              }
            }
            }
            }
    
    """

    entity_list = client.execute(query=query)['data']['masterEntityPurposes']['edges']
    
    id=[]
    entity_purpose=[]
    for i in entity_list:
     id.append(i['node']['id'])
     entity_purpose.append(i['node']['entityPurpose'])

      
    response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Entity_purpose.csv"'},
    )
    
    writer = csv.writer(response)
    writer.writerow(['Id', 'Entity Purpose'])
    for i in range(len(id)):
      writer.writerow([id[i],entity_purpose[i]])

    return response

def exportmastercrmcommonstatus(request):
  query="""
        query{
                    masterCrmCommStatus{
                    edges{
                       node{
                          id
                            status
                          }
                        }
                      }

                    }

       """
  export=client.execute(query=query)['data']['masterCrmCommStatus']['edges']
  id=[]
  status=[]

  for i in export:
    id.append(i['node']['id'])
    status.append(i['node']['status'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Crm_common_Status.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Status'])
  for i in range(len(id)):
    writer.writerow([id[i],status[i]])

  return response

def exportIndustry(request):
  query="""
       query{
        masterIndustry{
         edges{
            node{
            id
           industry
      }
    }
  }
    }
  """
  export=client.execute(query=query)['data']['masterIndustry']['edges']
  id=[]
  industry=[]

  for i in export:
    id.append(i['node']['id'])
    industry.append(i['node']['industry'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_industry.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Industry'])
  for i in range(len(id)):
    writer.writerow([id[i],industry[i]])

  return response

def masterproductcategory(reqeust):
  query="""
      query{
       masterProductCategory{
       edges{
       node{
           id
           name
        
         abbv
      }
    }
     }
   }
  
  """

  querys=client.execute(query=query)['data']['masterProductCategory']['edges']

  id=[]
  name=[]
  abbrv=[]
  for i in querys:
    id.append(i['node']['id'])
    name.append(i['node']['name'])
    abbrv.append(i['node']['abbv'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_Category.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Name',"Abbrv"])
  for i in range(len(id)):
    writer.writerow([id[i],name[i],abbrv[i]])

  return response




def mastercrmCommonTypeData(request):
  query = """

            query{
              masterCrmCommType{
                edges{
                  node{
               id
                type
                }
                }
                }
                }
                """


  export=client.execute(query=query)['data']['masterCrmCommType']['edges']
  id=[]
  type=[]

  for i in export:
    id.append(i['node']['id'])
    type.append(i['node']['type'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Crm_common_Type.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Type'])
  for i in range(len(id)):
    writer.writerow([id[i],type[i]])

  return response
def crmsource(request):
 

  query = """
        query{
        masterCrmSource
          {
          edges{
            node{
            id
            source
              }
            }
            }
              }
          """
           


  export=client.execute(query=query)['data']['masterCrmSource']['edges']
  id=[]
  source=[]

  for i in export:
    id.append(i['node']['id'])
    source.append(i['node']['source'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Crm_Source.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Source'])
  for i in range(len(id)):
    writer.writerow([id[i],source[i]])

  return response
#========CRM Status========
def crmstatus(request):
 

  query = """
        query{
        masterCrmStatus
          {
          edges{
            node{
            id
            status
              }
            }
            }
              }
          """
           


  export=client.execute(query=query)['data']['masterCrmStatus']['edges']
  id=[]
  status=[]

  for i in export:
    id.append(i['node']['id'])
    status.append(i['node']['status'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Crm_Status.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Status'])
  for i in range(len(id)):
    writer.writerow([id[i],status[i]])

  return response

#========Master User Type========
def usertypes(request):
 

  query = """
         query{
                  masterUserTypes{
                  edges{
                    node{

        
                  code
                  isEdit
                  type
                }
              }
                }
                  }
          """
           


  export=client.execute(query=query)['data']['masterUserTypes']['edges']
  code=[]
  isedit=[]
  type=[]

  for i in export:
    type.append(i['node']['type'])
    isedit.append(i['node']['isEdit'])
    code.append(i['node']['code'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="UserTypes.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Type', 'Code','Is Edit'])
  for i in range(len(code)):
    writer.writerow([type[i],code[i],isedit[i]])

  return response
#========Master Product SubCategory========
def masterproductSubcategory(request):
 

  query = """
         query{
                  masterProductSubcategory{
                  edges{
                    node{

                    id
                              name
                      subcatCode
                      subCatIgst
                      subCatCgst
                      subCatSgst
                       subCatHsn
                                  abbv
                                category{
                                              id
                                              name
                                }
                }
                }
              }
                }
                  
          """
           


  export=client.execute(query=query)['data']['masterProductSubcategory']['edges']
  id=[]
  name=[]
  hsn=[]
  abbrv=[]
  code=[]
  igst=[]
  sgst=[]
  cgst=[]
  category=[]

                      
                      
                      
                       
  for i in export:
    id.append(i['node']['id'])
    name.append(i['node']['name'])
    hsn.append(i['node']['subCatHsn'])
    abbrv.append(i['node']['abbv'])
    code.append(i['node']['subcatCode'])
    igst.append(i['node']['subCatIgst'])
    sgst.append(i['node']['subCatSgst'])
    cgst.append(i['node']['subCatCgst'])
    
    category.append(i['node']['category'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_SubCatgegory.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Name','Abbv',"Code","SubCategoryHsn","SubCategorySGST","SubCategoryCGST","SubCategoryIGST","Category"])
  for i in range(len(id)):
    writer.writerow([ id[i],name[i],abbrv[i],code[i],hsn[i],sgst[i],cgst[i],igst[i],category[i]['name']])

  return response



def exportSalutation(request):
  query="""
       query{
        masterSalutation{
         edges{
            node{
            id
           salutation
      }
    }
  }
    }
  """
  export=client.execute(query=query)['data']['masterSalutation']['edges']
  id=[]
  salutation=[]

  for i in export:
    id.append(i['node']['id'])
    salutation.append(i['node']['salutation'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_salutation.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Salutation'])
  for i in range(len(id)):
    writer.writerow([id[i],salutation[i]])

  return response

  
def exportnutritiontype(request):
  query="""
       query{
        masterProductNutritionType{
         edges{
            node{
            id
           type
      }
    }
  }
    }
  """
  export=client.execute(query=query)['data']['masterProductNutritionType']['edges']
  id=[]
  type=[]

  for i in export:
    id.append(i['node']['id'])
    type.append(i['node']['type'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_NutritionType.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Type'])
  for i in range(len(id)):
    writer.writerow([id[i],type[i]])

  return response

def exportDesignation(request):
  query="""
       query{
        masterDesignation{
         edges{
            node{
            id
           designation
      }
    }
  }
    }
  """
  export=client.execute(query=query)['data']['masterDesignation']['edges']
  id=[]
  designation=[]

  for i in export:
    id.append(i['node']['id'])
    designation.append(i['node']['designation'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Designation.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Designation'])
  for i in range(len(id)):
    writer.writerow([id[i],designation[i]])

  return response


def measuretype(request):
  query="""
       query{
                  masterProductMeasureType{
                  edges{
                    node{

                    measureType
                  id
               
                }
              }
                }
                  }
  """
  export=client.execute(query=query)['data']['masterProductMeasureType']['edges']
  id=[]
  measureType=[]

  for i in export:
    id.append(i['node']['id'])
    measureType.append(i['node']['measureType'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_Measure_Type.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'MeasureType'])
  for i in range(len(id)):
    writer.writerow([id[i],measureType[i]])

  return response

def measureunit(request):
  query="""
       query{
                  masterProductMeasureUnit{
                  edges{
                    node{

                    unit
                  id
               
                }
              }
                }
                  }
  """
  export=client.execute(query=query)['data']['masterProductMeasureUnit']['edges']
  id=[]
  unit=[]

  for i in export:
    id.append(i['node']['id'])
    unit.append(i['node']['unit'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_Measure_Unit.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'MeasureUnit'])
  for i in range(len(id)):
    writer.writerow([id[i],unit[i]])

  return response


def nextstep(request):
  query="""
       query{
                  masterProductNextStep{
                  edges{
                    node{

                    nextStep
                  id
               
                }
              }
                }
                  }
  """
  export=client.execute(query=query)['data']['masterProductNextStep']['edges']
  id=[]
  next=[]

  for i in export:
    id.append(i['node']['id'])
    next.append(i['node']['nextStep'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_NextStep.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'NextStpe'])
  for i in range(len(id)):
    writer.writerow([id[i],next[i]])

  return response

def tdoctype(request):
  query="""
       query{
                  masterDoctype{
                  edges{
                    node{

                    transactionDoctype
                  id
               
                }
              }
                }
                  }
  """
  export=client.execute(query=query)['data']['masterDoctype']['edges']
  id=[]
  next=[]

  for i in export:
    id.append(i['node']['id'])
    next.append(i['node']['transactionDoctype'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Transaction_Doctype.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Transaction Doctype'])
  for i in range(len(id)):
    writer.writerow([id[i],next[i]])

  return response
def transaction_type(request):
  query="""
       query{
                  masterTransactionType{
                  edges{
                    node{

                    transactionType
                  id
               
                }
              }
                }
                  }
  """
  export=client.execute(query=query)['data']['masterTransactionType']['edges']
  id=[]
  next=[]

  for i in export:
    id.append(i['node']['id'])
    next.append(i['node']['transactionType'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Transaction_Type.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Transaction Type'])
  for i in range(len(id)):
    writer.writerow([id[i],next[i]])

  return response
def transaction_status(request):
  query="""
       query{
                  masterTransactionStatus{
                  edges{
                    node{

                    transactionStatus
                  id
               
                }
              }
                }
                  }
  """
  export=client.execute(query=query)['data']['masterTransactionStatus']['edges']
  id=[]
  next=[]

  for i in export:
    id.append(i['node']['id'])
    next.append(i['node']['transactionStatus'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Transaction_Status.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Transaction Status'])
  for i in range(len(id)):
    writer.writerow([id[i],next[i]])

  return response

def product_packaging_d(request):
  query="""
       query{
                  masterProductPackaging{
                  edges{
                    node{

                    packagingType
                  id
               
                }
              }
                }
                  }
  """
  export=client.execute(query=query)['data']['masterProductPackaging']['edges']
  id=[]
  next=[]

  for i in export:
    id.append(i['node']['id'])
    next.append(i['node']['packagingType'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_Packaging.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Product Packaging'])
  for i in range(len(id)):
    writer.writerow([id[i],next[i]])

  return response
def product_taste(request):
  query="""
      query{
      masterProductTaste{
      edges{
        node{

        taste
      id

    }
  }
    }
      }
  """
  export=client.execute(query=query)['data']['masterProductTaste']['edges']
  id=[]
  next=[]

  for i in export:
    id.append(i['node']['id'])
    next.append(i['node']['taste'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_Taste.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Product Taste'])
  for i in range(len(id)):
    writer.writerow([id[i],next[i]])

  return response
def product_state(request):
  query="""
      query{
      masterProductState{
      edges{
        node{

        state
      id

    }
  }
    }
      }
  """
  export=client.execute(query=query)['data']['masterProductState']['edges']
  id=[]
  next=[]

  for i in export:
    id.append(i['node']['id'])
    next.append(i['node']['state'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Product_State.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Product State'])
  for i in range(len(id)):
    writer.writerow([id[i],next[i]])

  return response
def payment_terms(request):
  query="""
      query{
      masterPaymentTerms{
      edges{
        node{

        term
          creditDays
          paymentPercent
      id

    }
  }
    }
      }
  """
  export=client.execute(query=query)['data']['masterPaymentTerms']['edges']
  id=[]
  term=[]
  c=[]
  p=[]

  for i in export:
    id.append(i['node']['id'])
    term.append(i['node']['term'])
    c.append(i['node']['creditDays'])
    p.append(i['node']['paymentPercent'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Master_Payment_Term.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'Term','Credit Dayas','Payment Percent'])
  for i in range(len(id)):
    writer.writerow([id[i],term[i],c[i],p[i]])

  return response
