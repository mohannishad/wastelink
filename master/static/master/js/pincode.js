function validate_pincode(){
  let country=document.forms['pincodedata']['country'].value;
  let state=document.forms['pincodedata']['state'].value;
  let district=document.forms['pincodedata']['district'].value;
  let pincode=document.forms['pincodedata']['pincode'].value;
  if(country==''){
    document.getElementById('country-m').innerHTML="This field cannot be blank";
    return false;
  }
  if(state==''){
    document.getElementById('state-m').innerHTML="This field cannot be blank";
    return false;
  }
  if(district==''){
    document.getElementById('district-m').innerHTML="This field cannot be blank";
    return false;
  }
  if(pincode==''){
    document.getElementById('pincode-m').innerHTML="This field cannot be blank";
    return false;
  }
}