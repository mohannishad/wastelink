function validate_subcategory() {
   
  let name=document.forms['subcategory']['name'].value;
  let abbrv=document.forms['subcategory']['abbv'].value;
  let pname=document.forms['subcategory']['category'].value;
  
  if(name=='' ){
  document.getElementById('name-m').innerHTML="This field cannot be blank";
  return false;
  }
  if(abbrv==''){
  document.getElementById('abbv-m').innerHTML="This field cannot be blank";
  return false;
  }
  if(pname=="Please Select Product Category Name"){
  document.getElementById('pname-m').innerHTML="This field is required";
  return false;
  }
  }