from django.shortcuts import render, redirect
from os import path
from wastelink.settings import client
from python_graphql_client import GraphqlClient

def view_entity_purpose(request, id):
  if len(dict(request.session))>0: 
    query = """

        query($id:Int!){
                  masterEntityPurposeById(id:$id){
                    entityPurpose
                    id
                  }
                }

        """
    varss = {"id": id}
    purposes = client.execute(query=query, variables=varss)['data']['masterEntityPurposeById']
    return render(request, path.join('master/viewpages', 'view_entity_purpose.html'), {"purposes": purposes})
  else:
    return redirect('login')
def view_entity_type(request, id):
  if len(dict(request.session))>0:    
    query = """

            query($id:Int!){
                      masterEntityTypeById(id:$id){
                        id
                        entityType
                        hasChildren
                        isBase
                      }
                    }

            """
    varss = {"id": id}
    types = client.execute(query=query, variables=varss)['data']['masterEntityTypeById']
    return render(request, path.join('master/viewpages', 'view_entity_type.html'), {"types": types})
  else:
    return redirect('login')


def view_product_taste(request, id):
  if len(dict(request.session))>0:     
    query = """
                        query($id:Int!){
                          masterProductTasteById(id:$id){
                            id
                            taste
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductTasteById']
    return render(request, path.join('master/viewpages', 'view_product_taste.html'), {"data": data})
  else:
    return redirect('login')
def view_product_state(request, id):
  if len(dict(request.session))>0:   
    query = """
                          query($id:Int!){
                            masterProductStateById(id:$id){
                              id
                              state
                            }
                          }
                              """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductStateById']
    return render(request, path.join('master/viewpages', 'view_product_state_type.html'), {"data": data})
  else:
    return redirect('login')

def view_product_nextstep(request, id):
  if len(dict(request.session))>0:    
    query = """
                        query($id:Int!){
                          masterProductNextStepById(id:$id){
                            id
                            nextStep
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductNextStepById']
    return render(request, path.join('master/viewpages', 'view_product_nextstep.html'), {"data": data})
  else:
    return redirect('login')


def view_product_measure_unit(request, id):
  if len(dict(request.session))>0:    
    query = """
                          query($id:Int!){
                            masterProductMeasureUnitById(id:$id){
                              id
                              unit
                            }
                          }
                              """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductMeasureUnitById']
    return render(request, path.join('master/viewpages', 'view_product_measure_unit.html'), {"data": data})
  else:
    return redirect('login')
def view_product_measure_type(request, id):
  if len(dict(request.session))>0:    
    query = """
                        query($id:Int!){
                          masterProductMeasureTypeById(id:$id){
                            id
                            measureType
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductMeasureTypeById']
    return render(request, path.join('master/viewpages', 'view_product_measure_type.html'), {"data": data})
  else:
    return redirect('login')
def view_product_nutrition_type(request, id):
  if len(dict(request.session))>0:     
    query = """
                    query($id:Int!){
                      masterProductNutritionTypeById(id:$id){
                        id
                        type
                      }
                    }

                        """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductNutritionTypeById']
    return render(request, path.join('master/viewpages', 'view_product_nutrition_type.html'), {"data": data})
  else:
    return redirect('login')

def view_product_subcategory(request, id):
  if len(dict(request.session))>0:     

    q = """
        
        query{
            masterProductCategory{
              edges{
                node{

               
                    id
                    name 
                    }
                     }
              }
                }
            """
    data1 = client.execute(query=q)['data']['masterProductCategory']

    query = """
                query($id:Int!){
                  masterProductSubcategoryById(id:$id){
                    id
                    name
                    abbv
                    hsn
                    category{
                        id
                        name
                    }
                  }
                }
                    
                    """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductSubcategoryById']

    if request.method == 'POST':
        name = request.POST.get('name')
        abbv = request.POST.get('abbv')
        hsn = request.POST.get('hsn')
        category = request.POST.get('category')

        q = """mutation($id:ID!, $name:String,$abbv:String,$hsn:String,$categoryId:Int){
                  updateMasterProductSubcategory(id:$id, name:$name,abbv:$abbv hsn:$hsn, categoryId:$categoryId){
                    ok
                  }
                }"""
        v = {"id": id, "name": name, "hsn": hsn, "abbv": abbv, "categoryId": category}

        client.execute(query=q, variables=v)
        return redirect("product_subcategory_list")

    return render(request, path.join('master/viewpages', 'view_product_subcategory.html'), {"data": data, "data1": data1})
  else:
    return redirect('login')

def view_product_category(request, id):
  if len(dict(request.session))>0:   
    query = """
                query($id:Int!){
                  masterProductCategoryById(id:$id){
                    id
                    name
                    abbv
                  }
                }
                    
                    """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductCategoryById']
    return render(request, path.join('master/viewpages', 'view_product_category.html'), {"data": data})
  else:
    return redirect('login')


def view_industry(request, id):
  if len(dict(request.session))>0:     
    query = """
      
                    query($id:Int!){
                      masterIndustryById(id:$id)
                      {
                        id
                        industry
                      }
                    }

                """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterIndustryById']
    return render(request, path.join('master/viewpages', 'view_industry.html'), {"data": data})
  else:
    return redirect('login')

def view_user_type(request, id):
  if len(dict(request.session))>0:    
    query = """
                            query($code:String!){
                              masterUserTypeByCode(code:$code){
                                type
                                code
                                isEdit
                              }
                            }
                                """
    varss = {"code": id}
    data = client.execute(query=query, variables=varss)['data']['masterUserTypeByCode']
    return render(request, path.join('master/viewpages', 'view_user_type.html'), {"data": data})
  else:
    return redirect('login')


def view_crm_comm_status(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterCrmCommStatusById(id:$id){
                            id
                            status
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmCommStatusById']
    return render(request, path.join('master/viewpages', 'view_crm_comm_status.html'), {"data": data})
  else:
    return redirect('login')

def view_crm_comm_type(request, id):
  if len(dict(request.session))>0:     
    query = """
                        query($id:Int!){
                          masterCrmCommTypeById(id:$id){
                            id
                            type
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmCommTypeById']
    return render(request, path.join('master/viewpages', 'view_crm_comm_type.html'), {"data": data})
  else:
    return redirect('login')


def view_crm_source(request, id):
  if len(dict(request.session))>0:    
    query = """
                        query($id:Int!){
                          masterCrmSourceById(id:$id){
                            id
                            source
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmSourceById']
    return render(request, path.join('master/viewpages', 'view_crm_source.html'), {"data": data})
  else:
    return redirect('login')


def view_crm_status(request, id):
  if len(dict(request.session))>0:    
    query = """
                        query($id:Int!){
                          masterCrmStatusById(id:$id){
                            id
                            status
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmStatusById']
    return render(request, path.join('master/viewpages', 'view_crm_status.html'), {"data": data})
  else:
    return redirect('login')
def view_salutation(request, id):
  if len(dict(request.session))>0:    
    query = """
                        query($id:Int!){
                          masterSalutationById(id:$id){
                            id
                          salutation
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterSalutationById']
    return render(request, path.join('master/viewpages', 'view_salutation.html'), {"data": data})
  else:
    return redirect('login')
def view_designation(request, id):
  if len(dict(request.session))>0:      
    query = """
                        query($id:Int!){
                          masterDesignationById(id:$id){
                            id
                          designation
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterDesignationById']
    return render(request, path.join('master/viewpages', 'view_designation.html'), {"data": data})
  else:
    return redirect('login')
def view_transaction_status(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterTransactionStatusById(id:$id){
                            id
                          transactionStatus
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterTransactionStatusById']
    return render(request, path.join('master/viewpages', 'view_transaction_status.html'), {"data": data})
  else:
    return redirect('login')
def view_transaction_type(request, id):
  if len(dict(request.session))>0:     
    query = """
                        query($id:Int!){
                          masterTransactionTypeById(id:$id){
                            id
                          transactionType
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterTransactionTypeById']
    return render(request, path.join('master/viewpages', 'view_transaction_type.html'), {"data": data})
  else:
    return redirect('login')
def view_transaction_doctype(request, id):
  if len(dict(request.session))>0:    
    query = """
                       query($id:Int!){
                          masterDoctypeById(id:$id){
                            id
                          transactionDoctype
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterDoctypeById']
    return render(request, path.join('master/viewpages', 'view_transaction_doctype.html'), {"data": data})
  else:
    return redirect('login')
def view_product_packaging(request, id):
  if len(dict(request.session))>0:     
    query = """
                       query($id:Int!){
                          masterProductPackagingById(id:$id){
                            id
                          packagingType
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductPackagingById']
    return render(request, path.join('master/viewpages', 'view_product_packaging.html'), {"data": data})
  else:
    return redirect('login')
def view_paymenterms(request, id):
  if len(dict(request.session))>0:    
    query = """
    query($id:Int!){
          masterPaymentTermsById(id:$id){
            id
        term
        creditDays
        paymentPercent
          }
        }
         """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterPaymentTermsById']
    return render(request, path.join('master/viewpages', 'view_paymentterm.html'), {"data": data})
  else:
    return redirect('login')