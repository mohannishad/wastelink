from django.shortcuts import redirect
from django.contrib import messages
from wastelink.settings import client
import pandas as pd
import numpy as np
import os
import math
import sys
from django.conf import settings
from django.core.files.storage import FileSystemStorage
 
def masterPurpose(request):
  if request.method=="POST":
  
    files=request.FILES['files']
    extension=files.name.split(".")[1]
    print(extension)
    if extension != 'xlsx':
      messages.warning(request, 'Invalid extension. Please upload valid .xlsx extension file.')
      return redirect('entity_purpose_list')

    else:
      fs = FileSystemStorage()
      filename = fs.save(files.name, files)
      uploaded_file_url = fs.url(filename)
      excel_file = uploaded_file_url
      print(excel_file)
      a=pd.read_excel("."+excel_file)
      # a=pd.read_excel(excel_file)
      dbframe = a
      print(dbframe)
      for dbframe in dbframe.itertuples():
        dbframe[1]
        query="""
          mutation($entityPurpose:String){
                createMasterEntityPurpose(entityPurpose:$entityPurpose){
                  ok
                }
              }
            
            """
        v={
          "entityPurpose": dbframe[1]
        }

        data=client.execute(query=query,variables=v)
      return redirect('entity_purpose_list')

  return redirect('entity_purpose_list')



def masterpincodedata(request):
  if request.method=="POST":
  
    files=request.FILES['files']
    extension=files.name.split(".")[1]
    if extension != 'xlsx':
      messages.warning(request, 'Invalid extension. Please upload valid .xlsx extension file.')
      return redirect('pincode_list')

    else:
      fs = FileSystemStorage()
      filename = fs.save(files.name, files)
      uploaded_file_url = fs.url(filename)
      excel_file = uploaded_file_url
      # print(excel_file)
      # a=pd.read_excel("."+excel_file)
      # a=pd.read_excel(excel_file)
      dataframe = pd.read_excel("."+excel_file, sheet_name=0)
      columns_list = list(dataframe.columns)
      dataframe = dataframe.replace({np.nan: None})

      print("data farakkkkkkkkk")
      print(dataframe)
      country_list = []
      state_list = []
      district_list = []
      city_list = []
      locality_list = []
      pincode_list = []

  for i in columns_list:
      if i.lower() == 'country':
          country_list.extend(dataframe[i].tolist())
      if i.lower() == 'state':
          state_list.extend(dataframe[i].tolist())
      if i.lower() == 'district':
          district_list.extend(dataframe[i].tolist())
      if i.lower() == 'city':
          city_list.extend(dataframe[i].tolist())
      if i.lower() == 'locality':
          locality_list.extend(dataframe[i].tolist())
      if i.lower() == 'pincode':
          pincode_list.extend(dataframe[i].tolist())

  q = '''

  mutation($country:String, $state:String, $city:String, $district:String, $pincode:String, $locality:String){
            createPincode(country:$country, state:$state, city:$city, district:$district, pincode:$pincode, locality:$locality){
              ok
            }
          }

  '''


  for i in range(len(dataframe)):
      v = {
          "country": country_list[i],
          "state": state_list[i],
          "city": city_list[i],
          "district": district_list[i],
          "locality": locality_list[i],
          "pincode": pincode_list[i]
      }
      client.execute(query=q, variables=v)
       

  return redirect('pincode_list')

def masterproductSubcategorys(request):
  if request.method=="POST":
  
    files=request.FILES['files']
    extension=files.name.split(".")[1]
    if extension != 'xlsx':
      messages.warning(request, 'Invalid extension. Please upload valid .xlsx extension file.')
      return redirect('product_subcategory_list')

    else:
      fs = FileSystemStorage()
      filename = fs.save(files.name, files)
      uploaded_file_url = fs.url(filename)
      excel_file = uploaded_file_url
      print(excel_file)
      a=pd.read_excel("."+excel_file)
      # a=pd.read_excel(excel_file)
      dbframe = a
      
      for dbframe in dbframe.itertuples():
        dbframe[1]
        query="""
          mutation($entityPurpose:String){
                createMasterEntityPurpose(entityPurpose:$entityPurpose){
                  ok
                }
              }
            
            """
        v={
          "entityPurpose": dbframe[1]
        }

        data=client.execute(query=query,variables=v)
      return redirect('product_subcategory_list')

  return redirect('product_subcategory_list')

def masterproductcategorys(request):
  if request.method=="POST":
  
    files=request.FILES['files']
    extension=files.name.split(".")[1]
    if extension != 'xlsx':
      messages.warning(request, 'Invalid extension. Please upload valid .xlsx extension file.')
      return redirect('product_category_list')

    else:
      fs = FileSystemStorage()
      filename = fs.save(files.name, files)
      uploaded_file_url = fs.url(filename)
      excel_file = uploaded_file_url

      dataframe=pd.read_excel("."+excel_file,sheet_name=0)
      
        # dataframe = pd.read_excel("Book1.xlsx", sheet_name=0)
      columns_list = list(dataframe.columns)

      q1 = '''
          query{
            masterProductCategory{
              edges{
                node{
                  name
                  abbv
                  catCode
                }
              }
            }
          }
      '''
      prod_cat_list = client.execute(query=q1)['data']['masterProductCategory']['edges']

      names = []
      abbvs = []
      codes = []

      for i in prod_cat_list:
          names.append(i['node']['name'])
          abbvs.append(i['node']['abbv'])
          codes.append(i['node']['catCode'])

      name_list = []
      cat_code_list = []
      abbv_list = []
      sub_cat_hsn_list = []
      cat_cgst_list = []
      cat_sgst_list = []
      cat_igst_list = []

      col_category_name = ''
      col_cat_code = ''
      col_abbv = ''

    for i in columns_list:
        if i.lower() in 'category name':
            col_category_name += i
            for j in dataframe[i].tolist():
                if type(j) == float and math.isnan(j):
                    # print('Name cannot be blank')
                    messages.warning(request, 'Category Name cannot be blank')
                    return redirect('product_category_list')
                    # sys.exit()
                if j in names:
                    messages.warning(request, f'{j}-Name already exists')
                    
                    dataframe.drop(dataframe.loc[dataframe[col_category_name] == j].index, inplace=True)
                    return redirect('product_category_list')
    if i.lower() in 'abbv':
        col_abbv += i
        for j in dataframe[i].tolist():
            if type(j) == float and math.isnan(j):
                # print('abbreviation cannot be blank')
                messages.warning(request, 'abbreviation cannot be blank')
                return redirect('product_category_list')
                
                # sys.exit()
            if j in abbvs:
                messages.warning(request, f'{j}-Abbvs already exists')
                dataframe.drop(dataframe.loc[dataframe[col_abbv] == j].index, inplace=True)
                return redirect('product_category_list')
    if i.lower() in 'cat_code':
        col_cat_code += i
        for j in dataframe[i].tolist():
            if type(j) == float and math.isnan(j):
                # print('code cannot be blank')
                messages.warning(request, 'Code cannot be blank')
                return redirect('product_category_list')
                # sys.exit()
            if j in codes:
                messages.warning(request, f'{j}-Code already exists')
                dataframe.drop(dataframe.loc[dataframe[col_cat_code] == j].index, inplace=True)
                return redirect('product_category_list')
    if "gst" in i:
        dataframe[i] = dataframe[i].replace({np.nan: 0})

    dataframe = dataframe.replace({np.nan: None})

    for i in columns_list:
        if i.lower() in 'category name':
            name_list.extend(dataframe[i].tolist())
        if i.lower() in 'cat_code':
            cat_code_list.extend(dataframe[i].tolist())
        if i.lower() == 'abbv':
            abbv_list.extend(dataframe[i].tolist())
        if i.lower() == 'sub_cat_hsn':
            sub_cat_hsn_list.extend(dataframe[i].tolist())
        if i.lower() == 'cat_cgst':
            cat_cgst_list.extend(dataframe[i].tolist())
        if i.lower() == 'cat_sgst':
            cat_sgst_list.extend(dataframe[i].tolist())
        if i.lower() == 'cat_igst':
            cat_igst_list.extend(dataframe[i].tolist())

        q = '''

                mutation($name:String, $abbv:String, $catcode:String, $hsn:String,$cgst:Float,$sgst:Float,$igst:Float ){
                  createMasterProductCategory(name:$name, abbv:$abbv, catCode:$catcode, subCatHsn:$hsn,
                                                                        catCgst:$cgst, catSgst:$sgst, catIgst:$igst){
                    ok
                  }
                }

        '''

    for i in range(len(dataframe)):
          v = {
              "name": name_list[i],
              "catcode": cat_code_list[i],
              "abbv": abbv_list[i],
              "hsn": sub_cat_hsn_list[i],
              "cgst": cat_cgst_list[i],
              "sgst": cat_sgst_list[i],
              "igst": cat_igst_list[i]
              }
          client.execute(query=q, variables=v)
    return redirect('product_category_list')

  return redirect('product_category_list')


