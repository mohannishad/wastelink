from django.contrib import messages
from django.core import paginator
from django.shortcuts import render, redirect
from os import path
from django.views import View
from django.views.generic.base import TemplateView
from python_graphql_client import GraphqlClient
import openpyxl
import pandas as pd
from django.core.paginator import Paginator
from requests.api import request
# =====Entity Purpose=====
from core.utility_functions import page_user
from wastelink.settings import client
def entity_purpose_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        entityPurposeSearch=request.POST.get('entityPurposeSearch')
        request.session['entityPurposeSearch']=entityPurposeSearch
        request.session['entityPurposeCurrentPage']=1
        request.session['entity_purpose_offset']=0
        
    if request.POST.get("entityPurposeSize")==None:
       pass
    else:
       request.session['entityPurposePageSize']=int(request.POST.get('entityPurposeSize'))
       request.session['entity_purpose_first']=request.session['entityPurposePageSize']
       request.session['entity_purpose_offset']=0
       request.session['entityPurposeCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['entityPurposeSearch']=None
      request.session['entity_purpose_offset']=0
      request.session['entityPurposePageSize']=None
      request.session['entity_purpose_first']=10 #defalt Value 

    query2="""
           query($search:String){
                    masterEntityPurposes(entityPurpose_Icontains:$search){
                      edges{
                        node{
                    
                      id
                    }
                  }
                  }
                  
                  }
    
        """
    td=client.execute(query=query2,variables={"search": request.session['entityPurposeSearch']})['data']['masterEntityPurposes']['edges']
    total_records = len(td)
    if total_records%request.session['entity_purpose_first']==0:
      total_pages = total_records//request.session['entity_purpose_first']
    else:
      total_pages = total_records//request.session['entity_purpose_first']+1

    query = """
          query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                    masterEntityPurposes(offset:$offset,first:$first,last:$last,entityPurpose_Icontains:$search,orderBy:$orderBy){
                      edges{
                        node{
                      entityPurpose
                      id
                    }
                  }
                  }
                  
                  }
    """
    if request.POST.get('First')=="First": 
      request.session["entity_purpose_offset"]=0
      request.session['entityPurposeCurrentPage']=1
    
    last=None
    if request.POST.get('Last')=="Last":
      request.session['entityPurposeCurrentPage']=total_pages
      if total_records%int(request.session['entity_purpose_first'])==0:
        last=total_records%int(request.session['entity_purpose_first'])+int(request.session['entity_purpose_first'])
        request.session['entity_purpose_offset']=total_records-int(request.session['entity_purpose_first'])
      else:
        last=total_records%int(request.session['entity_purpose_first'])
        request.session['entity_purpose_offset']=total_records-total_records%int(request.session['entity_purpose_first'])
    
     
    if request.POST.get('Previous')=="Previous":
      request.session["entity_purpose_offset"]-=int(request.session["entity_purpose_first"])
      request.session['entityPurposeCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session["entity_purpose_offset"]+=int(request.session["entity_purpose_first"])
      request.session['entityPurposeCurrentPage']+=1

    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['entityPurposeSorting']=='id':
        request.session['entityPurposeSorting']='-id'
      else:
        request.session['entityPurposeSorting']='id'
    
    if request.POST.get('entitypurpose')=="Entity Purpose":
      if request.session['entityPurposeSorting']=='entity_purpose':
        request.session['entityPurposeSorting']='-entity_purpose'
      else:
        request.session['entityPurposeSorting']='entity_purpose'

    #========End Sorting=========#
    v={
      "offset":request.session["entity_purpose_offset"],
      "first":request.session["entity_purpose_first"],
      "last":last,
      "search": request.session['entityPurposeSearch'],
      "orderBy":request.session['entityPurposeSorting']

    }
    
    entity_list = client.execute(query=query,variables=v)['data']['masterEntityPurposes']['edges']
    max_d="""
            query{
                masterEntityPurposeCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterEntityPurposeCount']
    return render(request, path.join('master', 'entity_purpose_list.html'),{"entity_list": entity_list,'max_page':total_pages,"max_row":max_row})
  else:
    return redirect('login')




def create_entity_purpose(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        entity_purpose = request.POST.get('entitypurpose').title()

        q = """mutation($entityPurpose:String){
                      createMasterEntityPurpose(entityPurpose:$entityPurpose){
                            ok    
                      }
                    }"""
        v = {"entityPurpose": entity_purpose}
        client.execute(query=q, variables=v)
        return redirect("entity_purpose_list")

    return render(request, path.join('master', 'create_entity_purpose.html'))
  else:
    return redirect('login')

def update_entity_purpose(request, id):
  if len(dict(request.session))>0:   
    query = """

        query($id:Int!){
                  masterEntityPurposeById(id:$id){
                    entityPurpose
                    id
                  }
                }

        """
    varss = {"id": id}

    purposes = client.execute(query=query, variables=varss)['data']['masterEntityPurposeById']

    if request.method == 'POST':
        entity_purpose = request.POST.get('entitypurpose').title()

        q = """mutation($id:ID, $entityPurpose:String){
                  updateMasterEntityPurpose(id:$id,entityPurpose:$entityPurpose){
                        ok    
                  }
                }"""
        v = {"entityPurpose": entity_purpose, "id": id}
        client.execute(query=q, variables=v)
        return redirect("entity_purpose_list")
 
    return render(request, path.join('master', 'update_entity_purpose.html'), {"purposes": purposes})
  else:
    return redirect('login')

def delete_entity_purpose(request, id):
  
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                          deleteMasterEntityPurpose(id:$id){
                                ok    
                          }
                        }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('entity_purpose_list')

    query = """query($id:Int!)
        {
            masterEntityPurposeById(id: $id){
            entityPurpose
        }
        }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterEntityPurposeById']

    return render(request, path.join('master', 'delete_entity_purpose.html'), {"data": data})
  else:
    return redirect('login')

# =====Entity Type=====
def entity_type_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        searchEntityType=request.POST.get('searchEntityType')
        request.session['masterEntityTypeSearch']=searchEntityType
        request.session['masterEntityTypeOffset']=0
        request.session['masterEntityTypeCurrentPage']=1
    #====Filter=======
    if request.POST.get("masterEntityTypeIsBase")==None:
       pass
    elif request.POST.get("masterEntityTypeIsBase")=="False":
      request.session['masterEntityTypeIsBase']=False
      request.session['masterEntityTypeOffset']=0
      request.session['masterEntityTypeCurrentPage']=1
    else:
      isbasefilter=request.POST.get('masterEntityTypeIsBase')
      request.session['masterEntityTypeIsBase']=isbasefilter
      request.session['masterEntityTypeOffset']=0
      request.session['masterEntityTypeCurrentPage']=1

    #-----Children Filter---------
    if request.POST.get("masterEntityTypeHasChildren")==None:
       pass
    elif request.POST.get("masterEntityTypeHasChildren")=="False":
      request.session['masterEntityTypeHasChildren']=False
      request.session['masterEntityTypeOffset']=0
      request.session['masterEntityTypeCurrentPage']=1
    else:
      haschildfilter=request.POST.get('masterEntityTypeHasChildren')
      request.session['masterEntityTypeHasChildren']=haschildfilter
      request.session['masterEntityTypeOffset']=0
      request.session['masterEntityTypeCurrentPage']=1
    #====End Filter=======
    if request.POST.get("masterEntityTypePageSize")==None:
       pass
    else:
       request.session['masterEntityTypePageSize']=int(request.POST.get('masterEntityTypePageSize'))
       request.session['masterEntityTypeFirst']=request.session['masterEntityTypePageSize']
       request.session['masterEntityTypeOffset']=0
       request.session['masterEntityTypeCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterEntityTypeSearch']=None
      request.session['masterEntityTypePageSize']=None
      request.session['masterEntityTypeIsBase']=None
      request.session['masterEntityTypeHasChildren']=None
      request.session['masterEntityTypeFirst']=10 #defalt Value 
      
    query2 = """

              query($search:String,$isBase:Boolean,$hasChildren:Boolean){
              masterEntityType(entityType_Icontains:$search,isBase:$isBase,hasChildren:$hasChildren){
                edges{
                  node{
                id
                
               }
               }
              }
                }

        """
    v2={
         "search":request.session['masterEntityTypeSearch'],
         "isBase":request.session['masterEntityTypeIsBase'],
         "hasChildren":request.session['masterEntityTypeHasChildren']
    }
    td=client.execute(query=query2,variables=v2)['data']['masterEntityType']['edges']
    total_records = len(td)
    if total_records%request.session['masterEntityTypeFirst']==0:
      total_pages = total_records//request.session['masterEntityTypeFirst']
    else:
      total_pages = total_records//request.session['masterEntityTypeFirst']+1
    query = """

               query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String,$isBase:Boolean,$hasChildren:Boolean){
              masterEntityType(offset:$offset,first:$first,last:$last,entityType_Icontains:$search,orderBy:$orderBy,isBase:$isBase,hasChildren:$hasChildren){
                edges{
                  node{
                id
                entityType
                isBase
                hasChildren
               }
               }
              }
                }

        """
   
    if request.POST.get('Next')=="Next":
      request.session["masterEntityTypeOffset"]+=int(request.session["masterEntityTypeFirst"])
      request.session['masterEntityTypeCurrentPage']+=1
    if request.POST.get('First')=="First":
      request.session['masterEntityTypeCurrentPage']=1
      request.session["masterEntityTypeOffset"]=0
    if request.POST.get('Previous')=="Previous":
      request.session["masterEntityTypeOffset"]-=int(request.session["masterEntityTypeFirst"])
      request.session['masterEntityTypeCurrentPage']-=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterEntityTypeCurrentPage']=total_pages
      if total_records%int(request.session['masterEntityTypeFirst'])==0:
        last=total_records%int(request.session['masterEntityTypeFirst'])+int(request.session['masterEntityTypeFirst'])
        request.session['masterEntityTypeOffset']=total_records-int(request.session['masterEntityTypeFirst'])
      else:
        last=total_records%int(request.session['masterEntityTypeFirst'])
        request.session['masterEntityTypeOffset']=total_records-total_records%int(request.session['masterEntityTypeFirst'])
    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
     
      if request.session['masterEntityTypeSorting']=='id':
        request.session['masterEntityTypeSorting']='-id'
      else:
        request.session['masterEntityTypeSorting']='id'

    if request.POST.get('entitytype')=="Entity Type":
   
      if request.session['masterEntityTypeSorting']=='entity_type':
        request.session['productCompositionSorting']='-entity_type'
      else:
        request.session['masterEntityTypeSorting']='entity_type'
    
    v={
      "offset":request.session["masterEntityTypeOffset"],
      "first":request.session["masterEntityTypeFirst"],
      "last":last,
      "search":request.session['masterEntityTypeSearch'],
      "orderBy":request.session['masterEntityTypeSorting'],
      "isBase":request.session['masterEntityTypeIsBase'],
      "hasChildren":request.session['masterEntityTypeHasChildren']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterEntityType']['edges']
    max_d="""
            query{
                masterEntityTypeCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterEntityTypeCount']
    return render(request, path.join('master', 'entity_type_list.html'), {"entity_list":entity_list,"max_page":total_pages,"max_row":max_row})
  else:
    return redirect('login')

def create_entity_type(request):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        entity_type = request.POST.get('entitytype').title()
        isBase = request.POST.get('isbase')
        if isBase == None:
            isBase = False
        hasChildren = request.POST.get('haschildren')
        if hasChildren == None:
            hasChildren = False

        q = """mutation($entityType:String, $isBase:Boolean, $hasChildren:Boolean){
                          createMasterEntityType(entityType:$entityType, isBase:$isBase, hasChildren:$hasChildren){
                                ok    
                          }
                        }"""
        v = {"entityType": entity_type, "isBase": isBase, "hasChildren": hasChildren}
        client.execute(query=q, variables=v)
        return redirect("entity_type_list")

    return render(request, path.join('master', 'create_entity_type.html'))
  else:
    return redirect('login')

def update_entity_type(request, id):
  if len(dict(request.session))>0:   
    query = """

            query($id:Int!){
                      masterEntityTypeById(id:$id){
                        entityType
                        hasChildren
                        isBase
                      }
                    }

            """
    varss = {"id": id}

    types = client.execute(query=query, variables=varss)['data']['masterEntityTypeById']

    if request.method == 'POST':
        entity_type = request.POST.get('entitytype')
        isBase = request.POST.get('isbase')
        if isBase == None:
            isBase = False
        hasChildren = request.POST.get('haschildren')
        if hasChildren == None:
            hasChildren = False

        q = """mutation($id:ID, $entityType:String, $hasChildren:Boolean, $isBase:Boolean){
                      updateMasterEntityType(id:$id,entityType:$entityType,isBase:$isBase, hasChildren:$hasChildren){
                            ok    
                      }
                    }"""
        v = {"entityType": entity_type, "id": id, "hasChildren": hasChildren, "isBase": isBase}
        client.execute(query=q, variables=v)
        return redirect("entity_type_list")

    return render(request, path.join('master', 'update_entity_type.html'), {"types": types})
  else:
    return redirect('login')

def delete_entity_type(request, id):
  if len(dict(request.session))>0:   

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                              deleteMasterEntityType(id:$id){
                                    ok    
                              }
                            }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('entity_type_list')

    query = """query($id:Int!)
            {
                masterEntityTypeById(id: $id){
                entityType
            }
            }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterEntityTypeById']

    return render(request, path.join('master', 'delete_entity_type.html'), {"data": data})
  else:
    return redirect('login')

# =====pincode=====
def pincode_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        
        search=request.POST.get('search')
        request.session['masterPincodeSearch']=search
        request.session['masterPincodeOffset']=0
        request.session['masterPincodeCurrentPage']=1

    if request.POST.get("masterPincodeSize")==None:
       pass
    else:
       request.session['masterPincodeSize']=int(request.POST.get('masterPincodeSize'))
       request.session['masterPincodeFirst']=request.session['masterPincodeSize']
       request.session['masterPincodeOffset']=0
       request.session['masterPincodeCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterPincodeSearch']=None
      request.session['masterPincodeSize']=None
      request.session['masterPincodeFirst']=10 #defalt Value 
    
    # query2 = """

    #           query($search:String){
    #               masterPincodeAll(pincode_Icontains:$search){
    #             edges{
    #               node{
    #             id
                    
    #               }
    #             }
    #         }
    #         }

    #               """
    # td=client.execute(query=query2,variables={"search": request.session['masterPincodeSearch']})['data']['masterPincodeAll']['edges']
    
    tdata="""
    query{
        masterPincodeCount
      }
    """
    td=client.execute(query=tdata)['data']['masterPincodeCount']
    total_records = td
    
    
    
    if total_records%request.session['masterPincodeFirst']==0:
      total_pages = total_records//request.session['masterPincodeFirst']
    else:
      total_pages = total_records//request.session['masterPincodeFirst']+1
    
    query = """

              query($offset:Int,$first:Int,$search:String,$orderby:String){
                  masterPincodeAll(offset:$offset,first:$first,pincode_Icontains:$search,orderBy:$orderby){
                edges{
                  node{
                id
                country
                state
                district
                city
                locality
                pincode
              }
            }
        }
        }
                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterPincodeOffset']=0
      request.session['masterPincodeCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterPincodeOffset']-=request.session['masterPincodeFirst']
      request.session['masterPincodeCurrentPage']-=1

    if request.POST.get('Next')=="Next":
      request.session['masterPincodeOffset'] += int(request.session['masterPincodeFirst'])
      request.session['masterPincodeCurrentPage'] +=1
     
    
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterPincodeCurrentPage']=total_pages
      if total_records%int(request.session['masterPincodeFirst'])==0:
        last=total_records%int(request.session['masterPincodeFirst'])+int(request.session['masterPincodeFirst'])
        request.session['masterPincodeOffset']=total_records-int(request.session['masterPincodeFirst'])
      else:
        last=total_records%int(request.session['masterPincodeFirst'])
        request.session['masterPincodeOffset']=total_records-total_records%int(request.session['masterPincodeFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    # if request.POST.get('id')=="Id":
    #   if request.session['masterCrmStatusSorting']=='id':
    #     request.session['masterCrmStatusSorting']='-id'
    #   else:
    #     request.session['masterCrmStatusSorting']='id'

    # if request.POST.get('status')=="Status":
    #   if request.session['masterCrmStatusSorting']=='status':
    #     request.session['masterCrmStatusSorting']='-status'
    #   else:
    #     request.session['masterCrmStatusSorting']='status'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterPincodeOffset'],
      "first":request.session['masterPincodeFirst'],
      "search":request.session['masterPincodeSearch'],
      "last":last,
      "orderBy":request.session['masterPincodeSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterPincodeAll']['edges']
    max_t="""
    query{
        masterPincodeCount
      }
    """
    max_rows=client.execute(query=max_t)['data']['masterPincodeCount']
    return render(request, path.join('master', 'pincode_list.html'),{"entity_list": entity_list,"max_page":total_pages,"max_rows":max_rows})
  else:
    return redirect('login')
def create_pincode(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        country = request.POST.get('country').title()
        state = request.POST.get('state').title()
        district = request.POST.get('district').upper()
        city = request.POST.get('city').title()
        pincode = request.POST.get('pincode')
        locality = request.POST.get('locality').title()

         

        query = """

       mutation($country:String, $state:String, $city:String, $district:String, $pincode:String, $locality:String){
              createPincode(Country:$country, State:$state, City:$city, District:$district, Pincode:$pincode, Locality:$locality){
                ok
              }
            }   

        """

        variables = {"country": country, "state": state, "city": city, "district": district, "pincode": pincode,
                     "locality": locality}

        client.execute(query=query, variables=variables)

        return redirect('pincode_list')
    return render(request, path.join('master', 'create_pincode.html'))
  else:
    return redirect('login')

def update_pincode(request, id):
  if len(dict(request.session))>0:  
    q = """
        query($id:Int!){
          masterPincodeById(id:$id){
            id
            Country
            State
            District
            Pincode
            Locality
            City
          }
        }
    """
    v = {"id": id}
    data = client.execute(query=q, variables=v)['data']['masterPincodeById']

    if request.method == 'POST':
        country = request.POST.get('country').title()
        state = request.POST.get('state').title()
        district = request.POST.get('district').upper()
        city = request.POST.get('city').title()
        locality = request.POST.get('locality').title()
        pincode = request.POST.get('pincode')

        q = """
            mutation($country:String, $state:String, $city:String, $district:String, $pincode:String, $locality:String, $id:ID){
              updatePincode(Country:$country, State:$state, City:$city, District:$district, Pincode:$pincode, Locality:$locality, id:$id){
                ok
              }
            }
            """
        v = {"id": id, "country": country, "state": state, "city": city, "district": district, "pincode": pincode,
             "locality": locality}

        client.execute(query=q, variables=v)
        return redirect("pincode_list")

    return render(request, path.join('master', 'update_pincode.html'), {"data": data})
  else:
    return redirect('login')

def delete_pincode(request, id):
  if len(dict(request.session))>0:  
    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                      deletePincode(id:$id){
                                            ok    
                                      }
                                    }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('pincode_list')

    query = """

            query($id:Int!){
                          masterPincodeById(id:$id)
                          {
                            id
                            Pincode
                          }
                        }

                    """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterPincodeById']

    return render(request, path.join('master', 'delete_pincode.html'), {"data": data})
  else:
    return redirect('login')

# =====Industry=====
def industry_list(request):
  if len(dict(request.session))>0:

    if request.POST.get('Search')=="Search":
        searchIndustry=request.POST.get('searchIndustry')
        request.session['masterIndustrySearch']=searchIndustry
        request.session['masterIndustryOffset']=0
        request.session['masterIndustryCurrentPage']=1

    if request.POST.get("masterIndustrySize")==None:
       pass
    else:
       request.session['masterIndustrySize']=int(request.POST.get('masterIndustrySize'))
       request.session['masterIndustryFirst']=request.session['masterIndustrySize']
       request.session['masterIndustryOffset']=0
       request.session['masterIndustryCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterIndustrySearch']=None
      request.session['masterIndustrySize']=None
      request.session['masterIndustryFirst']=10 #defalt Value 
    
    query = """
            query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
           masterIndustry(offset:$offset,first:$first,last:$last,industry_Icontains:$search,orderBy:$orderBy){
          edges{
           node{
              id
              industry
              }
             }
            }
           }
      """
    v2={
        "search":request.session['masterIndustrySearch']
    }

    td=client.execute(query=query,variables=v2)['data']['masterIndustry']['edges']
    total_records = len(td)

    if total_records%request.session['masterIndustryFirst']==0:
      total_pages = total_records//request.session['masterIndustryFirst']
    else:
      total_pages = total_records//request.session['masterIndustryFirst']+1

    #========Start Search======

    
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterIndustryOffset']=0
      request.session['masterIndustryCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterIndustryOffset']-=request.session['masterIndustryFirst']
      request.session['masterIndustryCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterIndustryOffset']+=request.session['masterIndustryFirst']
      request.session['masterIndustryCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterIndustryCurrentPage']=total_pages
      if total_records%int(request.session['masterIndustryFirst'])==0:
        last=total_records%int(request.session['masterIndustryFirst'])+int(request.session['masterIndustryFirst'])
        request.session['masterIndustryOffset']=total_records-int(request.session['masterIndustryFirst'])
      else:
        last=total_records%int(request.session['masterIndustryFirst'])
        request.session['masterIndustryOffset']=total_records-total_records%int(request.session['masterIndustryFirst'])
      
    #====End Paginator====
    #========Start Sorting=======#
    
    if request.POST.get('sorting_id')=="Id":
      if request.session['masterIndustrySort']=='id':
        request.session['masterIndustrySort']='-id'
      else:
        request.session['masterIndustrySort']='id'
    
    if request.POST.get('sorting_industry')=="Industry":
      if request.session['masterIndustrySort']=='industry':
        request.session['masterIndustrySort']='-industry'
      else:
        request.session['masterIndustrySort']='industry'

    #========End Sorting=========#
    
    
   
    
    v={
        "offset":request.session['masterIndustryOffset'],
        "first":request.session['masterIndustryFirst'],
        "last":last,
        "orderBy": request.session['masterIndustrySort'],
        "search":request.session['masterIndustrySearch']

      }
   
   
    entity_list =client.execute(query=query,variables=v)['data']['masterIndustry']['edges']

    max_d="""
            query{
                 masterIndustryCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterIndustryCount']

    return render(request, path.join('master', 'industry_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row": max_row})
  else:
    return redirect('login')

def create_industry(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        industry_name = request.POST.get('industry_name').title()

         

        query = """

       mutation($industry: String){
      createMasterIndustry(industry:$industry){
        ok
      }
    }

        """

        variables = {"industry": industry_name}

        data = client.execute(query=query, variables=variables)

        return redirect('industry_list')
    return render(request, path.join('master', 'create_industry.html'))
  else:
    return redirect('login')

def update_industry(request, id):
  if len(dict(request.session))>0:   
    query = """
      
                    query($id:Int!){
                      masterIndustryById(id:$id)
                      {
                        id
                        industry
                      }
                    }

                """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterIndustryById']

    if request.method == 'POST':
        industry = request.POST.get('industry_name').title()

        q = """mutation($id:ID, $industry:String){
                  updateMasterIndustry(id:$id, industry:$industry){
                    ok
                  }
                }"""
        v = {"id": id, "industry": industry}

        client.execute(query=q, variables=v)
        return redirect("industry_list")

    return render(request, path.join('master', 'update_industry.html'), {"data": data})
  else:
    return redirect('login')

def delete_industry(request, id):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                  deleteMasterIndustry(id:$id){
                                        ok    
                                  }
                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('industry_list')

    query = """
        
        query($id:Int!){
                      masterIndustryById(id:$id)
                      {
                        id
                        industry
                      }
                    }
    
                """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterIndustryById']

    return render(request, path.join('master', 'delete_industry.html'), {"data": data})
  else:
    return redirect('login')

# =====Product Cateogry=====
def product_category_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterProductCategory')
        request.session['masterProductCategorySearch']=search
        request.session['masterProductCategoryOffset']=0
        request.session['masterProductCategoryCurrentPage']=1

    if request.POST.get("masterProdctCategoryCode")==None:
       pass
    else:
       request.session['masterProdctCategoryCode']=request.POST.get('masterProdctCategoryCode')
       request.session['masterProductCategoryOffset']=0
       request.session['masterProductCategoryCurrentPage']=1

    if request.POST.get("masterProductCategorySize")==None:
       pass
    else:
       request.session['masterProductCategorySize']=int(request.POST.get('masterProductCategorySize'))
       request.session['masterProductCategoryFirst']=request.session['masterProductCategorySize']
       request.session['masterProductCategoryOffset']=0
       request.session['masterProductCategoryCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterProductCategorySearch']=None
      request.session['masterProductCategorySize']=None

      request.session['masterProductCategoryCode']=None
      request.session['masterProductCategoryFirst']=10 #defalt Value 


    query2 = """

              query($search:String,$code:String)
                {
                masterProductCategory(name_Icontains:$search,catCode:$code){
                edges{
               node{
               id
       
                    }
                  }
                }
              }

                  """
    k={
      
       "search":request.session['masterProdctCategorySearch'],
       "code":request.session['masterProdctCategoryCode']
    }          
    td=client.execute(query=query2,variables=k)['data']['masterProductCategory']['edges']
    total_records = len(td)
    if total_records%request.session['masterProductCategoryFirst']==0:
      total_pages = total_records//request.session['masterProductCategoryFirst']
    else:
      total_pages = total_records//request.session['masterProductCategoryFirst']+1

    query = """
              query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String,$code:String){
                masterProductCategory(offset:$offset,first:$first,last:$last,name_Icontains:$search,orderBy:$orderBy,catCode:$code){
                  edges{
                    node{
                      id
                      name
                      abbv
                      catCode
                      catCgst
                      catHsn
                      catSgst
                      catIgst
                    }
                  }
                }
              }
            """
    
    if request.POST.get('First')=="First":
       request.session['masterProductCategoryOffset']=0
       request.session['masterProductCategoryCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
        request.session['masterProductCategoryOffset'] -= int(request.session['masterProductCategoryFirst'])
        request.session['masterProductCategoryCurrentPage'] -=1
    if request.POST.get('Next')=="Next":
       request.session['masterProductCategoryOffset'] += int(request.session['masterProductCategoryFirst'])
       request.session['masterProductCategoryCurrentPage'] +=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterProductCategoryCurrentPage']=total_pages
      if total_records%int(request.session['masterProductCategoryFirst'])==0:
        last=total_records%int(request.session['masterProductCategoryFirst'])+int(request.session['masterProductCategoryFirst'])
        request.session['masterProductCategoryOffset']=total_records-int(request.session['masterProductCategoryFirst'])
      else:
        last=total_records%int(request.session['masterProductCategoryFirst'])
        request.session['masterProductCategoryOffset']=total_records-total_records%int(request.session['masterProductCategoryFirst'])
      
      #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterProductCategorySort']=='id':
        request.session['masterProductCategorySort']='-id'
      else:
        request.session['masterProductCategorySort']='id'

    if request.POST.get('name')=="Name":
      if request.session['masterProductCategorySort']=='name':
        request.session['masterProductCategorySort']='-name'
      else:
        request.session['masterProductCategorySort']='name'

    if request.POST.get('abbrv')=="Abbrv":
      if request.session['masterProductCategorySort']=='abbv':
        request.session['masterProductCategorySort']='-abbv'
      else:
        request.session['masterProductCategorySort']='abbv'
    if request.POST.get('code')=="Code":
      if request.session['masterProductCategorySort']=='cat_code':
        request.session['masterProductCategorySort']='-cat_code'
      else:
        request.session['masterProductCategorySort']='cat_code'

    if request.POST.get('hsn')=="Hsn":
      pass
      # if request.session['masterProductCategorySort']=='sub_cat_hsn':
      #   request.session['masterProductCategorySort']='-sub_cat_hsn'
      # else:
      #   request.session['masterProductCategorySort']='sub_cat_hsn'
    #========End Sorting=========#  


    v={
        "offset":request.session['masterProductCategoryOffset'],
        "first":request.session['masterProductCategoryFirst'],
        "search":request.session['masterProdctCategorySearch'],
        "code":request.session['masterProdctCategoryCode'],
        "last":last,
        "orderBy":request.session['masterProductCategorySort']
      
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterProductCategory']['edges']
    
    td="""
            query{
          masterProductCategoryCount
        }
    """
    max_rows=client.execute(query=td)['data']["masterProductCategoryCount"]
   
    que_code="""
            query{
            masterProductCategory{
              edges{
                node{
                  catCode
                }
              }
            }
          }
     """
    f_data=client.execute(query=que_code)['data']['masterProductCategory']['edges']
    return render(request, path.join('master', 'product_category_list.html'), {"entity_list":entity_list,'max_page':total_pages,"max_rows":max_rows,"f_data":f_data})
  else:
    return redirect('login')

def create_product_category(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        name = request.POST.get('name').title()
        abbv = request.POST.get('abbv').upper()
        code=request.POST.get('code').upper()
        subcathsn=request.POST.get('subcathsn').upper()
        cgst=request.POST.get('cgst')
        sgst=request.POST.get('sgst')
        igst=request.POST.get('igst')
         

        query = """

         mutation($abbv:String,$catCgst:Float,$catCode:String,$catIgst:Float,$catSgst:Float,$name:String,$subCatHsn:String){
          createMasterProductCategory(abbv:$abbv,catCgst:$catCgst,catCode:$catCode,catIgst:$catIgst,catSgst:$catSgst,name:$name,catHsn:$subCatHsn){
            ok
          }
        }
        """

        variables = {
          "name": name,
          "abbv": abbv,
          "subCatHsn": subcathsn,
          "catCode":code,
          "catCgst": cgst,
          "catSgst": sgst,
          "catIgst": igst
          }

        client.execute(query=query, variables=variables)

        return redirect('product_category_list')
    return render(request, path.join('master', 'create_product_category.html'))
  else:
    return redirect('login')

def update_product_category(request, id):
  if len(dict(request.session))>0:   
    query = """
                query($id:Int!){
                  masterProductCategoryById(id:$id){
                    id
                    catCgst
                    catCode
                    catSgst
                    catIgst
                    subCatHsn
                    name
                    abbv
                  }
                }
                    
                    """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductCategoryById']

    if request.method == 'POST':
        name = request.POST.get('name').title()
        abbv = request.POST.get('abbv').upper()
        code=request.POST.get('code').upper()
        subcathsn=request.POST.get('subcathsn').upper()
        cgst=request.POST.get('cgst')
        sgst=request.POST.get('sgst')
        igst=request.POST.get('igst')
        q = """mutation($id:ID!,$abbv:String,$catCgst:Float,$catCode:String,$catIgst:Float,$catSgst:Float,$name:String,$subCatHsn:String){
                          updateMasterProductCategory(id:$id,abbv:$abbv,catCgst:$catCgst,catCode:$catCode,catIgst:$catIgst,catSgst:$catSgst,name:$name,subCatHsn:$subCatHsn){
                            ok
                          }
                        }"""
        v = {
          "id": id, 
          "name": name,
          "abbv": abbv,
          "subCatHsn": subcathsn,
          "catCode":code,
          "catCgst": cgst,
          "catSgst": sgst,
          "catIgst": igst
        }

        client.execute(query=q, variables=v)
        return redirect("product_category_list")

    return render(request, path.join('master', 'update_product_category.html'), {"data": data})
  else:
    return redirect('login')

def delete_product_category(request, id):
  if len(dict(request.session))>0:   
  
    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                      deleteMasterProductCategory(id:$id){
                                            ok    
                                      }
                                    }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_category_list')

    query = """

            query($id:Int!){
                  masterProductCategoryById(id:$id){
                    id
                    name
                    abbv
                  }
                }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductCategoryById']

    return render(request, path.join('master', 'delete_product_category.html'), {"data": data})
  else:
    return redirect('login')

# =====Product Sucateogry=====
def product_subcategory_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('searchmasterSubCategory')
        request.session['masterSubCategorySearch']=search
        request.session['masterSubCategoryOffset']=0
        request.session['masterSubCategoryCurrentPage']=1

    if request.POST.get("masterSubCategoryCode")==None:
       pass
    else:
       request.session['masterSubCategoryCode']=request.POST.get('masterSubCategoryCode')
       request.session['masterSubCategoryOffset']=0
       request.session['masterSubCategoryCurrentPage']=1
    if request.POST.get("masterSubCategoryCategory")==None:
       pass
    else:
       request.session['masterSubCategoryCategory']=request.POST.get('masterSubCategoryCategory')
       request.session['masterSubCategoryOffset']=0
       request.session['masterSubCategoryCurrentPage']=1

    #End Filter
    if request.POST.get("masterSubCategorySize")==None:
       pass
    else:
       request.session['masterSubCategorySize']=int(request.POST.get('masterSubCategorySize'))
       request.session['masterSubCategoryFirst']=request.session['masterSubCategorySize']
       request.session['masterSubCategoryOffset']=0
       request.session['masterSubCategoryCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterSubCategorySearch']=None
      request.session['masterSubCategoryCode']=None
      request.session['masterSubCategoryCategory']=None
      request.session['masterSubCategorySize']=None
      request.session['masterSubCategoryFirst']=10 #defalt Value 
    
    query2 = """

              query($search:String,$code:String,$category:String){
                  masterProductSubcategory(name_Icontains:$search,subcatCode:$code,categoryId_Name:$category){
                  edges{
                    node{

                    
                  id
               
                }
              }
                }
                  }

                  """
    v={
      "search":request.session['masterSubCategorySearch'],
      "code":request.session['masterSubCategoryCode'],
      "category":request.session['masterSubCategoryCategory']
      }
    td=client.execute(query=query2,variables=v)['data']['masterProductSubcategory']['edges']
    total_records = len(td)
    if total_records%request.session['masterSubCategoryFirst']==0:
      total_pages = total_records//request.session['masterSubCategoryFirst']
    else:
      total_pages = total_records//request.session['masterSubCategoryFirst']+1
    
    query = """

               query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String,$code:String,$category:String){
                  masterProductSubcategory(offset:$offset,first:$first,last:$last,name_Icontains:$search,orderBy:$orderBy,subcatCode:$code,categoryId_Name:$category){
                  edges{
                    node{

                    
                  id
                  name
                      subcatCode
                      subCatIgst
                      subCatCgst
                      subCatSgst
                       subCatHsn
                      abbv
                    category{
                                  id
                                  name
                                }
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterSubCategoryOffset']=0
      request.session['masterSubCategoryCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterSubCategoryOffset']-=request.session['masterSubCategoryFirst']
      request.session['masterSubCategoryCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterSubCategoryOffset']+=request.session['masterSubCategoryFirst']
      request.session['masterSubCategoryCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterSubCategoryCurrentPage']=total_pages
      if total_records%int(request.session['masterSubCategoryFirst'])==0:
        last=total_records%int(request.session['masterSubCategoryFirst'])+int(request.session['masterSubCategoryFirst'])
        request.session['masterSubCategoryOffset']=total_records-int(request.session['masterSubCategoryFirst'])
      else:
        last=total_records%int(request.session['masterSubCategoryFirst'])
        request.session['masterSubCategoryOffset']=total_records-total_records%int(request.session['masterSubCategoryFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
     
      if request.session['masterSubCategorySorting']=='id':
        request.session['masterSubCategorySorting']='-id'
      else:
        request.session['masterCrmStatusSorting']='id'

    if request.POST.get('name')=="Name":
      if request.session['masterSubCategorySorting']=='name':
        request.session['masterSubCategorySorting']='-name'
      else:
        request.session['masterSubCategorySorting']='name'

    if request.POST.get('abbrv')=="Abbrv":
      if request.session['masterSubCategorySorting']=='abbv':
        request.session['masterSubCategorySorting']='-abbv'
      else:
        request.session['masterSubCategorySorting']='abbv'


    if request.POST.get('hsn')=="SubCategoryHsn":
      if request.session['masterSubCategorySorting']=='sub_cat_hsn':
        request.session['masterSubCategorySorting']='-sub_cat_hsn'
      else:
        request.session['masterSubCategorySorting']='sub_cat_hsn'

    if request.POST.get('code')=="Code":
      if request.session['masterSubCategorySorting']=='subcat_code':
        request.session['masterSubCategorySorting']='-subcat_code'
      else:
        request.session['masterSubCategorySorting']='subcat_code'


    if request.POST.get('category')=="Category":
      if request.session['masterSubCategorySorting']=='category_id__name':
        request.session['masterSubCategorySorting']='-category_id__name'
      else:
        request.session['masterSubCategorySorting']='category_id__name'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterSubCategoryOffset'],
      "first":request.session['masterSubCategoryFirst'],
      "search":request.session['masterSubCategorySearch'],
      "last":last,
      "code":request.session['masterSubCategoryCode'],
      "category":request.session['masterSubCategoryCategory'],
      "orderBy":request.session['masterSubCategorySorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterProductSubcategory']['edges']
    
    querys_t="""
        query{
          masterProductSubcategoryCount
        }
     """
    max_rows=client.execute(query=querys_t)['data']['masterProductSubcategoryCount']
    

    #====fatching data========

    que_code="""
            query{
              masterProductSubcategory{
                edges{
                  node{
                    id
                    subcatCode
                    
                  }
                }
              }
            }
    """
    que_cate="""
    query{
  masterProductCategory{
    edges{
      node{
          id
          name
        
            }
          }
        }
      }
    """
    f_data=client.execute(query=que_code)['data']['masterProductSubcategory']['edges']
    c_data=client.execute(query=que_cate)['data']['masterProductCategory']['edges']
    return render(request, path.join('master', 'product_subcategory_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_rows":max_rows,"f_data":f_data,"c_data":c_data})
  else:
    return redirect('login')

def create_product_subcategory(request):
  if len(dict(request.session))>0:   

    query = """
            query{
            masterProductCategory{
              edges{
                node{
            id
            name
            }
            }
                 }
              }
    """
    data = client.execute(query=query)['data']['masterProductCategory']['edges']
    if request.method == 'POST':
        name = request.POST.get('name')
        abbv = request.POST.get('abbv')
        hsn = request.POST.get('hsn')
        code=request.POST.get('code')
        subcatsgst=request.POST.get('subcatsgst')
        subCatCgst=request.POST.get('subCatCgst')
        subCatIgst=request.POST.get('subCatIgst')
        category = request.POST.get('category')

         

        query = """

       mutation($abbv: String,$categoryId: Int,$name: String,$subCatCgst: Int,$subCatHsn: String,$subCatIgst: Int,$subCatSgst: Int,$subcatCode: String){
          createMasterProductSubcategory(name: $name, abbv: $abbv,subCatCgst:$subCatCgst,subCatHsn:$subCatHsn, subCatSgst:$subCatSgst,subcatCode:$subcatCode,categoryId:$categoryId,subCatIgst:$subCatIgst){
            ok
          }
        }

        """

        variables = {
        "name": name, 
        "abbv": abbv, 
        "subCatHsn": hsn, 
        "categoryId":category,
        "subCatCgst":subCatCgst,
        "subCatSgst":subcatsgst,
        "subCatIgst":subCatIgst,
        "subcatCode":code
        }

        client.execute(query=query, variables=variables)

        return redirect('product_subcategory_list')
    return render(request, path.join('master', 'create_product_subcategory.html'), {"data": data})
  else:
    return redirect('login')

def update_product_subcategory(request, id):
     
  if len(dict(request.session))>0:
    q = """
        
        query{
            masterProductCategory{
              edges{
                node{
                    id
                    name 
                    }
                }
                 }
              }
            """
    data1 = client.execute(query=q)['data']['masterProductCategory']['edges']

    query = """
                query($id:Int!){
                  masterProductSubcategoryById(id:$id){
                    id
                    name
                    abbv
                    subcatCode
                    subCatIgst
                    subCatCgst
                    subCatSgst
                    subCatHsn
                    category{
                        id
                        name
                    }
                  }
                }
                    
                    """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductSubcategoryById']

    if request.method == 'POST':
        name = request.POST.get('name')
        abbv = request.POST.get('abbv')
        hsn = request.POST.get('hsn')
        code=request.POST.get('code')
        subcatsgst=request.POST.get('subcatsgst')
        subCatCgst=request.POST.get('subCatCgst')
        subCatIgst=request.POST.get('subCatIgst')
        category = request.POST.get('category')
     

        q = """mutation($id:ID!, $abbv: String,$categoryId: Int,$name: String,$subCatCgst: Int,$subCatHsn: String,$subCatIgst: Int,$subCatSgst:Int,$subcatCode:String){
              updateMasterProductSubcategory(id:$id,name: $name, abbv: $abbv,subCatCgst:$subCatCgst,subCatHsn:$subCatHsn, subCatSgst:$subCatSgst,subcatCode:$subcatCode,categoryId:$categoryId,subCatIgst:$subCatIgst){
                    ok
                  }
                }"""
        v = {
        "id": id, 
        "name": name, 
        "abbv": abbv, 
        "subCatHsn": hsn, 
        "categoryId":category,
        "subCatCgst":subCatCgst,
        "subCatSgst":subcatsgst,
        "subCatIgst":subCatIgst,
        "subcatCode":code
          }

        client.execute(query=q, variables=v)
        return redirect("product_subcategory_list")

    return render(request, path.join('master', 'update_product_subcategory.html'), {"data": data, "data1": data1})
  else:
    return redirect('login')

def delete_product_subcategory(request, id):
  if len(dict(request.session))>0:   
  
    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                      deleteMasterProductSubcategory(id:$id){
                                            ok    
                                      }
                                    }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_subcategory_list')

    query = """

            query($id:Int!){
                  masterProductSubcategoryById(id:$id){
                    id
                    name    
                    abbv
                    
                    category{
                        id
                        name
                    }
                  }
                }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductSubcategoryById']

    return render(request, path.join('master', 'delete_product_subcategory.html'), {"data": data})
  else:
    return redirect('login')

# =====Product Nutrition Type=====
def product_nutrition_type_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterNutritionType')
        request.session['masterNutritionTypeSearch']=search
        request.session['masterNutritionTypeOffset']=0
        request.session['masterNutritionTypeCurrentPage']=1

    if request.POST.get("masterNutritionTypeSize")==None:
       pass
    else:
       request.session['masterNutritionTypeSize']=int(request.POST.get('masterNutritionTypeSize'))
       request.session['masterNutritionTypeFirst']=request.session['masterNutritionTypeSize']
       request.session['masterNutritionTypeOffset']=0
       request.session['masterNutritionTypeCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterNutritionTypeSearch']=None
      request.session['masterNutritionTypeSize']=None
      request.session['masterNutritionTypeFirst']=10 #defalt Value 
    
    query2 = """

              query($search:String){
                  masterProductNutritionType(type_Icontains:$search){
                  edges{
                    node{

                    
                  id
               
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterNutritionTypeSearch']})['data']['masterProductNutritionType']['edges']
    total_records = len(td)
    if total_records%request.session['masterNutritionTypeFirst']==0:
      total_pages = total_records//request.session['masterNutritionTypeFirst']
    else:
      total_pages = total_records//request.session['masterNutritionTypeFirst']+1
    
    query = """

              query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterProductNutritionType(offset:$offset,first:$first,last:$last,type_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                  id
                  type
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterNutritionTypeOffset']=0
      request.session['masterNutritionTypeCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterNutritionTypeOffset']-=request.session['masterNutritionTypeFirst']
      request.session['masterNutritionTypeCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterNutritionTypeOffset']+=request.session['masterNutritionTypeFirst']
      request.session['masterNutritionTypeCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterNutritionTypeCurrentPage']=total_pages
      if total_records%int(request.session['masterNutritionTypeFirst'])==0:
        last=total_records%int(request.session['masterNutritionTypeFirst'])+int(request.session['masterNutritionTypeFirst'])
        request.session['masterNutritionTypeOffset']=total_records-int(request.session['masterNutritionTypeFirst'])
      else:
        last=total_records%int(request.session['masterNutritionTypeFirst'])
        request.session['masterNutritionTypeOffset']=total_records-total_records%int(request.session['masterNutritionTypeFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('ids')=="Id":
      if request.session['masterNutritionTypeSorting']=='id':
        request.session['masterNutritionTypeSorting']='-id'
      else:
        request.session['masterCrmStatusSorting']='id'

    if request.POST.get('type')=="Type":
      if request.session['masterNutritionTypeSorting']=='type':
        request.session['masterNutritionTypeSorting']='-type'
      else:
        request.session['masterNutritionTypeSorting']='type'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterNutritionTypeOffset'],
      "first":request.session['masterNutritionTypeFirst'],
      "search":request.session['masterNutritionTypeSearch'],
      "last":last,
      "orderBy":request.session['masterNutritionTypeSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterProductNutritionType']['edges']
    max_d="""
            query{
                masterProductNutritionTypeCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterProductNutritionTypeCount']

    return render(request, path.join('master', 'product_nutrition_type_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row":max_row})
  else:
    return redirect('login')

def create_product_nutrition_type(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        type = request.POST.get('type').title()

         

        query = """

       mutation($type: String){
          createMasterProductNutritionType(type: $type){
            ok
          }
        }

        """

        variables = {"type": type}

        client.execute(query=query, variables=variables)

        return redirect('product_nutrition_type_list')
    return render(request, path.join('master', 'create_product_nutrition_type.html'))
  else:
    return redirect('login')

def update_product_nutrition_type(request, id):
  if len(dict(request.session))>0:   
    query = """
                    query($id:Int!){
                      masterProductNutritionTypeById(id:$id){
                        id
                        type
                      }
                    }

                        """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductNutritionTypeById']

    if request.method == 'POST':
        type = request.POST.get('type').title()

        q = """mutation($id:ID!, $type:String){
                      updateMasterProductNutritionType(id:$id, type:$type){
                        ok
                      }
                    }"""

        v = {"id": id, "type": type}

        client.execute(query=q, variables=v)
        return redirect("product_nutrition_type_list")

    return render(request, path.join('master', 'update_product_nutrition_type.html'), {"data": data})
  else:
    return redirect('login')

def delete_product_nutrition_type(request, id):
  if len(dict(request.session))>0:   
  
    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                      deleteMasterProductNutritionType(id:$id){
                                            ok    
                                      }
                                    }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_nutrition_type_list')

    query = """

            query($id:Int!){
                  masterProductNutritionTypeById(id:$id){
                    id
                    type
                  }
                }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductNutritionTypeById']

    return render(request, path.join('master', 'delete_product_nutrition_type.html'), {"data": data})
  else:
    return redirect('login')

# =====Product Measure Type=====
def product_measure_type_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterMeasureType')
        request.session['masterMeasureTypeSearch']=search
        request.session['masterMeasureTypeOffset']=0
        request.session['masterMeasureTypeCurrentPage']=1

    if request.POST.get("masterMeasureTypeSize")==None:
       pass
    else:
       request.session['masterMeasureTypeSize']=int(request.POST.get('masterMeasureTypeSize'))
       request.session['masterMeasureTypeFirst']=request.session['masterMeasureTypeSize']
       request.session['masterMeasureTypeOffset']=0
       request.session['masterMeasureTypeCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterMeasureTypeSearch']=None
      request.session['masterMeasureTypeSize']=None
      request.session['masterMeasureTypeFirst']=10 #defalt Value 
    
    query2 = """

             query($search:String){
                  masterProductMeasureType(measureType_Icontains:$search){
                  edges{
                    node{

                    
                  id
               
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterMeasureTypeSearch']})['data']['masterProductMeasureType']['edges']
    total_records = len(td)
    if total_records%request.session['masterMeasureTypeFirst']==0:
      total_pages = total_records//request.session['masterMeasureTypeFirst']
    else:
      total_pages = total_records//request.session['masterMeasureTypeFirst']+1
    
    query = """

              query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterProductMeasureType(offset:$offset,first:$first,last:$last,measureType_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                  id
                  measureType
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterMeasureTypeOffset']=0
      request.session['masterMeasureTypeCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterMeasureTypeOffset']-=request.session['masterMeasureTypeFirst']
      request.session['masterMeasureTypeCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterMeasureTypeOffset']+=request.session['masterMeasureTypeFirst']
      request.session['masterMeasureTypeCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterMeasureTypeCurrentPage']=total_pages
      if total_records%int(request.session['masterMeasureTypeFirst'])==0:
        last=total_records%int(request.session['masterMeasureTypeFirst'])+int(request.session['masterMeasureTypeFirst'])
        request.session['masterMeasureTypeOffset']=total_records-int(request.session['masterMeasureTypeFirst'])
      else:
        last=total_records%int(request.session['masterMeasureTypeFirst'])
        request.session['masterMeasureTypeOffset']=total_records-total_records%int(request.session['masterMeasureTypeFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterMeasureTypeSorting']=='id':
        request.session['masterMeasureTypeSorting']='-id'
      else:
        request.session['masterMeasureTypeSorting']='id'

    if request.POST.get('measuretype')=="Measure Type":
      if request.session['masterMeasureTypeSorting']=='measure_type':
        request.session['masterMeasureTypeSorting']='-measure_type'
      else:
        request.session['masterMeasureTypeSorting']='measure_type'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterMeasureTypeOffset'],
      "first":request.session['masterMeasureTypeFirst'],
      "search":request.session['masterMeasureTypeSearch'],
      "last":last,
      "orderBy":request.session['masterMeasureTypeSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterProductMeasureType']['edges']
    max_d="""
            query{
                masterProductMeasureTypeCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterProductMeasureTypeCount']

    return render(request, path.join('master', 'product_measure_type_list.html'),{"entity_list": entity_list,"max_page":total_pages,"max_row":max_row})
  else:
    return redirect('login')

def create_product_measure_type(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        measuretype = request.POST.get('measuretype')

         

        query = """

       mutation($measuretype: String){
          createMasterProductMeasureType(measureType: $measuretype){
            ok
          }
        }

        """

        variables = {"measuretype": measuretype}

        client.execute(query=query, variables=variables)

        return redirect('product_measure_type_list')
    return render(request, path.join('master', 'create_product_measure_type.html'))
  else:
    return redirect('login')

def update_product_measure_type(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterProductMeasureTypeById(id:$id){
                            id
                            measureType
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductMeasureTypeById']

    if request.method == 'POST':
        measuretype = request.POST.get('measuretype')

        q = """mutation($id:ID!, $measuretype:String){
                          updateMasterProductMeasureType(id:$id, measureType:$measuretype){
                            ok
                          }
                        }"""

        v = {"id": id, "measuretype": measuretype}

        client.execute(query=q, variables=v)
        return redirect("product_measure_type_list")

    return render(request, path.join('master', 'update_product_measure_type.html'), {"data": data})
  else:
    return redirect('login')

def delete_product_measure_type(request, id):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                          deleteMasterProductMeasureType(id:$id){
                                                ok    
                                          }
                                        }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_measure_type_list')

    query = """

                query($id:Int!){
                      masterProductMeasureTypeById(id:$id){
                        id
                        measureType
                      }
                    }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductMeasureTypeById']

    return render(request, path.join('master', 'delete_product_measure_type.html'), {"data": data})
  else:
    return redirect('login')

# =====Product State =====
def product_state_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterState')
        request.session['masterStateSearch']=search
        request.session['masterStateOffset']=0
        request.session['masterStateCurrentPage']=1

    if request.POST.get("masterStateSize")==None:
       pass
    else:
       request.session['masterStateSize']=int(request.POST.get('masterStateSize'))
       request.session['masterStateFirst']=request.session['masterStateSize']
       request.session['masterStateOffset']=0
       request.session['masterStateCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterStateSearch']=None
      request.session['masterStateSize']=None
      request.session['masterStateFirst']=10 #defalt Value 
    
    query2 = """

              
              query($search:String){
                  masterProductState(state_Icontains:$search){
                  edges{
                    node{

                    
                  id
                 
               
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterStateSearch']})['data']['masterProductState']['edges']
    total_records = len(td)
    if total_records%request.session['masterStateFirst']==0:
      total_pages = total_records//request.session['masterStateFirst']
    else:
      total_pages = total_records//request.session['masterStateFirst']+1
    
    query = """

               query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterProductState(offset:$offset,first:$first,last:$last,state_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                  id
                  state
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterStateOffset']=0
      request.session['masterStateCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterStateOffset']-=request.session['masterStateFirst']
      request.session['masterStateCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterStateOffset']+=request.session['masterStateFirst']
      request.session['masterStateCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterStateCurrentPage']=total_pages
      if total_records%int(request.session['masterStateFirst'])==0:
        last=total_records%int(request.session['masterStateFirst'])+int(request.session['masterStateFirst'])
        request.session['masterStateOffset']=total_records-int(request.session['masterStateFirst'])
      else:
        last=total_records%int(request.session['masterStateFirst'])
        request.session['masterStateOffset']=total_records-total_records%int(request.session['masterStateFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterStateSorting']=='id':
        request.session['masterStateSorting']='-id'
      else:
        request.session['masterStateSorting']='id'

    if request.POST.get('state')=="State":
      if request.session['masterStateSorting']=='state':
        request.session['masterStateSorting']='-state'
      else:
        request.session['masterStateSorting']='state'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterStateOffset'],
      "first":request.session['masterStateFirst'],
      "search":request.session['masterStateSearch'],
      "last":last,
      "orderBy":request.session['masterStateSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterProductState']['edges']
    max_d="""
            query{
                masterProductStateCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterProductStateCount']
    return render(request, path.join('master', 'product_state_type_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row":max_row })
  else:
    return redirect('login')

def create_product_state(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        state = request.POST.get('state_type').title()

         

        query = """

     mutation($state: String){
        createMasterProductState(state: $state){
          ok
        }
      }

      """

        variables = {"state": state}

        client.execute(query=query, variables=variables)

        return redirect('product_state_list')
    return render(request, path.join('master', 'create_product_state_type.html'))
  else:
    return redirect('login')

def update_product_state(request, id):
  if len(dict(request.session))>0:   
    query = """
                          query($id:Int!){
                            masterProductStateById(id:$id){
                              id
                              state
                            }
                          }
                              """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductStateById']

    if request.method == 'POST':
        state = request.POST.get('state_type').title()

        q = """mutation($id:ID!, $state:String){
                            updateMasterProductState(id:$id, state:$state){
                              ok
                            }
                          }"""

        v = {"id": id, "state": state}

        client.execute(query=q, variables=v)
        return redirect("product_state_list")

    return render(request, path.join('master', 'update_product_state_type.html'), {"data": data})
  else:
    return redirect('login')

def delete_product_state(request, id):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                            deleteMasterProductState(id:$id){
                                  ok    
                            }
                          }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_state_list')

    query = """

              query($id:Int!){
                    masterProductStateById(id:$id){
                      id
                      state
                    }
                  }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductStateById']

    return render(request, path.join('master', 'delete_product_state_type.html'), {"data": data})
  else:
    return redirect('login')

# =====Product Taste=====
def product_taste_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterTaste')
        request.session['masterTasteSearch']=search
        request.session['masterTasteOffset']=0
        request.session['masterTasteCurrentPage']=1

    if request.POST.get("masterTasteSize")==None:
       pass
    else:
       request.session['masterTasteSize']=int(request.POST.get('masterTasteSize'))
       request.session['masterTasteFirst']=request.session['masterTasteSize']
       request.session['masterTasteOffset']=0
       request.session['masterTasteCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
       request.session['masterTasteSearch']=None
       request.session['masterTasteSize']=None
       request.session['masterTasteSorting']=None
       request.session['masterTasteFirst']=10 #defalt Value 
    
    query2 = """
  
              query($search:String){
                  masterProductTaste(taste_Icontains:$search){
                  edges{
                    node{

                    
                  id
                 
               
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterTasteSearch']})['data']['masterProductTaste']['edges']
    total_records = len(td)
    if total_records%request.session['masterTasteFirst']==0:
      total_pages = total_records//request.session['masterTasteFirst']
    else:
      total_pages = total_records//request.session['masterTasteFirst']+1
    
    query = """

               query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterProductTaste(offset:$offset,first:$first,last:$last,taste_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                  id
                  taste
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterTasteOffset']=0
      request.session['masterTasteCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterTasteOffset']-=request.session['masterTasteFirst']
      request.session['masterTasteCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterTasteOffset']+=request.session['masterTasteFirst']
      request.session['masterTasteCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterTasteCurrentPage']=total_pages
      if total_records%int(request.session['masterTasteFirst'])==0:
        last=total_records%int(request.session['masterTasteFirst'])+int(request.session['masterTasteFirst'])
        request.session['masterTasteOffset']=total_records-int(request.session['masterTasteFirst'])
      else:
        last=total_records%int(request.session['masterTasteFirst'])
        request.session['masterTasteOffset']=total_records-total_records%int(request.session['masterTasteFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    # if request.POST.get('id')=="Id":
    #   if request.session['masterStateSorting']=='id':
    #     request.session['masterStateSorting']='-id'
    #   else:
    #     request.session['masterStateSorting']='id'

    if request.POST.get('state')=="Taste":
      if request.session['masterStateSorting']=='taste':
        request.session['masterStateSorting']='-taste'
      else:
        request.session['masterStateSorting']='taste'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterTasteOffset'],
      "first":request.session['masterTasteFirst'],
      "search":request.session['masterTasteSearch'],
      "last":last,
      "orderBy":request.session['masterTasteSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterProductTaste']['edges']
    max_d="""
            query{
               masterProductTasteCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterProductTasteCount']
    return render(request, path.join('master', 'product_taste_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row":max_row })
  else:
    return redirect('login')
def create_product_taste(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        taste = request.POST.get('taste').title()

         

        query = """

       mutation($taste: String){
          createMasterProductTaste(taste: $taste){
            ok
          }
        }

        """

        variables = {"taste": taste}

        client.execute(query=query, variables=variables)

        return redirect('product_taste_list')
    return render(request, path.join('master', 'create_product_taste.html'))
  else:
    return redirect('login')

def update_product_taste(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterProductTasteById(id:$id){
                            id
                            taste
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductTasteById']

    if request.method == 'POST':
        taste = request.POST.get('taste').title()

        q = """mutation($id:ID!, $taste:String){
                          updateMasterProductTaste(id:$id, taste:$taste){
                            ok
                          }
                        }"""

        v = {"id": id, "taste": taste}

        client.execute(query=q, variables=v)
        return redirect("product_taste_list")

    return render(request, path.join('master', 'update_product_taste.html'), {"data": data})
  else:
    return redirect('login')

def delete_product_taste(request, id):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                          deleteMasterProductTaste(id:$id){
                                                ok    
                                          }
                                        }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_taste_list')

    query = """

                query($id:Int!){
                      masterProductTasteById(id:$id){
                        id
                        taste
                      }
                    }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductTasteById']

    return render(request, path.join('master', 'delete_product_taste.html'), {"data": data})
  else:
    return redirect('login')

# =====create_crm_comm_status===========
def crm_comm_status_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        mastercrmsearchsj=request.POST.get('mastercrmsearchsj')
        request.session['masterCrmCommonStatusSearch']=mastercrmsearchsj
        request.session['masterCrmCommonStatusOffset']=0
        request.session['masterCrmCommonStatusCurrentPage']=1
    

    if request.POST.get("masterCrmCommonStatusSize")==None:
       pass
    else:
       request.session['masterCrmCommonStatusSize']=int(request.POST.get('masterCrmCommonStatusSize'))
       request.session['masterCrmCommonStatusFirst']=request.session['masterCrmCommonStatusSize']
       request.session['masterCrmCommonStatusOffset']=0
       request.session['masterCrmCommonStatusCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterCrmCommonStatusSearch']=None
      request.session['masterCrmCommonStatusSize']=None
      request.session['masterCrmCommonStatusFirst']=10 #defalt Value 
    query2= """
                    query($search:String){
                    masterCrmCommStatus(status_Icontains:$search){
                    edges{
                       node{
                          id
                            
                          }
                        }
                      }

                    }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterCrmCommonStatusSearch']})['data']['masterCrmCommStatus']['edges']
    total_records = len(td)
    if total_records%request.session['masterCrmCommonStatusFirst']==0:
      total_pages = total_records//request.session['masterCrmCommonStatusFirst']
    else:
      total_pages = total_records//request.session['masterCrmCommonStatusFirst']+1


    query= """
                    query($offset:Int,$first:Int,$last:Int,$orderBy:String,$search:String){
                    masterCrmCommStatus(offset:$offset,first:$first,last:$last,orderBy:$orderBy,status_Icontains:$search){
                    edges{
                       node{
                          id
                            status
                          }
                        }
                      }

                    }

                  """


  
    if request.POST.get('Next')=="Next":
      request.session["masterCrmCommonStatusOffset"]+=int(request.session["masterCrmCommonStatusFirst"])
      request.session['masterCrmCommonStatusCurrentPage']+=1
    if request.POST.get('First')=="First":
      request.session['masterCrmCommonStatusCurrentPage']=1
      request.session["masterCrmCommonStatusOffset"]=0
    if request.POST.get('Previous')=="Previous":
      request.session["masterCrmCommonStatusOffset"]-=int(request.session["masterCrmCommonStatusFirst"])
      request.session['masterCrmCommonStatusCurrentPage']-=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterCrmCommonStatusCurrentPage']=total_pages
      if total_records%int(request.session['masterCrmCommonStatusFirst'])==0:
        last=total_records%int(request.session['masterCrmCommonStatusFirst'])+int(request.session['masterCrmCommonStatusFirst'])
        request.session['masterCrmCommonStatusOffset']=total_records-int(request.session['masterCrmCommonStatusFirst'])
      else:
        last=total_records%int(request.session['masterCrmCommonStatusFirst'])
        request.session['masterCrmCommonStatusOffset']=total_records-total_records%int(request.session['masterCrmCommonStatusFirst'])  
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterCrmCommonStatusSorting']=='id':
        request.session['masterCrmCommonStatusSorting']='-id'
      else:
        request.session['masterCrmCommonStatusSorting']='id'

    if request.POST.get('status')=="Status":
      if request.session['masterCrmCommonStatusSorting']=='status':
        request.session['masterCrmCommonStatusSorting']='-status'
      else:
        request.session['masterCrmCommonStatusSorting']='status'
      
    v= {
      "offset":request.session['masterCrmCommonStatusOffset'],
      "first":request.session['masterCrmCommonStatusFirst'],
      "last":last,
      "search":request.session["masterCrmCommonStatusSearch"],
      "orderBy":request.session['masterCrmCommonStatusSorting']
       }
    entity_list = client.execute(query=query,variables=v)['data']['masterCrmCommStatus']['edges']
    max_d="""
            query{
                masterCrmCommStatusCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterCrmCommStatusCount']
    
    return render(request, path.join('master', 'crm_comm_status_list.html'), {"entity_list":entity_list,"max_page":total_pages,"max_row":max_row})
  else:
    return redirect('login')

def create_crm_comm_status(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        status = request.POST.get('status')

         

        query = """

       mutation($status: String){
          createMasterCrmCommStatus(status: $status){
            ok
          }
        }

        """

        variables = {"status": status}

        client.execute(query=query, variables=variables)

        return redirect('crm_comm_status_list')
    return render(request, path.join('master', 'create_crm_comm_status.html'))
  else:
    return redirect('login')

def update_crm_comm_status(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterCrmCommStatusById(id:$id){
                            id
                            status
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmCommStatusById']

    if request.method == 'POST':
        status = request.POST.get('status')

        q = """mutation($id:ID!, $status:String){
                          updateMasterCrmCommStatus(id:$id, status:$status){
                            ok
                          }
                        }"""

        v = {"id": id, "status": status}

        client.execute(query=q, variables=v)
        return redirect("crm_comm_status_list")

    return render(request, path.join('master', 'update_crm_comm_status.html'), {"data": data})
  else:
    return redirect('login')

def delete_crm_comm_status(request, id):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                              deleteMasterCrmCommStatus(id:$id){
                                                    ok    
                                              }
                                            }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('crm_comm_status_list')

    query = """

                    query($id:Int!){
                          masterCrmCommStatusById(id:$id){
                            id
                            status
                          }
                        }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterCrmCommStatusById']

    return render(request, path.join('master', 'delete_crm_comm_status.html'), {"data": data})
  else:
    return redirect('login')

# =====Crm common Type===========
def crm_comm_type_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        searchdata=request.POST.get('searchdata')
        request.session['masterCrmCommonTypeSearch']=searchdata
        request.session['masterCrmCommonTypeOffset']=0
        request.session['masterCrmCommonTypeCurrentPage']=1

    if request.POST.get("masterCrmCommonTypeSize")==None:
       pass
    else:
       request.session['masterCrmCommonTypeSize']=int(request.POST.get('masterCrmCommonTypeSize'))
       request.session['masterCrmCommonTypeFirst']=request.session['masterCrmCommonTypeSize']
       request.session['masterCrmCommonTypeOffset']=0
       request.session['masterCrmCommonTypeCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterCrmCommonTypeSearch']=None
      request.session['masterCrmCommonTypeSize']=None
      request.session['masterCrmCommonTypeFirst']=10 #defalt Value 
    query2="""
        query($search:String){
              masterCrmCommType(type_Icontains:$search){
                edges{
                  node{
               id
                type
                }
                }
                }
                }
    
    
    
    """
    td=client.execute(query=query2,variables={"search": request.session['masterCrmCommonTypeSearch']})['data']['masterCrmCommType']['edges']
    total_records = len(td)
    if total_records%request.session['masterCrmCommonTypeFirst']==0:
      total_pages = total_records//request.session['masterCrmCommonTypeFirst']
    else:
      total_pages = total_records//request.session['masterCrmCommonTypeFirst']+1


    query = """

            query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
              masterCrmCommType(offset:$offset,first:$first,last:$last,type_Icontains:$search,orderBy:$orderBy){
                edges{
                  node{
               id
                type
                }
                }
                }
                }
                """
  

    if request.POST.get('Next')=="Next":
      request.session['masterCrmCommonTypeOffset']+=int(request.session['masterCrmCommonTypeFirst'])
      request.session['masterCrmCommonTypeCurrentPage']+=1   
    if request.POST.get('Previous')=="Previous":
      request.session['masterCrmCommonTypeOffset']-=int(request.session['masterCrmCommonTypeFirst'])
      request.session['masterCrmCommonTypeCurrentPage']-=1  
    if request.POST.get('First')=="First":
      request.session['masterCrmCommonTypeCurrentPage']=1 
      request.session['masterCrmCommonTypeOffset']=0
    last=None 
    if request.POST.get("Last")=="Last":
      request.session['masterCrmCommonTypeCurrentPage']=total_pages
      if total_records%int(request.session['masterCrmCommonTypeFirst'])==0:
        last=total_records%int(request.session['masterCrmCommonTypeFirst'])+int(request.session['masterCrmCommonTypeFirst'])
        request.session['masterCrmCommonTypeOffset']=total_records-int(request.session['masterCrmCommonTypeFirst'])
      else:
        last=total_records%int(request.session['masterCrmCommonTypeFirst'])
        request.session['masterCrmCommonTypeOffset']=total_records-total_records%int(request.session['masterCrmCommonTypeFirst'])
    #====End Paginator====
      #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterCrmCommonTypeSorting']=='id':
        request.session['masterCrmCommonTypeSorting']='-id'
      else:
        request.session['masterCrmCommonTypeSorting']='id'

    if request.POST.get('type')=="Type":
      if request.session['masterCrmCommonTypeSorting']=='type':
        request.session['masterCrmCommonTypeSorting']='-type'
      else:
        request.session['masterCrmCommonTypeSorting']='type'
              
    v={
      "offset":request.session['masterCrmCommonTypeOffset'],
      "first":request.session['masterCrmCommonTypeFirst'],
      "last":last,
      "search":request.session['masterCrmCommonTypeSearch'],
      "orderBy":request.session['masterCrmCommonTypeSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterCrmCommType']['edges']
    max_d="""
            query{
                masterCrmCommTypeCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterCrmCommTypeCount']
    
    return render(request, path.join('master', 'crm_comm_type_list.html'), {"entity_list":entity_list,"max_page":total_pages,"max_row":max_row})
  else:
    return redirect('login')

def create_crm_comm_type(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        type = request.POST.get('type')

         

        query = """

           mutation($type: String){
              createMasterCrmCommType(type: $type){
                ok
              }
            }

        """

        variables = {"type": type}

        client.execute(query=query, variables=variables)

        return redirect('crm_comm_type_list')
    return render(request, path.join('master', 'create_crm_comm_type.html'))
  else:
    return redirect('login')

def update_crm_comm_type(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterCrmCommTypeById(id:$id){
                            id
                            type
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmCommTypeById']

    if request.method == 'POST':
        type = request.POST.get('type')

        q = """mutation($id:ID!, $type:String){
                          updateMasterCrmCommType(id:$id, type:$type){
                            ok
                          }
                        }"""

        v = {"id": id, "type": type}

        client.execute(query=q, variables=v)
        return redirect("crm_comm_type_list")

    return render(request, path.join('master', 'update_crm_comm_type.html'), {"data": data})
  else:
    return redirect('login')

def delete_crm_comm_type(request, id):
  if len(dict(request.session))>0:   

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                                  deleteMasterCrmCommType(id:$id){
                                                        ok    
                                                  }
                                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('crm_comm_type_list')

    query = """

                        query($id:Int!){
                              masterCrmCommTypeById(id:$id){
                                id
                                type
                              }
                            }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterCrmCommTypeById']

    return render(request, path.join('master', 'delete_crm_comm_type.html'), {"data": data})
  else:
    return redirect('login')

# =====Crm Source===========
def crm_source_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        searchcrmsource=request.POST.get('searchcrmsource')
        request.session['masterCrmSourceSearch']=searchcrmsource
        request.session['masterCrmSourceOffset']=0
        request.session['masterCrmSourceCurrentPage']=1

    if request.POST.get("mastercrmsourceSize")==None:
       pass
    else:
       request.session['masterCrmSourceSize']=int(request.POST.get('mastercrmsourceSize'))
       request.session['masterCrmSourceFirst']=request.session['masterCrmSourceSize']
       request.session['masterCrmSourceOffset']=0
       request.session['masterCrmSourceCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterCrmSourceSearch']=None
      request.session['masterCrmSourceSize']=None
      request.session['masterCrmSourceFirst']=10 #defalt Value
      

    query2= """
          query($search:String){
          masterCrmSource(source_Icontains:$search)
            {
               edges{
                  node{
                  id
                 
                   }
                  }
                  }
                   }
               """

    td=client.execute(query=query2,variables={"search": request.session['masterCrmSourceSearch']})['data']['masterCrmSource']['edges']
    total_records = len(td)
    if total_records%request.session['masterCrmSourceFirst']==0:
      total_pages = total_records//request.session['masterCrmSourceFirst']
    else:
      total_pages = total_records//request.session['masterCrmSourceFirst']+1


    query = """
          query($offset:Int, $first:Int,$last:Int,$search:String,$orderBy:String){
          masterCrmSource(offset:$offset,first:$first,last:$last,source_Icontains:$search,orderBy:$orderBy)
            {
               edges{
                  node{
                  id
                  source
                   }
                  }
                  }
                   }
               """

    
    if request.POST.get('First')=="First":
       request.session['masterCrmSourceOffset']=0
       request.session['masterCrmSourceCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
       request.session['masterCrmSourceOffset'] -=int(request.session['masterCrmSourceFirst'])
       request.session['masterCrmSourceCurrentPage']-=1
    if request.POST.get('Next')=="Next":
       request.session['masterCrmSourceOffset'] +=int(request.session['masterCrmSourceFirst'])
       request.session['masterCrmSourceCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
        request.session['masterCrmSourceCurrentPage']=total_pages
        if total_records%int(request.session['masterCrmSourceFirst'])==0:
          last=total_records%int(request.session['masterCrmSourceFirst'])+int(request.session['masterCrmSourceFirst'])
          request.session['masterCrmSourceOffset']=total_records-int(request.session['masterCrmSourceFirst'])
        else:
          last=total_records%int(request.session['masterCrmSourceFirst'])
          request.session['masterCrmSourceOffset']=total_records-total_records%int(request.session['masterCrmSourceFirst'])
     #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":

      if request.session['masterCrmSourceSorting']=='id':
        request.session['masterCrmSourceSorting']='-id'
      else:
        request.session['masterCrmSourceSorting']='id'

    if request.POST.get('source')=="Source":

      if request.session['masterCrmSourceSorting']=='source':
        request.session['masterCrmSourceSorting']='-source'
      else:
        request.session['masterCrmSourceSorting']='source'

    
    v={
      "offset":request.session['masterCrmSourceOffset'],
      "first":request.session['masterCrmSourceFirst'],
      "search":request.session['masterCrmSourceSearch'],
      "last":last,
      "orderBy":request.session['masterCrmSourceSorting']

    }
    entity_list = client.execute(query=query,variables=v)['data']['masterCrmSource']['edges']
    max_d="""
            query{
                  masterCrmSourceCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterCrmSourceCount']
    return render(request, path.join('master', 'crm_source_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row": max_row})
  else:
    return redirect('login')

def create_crm_source(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        source = request.POST.get('source')
        query = """

           mutation($source: String){
              createMasterCrmSource(source: $source){
                ok
              }
            }

        """

        variables = {"source": source}

        client.execute(query=query, variables=variables)

        return redirect('crm_source_list')
    return render(request, path.join('master', 'create_crm_source.html'))
  else:
    return redirect('login')

def update_crm_source(request, id):
  if len(dict(request.session))>0:  
    query = """
                        query($id:Int!){
                          masterCrmSourceById(id:$id){
                            id
                            source
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmSourceById']

    if request.method == 'POST':
        source = request.POST.get('source')

        q = """mutation($id:ID!, $source:String){
                          updateMasterCrmSource(id:$id, source:$source){
                            ok
                          }
                        }"""

        v = {"id": id, "source": source}

        client.execute(query=q, variables=v)
        return redirect("crm_source_list")

    return render(request, path.join('master', 'update_crm_source.html'), {"data": data})
  else:
    return redirect('login')

def delete_crm_source(request, id):
  if len(dict(request.session))>0:   

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                                  deleteMasterCrmSource(id:$id){
                                                        ok    
                                                  }
                                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('crm_source_list')

    query = """

                        query($id:Int!){
                              masterCrmSourceById(id:$id){
                                id
                                source
                              }
                            }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterCrmSourceById']

    return render(request, path.join('master', 'delete_crm_source.html'), {"data": data})
  else:
    return redirect('login')

# =====Crm Status===========
def crm_status_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('search')
        request.session['masterCrmStatusSearch']=search
        request.session['masterCrmStatusOffset']=0
        request.session['masterCrmStatusCurrentPage']=1

    if request.POST.get("masterCrmStatusSize")==None:
       pass
    else:
       request.session['masterCrmStatusSize']=int(request.POST.get('masterCrmStatusSize'))
       request.session['masterCrmStatusFirst']=request.session['masterCrmStatusSize']
       request.session['masterCrmStatusOffset']=0
       request.session['masterCrmStatusCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterCrmStatusSearch']=None
      request.session['masterCrmStatusSize']=None
      request.session['masterCrmStatusFirst']=10 #defalt Value 
    
    query2 = """

              query($search:String){
                  masterCrmStatus(status_Icontains:$search){
                  edges{
                    node{

                    
                  id
               
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterCrmStatusSearch']})['data']['masterCrmStatus']['edges']
    total_records = len(td)
    if total_records%request.session['masterCrmStatusFirst']==0:
      total_pages = total_records//request.session['masterCrmStatusFirst']
    else:
      total_pages = total_records//request.session['masterCrmStatusFirst']+1
    
    query = """

              query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterCrmStatus(offset:$offset,first:$first,last:$last,status_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                  id
                  status
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterCrmStatusOffset']=0
      request.session['masterCrmStatusCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterCrmStatusOffset']-=request.session['masterCrmStatusFirst']
      request.session['masterCrmStatusCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterCrmStatusOffset']+=request.session['masterCrmStatusFirst']
      request.session['masterCrmStatusCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterCrmStatusCurrentPage']=total_pages
      if total_records%int(request.session['masterCrmStatusFirst'])==0:
        last=total_records%int(request.session['masterCrmStatusFirst'])+int(request.session['masterCrmStatusFirst'])
        request.session['masterCrmStatusOffset']=total_records-int(request.session['masterCrmStatusFirst'])
      else:
        last=total_records%int(request.session['masterCrmStatusFirst'])
        request.session['masterCrmStatusOffset']=total_records-total_records%int(request.session['masterCrmStatusFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterCrmStatusSorting']=='id':
        request.session['masterCrmStatusSorting']='-id'
      else:
        request.session['masterCrmStatusSorting']='id'

    if request.POST.get('status')=="Status":
      if request.session['masterCrmStatusSorting']=='status':
        request.session['masterCrmStatusSorting']='-status'
      else:
        request.session['masterCrmStatusSorting']='status'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterCrmStatusOffset'],
      "first":request.session['masterCrmStatusFirst'],
      "search":request.session['masterCrmStatusSearch'],
      "last":last,
      "orderBy":request.session['masterCrmStatusSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterCrmStatus']['edges']

    max_d="""
            query{
                 masterCrmStatusCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterCrmStatusCount']

    return render(request, path.join('master', 'crm_status_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row": max_row})
  else:
    return redirect('login')

def create_crm_status(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        status = request.POST.get('status').title()

         

        query = """

           mutation($status: String){
              createMasterCrmStatus(status:$status){
                ok
              }
            }

        """

        variables = {"status": status}

        client.execute(query=query, variables=variables)

        return redirect('crm_status_list')
    return render(request, path.join('master', 'create_crm_status.html'))
  else:
    return redirect('login')

def update_crm_status(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterCrmStatusById(id:$id){
                            id
                            status
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterCrmStatusById']

    if request.method == 'POST':
        status = request.POST.get('status')

        q = """mutation($id:ID!, $status:String){
                          updateMasterCrmStatus(id:$id, status:$status){
                            ok
                          }
                        }"""

        v = {"id": id, "status": status}

        client.execute(query=q, variables=v)
        return redirect("crm_status_list")

    return render(request, path.join('master', 'update_crm_status.html'), {"data": data})
  else:
    return redirect('login')

def delete_crm_status(request, id):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                                  deleteMasterCrmStatus(id:$id){
                                                        ok    
                                                  }
                                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('crm_status_list')

    query = """

                        query($id:Int!){
                              masterCrmStatusById(id:$id){
                                id
                                status
                              }
                            }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterCrmStatusById']

    return render(request, path.join('master', 'delete_crm_status.html'), {"data": data})
  else:
    return redirect('login')

# ====User Type=====

def user_type_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterUserType')
        request.session['masterUserTypeSearch']=search
        request.session['masterUserTypeOffset']=0
        request.session['masterUserTypeCurrentPage']=1

    if request.POST.get("masterUserTypeSize")==None:
       pass
    else:
       request.session['masterUserTypeSize']=int(request.POST.get('masterUserTypeSize'))
       request.session['masterUserTypeFirst']=request.session['masterUserTypeSize']
       request.session['masterUserTypeOffset']=0
       request.session['masterUserTypeCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterUserTypeSearch']=None
      request.session['masterUserTypeSize']=None
      request.session['masterUserTypeFirst']=10 #defalt Value 
    
    query2 = """

              query($search:String){
                  masterUserTypes(type_Icontains:$search){
                  edges{
                    node{
                  type
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterUserTypeSearch']})['data']['masterUserTypes']['edges']
    total_records = len(td)
    if total_records%request.session['masterUserTypeFirst']==0:
      total_pages = total_records//request.session['masterUserTypeFirst']
    else:
      total_pages = total_records//request.session['masterUserTypeFirst']+1
    
    query = """

             query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterUserTypes(offset:$offset,first:$first,last:$last,type_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                
                  code
                  isEdit
                  type
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterUserTypeOffset']=0
      request.session['masterUserTypeCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterUserTypeOffset']-=request.session['masterUserTypeFirst']
      request.session['masterUserTypeCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterUserTypeOffset']+=request.session['masterUserTypeFirst']
      request.session['masterUserTypeCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterUserTypeCurrentPage']=total_pages
      if total_records%int(request.session['masterUserTypeFirst'])==0:
        last=total_records%int(request.session['masterUserTypeFirst'])+int(request.session['masterUserTypeFirst'])
        request.session['masterUserTypeOffset']=total_records-int(request.session['masterUserTypeFirst'])
      else:
        last=total_records%int(request.session['masterUserTypeFirst'])
        request.session['masterUserTypeOffset']=total_records-total_records%int(request.session['masterUserTypeFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('type')=="Type":
      if request.session['masterUserTypeSorting']=='type':
        request.session['masterUserTypeSorting']='-type'
      else:
        request.session['masterUserTypeSorting']='type'

    if request.POST.get('code')=="Code":
      if request.session['masterUserTypeSorting']=='code':
        request.session['masterUserTypeSorting']='-code'
      else:
        request.session['masterUserTypeSorting']='code'

    if request.POST.get('is_edit')=="Is Edit":
      if request.session['masterUserTypeSorting']=='is_edit':
        request.session['masterUserTypeSorting']='-is_edit'
      else:
        request.session['masterUserTypeSorting']='is_edit'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterUserTypeOffset'],
      "first":request.session['masterUserTypeFirst'],
      "search":request.session['masterUserTypeSearch'],
      "last":last,
      "orderBy":request.session['masterUserTypeSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterUserTypes']['edges']
    max_d="""
            query{
                masterUserTypeCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterUserTypeCount']
    
    return render(request, path.join('master', 'user_type_list.html'),{"entity_list": entity_list,"max_page":total_pages,'max_row':max_row})
  else:
    return redirect('login')

def create_user_type(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        type = request.POST.get('type').title()
        code = request.POST.get('code').upper()
        isedit = request.POST.get('isedit')

         

        query = """

           mutation($type: String, $code: String, $isedit:String){
              createMasterUserType(type: $type, code:$code, isEdit:$isedit){
                ok
              }
            }

        """

        variables = {"type": type, "code": code, "isedit": isedit}

        client.execute(query=query, variables=variables)

        return redirect('user_type_list')
    return render(request, path.join('master', 'create_user_type.html'))
  else:
    return redirect('login')

def update_user_type(request, id):
  if len(dict(request.session))>0:   
    query = """
                            query($code:String!){
                              masterUserTypeByCode(code:$code){
                                type
                                code
                                isEdit
                              }
                            }
                                """
    varss = {"code": id}
    data = client.execute(query=query, variables=varss)['data']['masterUserTypeByCode']

    if request.method == 'POST':
        type = request.POST.get('type').title()
        code = request.POST.get('code').upper()
        isedit = request.POST.get('isedit')

        q = """mutation($cc:String, $code:String, $isedit:String, $type:String){
              updateMasterUserType(currentCode:$cc, code:$code, isEdit:$isedit, type:$type){
                ok
              }
            }"""

        v = {"cc": id, "type": type, "code": code, "isedit": isedit}

        client.execute(query=q, variables=v)
        return redirect("user_type_list")

    return render(request, path.join('master', 'update_user_type.html'), {"data": data})
  else:
    return redirect('login')

def delete_user_type(request, id):
  if len(dict(request.session))>0:   

    if request.method == 'POST':
        mutation = """mutation($code:String){
                                                      deleteMasterUserType(code:$code){
                                                            ok    
                                                      }
                                                    }"""
        mvars = {
            "code": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('user_type_list')

    query = """

                            query($code:String!){
                                  masterUserTypeByCode(code:$code){
                                    type
                                  }
                                }"""

    variables = {
        "code": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterUserTypeByCode']

    return render(request, path.join('master', 'delete_user_type.html'), {"data": data})
  else:
    return redirect('login')

def product_measure_unit_list(request):
  if len(dict(request.session))>0:   
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterMeasureUnit')
        request.session['masterMeasureUnitSearch']=search
        request.session['masterMeasureUnitOffset']=0
        request.session['masterMeasureUnitCurrentPage']=1

    if request.POST.get("masterMeasureUnitSize")==None:
       pass
    else:
       request.session['masterMeasureUnitSize']=int(request.POST.get('masterMeasureUnitSize'))
       request.session['masterMeasureUnitFirst']=request.session['masterMeasureUnitSize']
       request.session['masterMeasureUnitOffset']=0
       request.session['masterMeasureUnitCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterMeasureUnitSearch']=None
      request.session['masterMeasureUnitSize']=None
      request.session['masterMeasureUnitFirst']=10 #defalt Value 
    
    query2 = """

               query($search:String){
                  masterProductMeasureUnit(unit_Icontains:$search){
                  edges{
                    node{

                    
                  id
                
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterMeasureUnitSearch']})['data']['masterProductMeasureUnit']['edges']
    total_records = len(td)
    if total_records%request.session['masterMeasureUnitFirst']==0:
      total_pages = total_records//request.session['masterMeasureUnitFirst']
    else:
      total_pages = total_records//request.session['masterMeasureUnitFirst']+1
    
    query = """

              query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterProductMeasureUnit(offset:$offset,first:$first,last:$last,unit_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                  id
                 unit
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterMeasureUnitOffset']=0
      request.session['masterMeasureUnitCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterMeasureUnitOffset']-=request.session['masterMeasureUnitFirst']
      request.session['masterMeasureUnitCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterMeasureUnitOffset']+=request.session['masterMeasureUnitFirst']
      request.session['masterMeasureUnitCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterMeasureUnitCurrentPage']=total_pages
      if total_records%int(request.session['masterMeasureUnitFirst'])==0:
        last=total_records%int(request.session['masterMeasureUnitFirst'])+int(request.session['masterMeasureUnitFirst'])
        request.session['masterMeasureUnitOffset']=total_records-int(request.session['masterMeasureUnitFirst'])
      else:
        last=total_records%int(request.session['masterMeasureUnitFirst'])
        request.session['masterMeasureUnitOffset']=total_records-total_records%int(request.session['masterMeasureUnitFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterMeasureUnitSorting']=='id':
        request.session['masterMeasureUnitSorting']='-id'
      else:
        request.session['masterMeasureUnitSorting']='id'

    if request.POST.get('measureunit')=="Measure Unit":
      if request.session['masterMeasureUnitSorting']=='unit':
        request.session['masterMeasureUnitSorting']='-unit'
      else:
        request.session['masterMeasureUnitSorting']='unit'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterMeasureUnitOffset'],
      "first":request.session['masterMeasureUnitFirst'],
      "search":request.session['masterMeasureUnitSearch'],
      "last":last,
      "orderBy":request.session['masterMeasureUnitSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterProductMeasureUnit']['edges']
    max_d="""
            query{
                masterProductMeasureUnitCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterProductMeasureUnitCount']
    

    return render(request, path.join('master', 'product_measure_unit_list.html'), {"entity_list": entity_list,"max_page":total_pages,'max_row':max_row})
  else:
    return redirect('login')

def create_product_measure_unit(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        unit = request.POST.get('measureunit').title()

         

        query = """

     mutation($unit: String){
        createMasterProductMeasureUnit(unit: $unit){
          ok
        }
      }

      """

        variables = {"unit": unit}

        client.execute(query=query, variables=variables)

        return redirect('product_measure_unit_list')
    return render(request, path.join('master', 'create_product_measure_unit.html'))
  else:
    return redirect('login')

def update_product_measure_unit(request, id):
  if len(dict(request.session))>0:   
    query = """
                          query($id:Int!){
                            masterProductMeasureUnitById(id:$id){
                              id
                              unit
                            }
                          }
                              """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductMeasureUnitById']

    if request.method == 'POST':
        unit = request.POST.get('measureunit').title()

        q = """mutation($id:ID!, $unit:String){
                            updateMasterProductMeasureUnit(id:$id, unit:$unit){
                              ok
                            }
                          }"""

        v = {"id": id, "unit": unit}

        client.execute(query=q, variables=v)
        return redirect("product_measure_unit_list")

    return render(request, path.join('master', 'update_product_measure_unit.html'), {"data": data})
  else:
    return redirect('login')

def delete_product_measure_unit(request, id):
  if len(dict(request.session))>0:   

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                            deleteMasterProductMeasureUnit(id:$id){
                                  ok    
                            }
                          }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_measure_unit_list')

    query = """

              query($id:Int!){
                    masterProductMeasureUnitById(id:$id){
                      id
                      unit
                    }
                  }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductMeasureUnitById']

    return render(request, path.join('master', 'delete_product_measure_unit.html'), {"data": data})
  else:
    return redirect('login')

# ======next Step====


def product_nextstep_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        search=request.POST.get('masterNextStep')
        request.session['masterNextStepSearch']=search
        request.session['masterNextStepOffset']=0
        request.session['masterNextStepCurrentPage']=1

    if request.POST.get("masterNextStepSize")==None:
       pass
    else:
       request.session['masterNextStepSize']=int(request.POST.get('masterNextStepSize'))
       request.session['masterNextStepFirst']=request.session['masterNextStepSize']
       request.session['masterNextStepOffset']=0
       request.session['masterNextStepCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterNextStepSearch']=None
      request.session['masterNextStepSize']=None
      request.session['masterNextStepFirst']=10 #defalt Value 
    
    query2 = """

               query($search:String){
                  masterProductNextStep(nextStep_Icontains:$search){
                  edges{
                    node{

                    
                  id
                      nextStep
               
                }
              }
                }
                  }

                  """
    td=client.execute(query=query2,variables={"search": request.session['masterNextStepSearch']})['data']['masterProductNextStep']['edges']
    total_records = len(td)
    if total_records%request.session['masterNextStepFirst']==0:
      total_pages = total_records//request.session['masterNextStepFirst']
    else:
      total_pages = total_records//request.session['masterNextStepFirst']+1
    
    query = """

              query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterProductNextStep(offset:$offset,first:$first,last:$last,nextStep_Icontains:$search,orderBy:$orderBy){
                  edges{
                    node{

                    
                  id
                  nextStep
                }
              }
                }
                  }

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterNextStepOffset']=0
      request.session['masterNextStepCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterNextStepOffset']-=request.session['masterNextStepFirst']
      request.session['masterNextStepCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterNextStepOffset']+=request.session['masterNextStepFirst']
      request.session['masterNextStepCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterNextStepCurrentPage']=total_pages
      if total_records%int(request.session['masterNextStepFirst'])==0:
        last=total_records%int(request.session['masterNextStepFirst'])+int(request.session['masterNextStepFirst'])
        request.session['masterNextStepOffset']=total_records-int(request.session['masterNextStepFirst'])
      else:
        last=total_records%int(request.session['masterNextStepFirst'])
        request.session['masterNextStepOffset']=total_records-total_records%int(request.session['masterNextStepFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterNextStepSorting']=='id':
        request.session['masterNextStepSorting']='-id'
      else:
        request.session['masterNextStepSorting']='id'

    if request.POST.get('nextstep')=="Next Step":
      if request.session['masterNextStepSorting']=='nextStep':
        request.session['masterNextStepSorting']='-nextStep'
      else:
        request.session['masterNextStepSorting']='nextStep'
    #========End Sorting=========#
    v2={
      "offset":request.session['masterNextStepOffset'],
      "first":request.session['masterNextStepFirst'],
      "search":request.session['masterNextStepSearch'],
      "last":last,
      "orderBy":request.session['masterNextStepSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['masterProductNextStep']['edges']
    max_d="""
            query{
                 masterProductNextStepCount
              }
        """
    max_row=client.execute(query=max_d)['data']['masterProductNextStepCount']
    return render(request, path.join('master', 'product_nextstep_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row":max_row})
  else:
    return redirect('login')

def create_product_nextstep(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        nextstep = request.POST.get('nextstep').title()

         

        query = """

   mutation($nextStep: String){
      createMasterProductNextStep(nextStep: $nextStep){
        ok
      }
    }

    """

        variables = {"nextStep": nextstep}

        client.execute(query=query, variables=variables)

        return redirect('product_nextstep_list')
    return render(request, path.join('master', 'create_product_nextstep.html'))
  else:
    return redirect('login')

def update_product_nextstep(request, id):
  if len(dict(request.session))>0:   
    query = """
                        query($id:Int!){
                          masterProductNextStepById(id:$id){
                            id
                            nextStep
                          }
                        }
                            """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterProductNextStepById']

    if request.method == 'POST':
        nextstep = request.POST.get('nextstep').title()

        q = """mutation($id:ID!, $nextstep:String){
                          updateMasterProductNextStep(id:$id, nextStep:$nextstep){
                            ok
                          }
                        }"""

        v = {"id": id, "nextstep": nextstep}

        client.execute(query=q, variables=v)
        return redirect("product_nextstep_list")

    return render(request, path.join('master', 'update_product_nextstep.html'), {"data": data})
  else:
    return redirect('login')

def delete_product_nextstep(request, id):
  if len(dict(request.session))>0:  

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                            deleteMasterProductNextStep(id:$id){
                                  ok    
                            }
                          }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_nextstep_list')

    query = """

              query($id:Int!){
                    masterProductNextStepById(id:$id){
                      id
                      nextStep
                    }
                  }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductNextStepById']

    return render(request, path.join('master', 'delete_product_nextstep.html'), {"data": data})
  else:
    return redirect('login')


# ======Salutation=======
def salutation_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        masterSalutation=request.POST.get('masterSalutation')
        request.session['masterSalutationSearch']=masterSalutation
        request.session['masterSalutationOffset']=0
        request.session['masterSalutationCurrentPage']=1

    if request.POST.get("masterSalutationSize")==None:
       pass
    else:
       request.session['masterSalutationSize']=int(request.POST.get('masterSalutationSize'))
       request.session['masterSalutationFirst']=request.session['masterSalutationSize']
       request.session['masterSalutationOffset']=0
       request.session['masterSalutationCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterSalutationSearch']=None
      request.session['masterSalutationSize']=None
      request.session['masterSalutationFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    masterSalutation(salutation_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['masterSalutationSearch']})['data']['masterSalutation']['edges']
    total_records = len(td)
    if total_records%request.session['masterSalutationFirst']==0:
      total_pages = total_records//request.session['masterSalutationFirst']
    else:
      total_pages = total_records//request.session['masterSalutationFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterSalutation(offset:$offset,first:$first,last:$last,salutation_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                    salutation
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterSalutationOffset']=0
      request.session['masterSalutationCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterSalutationOffset']-=request.session['masterSalutationFirst']
      request.session['masterSalutationCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterSalutationOffset']+=request.session['masterSalutationFirst']
      request.session['masterSalutationCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterSalutationCurrentPage']=total_pages
      if total_records%int(request.session['masterSalutationFirst'])==0:
        last=total_records%int(request.session['masterSalutationFirst'])+int(request.session['masterSalutationFirst'])
        request.session['masterSalutationOffset']=total_records-int(request.session['masterSalutationFirst'])
      else:
        last=total_records%int(request.session['masterSalutationFirst'])
        request.session['masterSalutationOffset']=total_records-total_records%int(request.session['masterSalutationFirst'])
    
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterSalutationSorting']=='id':
        request.session['masterSalutationSorting']='-id'
      else:
        request.session['masterSalutationSorting']='id'

    if request.POST.get('salutation')=="Salutation":
      if request.session['masterSalutationSorting']=='salutation':
        request.session['masterSalutationSorting']='-salutation'
      else:
        request.session['masterSalutationSorting']='salutation'

    #========End Sorting=========#
    v={
      "offset":request.session['masterSalutationOffset'],
      "first":request.session['masterSalutationFirst'],
      "search":request.session['masterSalutationSearch'],
      "last":last,
      "orderBy":request.session['masterSalutationSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterSalutation']['edges']
    t_d="""
            query{
          masterSalutationCount
        }
    
    """
    max_rows=client.execute(query=t_d)['data']["masterSalutationCount"]
    return render(request, path.join('master', 'salutation_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_rows":max_rows})
  else:
    return redirect('login')
def create_salutaion(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
          salutation_name = request.POST.get('salutation_name').title()
          print("mohakdls"*20)
          print(salutation_name)

          query = """

        mutation($salutation: String){
        createSalutation(salutation:$salutation){
          ok
              }
            }

          """

          variables = {"salutation": salutation_name}

          data = client.execute(query=query, variables=variables)

          return redirect('salutation_list')
    return render(request,path.join('master','create_salutation.html'))
  else:
    return redirect('login')
def update_salutation(request, id):
  if len(dict(request.session))>0:   
    query = """
      
                    query($id:Int!){
                      masterSalutationById(id:$id)
                      {
                        id
                        salutation
                      }
                    }

                """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterSalutationById']

    if request.method == 'POST':
        salutation = request.POST.get('salutation_name').title()

        q = """mutation($id:ID, $salutation:String){
                  updateSalutation(id:$id, salutation:$salutation){
                    ok
                  }
                }"""
        v = {"id": id, "salutation": salutation}

        client.execute(query=q, variables=v)
        return redirect("salutation_list")

    return render(request, path.join('master', 'update_salutation.html'), {"data": data})
  else:
    return redirect('login')

def delete_salutation(request, id):
  if len(dict(request.session))>0:   

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                  deleteSalutation(id:$id){
                                        ok    
                                  }
                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('salutation_list')

    query = """
        
        query($id:Int!){
                      masterSalutationById(id:$id)
                      {
                        id
                        salutation
                      }
                    }
    
                """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterSalutationById']

    return render(request, path.join('master', 'delete_salutation.html'), {"data": data})
  else:
    return redirect('login')
# ====== masterDesignation =======
def designation_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        masterSalutation=request.POST.get('masterDesignation')
        request.session['masterDesignationSearch']=masterSalutation
        request.session['masterDesignationOffset']=0
        request.session['masterDesignationCurrentPage']=1

    if request.POST.get("masterDesignationSize")==None:
       pass
    else:
       request.session['masterDesignationSize']=int(request.POST.get('masterDesignationSize'))
       request.session['masterDesignationFirst']=request.session['masterDesignationSize']
       request.session['masterDesignationOffset']=0
       request.session['masterDesignationCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterDesignationSearch']=None
      request.session['masterDesignationSize']=None
      request.session['masterDesignationFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    masterDesignation(designation_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['masterDesignationSearch']})['data']['masterDesignation']['edges']
    total_records = len(td)
    if total_records%request.session['masterDesignationFirst']==0:
      total_pages = total_records//request.session['masterDesignationFirst']
    else:
      total_pages = total_records//request.session['masterDesignationFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterDesignation(offset:$offset,first:$first,last:$last,designation_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                    designation
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterDesignationOffset']=0
      request.session['masterDesignationCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterDesignationOffset']-=request.session['masterDesignationFirst']
      request.session['masterDesignationCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterDesignationOffset']+=request.session['masterDesignationFirst']
      request.session['masterDesignationCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterDesignationCurrentPage']=total_pages
      if total_records%int(request.session['masterDesignationFirst'])==0:
        last=total_records%int(request.session['masterDesignationFirst'])+int(request.session['masterDesignationFirst'])
        request.session['masterDesignationOffset']=total_records-int(request.session['masterDesignationFirst'])
      else:
        last=total_records%int(request.session['masterDesignationFirst'])
        request.session['masterDesignationOffset']=total_records-total_records%int(request.session['masterDesignationFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterDesignationSorting']=='id':
        request.session['masterDesignationSorting']='-id'
      else:
        request.session['masterDesignationSorting']='id'

    if request.POST.get('designation')=="Designation":
      if request.session['masterDesignationSorting']=='designation':
        request.session['masterDesignationSorting']='-designation'
      else:
        request.session['masterDesignationSorting']='designation'

    #========End Sorting=========#
    v={
      "offset":request.session['masterDesignationOffset'],
      "first":request.session['masterDesignationFirst'],
      "search":request.session['masterDesignationSearch'],
      "last":last,
      "orderBy":request.session['masterDesignationSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterDesignation']['edges']
    t_d="""
            query{
          masterDesignationCount
        }
    
    """
    max_rows=client.execute(query=t_d)['data']["masterDesignationCount"]
    return render(request, path.join('master', 'designation_list.html'), {"entity_list": entity_list,"max_page":total_pages,"max_rows":max_rows})
  else:
    return redirect('login')
def create_designation(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
          designation_name = request.POST.get('designation_name').title()
        

          query = """

        mutation($designation: String){
        createDesignation(designation:$designation){
          ok
              }
            }

          """

          variables = {"designation": designation_name}

          data = client.execute(query=query, variables=variables)

          return redirect('designation_list')
    return render(request,path.join('master','create_designation.html'))
  else:
    return redirect('login')
def update_designation(request, id):
  if len(dict(request.session))>0:   
    query = """
      
                    query($id:Int!){
                      masterDesignationById(id:$id)
                      {
                        id
                        designation
                      }
                    }

                """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterDesignationById']

    if request.method == 'POST':
        designation = request.POST.get('designation_name').title()

        q = """mutation($id:ID, $designation:String){
                  updateDesignation(id:$id, designation:$designation){
                    ok
                  }
                }"""
        v = {"id": id, "designation": designation}

        client.execute(query=q, variables=v)
        return redirect("designation_list")
    
    return render(request, path.join('master', 'update_designation.html'), {"data": data})
  else:
    return redirect('login')

def delete_designation(request, id):
  if len(dict(request.session))>0:   

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                  deleteDesignation(id:$id){
                                        ok    
                                  }
                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('designation_list')

    query = """
        
        query($id:Int!){
                      masterDesignationById(id:$id)
                      {
                        id
                        designation
                      }
                    }
    
                """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterDesignationById']

    return render(request, path.join('master', 'delete_designation.html'), {"data": data})
  else:
    return redirect('login')



#======== Transaction  Type======
def transaction_type_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        masterTransactionType=request.POST.get('masterTransactionType')
        request.session['masterTransactionTypeSearch']=masterTransactionType
        request.session['masterTransactionTypeOffset']=0
        request.session['masterTransactionTypeCurrentPage']=1

    if request.POST.get("masterTransactionTypeSize")==None:
       pass
    else:
       request.session['masterTransactionTypeSize']=int(request.POST.get('masterTransactionTypeSize'))
       request.session['masterTransactionTypeFirst']=request.session['masterTransactionTypeSize']
       request.session['masterTransactionTypeOffset']=0
       request.session['masterTransactionTypeCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterTransactionTypeSearch']=None
      request.session['masterTransactionTypeSize']=None
      request.session['masterTransactionTypeFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    masterTransactionType(transactionType_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['masterTransactionTypeSearch']})['data']['masterTransactionType']['edges']
    total_records = len(td)
    if total_records%request.session['masterTransactionTypeFirst']==0:
      total_pages = total_records//request.session['masterTransactionTypeFirst']
    else:
      total_pages = total_records//request.session['masterTransactionTypeFirst']+1
    query = """

                  query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterTransactionType(offset:$offset,first:$first,last:$last,transactionType_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                    transactionType
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterTransactionTypeOffset']=0
      request.session['masterTransactionTypeCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterTransactionTypeOffset']-=request.session['masterTransactionTypeFirst']
      request.session['masterTransactionTypeCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterTransactionTypeOffset']+=request.session['masterTransactionTypeFirst']
      request.session['masterTransactionTypeCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterTransactionTypeCurrentPage']=total_pages
      if total_records%int(request.session['masterTransactionTypeFirst'])==0:
        last=total_records%int(request.session['masterTransactionTypeFirst'])+int(request.session['masterTransactionTypeFirst'])
        request.session['masterTransactionTypeOffset']=total_records-int(request.session['masterTransactionTypeFirst'])
      else:
        last=total_records%int(request.session['masterTransactionTypeFirst'])
        request.session['masterTransactionTypeOffset']=total_records-total_records%int(request.session['masterTransactionTypeFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterTransactionTypeSorting']=='id':
        request.session['masterTransactionTypeSorting']='-id'
      else:
        request.session['masterTransactionTypeSorting']='id'

    if request.POST.get('transactiontype')=="Transaction Type":
      if request.session['masterTransactionTypeSorting']=='transaction_type':
        request.session['masterTransactionTypeSorting']='-transaction_type'
      else:
        request.session['masterTransactionTypeSorting']='transaction_type'

    #========End Sorting=========#
    v={
      "offset":request.session['masterTransactionTypeOffset'],
      "first":request.session['masterTransactionTypeFirst'],
      "search":request.session['masterTransactionTypeSearch'],
      "last":last,
      "orderBy":request.session['masterTransactionTypeSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterTransactionType']['edges']
    return render(request,path.join('master','transaction_type_list.html'),{"entity_list": entity_list,"max_page":total_pages,"total_records":total_records})
  else:
    return redirect('login')

def create_transaction_type(request):
  if len(dict(request.session))>0:
    if request.method=="POST":
      transactiontype=request.POST.get('transactiontype').title()
      query="""
          mutation($transactionType:String){
          createMasterTransactionType(transactionType:$transactionType){
                      ok
            }
          }
      """
      v={
        "transactionType":transactiontype
      }
      client.execute(query=query,variables=v)
      return redirect("transaction_type_list")
    return render(request,path.join('master','create_transaction_type.html'))
  else:
    return redirect('login')

def update_transaction_type(request,id):
  if len(dict(request.session))>0:
    query = """
      
                    query($id:Int!){
                      masterTransactionTypeById(id:$id)
                      {
                        id
                       transactionType
                      }
                    }

                """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterTransactionTypeById']
    if request.method == 'POST':
        transactiontype = request.POST.get('transactiontype').title()

        q = """mutation($id:ID, $transactionType:String){
                  updateMasterTransactionType(id:$id, transactionType:$transactionType){
                    ok
                  }
                }"""
        v = {"id": id, "transactionType": transactiontype}

        client.execute(query=q, variables=v)
        return redirect("transaction_type_list")


    

   
    
    return render(request,path.join('master','update_transaction_type.html'),{"data":data})
  else:
    return redirect('login')
def delete_transaction_type(request,id):
  if len(dict(request.session))>0:
    if request.method == 'POST':
      mutation = """mutation($id:ID){
                                    deleteMasterTransactionType(id:$id){
                                          ok    
                                    }
                                  }"""
      mvars = {
          "id": id
      }
      client.execute(query=mutation, variables=mvars)
      return redirect('transaction_type_list')

    query = """
          
    query($id:Int!){
                    masterTransactionTypeById(id:$id)
                    {
                      id
                      transactionType
                    }
                  }

              """

    variables = {
          "id": id
      }
    data = client.execute(query=query, variables=variables)['data']['masterTransactionTypeById']
    return render(request,path.join('master','delete_transaction_type.html'),{'data':data})
  else:
    return redirect('login')
#======= End Transaction Type====
#=========Transaction Status========
def transaction_status_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        masterTransactionStatus=request.POST.get('masterTransactionStatus')
        request.session['masterTransactionStatusSearch']=masterTransactionStatus
        request.session['masterTransactionStatusOffset']=0
        request.session['masterTransactionStatusCurrentPage']=1

    if request.POST.get("masterTransactionStatusSize")==None:
       pass
    else:
       request.session['masterTransactionStatusSize']=int(request.POST.get('masterTransactionStatusSize'))
       request.session['masterTransactionStatusFirst']=request.session['masterTransactionStatusSize']
       request.session['masterTransactionStatusOffset']=0
       request.session['masterTransactionStatusCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterTransactionStatusSearch']=None
      request.session['masterTransactionStatusSize']=None
      request.session['masterTransactionStatusFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    masterTransactionStatus(transactionStatus_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['masterTransactionStatusSearch']})['data']['masterTransactionStatus']['edges']
    total_records = len(td)
    if total_records%request.session['masterTransactionStatusFirst']==0:
      total_pages = total_records//request.session['masterTransactionStatusFirst']
    else:
      total_pages = total_records//request.session['masterTransactionStatusFirst']+1
    query = """

                  query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterTransactionStatus(offset:$offset,first:$first,last:$last,transactionStatus_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                    transactionStatus
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterTransactionStatusOffset']=0
      request.session['masterTransactionStatusCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterTransactionStatusOffset']-=request.session['masterTransactionStatusFirst']
      request.session['masterTransactionStatusCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterTransactionStatusOffset']+=request.session['masterTransactionStatusFirst']
      request.session['masterTransactionStatusCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterTransactionStatusCurrentPage']=total_pages
      if total_records%int(request.session['masterTransactionStatusFirst'])==0:
        last=total_records%int(request.session['masterTransactionStatusFirst'])+int(request.session['masterTransactionStatusFirst'])
        request.session['masterTransactionStatusOffset']=total_records-int(request.session['masterTransactionStatusFirst'])
      else:
        last=total_records%int(request.session['masterTransactionStatusFirst'])
        request.session['masterTransactionStatusOffset']=total_records-total_records%int(request.session['masterTransactionStatusFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterTransactionStatusSorting']=='id':
        request.session['masterTransactionStatusSorting']='-id'
      else:
        request.session['masterTransactionStatusSorting']='id'

    if request.POST.get('transactionstatus')=="Transaction Status":
      if request.session['masterTransactionStatusSorting']=='transaction_status':
        request.session['masterTransactionStatusSorting']='-transaction_status'
      else:
        request.session['masterTransactionStatusSorting']='transaction_status'

    #========End Sorting=========#
    v={
      "offset":request.session['masterTransactionStatusOffset'],
      "first":request.session['masterTransactionStatusFirst'],
      "search":request.session['masterTransactionStatusSearch'],
      "last":last,
      "orderBy":request.session['masterTransactionStatusSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterTransactionStatus']['edges']
    total_data="""
                    query{
              masterTransactionStatusCount
              
              
            }
    
    """
    total_records=client.execute(query=total_data)['data']['masterTransactionStatusCount']
    
    return render(request,path.join('master','transaction_status_list.html'),{"entity_list": entity_list,"max_page":total_pages,"total_records":total_records})
  else:
    return redirect('login')
def create_transaction_status(request):
  if len(dict(request.session))>0:
    if request.method=="POST":
      transactionstatus=request.POST.get('transactionstatus').title()
      query="""
          mutation($transactioinStatus:String){
        createMasterTransactionStatus(transactionStatus:$transactioinStatus){

            ok
            
            }
              }
      
      """
      v={
        "transactioinStatus":transactionstatus
      }
      client.execute(query=query,variables=v)
      return redirect("transaction_status_list")
    return render(request,path.join('master','create_transaction_status.html'))
  else:
    return redirect('login')
def update_transaction_status(request,id):
  if len(dict(request.session))>0:
    query = """
      
                    query($id:Int!){
                      masterTransactionStatusById(id:$id)
                      {
                        id
                        transactionStatus
                      }
                    }

                """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterTransactionStatusById']

    if request.method == 'POST':
        transactionstatus = request.POST.get('transactionstatus').title()

        q = """mutation($id:ID, $transactionStatus:String){
                  updateMasterTransactionStatus(id:$id, transactionStatus:$transactionStatus){
                    ok
                  }
                }"""
        v = {"id": id, "transactionStatus": transactionstatus}

        client.execute(query=q, variables=v)
        return redirect("transaction_status_list")
    return render(request,path.join('master','update_transaction_status.html'),{'data':data})
  else:
    return redirect('login')
def delete_transaction_status(request,id):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                  deleteMasterTransactionStatus(id:$id){
                                        ok    
                                  }
                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('transaction_status_list')

    query = """
        
        query($id:Int!){
                      masterTransactionStatusById(id:$id)
                      {
                        id
                        transactionStatus
                      }
                    }
    
                """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterTransactionStatusById']
    return render(request,path.join('master','delete_transaction_status.html'),{'data':data})
  else:
    return redirect('login')
#=========End Transaction Status

#=========Transaction Doctype=====
def transaction_doctype_list(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        masterTransactionDoctype=request.POST.get('masterTransactionDoctype')
        request.session['masterTransactionDoctypeSearch']=masterTransactionDoctype
        request.session['masterTransactionDoctypeOffset']=0
        request.session['masterTransactionDoctypeCurrentPage']=1

    if request.POST.get("masterTransactionDoctypeSize")==None:
       pass
    else:
       request.session['masterTransactionDoctypeSize']=int(request.POST.get('masterTransactionDoctypeSize'))
       request.session['masterTransactionDoctypeFirst']=request.session['masterTransactionDoctypeSize']
       request.session['masterTransactionDoctypeOffset']=0
       request.session['masterTransactionDoctypeCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterTransactionDoctypeSearch']=None
      request.session['masterTransactionDoctypeSize']=None
      request.session['masterTransactionDoctypeFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    masterDoctype(transactionDoctype_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['masterTransactionDoctypeSearch']})['data']['masterDoctype']['edges']
    total_records = len(td)
    if total_records%request.session['masterTransactionDoctypeFirst']==0:
      total_pages = total_records//request.session['masterTransactionDoctypeFirst']
    else:
      total_pages = total_records//request.session['masterTransactionDoctypeFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterDoctype(offset:$offset,first:$first,last:$last,transactionDoctype_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                    transactionDoctype
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterTransactionDoctypeOffset']=0
      request.session['masterTransactionDoctypeCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterTransactionDoctypeOffset']-=request.session['masterTransactionDoctypeFirst']
      request.session['masterTransactionDoctypeCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterTransactionDoctypeOffset']+=request.session['masterTransactionDoctypeFirst']
      request.session['masterTransactionDoctypeCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterTransactionDoctypeCurrentPage']=total_pages
      if total_records%int(request.session['masterTransactionDoctypeFirst'])==0:
        last=total_records%int(request.session['masterTransactionDoctypeFirst'])+int(request.session['masterTransactionDoctypeFirst'])
        request.session['masterTransactionDoctypeOffset']=total_records-int(request.session['masterTransactionDoctypeFirst'])
      else:
        last=total_records%int(request.session['masterTransactionDoctypeFirst'])
        request.session['masterTransactionDoctypeOffset']=total_records-total_records%int(request.session['masterTransactionDoctypeFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('id')=="Id":
      if request.session['masterTransactionDoctypeSorting']=='id':
        request.session['masterTransactionDoctypeSorting']='-id'
      else:
        request.session['masterTransactionDoctypeSorting']='id'

    if request.POST.get('transactiondoctype')=="Transaction Doctype":
      if request.session['masterTransactionDoctypeSorting']=='transaction_doctype':
        request.session['masterTransactionDoctypeSorting']='-transaction_doctype'
      else:
        request.session['masterTransactionDoctypeSorting']='transaction_doctype'

    #========End Sorting=========#
    v={
      "offset":request.session['masterTransactionDoctypeOffset'],
      "first":request.session['masterTransactionDoctypeFirst'],
      "search":request.session['masterTransactionDoctypeSearch'],
      "last":last,
      "orderBy":request.session['masterTransactionDoctypeSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterDoctype']['edges']
    total_data="""
                  query{
              masterDoctypeCount
    
      
    }
    """
    total_records=client.execute(query=total_data)['data']['masterDoctypeCount']
    
    return render(request,path.join('master','transaction_doctype_list.htm'),{"entity_list": entity_list,"max_page":total_pages,"total_records":total_records})
  else:
    return redirect('login')

def create_transaction_doctype(request):
  if len(dict(request.session))>0:
    if request.method=="POST":
      transactiondoctype=request.POST.get('transactiondoctype').title()
      query="""
        mutation($transactionDoctype:String){
        createMasterDoctype(transactionDoctype:$transactionDoctype){
            ok
            
            }
              }
      """
      v={
        "transactionDoctype":transactiondoctype
      }
      client.execute(query=query,variables=v)
      return redirect("transaction_doctype_list")
    return render(request,path.join('master','create_transaction_doctype.html'))
  else:
    return redirect('login')
def update_transaction_doctype(request ,id):
  if len(dict(request.session))>0:
    query = """
      
                    
                    query($id:Int!){
                      masterDoctypeById(id:$id)
                      {
                        id
                        transactionDoctype
                      }
                    }

                """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['masterDoctypeById']

    if request.method == 'POST':
        doctype = request.POST.get('transactiondoctype').title()

        q = """mutation($id:ID, $transactionDoctype:String){
                  updateMasterDoctype(id:$id, transactionDoctype:$transactionDoctype){
                    ok
                  }
                }"""
        v = {"id": id, "transactionDoctype": doctype}

        client.execute(query=q, variables=v)
        return redirect("transaction_doctype_list")

    return render(request,path.join('master','update_transaction_doctype.html'),{'data': data})
  else:
    return redirect('login')
def delete_transaction_doctype(request ,id):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                  deleteMasterDoctype(id:$id){
                                        ok    
                                  }
                                }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('transaction_doctype_list')

    query = """
        
        query($id:Int!){
                      masterDoctypeById(id:$id)
                      {
                        id
                        transactionDoctype
                      }
                    }
    
                """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterDoctypeById']
    return render(request,path.join('master','delete_transaction_doctype.html'),{'data' : data})
  else:
    return redirect('login')
#=========End Transaction Doctype===


#==========PaymentTerms=========
def paymentterms_list(request):
  if len(dict(request.session))>0:
    #======Filter ==========
    if request.POST.get("masterPaymentTermsTerm")==None:
      pass
    else:
      masterPaymentTerms=request.POST.get('masterPaymentTermsTerm')
      request.session['masterPaymentTermsTerm']=masterPaymentTerms
      request.session['masterPaymentTermsOffset']=0
      request.session['masterPaymentTermsCurrentPage']=1


    if request.POST.get("masterPaymentCreaditDays")==None:
      pass
    else:
      masterPaymentCreaditDays=int(request.POST.get('masterPaymentCreaditDays'))
      request.session['masterPaymentTermsCreditDays']=masterPaymentCreaditDays
      request.session['masterPaymentTermsOffset']=0
      request.session['masterPaymentTermsCurrentPage']=1

    if request.POST.get("masterPaymentPercent")==None:
      pass
    else:
      masterPaymentPercent=request.POST.get('masterPaymentPercent')
      request.session['masterPaymentTermsPaymentPercent']=masterPaymentPercent
      request.session['masterPaymentTermsOffset']=0
      request.session['masterPaymentTermsCurrentPage']=1

     #====== End Filter ==========


    if request.POST.get("masterPaymentTermsSize")==None:
       pass
    else:
       request.session['masterPaymentTermsSize']=int(request.POST.get('masterPaymentTermsSize'))
       request.session['masterPaymentTermsFirst']=request.session['masterPaymentTermsSize']
       request.session['masterPaymentTermsOffset']=0
       request.session['masterPaymentTermsCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterPaymentTermsTerm']=None
      request.session['masterPaymentTermsSize']=None
      request.session['masterPaymentTermsCreditDays']=None
      request.session['masterPaymentTermsPaymentPercent']=None
      request.session['masterPaymentTermsSorting']=None
      request.session['masterPaymentTermsFirst']=10 #defalt Value 
    query2="""
                query($terms:String,$creditDays:Int,$paymentPercent:Int){
                    masterPaymentTerms(term:$terms,creditDays:$creditDays,paymentPercent:$paymentPercent){
                      edges{
                        node{
                          id
                          term
                          creditDays
                          paymentPercent
                        }
                      }
                    }
                  }
     
    """
    m={
       "terms": request.session['masterPaymentTermsTerm'],
       "creditDays":request.session['masterPaymentTermsCreditDays'],
       "paymentPercent":request.session['masterPaymentTermsPaymentPercent']
    }
    td=client.execute(query=query2,variables=m)['data']['masterPaymentTerms']['edges']
    total_records = len(td)
    if total_records%request.session['masterPaymentTermsFirst']==0:
      total_pages = total_records//request.session['masterPaymentTermsFirst']
    else:
      total_pages = total_records//request.session['masterPaymentTermsFirst']+1
    query = """

                query($offset:Int,$first:Int,$last:Int,$terms:String,$creditDays:Int,$paymentPercent:Int,$orderBy:String){
                masterPaymentTerms(offset:$offset,first:$first,last:$last,orderBy:$orderBy,term:$terms,creditDays:$creditDays,paymentPercent:$paymentPercent){
                  edges{
                    node{
                      id
                      term
                      creditDays
                      paymentPercent
                      
                    }
                  }
                }
              }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterPaymentTermsOffset']=0
      request.session['masterPaymentTermsCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterPaymentTermsOffset']-=request.session['masterPaymentTermsFirst']
      request.session['masterPaymentTermsCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterPaymentTermsOffset']+=request.session['masterPaymentTermsFirst']
      request.session['masterPaymentTermsCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterPaymentTermsCurrentPage']=total_pages
      if total_records%int(request.session['masterPaymentTermsFirst'])==0:
        last=total_records%int(request.session['masterPaymentTermsFirst'])+int(request.session['masterPaymentTermsFirst'])
        request.session['masterPaymentTermsOffset']=total_records-int(request.session['masterPaymentTermsFirst'])
      else:
        last=total_records%int(request.session['masterPaymentTermsFirst'])
        request.session['masterPaymentTermsOffset']=total_records-total_records%int(request.session['masterPaymentTermsFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('term')=="Term":
    
      if request.session['masterPaymentTermsSorting']=='term':
        request.session['masterPaymentTermsSorting']='-term'
      else:
        request.session['masterPaymentTermsSorting']='term'

    if request.POST.get('creditday')=="Credit Day":
     
      if request.session['masterPaymentTermsSorting']=='credit_days':
        request.session['masterPaymentTermsSorting']='-credit_days'
      else:
        request.session['masterPaymentTermsSorting']='credit_days'

    if request.POST.get('paymentparcent')=="Payment Parcent":
    
      if request.session['masterPaymentTermsSorting']=='payment_percent':
        request.session['masterPaymentTermsSorting']='-payment_percent'
      else:
        request.session['masterPaymentTermsSorting']='payment_percent'


    #========End Sorting=========#
    v={
      "offset":request.session['masterPaymentTermsOffset'],
      "first":request.session['masterPaymentTermsFirst'],
      "terms":request.session['masterPaymentTermsTerm'],
      "creditDays":request.session['masterPaymentTermsCreditDays'],
      "paymentPercent":request.session['masterPaymentTermsPaymentPercent'],
      "last":last,
      "orderBy":request.session['masterPaymentTermsSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterPaymentTerms']['edges']
    total_data="""
                  query{
              masterPaymentTermsCount
    
    
      
    }
    """
    total_records=client.execute(query=total_data)['data']['masterPaymentTermsCount']
    #==========Filter Data Fatch=========
    query_term="""
              query{
              masterPaymentTerms{
                edges{
                  node{
                    term
                   
                     creditDays
                     paymentPercent
                  }
                }
              }
            }
    """
    
    term_data=client.execute(query=query_term)['data']['masterPaymentTerms']['edges']
    #==========End Filter Data Fatch====
    return render(request,path.join('master','paymentterms_list.html'),{"entity_list": entity_list,"max_page":total_pages,"total_records":total_records,"data_term":term_data})
  else:
    return redirect('login')
def create_paymentterms(request):
  if len(dict(request.session))>0:
    if request.method=="POST":
      term=request.POST.get("term")
      creditdays=int(request.POST.get("creditdays"))
      paymentpercent=int(request.POST.get("paymentpercent"))
      term=str(term) +" Days"
      query="""
        mutation($terms:String,$creditdays:Int,$paymentpercent:Int){
          createMasterPaymentTerms(term:$terms,creditDays:$creditdays,paymentPercent:$paymentpercent){
                      ok
            }
          }
      """
      v={
        "terms":term,
        "creditdays":creditdays,
        "paymentpercent":paymentpercent
      }
      client.execute(query=query,variables=v)
      return redirect("paymentterms_list")
    return render(request,path.join('master','create_paymentterms.html'))
  else:
    return redirect('login')
def update_paymentterms(request,id):
  if len(dict(request.session))>0:
    if request.method == 'POST':
      term=request.POST.get("term")
      creditdays=request.POST.get("creditdays")
      paymentpercent=request.POST.get("paymentpercent")
      q = """mutation($id:ID, $term:String,$creditdays:Int,$paymentpercent:Int){
                    updateMasterPaymentTerms(id:$id, term:$term,creditDays:$creditdays,paymentPercent:$paymentpercent){
                      ok
                    }
                  }"""
      v = {"id": id, "term": term,"creditdays":creditdays,"paymentpercent":paymentpercent}

      client.execute(query=q, variables=v)
      return redirect("paymentterms_list")
    query = """
          
          query($id:Int!){
                        masterPaymentTermsById(id:$id)
                        {
                          id
                          term
                          creditDays
                          paymentPercent
                        }
                      }
      
      
                  """

    variables = {
          "id": id
      }
    data = client.execute(query=query, variables=variables)['data']['masterPaymentTermsById']
    return render(request,path.join('master','update_paymentterms.html'),{"data":data})
  else:
    return redirect('login')
def delete_paymentterms(request,id):
  if len(dict(request.session))>0:
    if request.method == 'POST':
          mutation = """mutation($id:ID){
                        deleteMasterPaymentTerms(id:$id){
                              ok    
                        }
                      }"""
          mvars = {
              "id": id
          }
          client.execute(query=mutation, variables=mvars)
          return redirect('paymentterms_list')

    query = """
          
          query($id:Int!){
                        masterPaymentTermsById(id:$id)
                        {
                          id
                          term
                        }
                      }
      
      
                  """

    variables = {
          "id": id
      }
    data = client.execute(query=query, variables=variables)['data']['masterPaymentTermsById']
    return render(request,path.join('master','delete_paymentterms.html'),{"data":data})
  else:
    return redirect('login')
#==========End PaymentTerms



#===========Product Packaging========
def productpackaginglist(request):
  if len(dict(request.session))>0:
    if request.POST.get('Search')=="Search":
        mastersearch=request.POST.get('masterProductPackagingSearch')
        request.session['masterProductPackagingSearch']=mastersearch
        request.session['masterProductPackagingOffset']=0
        request.session['masterProductPackagingCurrentPage']=1

    if request.POST.get("masterProductPackagingSize")==None:
       pass
    else:
       request.session['masterProductPackagingSize']=int(request.POST.get('masterProductPackagingSize'))
       request.session['masterProductPackagingFirst']=request.session['masterProductPackagingSize']
       request.session['masterProductPackagingOffset']=0
       request.session['masterProductPackagingCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['masterProductPackagingSearch']=None
      request.session['masterProductPackagingSize']=None
      request.session['masterProductPackagingFirst']=10 #defalt Value 
    query2="""
                 query($search:String){
                    masterProductPackaging(packagingType_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['masterProductPackagingSearch']})['data']['masterProductPackaging']['edges']
    total_records = len(td)
    if total_records%request.session['masterProductPackagingFirst']==0:
      total_pages = total_records//request.session['masterProductPackagingFirst']
    else:
      total_pages = total_records//request.session['masterProductPackagingFirst']+1
    query = """

                query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  masterProductPackaging(offset:$offset,first:$first,last:$last,packagingType_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                    packagingType
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['masterProductPackagingOffset']=0
      request.session['masterProductPackagingCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['masterProductPackagingOffset']-=request.session['masterProductPackagingFirst']
      request.session['masterProductPackagingCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['masterProductPackagingOffset']+=request.session['masterProductPackagingFirst']
      request.session['masterProductPackagingCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['masterProductPackagingCurrentPage']=total_pages
      if total_records%int(request.session['masterProductPackagingFirst'])==0:
        last=total_records%int(request.session['masterProductPackagingFirst'])+int(request.session['masterProductPackagingFirst'])
        request.session['masterProductPackagingOffset']=total_records-int(request.session['masterProductPackagingFirst'])
      else:
        last=total_records%int(request.session['masterProductPackagingFirst'])
        request.session['masterProductPackagingOffset']=total_records-total_records%int(request.session['masterProductPackagingFirst'])
    
    #====End Paginator====


    #========Start Sorting=======#
    
    # if request.POST.get('id')=="Id":
    #   if request.session['masterProductPackagingSorting']=='id':
    #     request.session['masterProductPackagingSorting']='-id'
    #   else:
    #     request.session['masterProductPackagingSorting']='id'

    if request.POST.get('productpackaging')=="Product Packaging":
      if request.session['masterProductPackagingSorting']=='packaging_type':
        request.session['masterProductPackagingSorting']='-packaging_type'
      else:
        request.session['masterProductPackagingSorting']='packaging_type'

    #========End Sorting=========#
    v={
      "offset":request.session['masterProductPackagingOffset'],
      "first":request.session['masterProductPackagingFirst'],
      "search":request.session['masterProductPackagingSearch'],
      "last":last,
      "orderBy":request.session['masterProductPackagingSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['masterProductPackaging']['edges']

    max_t="""
              query{
                  masterProductPackagingCount
                }
       """
    max_row=client.execute(query=max_t)['data']['masterProductPackagingCount']

    return render(request, path.join('master', 'ProductPackaginglist.html'), {"entity_list": entity_list,"max_page":total_pages,"max_row":max_row})
  else:
    return redirect('login')
def createproductpackaging(request):
  if len(dict(request.session))>0:
    if request.method == 'POST':
          productpackaging = request.POST.get('productpackaging').title()
          query = """

            mutation($productpackaging: String){
                createMasterProductPackaging(packagingType:$productpackaging){
                  ok
                }
              }

          """
          variables = {"productpackaging":productpackaging}
          client.execute(query=query,variables=variables)
          return redirect('productpackaginglist')
    return render(request,path.join('master','createProductPackaging.html'))
  else:
    return redirect('login')
def updateproductpackaging(request,id):
  if len(dict(request.session))>0:
      query = """
                          query($id:Int!){
                              masterProductPackagingById(id:$id){
                                id
                                packagingType
                              }
                            }
                                """
      varss = {"id": id}
      data = client.execute(query=query, variables=varss)['data']['masterProductPackagingById']

      if request.method == 'POST':
          productpackaging = request.POST.get('productpackaging').title()

          q = """mutation($id:ID!, $packagingtype:String){
                    updateMasterProductPackaging(id:$id, packagingType:$packagingtype){
                      ok
                    }
                  }"""

          v = {"id": id, "packagingtype": productpackaging}

          client.execute(query=q, variables=v)
          return redirect("productpackaginglist")

      return render(request,path.join('master','updateProductPackaging.html'),{'data':data})
  else:
    return redirect('login')
def deleteproductpackaging(request,id):
  if len(dict(request.session))>0:
    if request.method == 'POST':
        mutation = """
        mutation($id:ID){
          deleteMasterProductPackaging(id:$id){
                ok    
          }
        }"""
        mvars = {
            "id":id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('productpackaginglist')

    query = """
        
         query($id:Int!){
                      masterProductPackagingById(id:$id)
                      {
                        id
                        packagingType
                      }
                    }
    
                """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['masterProductPackagingById']
    return render(request,path.join('master','deleteProductPackaging.html'),{'data':data})
  else:
    return redirect('login')


   