from django.urls import path
from master.check_unique import crmcommstatus, crmcommtype, crmsources, crmstatuss, designations, doctypes, entitypurposes, entitytypes, industries, measuretypes, measureunits, nextsteps, packagings, paymentcreditdays, paymentpercent, paymentterms, salutations, states, tastes, transactionstatus, transactiontypes, types, user_codes, user_types

from master.excel_view import entity_purpose_excel

from .views import create_designation, create_paymentterms, create_salutaion, create_transaction_doctype, create_transaction_status, create_transaction_type, createproductpackaging, delete_designation, delete_paymentterms, delete_salutation, delete_transaction_doctype, delete_transaction_status, delete_transaction_type, deleteproductpackaging, designation_list, entity_purpose_list, create_entity_purpose, paymentterms_list, productpackaginglist, salutation_list, transaction_doctype_list, transaction_status_list, transaction_type_list, update_designation, update_entity_purpose, delete_entity_purpose, update_paymentterms, update_salutation, update_transaction_doctype, update_transaction_status, update_transaction_type, updateproductpackaging
from .views import entity_type_list, create_entity_type, update_entity_type, delete_entity_type
from .views import pincode_list, create_pincode, update_pincode, delete_pincode
from .views import industry_list, create_industry, update_industry, delete_industry
from .views import product_category_list, create_product_category, update_product_category, delete_product_category
from .views import product_subcategory_list, create_product_subcategory, update_product_subcategory, \
    delete_product_subcategory
from .views import product_nutrition_type_list, create_product_nutrition_type, update_product_nutrition_type, \
    delete_product_nutrition_type
from .views import product_measure_type_list, create_product_measure_type, update_product_measure_type, \
    delete_product_measure_type
from .views import product_state_list, create_product_state, update_product_state, delete_product_state
from .views import product_taste_list, create_product_taste, update_product_taste, delete_product_taste
from .views import product_measure_unit_list, create_product_measure_unit, update_product_measure_unit, \
    delete_product_measure_unit
from .views import product_nextstep_list, create_product_nextstep, update_product_nextstep, delete_product_nextstep

from .views import crm_comm_status_list, create_crm_comm_status, update_crm_comm_status, delete_crm_comm_status
from .views import crm_comm_type_list, create_crm_comm_type, update_crm_comm_type, delete_crm_comm_type
from .views import crm_source_list, create_crm_source, update_crm_source, delete_crm_source
from .views import crm_status_list, create_crm_status, update_crm_status, delete_crm_status

from .views import user_type_list, create_user_type, update_user_type, delete_user_type
#=======View path of all Master====
from .page_view import view_crm_comm_status, view_crm_comm_type, view_crm_source, view_crm_status, view_designation, view_entity_purpose,view_entity_type, view_industry, view_paymenterms, view_product_category, view_product_measure_type, view_product_measure_unit, view_product_nextstep, view_product_nutrition_type, view_product_packaging, view_product_state, view_product_subcategory, view_salutation, view_transaction_doctype, view_transaction_status, view_transaction_type, view_user_type

from .page_view import view_product_taste

#====Export Data=====
from .export_data import crmstatus, export_entity_purpose, exportDesignation,exportIndustry, exportSalutation, exportnutritiontype, masterproductSubcategory,masterproductcategory,exportmastercrmcommonstatus,mastercrmCommonTypeData, measuretype, measureunit, nextstep, payment_terms, product_packaging_d, product_state, product_taste, tdoctype, transaction_status, transaction_type, usertypes
from .export_data import crmsource
#========Upload Files=======
from .upload_files import masterPurpose, masterpincodedata, masterproductSubcategorys, masterproductcategorys
urlpatterns = [
    # ===== Entity Purpose =====
    path('entity/purpose/list', entity_purpose_list, name="entity_purpose_list"),
    #path('entity/purpose/insert', entity_purpose_excel, name="entity_purpose_excel"),
    path('entity/purpose/create', create_entity_purpose, name="create_entity_purpose"),
    path('entity/purpose/update/<id>', update_entity_purpose, name="update_entity_purpose"),
    path('entity/purpose/delete/<id>', delete_entity_purpose, name="delete_entity_purpose"),

    # ===== Entity Type =====
    path('entity/entity/list', entity_type_list, name="entity_type_list"),
    path('entity/entity/create', create_entity_type, name="create_entity_type"),
    path('entity/entity/update/<id>', update_entity_type, name="update_entity_type"),
    path('entity/entity/delete/<id>', delete_entity_type, name="delete_entity_type"),

    # ===== pincode =====
    path('pincode/list', pincode_list, name="pincode_list"),
    path('pincode/create', create_pincode, name="create_pincode"),
    path('pincode/update/<id>', update_pincode, name="update_pincode"),
    path('pincode/delete/<id>', delete_pincode, name="delete_pincode"),

    # ===== Industry =======
    path('industry/list', industry_list, name="industry_list"),
    path('industry/create', create_industry, name="create_industry"),
    path('industry/update/<id>', update_industry, name="update_industry"),
    path('industry/view/<id>', view_industry, name="view_industry"),
    path('industry/delete/<id>', delete_industry, name="delete_industry"),

    # =====Product Cateogry=====
    path('product/category/list', product_category_list, name='product_category_list'),
    path('product/category/create', create_product_category, name='create_product_category'),
    path('product/category/update/<id>', update_product_category, name='update_product_category'),
    path('product/category/view/<id>', view_product_category, name='view_product_category'),
    path('product/category/delete/<id>', delete_product_category, name='delete_product_category'),

    # =====Product Subcateogry=====
    path('product/subcategory/list', product_subcategory_list, name='product_subcategory_list'),
    path('product/subcategory/create', create_product_subcategory, name='create_product_subcategory'),
    path('product/subcategory/update/<id>', update_product_subcategory, name='update_product_subcategory'),
    path('product/subcategory/view/<id>', view_product_subcategory, name='view_product_subcategory'),
    path('product/subcategory/delete/<id>', delete_product_subcategory, name='delete_product_subcategory'),

    # =====Product Nutrition Type=====

    path('product/nutritiontype/list', product_nutrition_type_list, name='product_nutrition_type_list'),
    path('product/nutritiontype/create', create_product_nutrition_type, name='create_product_nutrition_type'),
    path('product/nutritiontype/update/<id>', update_product_nutrition_type, name='update_product_nutrition_type'),
    path('product/nutritiontype/view/<id>', view_product_nutrition_type, name='view_product_nutrition_type'),
    path('product/nutritiontype/delete/<id>', delete_product_nutrition_type, name='delete_product_nutrition_type'),

    # =====Product Measure Type=====
    path('product/measuretype/list', product_measure_type_list, name='product_measure_type_list'),
    path('product/measuretype/create', create_product_measure_type, name='create_product_measure_type'),
    path('product/measuretype/update/<id>', update_product_measure_type, name='update_product_measure_type'),
    path('product/measuretype/view/<id>', view_product_measure_type, name='view_product_measure_type'),
    path('product/measuretype/delete/<id>', delete_product_measure_type, name='delete_product_measure_type'),
    # =======Product Measure Unit=====
    path('product/measureunit/list', product_measure_unit_list, name='product_measure_unit_list'),
    path('product/measureunit/create', create_product_measure_unit, name='create_product_measure_unit'),
    path('product/measureunit/update/<id>', update_product_measure_unit, name='update_product_measure_unit'),
    path('product/measureunit/view/<id>', view_product_measure_unit, name='view_product_measure_unit'),
    path('product/measureunit/delete/<id>', delete_product_measure_unit, name='delete_product_measure_unit'),
    # ===========product next step=====
    path('product/nextstep/list', product_nextstep_list, name='product_nextstep_list'),
    path('product/nextstep/create', create_product_nextstep, name='create_product_nextstep'),
    path('product/nextstep/update/<id>', update_product_nextstep, name='update_product_nextstep'),
    path('product/nextstep/view/<id>', view_product_nextstep, name='view_product_nextstep'),
    path('product/nextstep/delete/<id>', delete_product_nextstep, name='delete_product_nextstep'),

    # =====Product State=====
    path('product/state/list', product_state_list, name='product_state_list'),
    path('product/state/create', create_product_state, name='create_product_state'),
    path('product/state/update/<id>', update_product_state, name='update_product_state'),
    path('product/state/delete/<id>', delete_product_state, name='delete_product_state'),
    path('product/state/view/<id>', view_product_state, name='view_product_state'),
    # =====Product Taste=====
    path('product/taste/list', product_taste_list, name='product_taste_list'),
    path('product/taste/create', create_product_taste, name='create_product_taste'),
    path('product/taste/update/<id>', update_product_taste, name='update_product_taste'),
    path('product/taste/delete/<id>', delete_product_taste, name='delete_product_taste'),
    path('product/taste/view/<id>', view_product_taste, name='view_product_taste'),

    # =====crm_comm_status=====
    path('crm/common/status/list', crm_comm_status_list, name='crm_comm_status_list'),
    path('crm/common/status/create', create_crm_comm_status, name='create_crm_comm_status'),
    path('crm/common/status/update/<id>', update_crm_comm_status, name='update_crm_comm_status'),
    path('crm/common/status/view/<id>', view_crm_comm_status, name='view_crm_comm_status'),
    path('crm/common/status/delete/<id>', delete_crm_comm_status, name='delete_crm_comm_status'),

    # =====crm_comm_type=====
    path('crm/common/type/list', crm_comm_type_list, name='crm_comm_type_list'),
    path('crm/common/type/create', create_crm_comm_type, name='create_crm_comm_type'),
    path('crm/common/type/update/<id>', update_crm_comm_type, name='update_crm_comm_type'),
    path('crm/common/type/view/<id>', view_crm_comm_type, name='view_crm_comm_type'),
    path('crm/common/type/delete/<id>', delete_crm_comm_type, name='delete_crm_comm_type'),

    # =====crm source======
    path('crm/source/list', crm_source_list, name='crm_source_list'),
    path('crm/source/create', create_crm_source, name='create_crm_source'),
    path('crm/source/update/<id>', update_crm_source, name='update_crm_source'),
    path('crm/source/view/<id>', view_crm_source, name='view_crm_source'),
    path('crm/source/delete/<id>', delete_crm_source, name='delete_crm_source'),

    # =====crm status======
    path('crm/status/list', crm_status_list, name='crm_status_list'),
    path('crm/status/create', create_crm_status, name='create_crm_status'),
    path('crm/status/update/<id>', update_crm_status, name='update_crm_status'),
    path('crm/status/view/<id>', view_crm_status, name='view_crm_status'),
    path('crm/status/delete/<id>', delete_crm_status, name='delete_crm_status'),

    # ===== User type=====
    path('usertype/list', user_type_list, name='user_type_list'),
    path('usertype/create', create_user_type, name='create_user_type'),
    path('usertype/update/<id>', update_user_type, name='update_user_type'),
    path('usertype/view/<id>', view_user_type, name='view_user_type'),
    path('usertype/delete/<id>', delete_user_type, name='delete_user_type'),
   
    #======salutation=====
    path('salutation/list',salutation_list,name="salutation_list"),
    path('salutation/create',create_salutaion,name="create_salutaion"),
    path('salutation/update/<id>',update_salutation,name="update_salutation"),
    path('salutation/view/<id>',view_salutation,name="view_salutation"),
    path('salutation/delete/<id>',delete_salutation,name="delete_salutation"),
    #======Designation=====
    path('designation/list',designation_list,name="designation_list"),
    path('designation/create',create_designation,name="create_designation"),
    path('designation/update/<id>',update_designation,name="update_designation"),
    path('designation/view/<id>',view_designation,name="view_designation"),
    path('designation/delete/<id>',delete_designation,name="delete_designation"),
    
    #======view of All Pages=======
    path('view/entitypurpose/<id>',view_entity_purpose,name="view_entity_purpose"),
    path('view/entitytype/<id>',view_entity_type,name="view_entity_type"),

    #=====Export Data pata======
    path('export/Entity/purpose',export_entity_purpose,name="export_entity_purpose"),
    path('export/Entity/industry',exportIndustry,name="exportIndustry"),
    path('export/Salutation',exportSalutation,name="exportSalutation"),
    path('export/Designation',exportDesignation,name="exportDesignation"),
    path('export/master/productcategory',masterproductcategory,name="masterproductcategory"),
    path('export/master/crm/common/status',exportmastercrmcommonstatus,name="exportmastercrmcommonstatus"),
    path('export/master/crm/common/type',mastercrmCommonTypeData,name="mastercrmCommonTypeData"),
    path('export/master/crm/source',crmsource,name="crmsource"),
    path('export/master/crm/status',crmstatus,name="crmstatus"),
    path('export/master/usertypes',usertypes,name="usertypes"),
    path('export/master/productSubcategory',masterproductSubcategory,name="masterproductSubcategory"),
    path('export/master/nutritiontype',exportnutritiontype,name="exportnutritiontype"),
    path('export/master/measuretype',measuretype,name="measuretype"),
    path('export/master/measuretunit',measureunit,name="measureunit"),
    path('export/master/nextstep',nextstep,name="nextstep"),
    path('export/master/doctype',tdoctype,name="tdoctype"),
    path('export/master/taste',product_taste,name="product_taste"),
    path('export/master/state',product_state,name="product_state"),
    path('export/master/term',payment_terms,name="payment_terms"),
    path('export/master/packaging',product_packaging_d,name="product_packaging_d"),
    path('export/master/transaction/type',transaction_type,name="transaction_type"),
    path('export/master/transaction/status',transaction_status,name="transaction_status"),
    
    #======Transaction Type======
    path('transactiontype/list',transaction_type_list,name='transaction_type_list'),
    path('transactiontype/create',create_transaction_type,name='create_transaction_type'),
    path('transactiontype/update/<id>',update_transaction_type,name='update_transaction_type'),
    path('transactiontype/view/<id>',view_transaction_type,name='view_transaction_type'),
    path('transactiontype/delete/<id>',delete_transaction_type,name='delete_transaction_type'),
    #======Transaction Status======
    path('transactionstatus/list',transaction_status_list,name='transaction_status_list'),
    path('transactionstatus/create',create_transaction_status,name='create_transaction_status'),
    path('transactionstatus/update/<id>',update_transaction_status,name='update_transaction_status'),
    path('transactionstatus/view/<id>',view_transaction_status,name='view_transaction_status'),
    path('transactionstatus/delete/<id>',delete_transaction_status,name='delete_transaction_status'),
    #======Transaction Status======
    path('transaction/doctype/list',transaction_doctype_list,name='transaction_doctype_list'),
    path('transaction/doctype/create',create_transaction_doctype,name='create_transaction_doctype'),
    path('transaction/doctype/update/<id>',update_transaction_doctype,name='update_transaction_doctype'),
    path('transaction/doctype/view/<id>',view_transaction_doctype,name='view_transaction_doctype'),
    path('transaction/doctype/delete/<id>',delete_transaction_doctype,name='delete_transaction_doctype'),
    #========Payment Terms====
    path('paymentterms/list',paymentterms_list,name="paymentterms_list"),
    path('paymentterms/create',create_paymentterms,name="create_paymentterms"),
    path('paymentterms/update/<id>',update_paymentterms,name="update_paymentterms"),
    path('paymentterms/view/<id>',view_paymenterms,name="view_paymenterms"),
    path('paymentterms/delete/<id>',delete_paymentterms,name="delete_paymentterms"),

    path('productpackaging/list',productpackaginglist,name="productpackaginglist"),
    path('productpackaging/create',createproductpackaging,name="createproductpackaging"),
    path('productpackaging/update/<id>',updateproductpackaging,name="updateproductpackaging"),
    path('productpackaging/view/<id>',view_product_packaging,name="view_product_packaging"),
    path('productpackaging/delete/<id>',deleteproductpackaging,name="deleteproductpackaging"),
    #==========Upload Files==============
    path('purpose/upload',masterPurpose,name="masterPurpose"),
    path('pincode/upload',masterpincodedata,name="masterpincodedata"),
    path('subcategory/upload',masterproductSubcategorys,name="masterproductSubcategorys"),
    path('category/upload',masterproductcategorys,name="masterproductcategorys"),
    
    #======= CheckuniqueValue=======
    path('crmcommstatus',crmcommstatus,name="crmcommstatus"),
    path('crmcommtype',crmcommtype,name="crmcommtype"),
    path('crmsources',crmsources,name="crmsources"),
    path('crmstatuss',crmstatuss,name="crmstatuss"),
    path('designations',designations,name="designations"),
    path('doctypes',doctypes,name="doctypes"),
    path('entitypurposes',entitypurposes,name="entitypurposes"),
    path('entitytypes',entitytypes,name="entitytypes"),
    path('industries',industries,name="industries"),
    path('paymentterms',paymentterms,name="paymentterms"),
    path('paymentcreditdays',paymentcreditdays,name="paymentcreditdays"),
    path('paymentpercent',paymentpercent,name="paymentpercent"),
    path('measuretypes',measuretypes,name="measuretypes"),
    path('measureunits',measureunits,name="measureunits"),
    path('nextsteps',nextsteps,name="nextsteps"),
    path('types',types,name="types"),
    path('packagings',packagings,name="packagings"),
    path('states',states,name="states"),
    path('tastes',tastes,name="tastes"),
    path('salutations',salutations,name="salutations"),
    path('transactionstatus',transactionstatus,name="transactionstatus"),
    path('transactiontypes',transactiontypes,name="transactiontypes"),
    path('user_codes',user_codes,name="user_codes"),
    path('user_types',user_types,name="user_types"),
]
