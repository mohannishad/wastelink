from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from python_graphql_client import GraphqlClient
from wastelink.settings import client
@csrf_exempt
def crmcommstatus(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                masterCrmCommStatus{
                  edges{
                  node{
                    status
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterCrmCommStatus']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['status'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)

@csrf_exempt
def crmcommtype(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                masterCrmCommType{
                  edges{
                  node{
                    type
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterCrmCommType']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['type'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def crmsources(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                masterCrmSource{
                  edges{
                  node{
                    source
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterCrmSource']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['source'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def crmstatuss(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                masterCrmStatus{
                  edges{
                  node{
                    status
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterCrmStatus']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['status'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)

@csrf_exempt
def designations(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                masterDesignation{
                  edges{
                  node{
                    designation
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterDesignation']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['designation'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def doctypes(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                masterDoctype{
                  edges{
                  node{
                     transactionDoctype
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterDoctype']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['transactionDoctype'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def entitypurposes(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                masterEntityPurposes{
                  edges{
                  node{
                     entityPurpose
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterEntityPurposes']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['entityPurpose'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def entitytypes(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                  masterEntityType{
                        edges{
                        node{
                          entityType
                        }
                        }
                      }
                    }
            """
    parent_entities = client.execute(query=query4)['data']['masterEntityType']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['entityType'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def industries(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                  masterIndustry{
                        edges{
                        node{
                          industry
                        }
                        }
                      }
                    }
            """
    parent_entities = client.execute(query=query4)['data']['masterIndustry']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['industry'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def paymentterms(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
                query{
                  masterPaymentTerms{
                        edges{
                        node{
                          term
                        }
                        }
                      }
                    }
            """
    parent_entities = client.execute(query=query4)['data']['masterPaymentTerms']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['term'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def paymentcreditdays(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search')
   
    query4 = """
                query{
                  masterPaymentTerms{
                        edges{
                        node{
                          creditDays
                        }
                        }
                      }
                    }
            """
    parent_entities = client.execute(query=query4)['data']['masterPaymentTerms']['edges']
    a = ''
    for i in parent_entities:
       
        if i['node']['creditDays'] == int(search):
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)

@csrf_exempt
def paymentpercent(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search')
    query4 = """
                query{
                  masterPaymentTerms{
                        edges{
                        node{
                           paymentPercent
                        }
                        }
                      }
                    }
            """
    parent_entities = client.execute(query=query4)['data']['masterPaymentTerms']['edges']
    a = ''
    for i in parent_entities:
       
        if i['node']['paymentPercent'] == int(search):
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)




@csrf_exempt
def measuretypes(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
            query{
            masterProductMeasureType{
                  edges{
                  node{
                    measureType
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterProductMeasureType']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['measureType'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def measureunits(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
            query{
            masterProductMeasureUnit{
                  edges{
                  node{
                    unit
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterProductMeasureUnit']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['unit'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def nextsteps(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
            query{
            masterProductNextStep{
                  edges{
                  node{
                    nextStep
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterProductNextStep']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['nextStep'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def types(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
           query{
            masterProductNutritionType{
                  edges{
                  node{
                    type
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterProductNutritionType']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['type'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def packagings(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
           query{
            masterProductPackaging{
                  edges{
                  node{
                    packagingType
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterProductPackaging']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['packagingType'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def states(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
           query{
            masterProductState{
                  edges{
                  node{
                    state
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterProductState']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['state'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def tastes(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
           query{
            masterProductTaste{
                  edges{
                  node{
                    taste
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterProductTaste']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['taste'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def salutations(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
           query{
            masterSalutation{
                  edges{
                  node{
                    salutation
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterSalutation']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['salutation'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def transactionstatus(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
           query{
            masterTransactionStatus{
                  edges{
                  node{
                    transactionStatus
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterTransactionStatus']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['transactionStatus'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def transactiontypes(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    
    query4 = """
           query{
            masterTransactionType{
                  edges{
                  node{
                    transactionType
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterTransactionType']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['transactionType'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
@csrf_exempt
def user_types(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    
    query4 = """
           query{
            masterUserTypes{
                  edges{
                  node{
                    type
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterUserTypes']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['type'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)
def user_codes(request):
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    
    query4 = """
           query{
            masterUserTypes{
                  edges{
                  node{
                    code
                  }
                  }
                }
              }
            """
    parent_entities = client.execute(query=query4)['data']['masterUserTypes']['edges']
    a = ''
    for i in parent_entities:
        if i['node']['code'].title() == search:
            a = 'This value already exists!'
    return JsonResponse(a, safe=False)