from django.urls import path

from transaction.dependencyfield import billingEntity_pickupEntity, entitytoauditoruser, entitytologisticentity, pickupEntity_toAddress, product_categoryandsubcatogry
from .views import  CreateSelectATP, CreateTransactionProduct, createitemdetails, transaction_products_list,create_transaction_products,update_transaction_products
from .views import transaction_bags_list,create_transaction_bags,update_transaction_bags,delete_transaction_bags
from .views import transaction_list,create_transaction
from .export_data import transaction_products,transaction_bags,transaction
from .pageviews import bags


urlpatterns = [
 path('transaction/list',transaction_list,name='transaction_list' ),
  path('transaction/create',create_transaction,name='create_transaction' ),

  path('transaction/bags/list',transaction_bags_list,name="transaction_bags_list"),
  path('transaction/bags/create',create_transaction_bags,name="create_transaction_bags"),
  path('transaction/bags/update/<id>',update_transaction_bags,name="update_transaction_bags"),
  path('transaction/bags/delete/<id>',delete_transaction_bags,name="delete_transaction_bags"),


  path('transaction/products/list',transaction_products_list,name="transaction_products_list"),
  path('transaction/products/create',create_transaction_products,name="create_transaction_products"),
  path('transaction/products/update/<id>',update_transaction_products,name="update_transaction_products"),
   
  path("transaction/itemdetails/create",createitemdetails,name="createitemdetails"),
  path('transaction/product/create',CreateTransactionProduct,name="CreateTransactionProduct"),
  path('transaction/atp/create',CreateSelectATP,name="CreateSelectATP"),
  # path('transaction/product/create',CreateTransactionProduct.as_view(),name="CreateTransactionProduct"),
  # path('transaction/atp/create',CreateSelectATP.as_view(),name="CreateSelectATP"),
  #========export Data=====

  path('transaction/products/export',transaction_products,name="transaction_products"),
  path('transaction/bags/export',transaction_bags,name="transaction_bags"),
  path('transaction/export',transaction,name="transaction"),
  #==========veiw pages==========

  path('transaction/bags/veiw/<id>',bags,name="bags"),

  #=========Dependency Field ==========
  path('billingEntity_pickupEntity/',billingEntity_pickupEntity,name="billingEntity_pickupEntity"),
  path('pickupEntity_toAddress/',pickupEntity_toAddress,name="pickupEntity_toAddress"),
  path('producttocategory/',product_categoryandsubcatogry,name="product_categoryandsubcatogry"),
  path('entitytoauditoruser/',entitytoauditoruser,name="entitytoauditoruser"),
  path('entitytologisticentity/',entitytologisticentity,name="entitytologisticentity"),
]
