from wastelink.settings import client
import csv
from django.http import HttpResponse

def transaction(request):
  query = """
         query{
      transaction{
    edges{
      node{
          id
          auditDoneDate
          auditEntity
          auditScheduleDate
          auditUser
          auditWeight
          auditedBagCount
          auditedUnitCount
          billingEntity
        {
          id
          name
        }
          createdBy {
            id
          }
          creationDate
          externalAudit
          logisticBagCount
          logisticUnitCount
          logisticWeight
          pickupAddress
        {
          addressLabel
        }
          pickupBagCount
          pickupDoneDate
          pickupEntity
        {
          id
        }
          pickupScheduleDate
          pickupUnitCount
          pickupWeight
          processingAddress
          processingEntity
          receivedBagCount
          receivedUnitCount
          receivedWeight
          ticketStatus
          transportEntity
          transportPickupDate
          transportScheduleDate
          transportUser
          type
          wlPickupSiteAudit
                  
                }
              }
            }
          }
      """

  data=client.execute(query=query)['data']['transaction']['edges']
  
  
  id=[]
  auditDoneDate=[]
  auditEntity=[]
  auditScheduleDate=[]
  auditUser=[]
  auditWeight=[]
  auditedBagCount=[]
  auditedUnitCount=[]
  billingEntity=[]
  createdBy=[]
  creationDate=[]
  externalAudit=[]
  logisticBagCount=[]
  logisticUnitCount=[]
  logisticWeights=[]
  pickupAddress=[]
  pickupBagCount=[]
  pickupDoneDate=[]
  pickupEntity=[]
  pickupScheduleDate=[]
  pickupUnitCount=[]
  pickupWeights=[]
  processingAddress=[]
  processingEntity=[]
  receivedBagCount=[]
  receivedUnitCount=[]
  receivedWeights=[]
  ticketStatus=[]
  transportEntity=[]
  transportPickupDate=[]
  transportScheduleDate=[]
  transportUser=[]
  type=[]
  wlPickupSiteAudit=[]
  for i in data:
    id.append(i['node']['id'])
    auditDoneDate.append(i['node']['auditDoneDate'])
    auditEntity.append(i['node']['auditEntity'])
    auditScheduleDate.append(i['node']['auditScheduleDate'])
    auditUser.append(i['node']['auditUser'])
    auditWeight.append(i['node']['auditWeight'])
    auditedBagCount.append(i['node']['auditedBagCount'])
    auditedUnitCount.append(i['node']['auditedUnitCount'])
    billingEntity.append(i['node']['billingEntity'])
    createdBy.append(i['node']['createdBy'])
    creationDate.append(i['node']['creationDate'])
    externalAudit.append(i['node']['externalAudit'])
    logisticBagCount.append(i['node']['logisticBagCount'])
    logisticUnitCount.append(i['node']['logisticUnitCount'])
    logisticWeights.append(i['node']['logisticWeight'])
    pickupAddress.append(i['node']['pickupAddress'])
    pickupBagCount.append(i['node']['pickupBagCount'])
    pickupDoneDate.append(i['node']['pickupDoneDate'])
    pickupEntity.append(i['node']['pickupEntity'])
    pickupScheduleDate.append(i['node']['pickupScheduleDate'])
    pickupUnitCount.append(i['node']['pickupUnitCount'])
    pickupWeights.append(i['node']['pickupWeight'])
    processingAddress.append(i['node']['processingAddress'])
    processingEntity.append(i['node']['processingEntity'])
    receivedBagCount.append(i['node']['receivedBagCount'])
    receivedUnitCount.append(i['node']['receivedUnitCount'])
    receivedWeights.append(i['node']['receivedWeight'])
    ticketStatus.append(i['node']['ticketStatus'])
    transportEntity.append(i['node']['transportEntity'])
    transportPickupDate.append(i['node']['transportPickupDate'])
    transportScheduleDate.append(i['node']['transportScheduleDate'])
    transportUser.append(i['node']['transportUser'])
    type.append(i['node']['type'])
    wlPickupSiteAudit.append(i['node']['wlPickupSiteAudit'])

  print(id)
  # m=int(ids)
 
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Transaction.csv"'},
    )

  writer = csv.writer(response)
  writer.writerow(["ID","CREATED_BY","CREATION_DATE","TYPE","TICKET_STATUS","BILLING_ENTITY","PICKUP_ENTITY","PICKUP_ADDRESS","PICKUP_SCHEDULE_DATE","PICKUP_DONE_DATE","EXTERNAL_AUDIT","WL_PICKUP_SITE_AUDIT","AUDIT_ENTITY","AUDIT_USER","AUDIT_SCHEDULE_DATE","AUDIT_DONE_DATE","TRANSPORT_ENTITY","TRANSPORT_USER","TRANSPORT_SCHEDULE_DATE","TRANSPORT_PICKUP_DATE","PROCESSING_ENTITY","PROCESSING_ADDRESS","PICKUP_UNIT_COUNT","AUDITED_UNIT_COUNT","LOGISTIC_UNIT_COUNT","RECEIVED_UNIT_COUNT","PICKUP_BAG_COUNT","AUDITED_BAG_COUNT","LOGISTIC_BAG_COUNT","RECEIVED_BAG_COUNT","PICKUP_WEIGHT","AUDIT_WEIGHT","LOGISTIC_WEIGHT","RECEIVED_WEIGHT"])
  for i in range(len(id)):

    writer.writerow([id[i],createdBy[i]['id'],creationDate[i],type[i],ticketStatus[i],billingEntity[i]['name'],pickupEntity[i]['id'],pickupAddress[i]['addressLabel'],pickupScheduleDate[i],pickupDoneDate[i],externalAudit[i],wlPickupSiteAudit[i],auditEntity[i],auditUser[i],auditScheduleDate[i],auditDoneDate[i],transportEntity[i],transportUser[i],transportScheduleDate[i],transportScheduleDate[i],processingEntity[i],processingAddress[i],pickupUnitCount[i],auditedUnitCount[i],logisticUnitCount[i],receivedUnitCount[i],pickupBagCount[i],auditedBagCount[i],logisticBagCount[i],receivedBagCount[i],pickupWeights[i],auditWeight[i],logisticWeights[i],receivedWeights[i]])
   
  return response

def transaction_products(request):
  query = """

      query{
      transactionProducts{
        edges{
          node{
            id
            transaction
            product
            bagNumber
            pickupWeight
            logisticUnits
            logisticWeight
            receivedUnits
            receivedWeight
            auditedUnits
            auditedWeight
            pickupUnits
                        
            }
          }
          }
          }
      """

  data=client.execute(query=query)['data']['transactionProducts']['edges']
  ids=[]
  transaction=[]
  product=[]
  bagNumber=[]
  pickupWeight=[]
  logisticUnits=[]
  logisticWeight=[]
  receivedUnits=[]
  receivedWeight=[]
  auditedUnits=[]
  auditedWeight=[]
  pickupUnits=[]    
  for i in data:
    ids.append(i['node']['id'])
    transaction.append(i['node']['transaction'])
    product.append(i['node']['product'])
    bagNumber.append(i['node']['bagNumber'])
    pickupWeight.append(i['node']['pickupWeight'])
    logisticUnits.append(i['node']['logisticUnits'])
    logisticWeight.append(i['node']['logisticWeight'])
    receivedUnits.append(i['node']['receivedUnits'])
    receivedWeight.append(i['node']['receivedWeight'])
    auditedUnits.append(i['node']['auditedUnits'])
    auditedWeight.append(i['node']['auditedWeight'])
    pickupUnits.append(i['node']['pickupUnits'])

 
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Transaction_Product.csv"'},
    )

  writer = csv.writer(response)
  writer.writerow(["ID","TRANSACTION","PRODUCT","BAG NUMBER","PICKUP WEIGHT","LOGISTIC UNITS","LOGISTIC WEIGHT","RECEIVED UNITS","RECEIVED WEIGHT","AUDITED UNITS","AUDITED WEIGHT","PICKUP UNITS"])
  for i in range(len(ids)):
    writer.writerow([ids[i], transaction[i],product[i],bagNumber[i],pickupWeight[i],logisticUnits[i],logisticWeight[i],receivedUnits[i],receivedWeight[i],auditedUnits[i],auditedWeight[i],pickupUnits[i]])

  return response
def transaction_bags(request):
  query = """

    query{
                  transactionBags{
                    edges{
                      node{
                    id
                    bagNumber
                    bagWeight
                    unitCount
                    isAudited
                    auditedBagWeight
                    auditedUnitCount
                    logisticBagWeight
                    logisticUnitCount
                    receivedBagWeight
                    receivedUnitCount
                   transaction{
                     id
                   }
                  }
                }
                }
                }
      """

  data=client.execute(query=query)['data']['transactionBags']['edges']
  id=[]
  bagNumber=[]
  bagWeight=[]
  unitCount=[]
  isAudited=[]
  auditedBagWeight=[]
  auditedUnitCount=[]
  logisticBagWeight=[]
  logisticUnitCount=[]
  receivedBagWeight=[]
  receivedUnitCount  =[] 
  transaction=[]
  for i in data:
    id.append(i['node']['id'])
    transaction.append(i['node']['transaction']["id"])
    bagWeight.append(i['node']['bagWeight'])
    bagNumber.append(i['node']['bagNumber'])
    unitCount.append(i['node']['unitCount'])
    isAudited.append(i['node']['isAudited'])
    auditedBagWeight.append(i['node']['auditedBagWeight'])
    auditedUnitCount.append(i['node']['auditedUnitCount'])
    logisticBagWeight.append(i['node']['logisticBagWeight'])
    logisticUnitCount.append(i['node']['logisticUnitCount'])
    receivedBagWeight.append(i['node']['receivedBagWeight'])
    receivedUnitCount.append(i['node']['receivedUnitCount'])

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Transaction_Bags.csv"'},
    )

  writer = csv.writer(response)
  writer.writerow(["ID","TRANSACTION","BAG NUMBER","BAG WEIGHT","UNIT COUNT","IS AUDITED","AUDITED BAG WEIGHT","AUDITED UNIT COUNT","LOGISTIC BAG WEIGHT","LOGISTIC UNIT COUNT","RECEIVED BAG WEIGHT","RECEIVED UNIT COUNT"])
  for i in range(len(id)):
    writer.writerow([id[i], transaction[i],bagNumber[i],bagWeight[i],unitCount[i],isAudited[i],auditedBagWeight[i],auditedUnitCount[i],logisticBagWeight[i],logisticUnitCount[i],receivedBagWeight[i],receivedUnitCount[i]])

  return response