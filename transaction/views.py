from functools import total_ordering
from django.shortcuts import redirect, render
from os import path

from requests.api import request
from wastelink.settings import client
from datetime import datetime
from django.views import View
#=======Transaction======

def transaction_list(request):
  
  query_list="""
              query{
              transaction{
                edges{
                  node{
                    id
                    
                    type {
                      id
                      transactionType
                    }
                     status{
                          id
                          transactionStatus
                        }
                    billingEntity{
                            id
                            name
                          }
                      pickupEntity{
                            id
                            name
                                }
                      pickupAddress{
                          id
                          addressLabel
                        }
                      pickupAuditEntity{
                              id
                              name
                            }
                       pickupAuditUser{
                          id
                          addressLabel
                        }
                       transportEntity{
                          id
                            name
                          }
                      receiveEntity{
                              id
                              name
                            }
                       receiveAddress{
                          id
                          addressLabel
                        }
                  ticketSerial
                  pickupScheduleDate
                  pickupUnitCount
                  pickupWeight
                  pickupBagCount
                  pickupAudit
                  receiveAudit
                }

              }
            }
            }
  
  """
  transaction_lists=client.execute(query=query_list)['data']['transaction']['edges']
  return render(request,path.join('transaction','transaction_list.html'),{"transaction":transaction_lists})




def create_transaction(request):
  mystatusdata="""
            query($m:String){
            masterTransactionStatus(transactionStatus_Icontains:$m){
              edges{
                node{
                  id
                  transactionStatus
                }
              }
            }
          }
            
  """
  statusdata=client.execute(query=mystatusdata,variables={ "m": "Draft"})['data']['masterTransactionStatus']['edges']
  s = ""
  for i in statusdata:
    s+=str(i['node']['id'])
  t=int(s)

  ticket_s="""
      query{
      lastTransaction{
      ticketSerial
        }
      }
      
  """
  ticket_s_data=client.execute(query=ticket_s)['data']['lastTransaction']

  ts=ticket_s_data['ticketSerial']

  ts=ts+1
  
  if request.method=="POST":
    types=request.POST.get("type")
    # status=request.POST.get("status")
    # start_date=datetime.now()
    
    
    my_date = datetime.now()
    startes=my_date.isoformat()
    billingentity=request.POST.get('billingentity')
    pickupentity=request.POST.get('pickupentity')
    pickupscheduledate=request.POST.get('pickupscheduledate')
    entityaddress=request.POST.get('entityaddress')
    pickupunitcount=request.POST.get('pickupunitcount')
    pickupweight=request.POST.get('pickupweight')
    pickupbagcount=request.POST.get('pickupbagcount')
    pickupaudit=request.POST.get('pickupaudit')
    receiveaudit=request.POST.get('receiveaudit')
    
    print("pickupaudit"*20)
    print(pickupaudit)
    print('x'*10)
    print(receiveaudit)
    print("pickupaudit"*20)
    create_t1="""
        mutation($creationdate:DateTime,$type:Int,$status:Int,$ticketserial:Int,$createdby:Int,
        $billingentity:Int,$pickupentity:Int,$startdate:DateTime,$pickupunitcount:Int,$pickupweight:Float
        ,$pickupbagcount:Int,$pickupscheduledate:DateTime,$pickupaddress:Int,
        ,$pickupaudit:Boolean,$receiveaudit:Boolean){
        createTransaction(creationDate:$creationdate,type:$type,status:$status,ticketSerial:$ticketserial,
          billingEntity:$billingentity,pickupEntity:$pickupentity,
        pickupAddress:$pickupaddress,startDate:$startdate,
          createdBy:$createdby,
          pickupUnitCount:$pickupunitcount,pickupWeight:$pickupweight,pickupBagCount:$pickupbagcount,
          pickupScheduleDate:$pickupscheduledate,pickupAudit:$pickupaudit,receiveAudit:$receiveaudit){
          ok
        }
      }
     """
    data={
      "type":types,
      "status":t,
      "ticketserial":ts,
      "billingentity":billingentity,
      "pickupentity":pickupentity,
      "startdate":startes,
      "creationdate":startes,
      "pickupunitcount":pickupunitcount,
      "pickupweight":pickupweight,
      "pickupbagcount":pickupbagcount,
      "pickupscheduledate":"20211221T161534",
      "pickupaddress":entityaddress,
      "createdby":2,
      "pickupaudit":pickupaudit,
      "receiveaudit":receiveaudit
    }
    client.execute(query=create_t1, variables=data)
    return redirect("createitemdetails") 


  entity_list="""
               query($e:String){
              entity(entityType_EntityType:$e){
                edges{
                  node{
                    id
                    name
                  }
                }
              }
            }
         """

  
  entity_data=client.execute(query=entity_list,variables={"e":"Company"})['data']['entity']['edges']
  
  #=====Master Data Fatch=======
  transactiontype="""
       query{
                  masterTransactionType{
                    edges{
                      node{
                        id
                        transactionType
                      }
                    }
                  }
                }
        """
  transactionstatus="""
       query{
                  masterTransactionStatus{
                    edges{
                      node{
                        id
                        transactionStatus
                      }
                    }
                  }
                }
        """
  
  data1=client.execute(query=transactiontype)['data']['masterTransactionType']['edges']
  data2=client.execute(query=transactionstatus)['data']['masterTransactionStatus']['edges']
  return render(request,path.join('transaction','create_transaction.html')
  ,{"entity_data":entity_data,"transactiontype":data1,"transactionstatus":data2})

def update_transaction(request):
  return render(request,path.join('transaction','update_transaction.html'))
def delete_transaction(request):
  return render(request,path.join('transaction','delete_transaction.html'))



#=====List Transaction products=====

def transaction_products_list(request):
    if request.POST.get('Search')=="Search":
      transactionproducts=request.POST.get('transactionproducts')
      request.session['transactionproductsSearch']=transactionproducts
    

    if request.POST.get("transactionproductsSize")==None:
       pass
    else:
       request.session['transactionproductsSize']=int(request.POST.get('transactionproductsSize'))
       request.session['transactionproductsFirst']=request.session['transactionproductsSize']
       request.session['transactionproductsOffset']=0
       request.session['transactionproductsCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['transactionproductsSearch']=None
      request.session['transactionproductsSize']=None
      request.session['transactionproductsFirst']=3 #defalt Value 
    query2="""
              query($search:Int){
              transactionProducts(bagNumber_Icontains:$search){
                edges{
                  node{
                    id
                  }
                }
              }
              }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['transactionproductsSearch']})['data']['transactionProducts']['edges']
    total_records = len(td)
    if total_records%request.session['transactionproductsFirst']==0:
      total_pages = total_records//request.session['transactionproductsFirst']
    else:
      total_pages = total_records//request.session['transactionproductsFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$orderBy:String,$search:Int){
                  transactionProducts(offset:$offset,first:$first,last:$last,orderBy:$orderBy,bagNumber_Icontains:$search){
                    edges{
                      node{
                        id
                        transaction
                        product
                        bagNumber
                        pickupWeight
                        logisticUnits
                        logisticWeight
                        receivedUnits
                        receivedWeight
                        auditedUnits
                        auditedWeight
                        pickupUnits
                        
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['transactionproductsOffset']=0
      request.session['transactionproductsCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['transactionproductsOffset']-=request.session['transactionproductsFirst']
      request.session['transactionproductsCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['transactionproductsOffset']+=request.session['transactionproductsFirst']
      request.session['transactionproductsCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['transactionproductsCurrentPage']=total_pages
      if total_records%int(request.session['transactionproductsFirst'])==0:
        last=total_records%int(request.session['transactionproductsFirst'])+int(request.session['transactionproductsFirst'])
        request.session['transactionproductsOffset']=total_records-int(request.session['transactionproductsFirst'])
      else:
        last=total_records%int(request.session['transactionproductsFirst'])
        request.session['transactionproductsOffset']=total_records-total_records%int(request.session['transactionproductsFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    # if request.POST.get('productcompo')=="Product":
    #   if request.session['productCompositionSorting']=='product__name':
    #     request.session['productCompositionSorting']='-product__name'
    #   else:
    #     request.session['productCompositionSorting']='product__name'

    # if request.POST.get('baseProduct')=="Base Product":
    #   if request.session['productCompositionSorting']=='base_product__name':
    #     request.session['productCompositionSorting']='-base_product__name'
    #   else:
    #     request.session['productCompositionSorting']='base_product__name'

    # if request.POST.get('compositionunit')=="Composition Unit":
    #   if request.session['productCompositionSorting']=='composition_unit__unit':
    #     request.session['productCompositionSorting']='-composition_unit__unit'
    #   else:
    #     request.session['productCompositionSorting']='composition_unit__unit'

    # if request.POST.get('compositioncon')=="Composition Conversion":
    #   if request.session['productCompositionSorting']=='composition_conversion':
    #     request.session['productCompositionSorting']='-composition_conversion'
    #   else:
    #     request.session['productCompositionSorting']='composition_conversion'

    # if request.POST.get('compositionvalue')=="Composition Value":
    #   if request.session['productCompositionSorting']=='composition_value':
    #     request.session['productCompositionSorting']='-composition_value'
    #   else:
    #     request.session['productCompositionSorting']='composition_value'

    # if request.POST.get('calories')=="Calorie":
    #   if request.session['productCompositionSorting']=='calorie':
    #     request.session['productCompositionSorting']='-calorie'
    #   else:
    #     request.session['productCompositionSorting']='calorie'
    
   

    #========End Sorting=========#
    v={
      "offset":request.session['transactionproductsOffset'],
      "first":request.session['transactionproductsFirst'],
      "search":request.session['transactionproductsSearch'],
      "last":last,
      "orderBy":request.session['transactionproductsSorting']
    }
    transaction_products = client.execute(query=query,variables=v)['data']['transactionProducts']['edges']
    return render(request,path.join('transaction','transaction_products_list.html'),{"transaction_products": transaction_products,"max_page":total_pages})

  





def create_transaction_products(request):
  if request.method=="POST":
    transaction=request.POST.get("transaction")
    product=request.POST.get("product")
    bagnumber=request.POST.get("bagnumber")
    pickupunits=request.POST.get("pickupunits")
    pickupweight=request.POST.get("pickupweight")
    auditedunit=request.POST.get("auditedunit")
    auditedweight=request.POST.get("auditedweight")
    logisticunits=request.POST.get("logisticunits")
    logisticweight=request.POST.get("logisticweight")
    receivedunits =request.POST.get("receivedunits")             
    receivedweight=request.POST.get("receivedweight")
    
    query="""
           mutation($auditedUnits: Int,$auditedWeight: Int,$bagNumber: Int,$logisticUnits: Int,$logisticWeight: Int,$pickupUnits: Int,$pickupWeight: Int,$product: Int,$receivedUnits: Int,$receivedWeight: Int,$transaction: Int){
          createTransactionProducts(auditedUnits: $auditedUnits,auditedWeight: $auditedWeight,bagNumber: $bagNumber,logisticUnits: $logisticUnits,logisticWeight: $logisticWeight,pickupUnits: $pickupUnits,pickupWeight: $pickupWeight,product: $product,receivedUnits: $receivedUnits,receivedWeight: $receivedWeight,transaction: $transaction
          ){
          ok
          }
          }
    """
    v= {
        "auditedUnits": auditedunit,
        "auditedWeight": auditedweight,
        "bagNumber":bagnumber,
        "logisticUnits":logisticunits,
        "logisticWeight":logisticweight ,
        "pickupUnits": pickupunits,
        "pickupWeight":pickupweight,
        "product": product,
        "receivedUnits": receivedunits,
        "receivedWeight": receivedweight,
        "transaction": transaction
        }
    
    data=client.execute(query=query,variables=v)['data']['createTransactionProducts']
    return redirect('transaction_products_list')
  return render(request,path.join('transaction','create_transaction_products.html'))
def update_transaction_products(request,id):
    #  query = """

    #              query($id:Int!){
    #               transactionProducts(){
    #                 edges{
    #                   node{
    #                     id
    #                     transaction
    #                     product
    #                     bagNumber
    #                     pickupWeight
    #                     logisticUnits
    #                     logisticWeight
    #                     receivedUnits
    #                     receivedWeight
    #                     auditedUnits
    #                     auditedWeight
    #                     pickupUnits
                        
    #               }
    #             }
    #             }
    #             }
    #        """
  return render(request,path.join('transaction','update_transaction_products.html'))

def delete_transaction_products(request):
  return render(request,path.join('transaction','delete_transaction_products.html'))



#====Transaction Bags======


def transaction_bags_list(request):

    if request.POST.get('Search')=="Search":
        transactionbags=request.POST.get('transactionbags')
        request.session['transactionbagsSearch']=transactionbags
        request.session['transactionbagsCurrentPage']=1
        request.session['transactionbagsOffset']=0
    #=========Filter Data=======
    if request.POST.get("filterisaudited")=="True":
      request.session['transactionbagsisAudited']=request.POST.get("filterisaudited")
    if request.POST.get("filterisaudited")=="False":
      request.session['transactionbagsisAudited']=request.POST.get("filterisaudited")
      request.session['transactionbagsisAudited']=False
      
    #=========End Filter Data====
    if request.POST.get("transactionbagsSize")==None:
       pass
    else:
       request.session['transactionbagsSize']=int(request.POST.get('transactionbagsSize'))
       request.session['transactionbagsFirst']=request.session['transactionbagsSize']
       request.session['transactionbagsOffset']=0
       request.session['transactionbagsCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['transactionbagsSearch']=None
      request.session['transactionbagsSize']=None
      request.session['transactionbagsisAudited']=None
      request.session['transactionbagsFirst']=10 #defalt Value 
    query2="""
                query($search:Int,$isAudited:Boolean){
                  transactionBags(bagNumber_Icontains:$search,isAudited:$isAudited){
                  edges{
                    node{
                      id
                    }
                  }
                  }
                }
     
    """
    vs={
         "search":request.session['transactionbagsSearch'],
         "isAudited":request.session['transactionbagsisAudited']
    }
    td=client.execute(query=query2,variables=vs)['data']['transactionBags']['edges']
    total_records = len(td)
    if total_records%request.session['transactionbagsFirst']==0:
      total_pages = total_records//request.session['transactionbagsFirst']
    else:
      total_pages = total_records//request.session['transactionbagsFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$search:Int,$orderBy:String,$isAudited:Boolean){
                  transactionBags(offset:$offset,first:$first,last:$last,bagNumber_Icontains:$search,orderBy:$orderBy,isAudited:$isAudited){
                    edges{
                      node{
                    id
                    bagNumber
                    bagWeight
                    unitCount
                    isAudited
                    auditedBagWeight
                    auditedUnitCount
                    logisticBagWeight
                    logisticUnitCount
                    receivedBagWeight
                    receivedUnitCount
                   transaction{
                     id
                   }
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['transactionbagsOffset']=0
      request.session['transactionbagsCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['transactionbagsOffset']-=request.session['transactionbagsFirst']
      request.session['transactionbagsCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['transactionbagsOffset']+=request.session['transactionbagsFirst']
      request.session['transactionbagsCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['transactionbagsCurrentPage']=total_pages
      if total_records%int(request.session['transactionbagsFirst'])==0:
        last=total_records%int(request.session['transactionbagsFirst'])+int(request.session['transactionbagsFirst'])
        request.session['transactionbagsOffset']=total_records-int(request.session['transactionbagsFirst'])
      else:
        last=total_records%int(request.session['transactionbagsFirst'])
        request.session['transactionbagsOffset']=total_records-total_records%int(request.session['transactionbagsFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('transaction')=="Transaction":
      if request.session['transactionbagsSorting']=='transaction__type':
        request.session['transactionbagsSorting']='-transaction__type'
      else:
        request.session['transactionbagsSorting']='transaction__type'
    if request.POST.get('bagnumber')=="Bag Number":
      if request.session['transactionbagsSorting']=='bag_number':
        request.session['transactionbagsSorting']='-bag_number'
      else:
        request.session['transactionbagsSorting']='bag_number'
    if request.POST.get('bagweight')=="Bag Weight":
      if request.session['transactionbagsSorting']=='bag_weight':
        request.session['transactionbagsSorting']='-bag_weight'
      else:
        request.session['transactionbagsSorting']='bag_weight'

    #========End Sorting=========#
    v={
      "offset":request.session['transactionbagsOffset'],
      "first":request.session['transactionbagsFirst'],
      "search":request.session['transactionbagsSearch'],
      "isAudited":request.session['transactionbagsisAudited'],
      "last":last,
      "orderBy":request.session['transactionbagsSorting']
    }
    transaction_bags = client.execute(query=query,variables=v)['data']['transactionBags']['edges']
    

    return render(request,path.join('transaction','transaction_bags_list.html'),{"transaction_bags": transaction_bags,"max_page":total_pages})

def create_transaction_bags(request):
  query2="""
       query{
      transaction{
    edges{
      node{
          id
          
                  
                }
              }
            }
          }
      """
  if request.method=="POST":
    transaction=request.POST.get('transaction')
    bagnumber=request.POST.get('bagnumber')
    bagweight=request.POST.get('bagweight')
    unitcount=request.POST.get('unitcount')
    isaudited=request.POST.get('isaudited')
   
    auditedbagweight=request.POST.get('auditedbagweight')
    auditedunitcount=request.POST.get('auditedunitcount')
    logisticbagweight=request.POST.get('logisticbagweight')
    logisticunitcount=request.POST.get('logisticunitcount')
    receivedbagweight=request.POST.get('receivedbagweight')
    receivedunitcount=request.POST.get('receivedunitcount')


    query3="""
     mutation($auditedBagWeight:Int,$auditedUnitCount: Int,$bagNumber: Int,$bagWeight: Int,$isAudited: Boolean,$logisticBagWeight: Int,$logisticUnitCount: Int,$receivedBagWeight: Int,$receivedUnitCount: Int,$transaction: Int,$unitCount: Int){
     createTransactionBags(auditedBagWeight:$auditedBagWeight,auditedUnitCount: $auditedUnitCount,bagNumber: $bagNumber,bagWeight: $bagWeight,isAudited: $isAudited,logisticBagWeight: $logisticBagWeight,logisticUnitCount: $logisticUnitCount,receivedBagWeight: $receivedBagWeight,receivedUnitCount: $receivedUnitCount,transaction: $transaction,unitCount: $unitCount){
        ok
        }
      }"""

    variable={
    "auditedBagWeight":auditedbagweight,
    "auditedUnitCount":auditedunitcount,
    "bagNumber":bagnumber,
    "bagWeight":bagweight,
    "isAudited":isaudited,
    "logisticBagWeight":logisticbagweight,
    "logisticUnitCount":logisticunitcount,
    "receivedBagWeight":receivedbagweight,
    "receivedUnitCount":receivedunitcount,
    "transaction":transaction,
    "unitCount":unitcount

    }
  
      
    # 
    # data = client.execute(query=query, variables=v)['data']['createTransactionBags']
    
    # return redirect('transaction_bags_list')

    data=client.execute(query=query3,variables=variable)
    return redirect('transaction_bags_list')
  
  transactioin=client.execute(query=query2)['data']['transaction']['edges']
  return render(request,path.join('transaction','create_transaction_bags.html'),{"transactioin":transactioin})

def update_transaction_bags(request,id):
  query2="""
       query{
      transaction{
    edges{
      node{
          id
          
                  
                }
              }
            }
          }
      """
  query3="""
          query($id:Int!){
                transactionBagsById(id:$id){
                  id
                  transaction{
                    id
                  }
                  bagNumber
                  bagWeight
                  unitCount
                  isAudited
                  auditedBagWeight
                  auditedUnitCount
                  logisticBagWeight
                  logisticUnitCount
                  receivedBagWeight
                  receivedUnitCount
              }
            }
              
  
  """
  varia={
    "id":id
  }
  if request.method=="POST":
    transaction=request.POST.get('transaction')
    bagnumber=request.POST.get('bagnumber')
    bagweight=request.POST.get('bagweight')
    unitcount=request.POST.get('unitcount')
    isaudited=request.POST.get('isaudited')
    if isaudited=="True":
      isaudited=True
    if isaudited=="False":
      isaudited=False
    auditedbagweight=request.POST.get('auditedbagweight')
    auditedunitcount=request.POST.get('auditedunitcount')
    logisticbagweight=request.POST.get('logisticbagweight')
    logisticunitcount=request.POST.get('logisticunitcount')
    receivedbagweight=request.POST.get('receivedbagweight')
    receivedunitcount=request.POST.get('receivedunitcount')
    updatequery="""
      mutation($auditedBagWeight: Int,$auditedUnitCount: Int,$bagNumber: Int,$bagWeight: Int,$id:ID,$isAudited: Boolean,$logisticBagWeight: Int,$logisticUnitCount: Int,$receivedBagWeight: Int,$receivedUnitCount: Int,$transaction: Int,$unitCount: Int){
      updateTransactionBags(
      auditedBagWeight:$auditedBagWeight,
      auditedUnitCount: $auditedUnitCount,
      bagNumber:$bagNumber,
      bagWeight:$bagWeight,
      id:$id,
      isAudited:$isAudited,
      logisticBagWeight:$logisticBagWeight,
      logisticUnitCount:$logisticUnitCount,
      receivedBagWeight:$receivedBagWeight,
      receivedUnitCount:$receivedUnitCount,
      transaction: $transaction,
      unitCount: $unitCount
      ){
        ok
      }
      }
  
      """
    updatevariable={
    "id":id,
    "auditedBagWeight":auditedbagweight,
    "auditedUnitCount":auditedunitcount,
    "bagNumber":bagnumber,
    "bagWeight":bagweight,
    "isAudited":isaudited,
    "logisticBagWeight":logisticbagweight,
    "logisticUnitCount":logisticunitcount,
    "receivedBagWeight":receivedbagweight,
    "receivedUnitCount":receivedunitcount,
    "transaction":transaction,
    "unitCount":unitcount
  }
    client.execute(query=updatequery,variables=updatevariable)
    return redirect("transaction_bags_list")
  data=client.execute(query=query3,variables=varia)['data']['transactionBagsById']

  transactions=client.execute(query=query2)['data']['transaction']['edges']
  return render(request,path.join('transaction','update_transaction_bags.html'),{'transactionss':transactions,"data":data})
def delete_transaction_bags(request,id):
  if request.method=="POST":
    query4="""
    
      mutation($id:ID){
            deleteTransactionBags(id:$id){
              ok
            }
          }
    
    """
    client.execute(query=query4,variables={"id":id})
    return redirect("transaction_bags_list")
  query3="""
          query($id:Int!){
                transactionBagsById(id:$id){
                  id
                  transaction{
                    id
                  }
                  bagNumber
                  
              }
            }
              
  
  """
  varia={
    "id":id
  }
  data=client.execute(query=query3,variables=varia)['data']['transactionBagsById']
  
  return render(request,path.join('transaction','delete_transaction_bags.html'),{"data":data})




#==========Create item Details=========

def createitemdetails(request):
  transaction_id="""
          query{
              lastTransaction{
              id
                }
              }
          
  """
  transactionid=client.execute(query=transaction_id)['data']['lastTransaction']
  t_id=transactionid['id']
  if request.method=="POST":
    bagid=request.POST.get("bagid")
    bagbarcode=request.POST.get("bagbarcode")
    bagweight=request.POST.get("bagweight")
    bagunitcount=request.POST.get("bagunitcount")

  
    query2="""
      mutation($bagBarcode: String,$bagNumber: Int,$pickupUnitCount: Int,$pickupWeight: Float,$transaction: Int){
            createTransactionBags(bagBarcode:$bagBarcode,bagNumber: $bagNumber,  pickupUnitCount: $pickupUnitCount,pickupWeight: $pickupWeight,transaction: $transaction,){
              ok
            }
          }
      """
    v={
    "bagBarcode":bagbarcode,
    "bagNumber":bagid,
    "pickupUnitCount":bagunitcount,
    "pickupWeight":bagweight,
    "transaction":t_id
    }
    client.execute(query=query2,variables=v)
    return redirect('CreateTransactionProduct')
  return render(request,path.join('transaction','create_item_details.html'))



def CreateTransactionProduct(request):
  transaction_id="""
          query{
              lastTransaction{
              id
                }
              }
                  
          """
  transactionid=client.execute(query=transaction_id)['data']['lastTransaction']
  t_id=transactionid['id']

  query="""
        query{
        product{
          edges{
            node{
              id
              name
            }
          }
        }
        }
    """
  query2="""
          query{
      masterProductPackaging{
        edges{
          node{
            id
            packagingType
          }
        }
      }
      }
    """
   

  query3="""
    query($filter:Float){
      transactionBags(transaction_Id:$filter){
        edges{
          node{
            id
            bagNumber
          
          }
        }
      }
      }
      """
  tb= {
            "filter": t_id
            }
  productdata=client.execute(query=query)['data']["product"]['edges']
  bagid=client.execute(query=query3,variables=tb)['data']['transactionBags']['edges']
  mpackaging=client.execute(query=query2)['data']["masterProductPackaging"]['edges']
  if request.method=="POST":
    product=request.POST.get('product')
    yields=request.POST.get('yield')
    rate=request.POST.get('rate')
    productpackaging=request.POST.get('productpackaging')
    bagnumber=request.POST.get('bagnumber')
    category=request.POST.get('category')
    subcategory=request.POST.get('subcategory')
    pickupweight=request.POST.get('pickupweight')
    pickupunitcount=request.POST.get('pickupunitcount')
    query="""
        mutation($transaction:Int,$product:Int,$rate:Float,$pickupWeight:Float,$bag:Int,$pickupUnitCount:Int,$category:Int,$subcategory:Int,$packagingStatus:Int,$yieldField:Float){
        createTransactionProducts(
        bag:$bag,
        transaction:$transaction,
        category:$category,
        packagingStatus: $packagingStatus,
        pickupUnitCount:$pickupUnitCount,
        pickupWeight:$pickupWeight,
        product:$product,
        rate:$rate,
        subcategory:$subcategory,
        yieldField:$yieldField){
            ok
          }
        }
            
    """
    v={
      "transaction":t_id,
      "bag":bagnumber,
      "category":category,
      "packagingStatus":productpackaging,
      "pickupUnitCount":pickupunitcount,
      "pickupWeight":pickupweight,
      "product":product,
      "rate":rate,
      "subcategory":subcategory,
      "yieldField":yields
     }
    client.execute(query=query,variables=v)

    return redirect("CreateSelectATP")
  return render(request,path.join('transaction','create_transaction_product.html'),{"product":productdata,"mpackaging":mpackaging,"bagid":bagid})



def CreateSelectATP(request):
  transaction_id="""
          query{
              lastTransaction{
              id
                }
              }
                  
          """
  transactionid=client.execute(query=transaction_id)['data']['lastTransaction']
  t_id=transactionid['id']
  entityquery="""
             query{
              entity{
              edges{
              node{
                id
                name
            }
            }
              }
            }
  
  """
  entitylist=client.execute(query=entityquery)['data']['entity']['edges']
  entityquery2="""
              query($entityPurpose_EntityPurpose:String){
              entity(entityPurpose_EntityPurpose:$entityPurpose_EntityPurpose){
              edges{
                  node{
                    id
                    name
                   }
                    }
                     }
                    }
              """
  v={
  "entityPurpose_EntityPurpose": "Processor"
   }
  entitylist2=client.execute(query=entityquery2,variables=v)['data']['entity']['edges']
  if request.method=="POST":
    entity=request.POST.get('entity')
    auditoruser=request.POST.get('auditoruser')
    logisticentity=request.POST.get('logisticentity')
    processingUnit=request.POST.get('processingUnit')
    query="""
    mutation($id:ID,$transportEntity:Int,$pickupAuditEntity:Int,$pickupAuditUser:Int,$status:Int,$receiveEntity:Int){
          updateTransaction(id:$id,transportEntity:$transportEntity,pickupAuditEntity:$pickupAuditEntity,pickupAuditUser:$pickupAuditUser,status:$status,receiveEntity:$receiveEntity){
            ok
          }
        },
    
    """
    v={
      "id": t_id,
      "pickupAuditUser": auditoruser,
      "pickupAuditEntity":entity,
      "status": 5,
      "transportEntity": logisticentity,
      "receiveEntity": processingUnit
    }
    return redirect("dashboard")
  return render(request,path.join('transaction','create_select_auditor_trans_pro.html'),{"entitylist":entitylist,"entitylist2":entitylist2})
