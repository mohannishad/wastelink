from os import path
from django.shortcuts import render
from wastelink.settings import client

def bags(request,id):
  query3="""
          query($id:Int!){
                transactionBagsById(id:$id){
                  id
                  transaction{
                    id
                  }
                  bagNumber
                  bagWeight
                  unitCount
                  isAudited
                  auditedBagWeight
                  auditedUnitCount
                  logisticBagWeight
                  logisticUnitCount
                  receivedBagWeight
                  receivedUnitCount
              }
            }
              
  
  """
  varia={
    "id":id
  }

  data=client.execute(query=query3,variables=varia)['data']['transactionBagsById']

  return render(request,path.join('transaction/viewpages','bags.html'),{"data":data})

