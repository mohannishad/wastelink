from django.http import JsonResponse, response
from django.views.decorators.csrf import csrf_exempt
import json
from wastelink.settings import client
@csrf_exempt
def billingEntity_pickupEntity(request):
   if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        query4 = """
                    query($id:Int!){
                  parentEntities(id:$id){
                    entity{
                    id
                    }
                  }
                }
        """

        variabless = {"id": int(search)}
        parent_entities = client.execute(query=query4, variables=variabless)['data']['parentEntities']
        parent_entities.append({'entity':{'id': search}})
        parent_en = []
        for i in parent_entities:
            a = int(i['entity']['id'])
            query5 = """query($id: Int!){

                    entityById(id:$id){
                    id
                    name
                }
                }"""

            variables5 = {"id": a}

            parent_name = client.execute(query=query5, variables=variables5)['data']['entityById']
            parent_en.append(parent_name)

        return JsonResponse(parent_en, safe=False)

@csrf_exempt
def pickupEntity_toAddress(request):
   if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        
        
        query9="""
        
               query($entity:Float!){
                  entityAddress(entity_Id:$entity){
                    edges{
                      node{
                        id
                            addressLabel
                                  }
                          }
                      }
                    }
        
        """
        var= {"entity": int(search)}
        entityid=client.execute(query=query9,variables=var)['data']['entityAddress']['edges']
        
        return JsonResponse(entityid, safe=False)
@csrf_exempt
def product_categoryandsubcatogry(request):
   if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        query9="""
              query($id:Int!){
                      productById(id:$id){
                        id
                      category{
                        id
                        name
                      }
                        subCategory
                        {
                          id
                          name
                        }
                      }
                    }
        
        """
        var= {"id": int(search)}
        categorys=client.execute(query=query9,variables=var)['data']['productById']
       
        m=[categorys]
        
        return JsonResponse(m, safe=False)


@csrf_exempt
def entitytoauditoruser(request):
  if request.method == 'POST':
    search = int(json.loads(request.body).get('search'))   
    query9="""
    
            query($entity:Float){
                    entityAuditor(entity_Id:$entity){
                      edges{
                        node{
                          id
                          auditorUser{
                          username
                          id
                          }
                        }
                      }
                    }
                  }
    
    """
    var= {"entity": int(search)}
    entityid=client.execute(query=query9,variables=var)['data']['entityAuditor']['edges']
    
    return JsonResponse(entityid, safe=False)

@csrf_exempt
def entitytologisticentity(request):
  if request.method == 'POST':
    search = int(json.loads(request.body).get('search'))
  
    query9="""
            query($entity:Float){
            entityTransporter(entity_Id:$entity){
              edges{
                node{
                  id
                  transporter{
                  id
                    name
                  }
                }
              }
            }
          }
    
    """
    var= {"entity": int(search)}
    entityid=client.execute(query=query9,variables=var)['data']['entityTransporter']['edges']
    
    return JsonResponse(entityid, safe=False)