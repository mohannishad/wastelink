from django.db import models
from django.utils import timezone as tz


class Supplier(models.Model):
    supplier_code = models.CharField(db_index=True, max_length=64)
    supplier_name = models.CharField(db_index=True, max_length=127)
    distributor_code = models.CharField(db_index=True, max_length=64)
    distributor_name = models.CharField(db_index=True, max_length=127)
    distributor_address = models.CharField(db_index=True, max_length=256)
    city = models.CharField(db_index=True, max_length=127)
    state = models.CharField(db_index=True, max_length=127)
    supplier_latitude = models.CharField(max_length=64,null=True, blank=True,default=None)
    supplier_longitude = models.CharField(max_length=64,null=True, blank=True,default=None)
    contact_number = models.CharField(max_length=64)
    created_by = models.CharField(max_length=64)
    created_on = models.DateTimeField(default=tz.now)
    parent_id = models.IntegerField()
    supplier_type = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return self.supplier_name

    class Meta:
        ordering = ['-id']


class SupplierwithCode(models.Model):
    supplier_name = models.CharField(db_index=True, max_length=127)
    distributor_code = models.CharField(db_index=True, max_length=64)
    distributor_name = models.CharField(db_index=True, max_length=127)

    def __str__(self):
        return self.supplier_name

    class Meta:
        ordering = ['-id']

