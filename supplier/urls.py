from os import path
from django.urls import path
# this is Supplier view 
from .views import sup_create,sup_list,sup_update,sup_delete
# this is Supplier_with_code view
from .views import swc_create,swc_list,swc_update,swc_delete
urlpatterns = [
  #===supplier path===#
  path('create/',sup_create,name='sup_create'),
  path('list/',sup_list,name='sup_list'),
  path('update/',sup_update,name='sup_update'),
  path('delete/',sup_delete,name='sup_delete'),
  #====swc path===#
  path('swc/create/',swc_create,name='swc_create'),
  path('swc/list/',swc_list,name='swc_list'),
  path('swc/update/',swc_update,name='swc_update'),
  path('swc/delete/',swc_delete,name='swc_delete'),
]
