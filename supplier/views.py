from django.shortcuts import render
from os import path
#===Supplier Create===#
def sup_create(request):
  return render(request,path.join('supplier','sup_create.html'))
#===Supplier List===#
def sup_list(request):
  return render(request,path.join('supplier','sup_list.html'))
#===Supplier Update===#
def sup_update(request):
  return render(request,path.join('supplier','sup_update.html'))
#===Supplier Delete===#
def sup_delete(request):
  return render(request,path.join('supplier','sup_delete.html'))
#====================Start Supplier with Code========#

#===Swc Create===#
def swc_create(request):
  return render(request,path.join('supplier','swc_create.html'))
#===Swc List====#
def swc_list(request):
  return render(request,path.join('supplier','swc_list.html'))
#===Swc Update===#
def swc_update(request):
  return render(request,path.join('supplier','swc_update.html'))
#===Swc Delete===#
def swc_delete(request):
  return render(request,path.join('supplier','swc_delete.html'))