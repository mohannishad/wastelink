from django.urls import path
from .views import login,logoutview,password_reset
from django.contrib.auth import views as auth_views


urlpatterns = [
  path('login/',login,name='login'),
  path('logout/',logoutview,name='logoutview'),
  path('Forget_password/', password_reset, name='reset_password'),
  path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name='login/password_reset_send.html'), name='password_reset_done'),
  path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='login/password_reset_form.html'), name='password_reset_confirm'),
  path('reset_password_complete/',auth_views.PasswordResetCompleteView.as_view(template_name='login/password_reset_done.html'),name='password_reset_complete'),
 
]