from django.contrib import auth, messages
from django.http import HttpResponse
from django.shortcuts import render, redirect, resolve_url
from django.contrib.auth import authenticate, login as wl_login, logout
from python_graphql_client import GraphqlClient
from django.utils.translation import gettext as _

from user.models import User
from os import path
from django.contrib import messages as msgs
from django.core.mail import send_mail, BadHeaderError
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.db.models import Q


# =====Login Function======#
def login(request):

    print('x'*22)
    # print(request.session['username'])
    client = GraphqlClient(endpoint="http://127.0.0.1:9000/graphql/")

    if not len(dict(request.session))>0:
        if request.method == 'POST':
            email = request.POST.get('email')
            # phone = request.POST.get('phone')
            password = request.POST.get('password')

            query = """mutation($u:String, $p:String!){
                  tokenAuth(email:$u, password:$p){
                    token
                    success
                    user{
                        id
                        username
                        email
                        firstName
                    }
                  }
                }"""
            v = {
                "u": email,
                "p": password
            }
            data = client.execute(query=query, variables=v)['data']['tokenAuth']
            if data['success'] == True:
                request.session['username'] = data['user']['username']
                request.session['firstname'] = data['user']['firstName']
                request.session['email'] = data['user']['email']

                request.session['userid'] = data['user']['id']
                return redirect('index')

            else:
                messages.info(request, 'Invalid username or password.')

            

    else:
        return redirect('index')

    # output = _("I am Mohan")
    return render(request, path.join('login', 'login.html'))


# =========Logout Function========#
def logoutview(request):
    request.session.flush()
    return redirect('login')


# ==========Reset Password======#

def password_reset(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            password_reset_form = request.POST.get('email')
            print(password_reset_form)
            a = User.objects.filter(Q(email=password_reset_form))
            if a.exists():
                for user in a:
                    subject = "Password Reset Requested"
                    s = "login/password_send.txt"
                    c = {'email': user.email,
                         'domain': '127.0.0.1:8000',
                         'site_name': '',
                         "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                         "user": user,
                         'token': default_token_generator.make_token(user),
                         'protocol': 'http',
                         }
                send_url = f"{c['protocol']}://{c['domain']}/reset/{c['uid']}/{c['token']}/"
                print('x' * 34)
                print(send_url)
                file1 = open("login/templates/login/password_send.txt", "w")
                file1.writelines(send_url)
                file1.close()
                email = render_to_string(s, c)
                print(email)
                try:

                    send_mail(subject, email, 'mohan.nishad.wastelink@gmail.com', [user.email], fail_silently=False)
                    return redirect('password_reset_done')
                except BadHeaderError:
                    return HttpResponse('Invalid header found.')
            else:
                msgs.error(request, 'Invalid Email Id ')
                return render(request, path.join('login', 'password_reset.html'))
    else:
        return redirect('logoutview')
    return render(request, path.join('login', 'password_reset.html'))
