

$(document).ready(function(){
	$("#formid").bootstrapValidator({
		feedbackIcons:{
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			
			 phone:{
			validators:{
				stringLength:{max:10,min:10,
											 message: 'Please enter only 10 digits '}, 
				
				notEmpty:{
					message:"Please Enter Phone Number"
				}
			}
		}
	 ,
	 email:{
			validators:{
				emailAddress:{
											 message: 'Please Enter Valid Email ID '}, 
				
				notEmpty:{
					message:"Please Enter Email ID"
				}
			}
		}

		}
	});

});
