from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from python_graphql_client import GraphqlClient
from wastelink.settings import client

@csrf_exempt
def load_id(request):
      
    if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        query4 = """
                    query($id:Int!){
                  parentEntities(id:$id){
                    entity{
                    id
                    }
                  }
                }
        """

        variabless = {"id": int(search)}
        parent_entities = client.execute(query=query4, variables=variabless)['data']['parentEntities']
        parent_entities.append({'entity':{'id': search}})
        parent_en = []
        for i in parent_entities:
            a = int(i['entity']['id'])
            query5 = """query($id: Int!){

                    entityById(id:$id){
                    id
                    name
                }
                }"""

            variables5 = {"id": a}

            parent_name = client.execute(query=query5, variables=variables5)['data']['entityById']
            parent_en.append(parent_name)

        return JsonResponse(parent_en, safe=False)
@csrf_exempt
def base_onIndustry(request):
      
    if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))

        query9="""
        
                query($id: Int!){
                    entityById(id:$id){
                   industry{
                    industry
                    id
                  }
                }
                }
        
        """
        var= {"id": int(search)}
        industry_e=client.execute(query=query9,variables=var)['data']['entityById']['industry']
       
       
        indus = []
        indus.append(industry_e)
       
        # print("End"*10)
       


        return JsonResponse(indus, safe=False)


@csrf_exempt
def check_unique(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').upper()
        print(search)
        query4 = """
                    query{
                      masterUserTypes{
                        code
                      }
                    }
                """

        parent_entities = client.execute(query=query4)['data']['masterUserTypes']
        a = ''

        for i in parent_entities:
            if i['code'] == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)


@csrf_exempt
def check_unique_ep(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()
        print(search)
        query4 = """
                    query{
                      masterEntityPurposes{
                        entityPurpose
                      }
                    }
                """

        parent_entities = client.execute(query=query4)['data']['masterEntityPurposes']
        a = ''
        for i in parent_entities:
            if i['entityPurpose'] == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)


@csrf_exempt
def check_unique_et(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()
        print(search)
        query4 = """
                    query{
                      masterEntityType{
                        entityType
                      }
                    }
                """

        parent_entities = client.execute(query=query4)['data']['masterEntityType']

        a = ''
        for i in parent_entities:
            if i['entityType'] == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)


@csrf_exempt
def check_unique_industry(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()
        query4 = """
                    query{
                      masterIndustry{
                        industry
                      }
                    }
                """
        parent_entities = client.execute(query=query4)['data']['masterIndustry']
        print(parent_entities)
        a = ''
        for i in parent_entities:
            if i['industry'].title() == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)


@csrf_exempt
def check_unique_prodname(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()
        query4 = """
                    query
                    {
                      masterProductCategory{
                        name
                      }
                    }
                """
        parent_entities = client.execute(query=query4)['data']['masterProductCategory']
        print(parent_entities)
        a = ''
        for i in parent_entities:
            if i['name'].title() == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)


@csrf_exempt
def check_unique_prodabbv(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()
        query4 = """
                query
                {
                  masterProductCategory{
                    abbv
                  }
                }
                """
        parent_entities = client.execute(query=query4)['data']['masterProductCategory']
        print(parent_entities)
        a = ''
        for i in parent_entities:
            if i['abbv'].title() == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)


@csrf_exempt
def check_unique_prodsubname(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()
        query4 = """
                    query
                    {
                      masterProductSubcategory{
                        name
                      }
                    }
                """
        parent_entities = client.execute(query=query4)['data']['masterProductSubcategory']
        print(parent_entities)
        a = ''
        for i in parent_entities:
            if i['name'].title() == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)


@csrf_exempt
def check_unique_prodsubabbv(request):
      
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()
        query4 = """
                query
                {
                  masterProductSubcategory{
                    abbv
                  }
                }
                """
        parent_entities = client.execute(query=query4)['data']['masterProductSubcategory']
        print(parent_entities)
        a = ''
        for i in parent_entities:
            if i['abbv'].title() == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)



#===============Entity App Check Unique Value========
@csrf_exempt
def entity_name(request):
    
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
               query{
                      entity{
                    name
                        }
                    }
                """
    parent_entities = client.execute(query=query4)['data']['entity']
    a = ''
    for i in parent_entities:
      if i['name'].title() == search:
        a = 'This value already exists!'
    return JsonResponse(a,safe=False)

@csrf_exempt
def brandname(request):
    
  if request.method == 'POST':
    search = json.loads(request.body).get('search').title()
    query4 = """
              query{
              entityBrand{
                    Brand
             }
                }
                """
    entitybrandname = client.execute(query=query4)['data']['entityBrand']
   
    a = ''
    for i in entitybrandname:
      if i['Brand'].title() == search:
        a = 'This value already exists!'
    return JsonResponse(a,safe=False)
