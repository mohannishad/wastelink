from django.urls import path
from entity.dependencyfield import auditor_auditor_user,productTocategory, tranporter
from entity.jsonapi import load_id, check_unique, check_unique_ep, check_unique_et, check_unique_industry, \
    check_unique_prodabbv, check_unique_prodname, check_unique_prodsubabbv, check_unique_prodsubname
from .pincodeApi import fetchdistrict_to, fatch_pincode_to, fatch_locality_to, fetchstate_to
from .views import auditor_list, category_entity_list, contact_list, create_auditor, create_contact, create_pricing, create_transporter, delete_auditor, delete_transporter, entity_list, create_entity, pricing_list, transporter_list, udpate_auditor, update_entity, delete_entity, update_transporter, view_entity
from .views import address_list, create_address, update_address, delete_address, create_address1
from .views import brand_list, create_brand, update_brand, delete_brand, create_brand1
from .views import crm_list, create_crm, update_crm, delete_crm
from .views import parent_list, create_parent, update_parent, delete_parent
#===viw====
from .views import view_address,view_brand
#====unique entity====
from .jsonapi import base_onIndustry, entity_name,brandname

#======Export Data ====
from .data_export import entity_crm, export_data_entity,export_data_brand,export_data_address,entity_parent
#======Upload File=====
from .upload_file import entity_file,upload_file_brand,upload_file_address
urlpatterns = [
    # =====Entity=====
    path('entity/list', entity_list, name="entity_list"),
    path('entity/create', create_entity, name="create_entity"),
    path('entity/update/<id>', update_entity, name="update_entity"),
    path('entity/delete/<id>', delete_entity, name="delete_entity"),
    
    # =====Address=====
    path('Servicelocation/list', address_list, name='address_list'),
    path('Servicelocation/create', create_address, name="create_address"),
    path('Servicelocation/create1', create_address1, name="create_address1"),
    path('Servicelocation/update/<id>', update_address, name="update_address"),
    path('Servicelocation/delete/<id>', delete_address, name="delete_address"),
    # =====Brand=====
    path('brand/list', brand_list, name='brand_list'),
    path('brand/create', create_brand, name='create_brand'),
    path('brand/create1', create_brand1, name='create_brand1'),
    path('brand/update/<id>', update_brand, name='update_brand'),
    path('brand/delete/<id>', delete_brand, name='delete_brand'),

    # =====Crm========
    path('crm/list', crm_list, name='crm_list'),
    path('crm/create', create_crm, name='create_crm'),
    path('crm/update/<id>', update_crm, name='update_crm'),
    path('crm/delete/<id>', delete_crm, name='delete_crm'),

    # =====Parent========
    path('parent/list', parent_list, name='parent_list'),
    path('parent/create', create_parent, name='create_parent'),
    path('parent/update/<id>', update_parent, name='update_parent'),
    path('parent/delete/<id>', delete_parent, name='delete_parent'),
    # =====Pricing ========
    path('pricing/list', pricing_list, name='pricing_list'),
    path('pricing/create', create_pricing, name='create_pricing'),
    # path('parent/update', update_parent, name='update_parent'),
    # path('parent/delete', delete_parent, name='delete_parent'),
     
    # =====Contact ========
    path('contact/list', contact_list, name='contact_list'),
    path('contact/create', create_contact, name='create_contact'),
    # path('parent/update', update_parent, name='update_parent'),
    # path('parent/delete', delete_parent, name='delete_parent'),
    # ===== Category Entity ========
    path('category/entity/list', category_entity_list, name='category_entity_list'),
    #path('pricing/create', create_pricing, name='create_pricing'),
    # path('parent/update', update_parent, name='update_parent'),
    # path('parent/delete', delete_parent, name='delete_parent'),
    
    
    #======Auditor=====
    path('auditor/list',auditor_list,name='auditor_list'),
    path('auditor/create',create_auditor,name='create_auditor'),
    path('auditor/update',udpate_auditor,name='_auditor'),
    path('auditor/delete',delete_auditor,name='delete_auditor'),
    #=====Transporter====
    path('transporter/list',transporter_list,name='transporter_list'),
    path('transporter/create',create_transporter,name='create_transporter'),
    path('transporter/update',update_transporter,name='update_transporter'),
    path('transporter/delete',delete_transporter,name='delete_transporter'),
    # =====JSONAPI=======

    path('fetchid/', load_id, name='fetchid'),
    path('industryid/', base_onIndustry, name='base_onIndustry'),
    path('checkUnique/', check_unique, name='check_unique'),
    path('checkUniqueIndustry/', check_unique_industry, name='check_unique_industry'),
    path('checkUniqueProdName/', check_unique_prodname, name='check_unique_prodname'),
    path('checkUniqueProdAbbv/', check_unique_prodabbv, name='check_unique_prodabbrv'),
    path('checkUniqueProdSubName/', check_unique_prodsubname, name='check_unique_prodsubname'),
    path('checkUniqueProdSubAbbv/', check_unique_prodsubabbv, name='check_unique_prodsubabbrv'),
    path('checkUniqueEp/', check_unique_ep, name='check_unique_ep'),
    path('checkUniqueEt/', check_unique_et, name='check_unique_et'),
    path('fetchDistrict/', fetchdistrict_to, name='fetchdistrict_to'),
    path('fetchPincode/', fatch_pincode_to, name='fatch_pincode_to'),
    path('fetchlocality/', fatch_locality_to, name='fatch_locality_to'),
    path('fetchstate/', fetchstate_to, name='fetchstate_to'),
    #========Entity App Check Unique Value====
    path('entity_name/',entity_name,name='entity_name'),
    path('brandname/',brandname,name='brandname'),

    #====Entity Views====
    path('entity/view/<id>', view_entity, name="view_entity"),
    #====Address Views====
    path('servicelocation/view/<id>', view_address, name="view_address"),
    #====Brand Name Views====
    path('brand/view/<id>', view_brand, name="view_brand"),
    #====Export data ===#
    path('entity/export/', export_data_entity, name="export_data_entity"),
    path('brand/export/', export_data_brand, name="export_data_brand"),
    path('servicelocation/export',export_data_address,name='export_data_address'),
    path('parent/export',entity_parent,name='entity_parent'),
    path('crm/export',entity_crm,name='entity_crm'),
    #=====Upload File ======
    path('entity/brand/upload/file',upload_file_brand,name="upload_file_brand"),
    path('entity/servicelocation/upload/file',upload_file_address,name="upload_file_address"),
    path('entity/file',entity_file,name="entity_file"),

    #========DependencyField=======

    path("entity/transporter/dependency",tranporter,name="tranporter"),
    path("entity/pricing/dependency",productTocategory,name="productTocategory"),
    path("entity/aditor/dependency",auditor_auditor_user,name="auditor_auditor_user"),
    # path("entity/contact/dependency",auditor_contact,name="auditor_contact"),

    

]
