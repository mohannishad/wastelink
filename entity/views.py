from django.contrib import messages
from django.http import response,HttpResponse
from django.http.request import HttpRequest
from django.shortcuts import render, redirect
from os import name, path, walk
from django.utils.regex_helper import NonCapture
from fastapi import Query
import pandas as pd
from django.views.decorators.csrf import csrf_exempt
from python_graphql_client import GraphqlClient
import datetime
import pandas as pd
import csv
from django.http import HttpResponse
from entity.jsonapi import entity_name
from master.views import designation_list
from wastelink.settings import client

from requests.api import request
from django.utils.translation import gettext as _

# ======Entity======#
# request.session.GET['offset']=0
def entity_list(request):

    query1 = """

        query{
          masterEntityType{
            edges{
              node{

            id
            entityType
          }
            }
            }
            }

          """

    query2 = """query{
            masterEntityPurposes{
              edges{
                
              node{
              id
              entityPurpose
            }
          }
            }
          }

"""
    query3 = """query{
          masterIndustry{
            edges{
              node{
            id
            industry
          }
                }
            }
        }
  
"""
    entity_type_lists=client.execute(query=query1)['data']['masterEntityType']['edges']
    entity_purpose_lists=client.execute(query=query2)['data']['masterEntityPurposes']['edges']
    industry_lists=client.execute(query=query3)['data']['masterIndustry']['edges']
   

    #filter data 
    industry=None
    entity_purpose=None
    entity_type=None
    search_data=None
    
    if request.POST.get('filterdata')=='Filter':
      request.session['search_data']=request.POST.get('search_data')
      request.session['pgno']=1
      request.session['offset']=0
    
    if request.method=="POST":
      # print(ent)
      if request.POST.get('entity_purpose')==None:
        pass
      else:
        request.session['pgno']=1
        request.session['offset']=0
        request.session['entity_purpose']=request.POST.get('entity_purpose')  
        # request.session['entity_type']=request.POST.get('entity_type')
      if request.POST.get('entity_type')==None:
        pass
      else:
        request.session['pgno']=1
        request.session['offset']=0
        request.session['entity_type']=request.POST.get('entity_type')
        # request.session['industry']=request.POST.get('industry')
      if request.POST.get('industry')==None:
        pass
      else:
        request.session['pgno']=1
        request.session['offset']=0
        request.session['industry']=request.POST.get('industry')
      if request.POST.get('num')==None:
        pass
      else:
        request.session['num']=int(request.POST.get('num'))
        request.session['max_rows']=request.session['num']
        request.session['offset']=0
        request.session['pgno']=1
        


    query = """
    query($in:String, $ep:String, $et:String, $name:String, $of:Int, $f:Int, $l:Int, $order:String){
    entity(industry_Industry:$in, entityPurpose_EntityPurpose:$ep, orderBy:$order,
      entityType_EntityType:$et, name_Icontains:$name, offset:$of, first:$f, last:$l){
     edges{
      node{
        id
        name
        legalName
        code
        entityType{
          entityType
        }
        entityPurpose{
          entityPurpose
        }
        industry{
          industry
        }
        email
        phone
        abbrv
      }
    }
  }
}    """
    
    query1 = """
    query($in:String, $ep:String, $et:String, $name:String, $of:Int, $f:Int, $l:Int){
    entity(industry_Industry:$in, entityPurpose_EntityPurpose:$ep,
      entityType_EntityType:$et, name_Icontains:$name, offset:$of, first:$f, last:$l){
          edges{
            node{
              id
            }
          }
        }
      }  
    """
    


    last=None
    # print(request.session['sort'])
    # print('xe'*20)
    v1= {
      "of":None,
      "f":None, 
      "l":None,
      "in":request.session['industry'],
      "ep":request.session['entity_purpose'],
      "et":request.session['entity_type'],
      "name":request.session['search_data'],
      "order":request.session['sort']
    }
    
    entity_list1 = client.execute(query=query1, variables=v1)['data']['entity']['edges']
    total_records = len(entity_list1)
    
    if total_records%int(request.session['max_rows'])==0:
      max_pages = total_records//int(request.session['max_rows'])
    else:
      max_pages = total_records//int(request.session['max_rows'])+1

      #---Start Sorting--
    if request.POST.get('td_sorting')=="name":
      if request.session['sort']=='name':
        request.session['sort']='-name'
      else:
        request.session['sort']='name'
    if request.POST.get('td_sorting')=="lname":
      if request.session['sort']=='legal_name':
        request.session['sort']='-legal_name'
      else:
        request.session['sort']='legal_name'
    if request.POST.get('td_sorting')=="code":
      if request.session['sort']=='code':
        request.session['sort']='-code'
      else:
        request.session['sort']='code'
    if request.POST.get('td_sorting')=="etype":
      if request.session['sort']=='entity_type__entity_type':
        request.session['sort']='-entity_type__entity_type'
      else:
        request.session['sort']='entity_type__entity_type'
    if request.POST.get('td_sorting')=="epurpose":
      if request.session['sort']=='entity_purpose__entity_purpose':
        request.session['sort']='-entity_purpose__entity_purpose'
      else:
        request.session['sort']='entity_purpose__entity_purpose'
    if request.POST.get('td_sorting')=="indus":
      if request.session['sort']=='industry__industry':
        request.session['sort']='-industry__industry'
      else:
        request.session['sort']='industry__industry'
    if request.POST.get('td_sorting')=="abbs":
      if request.session['sort']=='abbrv':
        request.session['sort']='-abbrv'
      else:
        request.session['sort']='abbrv'
      #--End Sorting--

    if request.POST.get('rfilter')=="Clear":
      request.session['search_data']=None
      request.session['entity_purpose']=None
      request.session['entity_type']=None
      request.session['industry']=None
      request.session['num']=None
      request.session['max_rows']=10
      # request.session['num']=None
      # return redirect('entity_list')
    if request.POST.get('f')=="First":
      request.session['pgno']=1
      request.session['offset']=0
    if request.POST.get('d')=='Next':
      request.session['offset']+=int(request.session['max_rows'])
      request.session['pgno']+=1
    if request.POST.get('p')=='Previous':
      request.session['offset']-=int(request.session['max_rows'])
      request.session['pgno']-=1
      if request.session['offset']<0:
        request.session['offset']=0
        request.session['pgno']=1

    if request.POST.get('l')=='Last':
      last=total_records%int(request.session['max_rows'])
      request.session['pgno']=max_pages

      if total_records%int(request.session['max_rows'])==0:
        request.session['offset']=total_records-int(request.session['max_rows'])
      
      else:
        request.session['offset']=total_records-total_records%int(request.session['max_rows'])
   
    v= {
      "of":request.session['offset'],
      
      "f":request.session['max_rows'], 
      "l":last,
      "in":request.session['industry'],
      "ep":request.session['entity_purpose'],
      "et":request.session['entity_type'],
      "name":request.session['search_data'],
      "order":request.session['sort']
    }
   
    entity_list = client.execute(query=query, variables=v)['data']['entity']['edges']
    return render(request, path.join('entity', 'entity_list.html'), {"entity_list": entity_list,'entity_type_lists':entity_type_lists,'entity_purpose_lists':entity_purpose_lists,'industry_lists':industry_lists,'mo':request.session['offset'], 'pg':request.session['pgno'], 'tpg':max_pages,"total_records":total_records})


@csrf_exempt
def create_entity(request):
  
    query = """

        query{
          masterEntityType{
            edges{
              node{
            id
            entityType
          }
          }
          }
        }

          """

    query1 = """query{
  masterEntityPurposes{
    edges{
              node{
    id
    entityPurpose
  }
  }
  }
}"""
    query2 = """query{
  masterIndustry{
    edges{
              node{
    id
    industry
  }
  }
  }
}"""

    query3 = """
    
                query{
              baseEntities{
                id
                name
              }
            }
    
    """

    # Synchronous request
    entity_type = client.execute(query=query)['data']['masterEntityType']['edges']
    entity_purpose = client.execute(query=query1)['data']['masterEntityPurposes']['edges']
    entity_industry = client.execute(query=query2)['data']['masterIndustry']['edges']
    base_entities = client.execute(query=query3)['data']['baseEntities']

    if request.method == 'POST':
        entity_type = request.POST.get('entity_type')
        entity_purpose = request.POST.get('entity_purpose')
        industry = request.POST.get('industry')
        name = request.POST.get('name')
        legal_name = request.POST.get('legal_name')
        code = request.POST.get('code')
        emailid = request.POST.get('emailid')
        phone = request.POST.get('phone')
        abbrv = request.POST.get('abbrv')
        parent_entity = request.POST.get('parent_entity')
        base_entity = request.POST.get('base_entity')
        website=request.POST.get('website')
        crmid=request.POST.get('crmid')
         
        query = """

    mutation($entityType:Int, $abbrv:String, $code:String, $email: String,
$entityPurpose:Int, $industry:Int, $legalname:String, $name:String, $phone:String, $createdby:Int,$crmid:String,$website:String){
  createEntity(entityType:$entityType, entityPurpose:$entityPurpose, industry:$industry, name:$name, legalName:$legalname, code:$code, email:$email, phone:$phone, abbrv:$abbrv,website:$website,crmId:$crmid, createdBy:$createdby){
    ok
  }
}

        """

        variables = {
            "industry": industry,
            "entityType": entity_type,
            "abbrv": abbrv,
            "code": code+str(datetime.datetime.now())[:3],
            "email": emailid,
            "entityPurpose": entity_purpose,
            "legalname": legal_name,
            "phone": phone,
            "name": name,
            "website":website,
            "crmid":crmid,
            "createdby": 2
        }

        data = client.execute(query=query, variables=variables)['data']

        if entity_type == "1":
            querylastentity = """query{
                                          lastEntity{
                                            id
                                          }
                                        }"""

             
            last_entity = client.execute(query=querylastentity)['data']['lastEntity']['id']

            mutation = """

            mutation($entity: Int, $baseEntity:Int, $parentEntity:Int){
                      createEntityParent(entity:$entity, baseEntity:$baseEntity, parentEntity:$parentEntity){
                        ok
                      }
                    }

            """
            variables1 = {
                "entity": int(last_entity),
                "baseEntity": int(last_entity),
                "parentEntity": int(last_entity)
            }

            client.execute(query=mutation, variables=variables1)

        else:

            querylastentity = """query{
                                                      lastEntity{
                                                        id
                                                      }
                                                    }"""

             
            last_entity = client.execute(query=querylastentity)['data']['lastEntity']['id']

            mutation = """

                        mutation($entity: Int, $baseEntity:Int, $parentEntity:Int){
                                  createEntityParent(entity:$entity, baseEntity:$baseEntity, parentEntity:$parentEntity){
                                    ok
                                  }
                                }

                        """
            variables1 = {
                "entity": int(last_entity),
                "baseEntity": int(base_entity),
                "parentEntity": int(parent_entity)
            }

            client.execute(query=mutation, variables=variables1)

        return redirect('create_address')

    return render(request, path.join('entity', 'create_entity.html'),
                  {"entity_type": entity_type, "entity_purpose": entity_purpose, "entity_industry": entity_industry,
                   "base_entities": base_entities})


def update_entity(request, id):
     

    query0 = """
    
    query($id:Int!){
    entityById(id:$id){
    id
    name
    }
    }
    
    """

    query1 = """
    query($id:Int!){
  entityById(id:$id){
    id
    entityType{
        id
      entityType
    }
    entityPurpose{
        id
      entityPurpose
    }
    industry{
        id
      industry
    }
    name
    legalName
    code
    email
    phone
    abbrv
    createdBy{
      email
    }
  }
}
    """
    variables = {"id": id}

    query2 = """query{
              masterEntityType{
                edges{
                  node{
                id
                entityType
                }
                }
                  }
            }"""

    query3 = """query{
  masterEntityPurposes{
    edges{
                  node{
    id
    entityPurpose
  }
  }
  }
}"""

    query4 = """query{
  masterIndustry{
    edges{
                  node{
    id
    industry
  }
  }
  }
}"""

    query5 = """

                    query{
                  baseEntities{
                  
                    id
                    name
                  }
                }
               
                

        """
    query6 = """
    
            query($entity:Int!){
                  entityParentById(entity:$entity){
                    entity{
                    name
                    id
                    }
                    baseEntity{
                    name
                    id
                    }
                    parentEntity{
                    name
                    id
                    }
                  }
                }
            
            """
    var6 = {"entity": id}

    if request.method == 'POST':
        name = request.POST.get('name')
        legal_name = request.POST.get('legal_name')
        entity_purpose = request.POST.get('entity_purpose')
        entity_type = request.POST.get('entity_type')
        industry = request.POST.get('industry')
        base_entity = request.POST.get('base_entity')
        parent_entity = request.POST.get('parent_entity')
        code = request.POST.get('code')
        abbrv = request.POST.get('abbrv')
        emailid = request.POST.get('emailid')
        phone = request.POST.get('phone')
       
        mutation = """mutation($id:ID,$abbrv:String, $code:String,$email:String,$entityPurpose:Int,
        $entityType:Int,$industry:Int,$legalName:String,$name:String,$phone:String, $createdby:Int){
  updateEntity(id:$id, abbrv: $abbrv, code:$code, email:$email,
               entityPurpose:$entityPurpose, entityType:$entityType,
               industry:$industry, legalName:$legalName, name:$name, phone:$phone, createdBy:$createdby){
    ok
  }
}"""
        mvar = {
            "id": id,
            "abbrv": abbrv,
            "code": code,
            "email": emailid,
            "entityPurpose": entity_purpose,
            "entityType": entity_type,
            "industry": industry,
            "legalName": legal_name,
            "name": name,
            "phone": phone,
            "createdby": 2
        }

        client.execute(query=mutation, variables=mvar)

        if entity_type == '1':
            mutat1 = """mutation($cr:Int, $entity:Int, $baseEntity:Int, $parentEntity:Int){
                                  updateEntityParent(currentEntity:$cr, entity:$entity, baseEntity:$baseEntity, parentEntity:$parentEntity){
                                    ok
                                  }
                                }"""
            vars1 = {"cr": id, "entity": id, "baseEntity": id, "parentEntity": id}
            client.execute(query=mutat1, variables=vars1)

        else:
            mutat1 = """mutation($cr:Int, $entity:Int, $baseEntity:Int, $parentEntity:Int){
                          updateEntityParent(currentEntity:$cr, entity:$entity, baseEntity:$baseEntity, parentEntity:$parentEntity){
                            ok
                          }
                        }"""
            vars1 = {"cr": id, "entity": id, "baseEntity": base_entity, "parentEntity": parent_entity}
            client.execute(query=mutat1, variables=vars1)

        return redirect('entity_list')

    data = client.execute(query=query1, variables=variables)['data']['entityById']
    type = client.execute(query=query2)['data']['masterEntityType']['edges']
    purpose = client.execute(query=query3)['data']['masterEntityPurposes']['edges']
    industry = client.execute(query=query4)['data']['masterIndustry']['edges']
    base_entities = client.execute(query=query5)['data']['baseEntities']
    parent_base = client.execute(query=query6, variables=var6)['data']['entityParentById']
    # print(parent_base)
    # entity_details = parent_base.values()
    # parent_base_details = []

    # for i in list(entity_details):
    #     varss = {"id": int(i['id'])}
    #     parent_base_details.append(client.execute(query=query0, variables=varss)['data']['entityById'])
    #
    # print(parent_base_details[0])
    # print(parent_base_details[1])
    # print(parent_base_details[2])

    return render(request, path.join('entity', 'update_entity.html'),
                  {"data": data, "entity_type": type, "entity_purpose": purpose,
                   "entity_industry": industry, "base_entities": base_entities, "parent_base": parent_base, })


def delete_entity(request, id):
     

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                          deleteEntity(id:$id){
                            ok
                          }
                        }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('entity_list')

    query = """query($id:Int!)
    {
        entityById(id: $id){
        name
    id
    }
    }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['entityById']

    return render(request, path.join('entity', 'delete_entity.html'), {"data": data})


# =====Address========#
def address_list(request):
    if request.POST.get('Search')=="Search":
        searchAddress=request.POST.get('searchAddress')
        request.session['entityAddressSearch']=searchAddress
    

    if request.POST.get("entityAddressSize")==None:
       pass
    else:
       request.session['entityAddressSize']=int(request.POST.get('entityAddressSize'))
       request.session['entityAddressFirst']=request.session['entityAddressSize']
       request.session['entityAddressOffset']=0
       request.session['entityAddressCurrentPage']=1

    

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['entityAddressSearch']=None
      request.session['entityAddressSize']=None
      request.session['entityAddressFirst']=10 #defalt Value 

    # Create the query string and variables required for the request.
    query2="""
            query($search:String){
                  entityAddress(addressLabel_Icontains:$search){
                    edges{
                      node{
                        id
                      }
                    }
                  }
              }
        """
    td=client.execute(query=query2,variables={"search":request.session['entityAddressSearch']})['data']['entityAddress']['edges']
    
    total_records = len(td)
    if total_records%request.session['entityAddressFirst']==0:
      total_pages = total_records//request.session['entityAddressFirst']
    else:
      total_pages = total_records//request.session['entityAddressFirst']+1

    
    query = """

                query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  entityAddress(offset:$offset,first:$first,last:$last,addressLabel_Icontains:$search,orderBy:$orderBy){
                    edges{
                  node{
                    id
                    entity{
                      name
                    }
                    line1
                    line2
                    country
                    state
                    district
                    city
                    locality
                    pincode
                    addressLabel
                    }
                    }
                  }
                }

                      """

    # Synchronous request
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['entityAddressOffset']=0
      request.session['entityAddressCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['entityAddressOffset']-=request.session['entityAddressFirst']
      request.session['entityAddressCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['entityAddressOffset']+=request.session['entityAddressFirst']
      request.session['entityAddressCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['entityAddressCurrentPage']=total_pages
      if total_records%int(request.session['entityAddressFirst'])==0:
        last=total_records%int(request.session['entityAddressFirst'])+int(request.session['entityAddressFirst'])
        request.session['entityAddressOffset']=total_records-int(request.session['entityAddressFirst'])
      else:
        last=total_records%int(request.session['entityAddressFirst'])
        request.session['entityAddressOffset']=total_records-total_records%int(request.session['entityAddressFirst'])
    
    #====End Paginator====
    #========Start Sorting=======#
    
    if request.POST.get('Entity_Name_sorting')=="Entity Name":
      if request.session['entityAddressSorting']=='entity__name':
        request.session['entityAddressSorting']='-entity__name'
      else:
        request.session['entityAddressSorting']='entity__name'
    
    if request.POST.get('addressLabelSorting')=="Address Label":
      if request.session['entityAddressSorting']=='address_label':
        request.session['entityAddressSorting']='-address_label'
      else:
        request.session['entityAddressSorting']='address_label'

    #========End Sorting=========#
    v={
      "offset":request.session['entityAddressOffset'],
      "first":request.session['entityAddressFirst'],
      "last":last,
      "search":request.session['entityAddressSearch'],
      "orderBy":request.session['entityAddressSorting']
    }
    address_list = client.execute(query=query,variables=v)['data']['entityAddress']['edges']

    # page_number = request.GET.get('page')
    # page_obj = page_user(address_list, page_number)

    return render(request, path.join('entity', 'address_list.html'), {"entity_list": address_list,"max_page":total_pages})


def create_address(request):
     

    country = """
    
        query{
              masterPincode{
                country
              }
            }
    
    """

    countries = client.execute(query=country)['data']['masterPincode']

    querylastentity = """query{
                                              lastEntity{
                                                id
                                              }
                                            }"""

     
    last_entity = client.execute(query=querylastentity)['data']['lastEntity']['id']
    
    print(last_entity)
    if request.method == 'POST':
        addresslabel = request.POST.get('addresslabel')
        address1 = request.POST.get('address1')
        address2 = request.POST.get('address2')
        country = request.POST.get('country')
        state = request.POST.get('state')
        district = request.POST.get('district')
        city = request.POST.get('city')
        locality = request.POST.get('locality')
        pincode = request.POST.get('pincode')
        lat = request.POST.get('lat')
        long = request.POST.get('long')
        gst = request.POST.get('gst')
        # entity = request.POST.get('entity')

         
        query = """

        mutation($line1:String, $line2:String,$city:String,$country:String,$district:String,$entity:Int,$gst:String,
					$lat:String,$long:String,$locality:String,$pincode:String,$state:String, $addresslabel:String){
          createEntityAddress(line1:$line1,line2:$line2,city:$city,country:$country,district:$district,entity:$entity,gst:$gst,lat:$lat,
  										long:$long,locality:$locality,pincode:$pincode,state:$state,addressLabel:$addresslabel){
              ok
            }
}


            """

        variables = {
            "line1": address1,
            "line2": address2,
            "city": city,
            "country": country,
            "district": district,
            "entity": int(last_entity),
            "gst": gst,
            "lat": lat,
            "long": long,
            "locality": locality,
            "pincode": pincode,
            "state": state,
            "addresslabel": addresslabel
        }
        data = client.execute(query=query, variables=variables)['data']
        return redirect('create_contact')

    return render(request, path.join('entity', 'create_address.html'), {"country": countries})


def create_address1(request):
     

    country = """

        query{
              masterPincode{
                country
              }
            }

    """

    countries = client.execute(query=country)['data']['masterPincode']

    entityquery = """
        
        query{
              entity{
                edges{
                  node{

                  
                id
                name
              }
            }
            }
            }
        
    """

    entitydetails = client.execute(query=entityquery)['data']['entity']['edges']

    if request.method == 'POST':
        address1 = request.POST.get('address1')
        address2 = request.POST.get('address2')
        addresslabel = request.POST.get('addresslabel')
        country = request.POST.get('country')
        state = request.POST.get('state')
        district = request.POST.get('district')
        city = request.POST.get('city')
        locality = request.POST.get('locality')
        pincode = request.POST.get('pincode')
        lat = request.POST.get('lat')
        long = request.POST.get('long')
        gst = request.POST.get('gst')
        entity = request.POST.get('entity')

         
        query = """

         mutation($line1:String, $line2:String,$city:String,$country:String,$district:String,$entity:Int,$gst:String,
					$lat:String,$long:String,$locality:String,$pincode:String,$state:String, $addresslabel:String){
          createEntityAddress(line1:$line1,line2:$line2,city:$city,country:$country,district:$district,entity:$entity,gst:$gst,lat:$lat,
  										long:$long,locality:$locality,pincode:$pincode,state:$state,addressLabel:$addresslabel){
              ok
            }
}


            """

        variables = {
            "line1": address1,
            "line2": address2,
            "city": city,
            "country": country,
            "district": district,
            "entity": entity,
            "gst": gst,
            "lat": lat,
            "long": long,
            "locality": locality,
            "pincode": pincode,
            "state": state,
            "addresslabel": addresslabel
        }
        client.execute(query=query, variables=variables)
        return redirect('address_list')

    return render(request, path.join('entity', 'create_address1.html'), {"country": countries, "entity": entitydetails})


def update_address(request, id):
     

    query1 = """
                query($id:Int!){
          entityAddressById(id:$id){
            id
            line1
            line2
            country
            state
            district
            city
            locality
            lat
            long
            gst
            pincode
            entity{
              id
              name
            }
            addressLabel
          }
        }
        """

    variables = {"id": id}

    query2 = """
            query{
              entity{
                id
                name
              }
            }
    """

    details = client.execute(query=query1, variables=variables)['data']['entityAddressById']
    entities = client.execute(query=query2)['data']['entity']

    if request.method == 'POST':
        address1 = request.POST.get('address1')
        address2 = request.POST.get('address2')
        addresslabel = request.POST.get('addresslabel')
        country = request.POST.get('country')
        state = request.POST.get('state')
        district = request.POST.get('district')
        city = request.POST.get('city')
        locality = request.POST.get('locality')
        pincode = request.POST.get('pincode')
        lat = request.POST.get('lat')
        long = request.POST.get('long')
        gst = request.POST.get('gst')
        entity = request.POST.get('entity')

         
        query = """

            mutation($id:ID, $line1:String, $line2:String,$city:String,$country:String,$district:String,$entity:Int,$gst:String,
    					$lat:String,$long:String,$locality:String,$pincode:String,$state:String, $addresslabel:String){
              updateEntityAddress(id:$id, line1:$line1,line2:$line2,city:$city,country:$country,district:$district,entity:$entity,gst:$gst,lat:$lat,
      										long:$long,locality:$locality,pincode:$pincode,state:$state, addressLabel:$addresslabel){
                  ok
                }
    }

                """

        variables = {
            "id": id,
            "line1": address1,
            "line2": address2,
            "city": city,
            "country": country,
            "district": district,
            "entity": entity,
            "gst": gst,
            "lat": lat,
            "long": long,
            "locality": locality,
            "pincode": pincode,
            "state": state,
            "addresslabel": addresslabel
        }
        client.execute(query=query, variables=variables)
        return redirect('address_list')

    return render(request, path.join('entity', 'update_address.html'), {"details": details, "entities": entities})


def delete_address(request, id):
     

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                          deleteEntityAddress(id:$id){
                            ok
                          }
                        }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('address_list')

    query = """
    
        query($id:Int!){
                  entityAddressById(id:$id){
                    entitydetail{
                      name
                    }
                    
                  }
                }
        
    """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['entityAddressById']['entitydetail']['name']

    return render(request, path.join('entity', 'delete_address.html'), {"data": data})


# =====Brand========#
def brand_list(request):

    if request.POST.get('Search')=="Search":
        searchBrand=request.POST.get('searchBrand')
        request.session['entityBrandSearch']=searchBrand
    

    if request.POST.get("entityBrandSize")==None:
       pass
    else:
       request.session['entitySize']=int(request.POST.get('entityBrandSize'))
       request.session['entityBrandFirst']=request.session['entitySize']
       request.session['entityBrandOffset']=0
       request.session['entityBrandCurrentPage']=1

    

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['entityBrandSearch']=None
      request.session['entitySize']=None
      request.session['entityBrnadFirst']=10 #defalt Value  


  
    query = """
        query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
          entityBrand(offset:$offset,first:$first,last:$last,brand_Icontains:$search,orderBy:$orderBy){
            edges{
              node{     
                id
                brand
                entity{
                  id
                  name    
                }
              }
            }
          }
        }
    """
    last=None
    query2="""
                    query($search:String){
                entityBrand(brand_Icontains:$search){
                  edges{
                    node{     
                      id
          }  
                  }
                }
                }

          """
    

    td=client.execute(query=query2,variables={"search":request.session['entityBrandSearch']})['data']['entityBrand']['edges']
    total_records = len(td)
    if total_records%request.session['entityBrandFirst']==0:
      total_pages = total_records//request.session['entityBrandFirst']
    else:
      total_pages = total_records//request.session['entityBrandFirst']+1
     

    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['entityBrandOffset']=0
      request.session['entityBrandCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['entityBrandOffset']-=request.session['entityBrandFirst']
      request.session['entityBrandCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['entityBrandOffset']+=request.session['entityBrandFirst']
      request.session['entityBrandCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['entityBrandCurrentPage']=total_pages
      if total_records%int(request.session['entityBrandFirst'])==0:
        last=total_records%int(request.session['entityBrandFirst'])+int(request.session['entityBrandFirst'])
        request.session['entityBrandOffset']=total_records-int(request.session['entityBrandFirst'])
      else:
        last=total_records%int(request.session['entityBrandFirst'])
        request.session['entityBrandOffset']=total_records-total_records%int(request.session['entityBrandFirst'])
    
    
    #====End Paginator====
    #========Start Sorting=======#
    
    if request.POST.get('entity_sorting')=="Entity Name":
      if request.session['entityBrandSorting']=='entity__name':
        request.session['entityBrandSorting']='-entity__name'
      else:
        request.session['entityBrandSorting']='entity__name'
    
    if request.POST.get('brand_sorting')=="Brand":
      if request.session['entityBrandSorting']=='brand':
        request.session['entityBrandSorting']='-brand'
      else:
        request.session['entityBrandSorting']='brand'

    #========End Sorting=========#




    v={
      "offset":request.session['entityBrandOffset'],
      "first":request.session['entityBrandFirst'],
      "last":last,
      "search":request.session['entityBrandSearch'],
      "orderBy":request.session['entityBrandSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['entityBrand']['edges']

    return render(request, path.join('entity', 'brand_list.html'), {"entity_list":entity_list,"max_page":total_pages})


def create_brand(request):
     

    querylastentity = """query{
                                                  lastEntity{
                                                    id
                                                  }
                                                }"""

     
    last_entity = client.execute(query=querylastentity)['data']['lastEntity']['id']

    if request.method == 'POST':
        brandname = request.POST.get('brandname')
        # entity = request.POST.get('entity')

         
        query = """

            mutation($brand:String, $entity:Int){
                      createEntityBrand(brand:$brand, entity:$entity){
                        ok
                      }
                    }
    

                """

        variables = {
            "brand": brandname,
            "entity": int(last_entity)
        }

        client.execute(query=query, variables=variables)
        return redirect('create_transporter')

    return render(request, path.join('entity', 'create_brand.html'))


def create_brand1(request):
     

    query = """
    query{
            entity{
              edges{
                node{
                  id
                  name
                }
              }
            }
            }
    """

    entities = client.execute(query=query)['data']['entity']['edges']

    if request.method == 'POST':
        brandname = request.POST.get('brandname')
        entity = request.POST.get('entity')

        #  
        query = """

            mutation($brand:String, $entity:Int){
                      createEntityBrand(brand:$brand, entity:$entity){
                        ok
                      }
                    }
    

                """

        variables = {
            "brand": brandname,
            "entity": entity
        }

        client.execute(query=query, variables=variables)
        return redirect('brand_list')

    return render(request, path.join('entity', 'create_brand1.html'), {"entities": entities})


def update_brand(request, id):
     

    query = """
            query($id:Int!){
              entityBrandById(id:$id){
                id
                brand
                entity{
                  id
                  name
                }
              }
            }
        """
    varss = {"id": id}

    query1 = """query{
                  entity{
                    edges{
                      node{
                    id
                    name
                  }
                  }
                  }
                }"""
    if request.method == 'POST':
        brandname = request.POST.get('brandname')
        entity = request.POST.get('entity')
        query2 = """
                
                mutation($id:ID,$brand:String,$entity:Int){
                  updateEntityBrand(id:$id, brand:$brand, entity:$entity){
                    ok
                  }
                }    
                """
        vars1 = {"id": id, "brand": brandname, "entity": entity}
        client.execute(query=query2, variables=vars1)
        return redirect("brand_list")
    data = client.execute(query=query, variables=varss)['data']['entityBrandById']
    entities = client.execute(query=query1)['data']['entity']['edges']
    return render(request, path.join('entity', 'update_brand.html'), {"data": data, "e": entities})


def delete_brand(request, id):
     

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                              deleteEntityBrand(id:$id){
                                ok
                              }
                            }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('brand_list')

    query = """

            query($id:Int!){
                      entityBrandById(id:$id){
                        brand
                        
                      }
                    }

        """

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['entityBrandById']['brand']

    return render(request, path.join('entity', 'delete_brand.html'), {"data": data})


# =====Crm========#
def crm_list(request):
    if request.POST.get('Search')=="Search":
        crmEntity=request.POST.get('crmEntity')
        request.session['crmEntitySearch']=crmEntity
    

    if request.POST.get("crmEntitySize")==None:
       pass
    else:
       request.session['crmEntitySize']=int(request.POST.get('crmEntitySize'))
       request.session['crmEntityFirst']=request.session['crmEntitySize']
       request.session['crmEntityOffset']=0
       request.session['crmEntityCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['crmEntitySearch']=None
      request.session['crmEntitySize']=None
      request.session['crmEntityFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    entityCrm(entity_Name_Icontains:$search){
                      edges{
                        node{
                          id
                        
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['crmEntitySearch']})['data']['entityCrm']['edges']
    total_records = len(td)
    if total_records%request.session['crmEntityFirst']==0:
      total_pages = total_records//request.session['crmEntityFirst']
    else:
      total_pages = total_records//request.session['crmEntityFirst']+1
    query = """

                query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  entityCrm(offset:$offset,first:$first,last:$last,entity_Name_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                        owner
                     entity{
                          id
                          name
                        }
                         
                        source{
                          id
                          source
                        }
                        status{
                          id
                          status
                        }
                        
                    }
                  }
                }
                }
                
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['crmEntityOffset']=0
      request.session['crmEntityCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['crmEntityOffset']-=request.session['crmEntityFirst']
      request.session['crmEntityCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['crmEntityOffset']+=request.session['crmEntityFirst']
      request.session['crmEntityCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['crmEntityCurrentPage']=total_pages
      if total_records%int(request.session['crmEntityFirst'])==0:
        last=total_records%int(request.session['crmEntityFirst'])+int(request.session['crmEntityFirst'])
        request.session['crmEntityOffset']=total_records-int(request.session['crmEntityFirst'])
      else:
        last=total_records%int(request.session['crmEntityFirst'])
        request.session['crmEntityOffset']=total_records-total_records%int(request.session['crmEntityFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('entity')=="Entity":
      if request.session['crmEntitySorting']=='entity__name':
        request.session['crmEntitySorting']='-entity__name'
      else:
        request.session['crmEntitySorting']='entity__name'

    if request.POST.get('source')=="Source":
      if request.session['crmEntitySorting']=='source__source':
        request.session['crmEntitySorting']='-source__source'
      else:
        request.session['crmEntitySorting']='source__source'

    if request.POST.get('status')=="Status":
      if request.session['crmEntitySorting']=='status__status':
        request.session['crmEntitySorting']='-status__status'
      else:
        request.session['crmEntitySorting']='status__status'

    if request.POST.get('owner')=="Owner":
      if request.session['crmEntitySorting']=='owner':
        request.session['crmEntitySorting']='-owner'
      else:
        request.session['crmEntitySorting']='owner'

   
   

    #========End Sorting=========#
    v={
      "offset":request.session['crmEntityOffset'],
      "first":request.session['crmEntityFirst'],
      "search":request.session['crmEntitySearch'],
      "last":last,
      "orderBy":request.session['crmEntitySorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['entityCrm']['edges']
    return render(request, path.join('entity', 'crm_list.html'), {"entity_list": entity_list,"max_page":total_pages})

def create_crm(request):
  query = """

                query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  entityCrm(offset:$offset,first:$first,last:$last,entity_Name_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    
                     entity{
                          id
                          name
                        }
                         
                        source{
                          id
                          source
                        }
                        status{
                          id
                          status
                        }
                        
                    }
                  }
                }
                }
                
           """
  entity_list = client.execute(query=query)['data']['entityCrm']['edges']
  if request.method=="POST":
    entity=request.POST.get('entity')
    source=request.POST.get('source')
    status=request.POST.get('status')
    owner=request.POST.get('owner')
    print(entity,source,status,owner)
    
    return redirect("crm_list")
  return render(request, path.join('entity', 'create_crm.html'),{"entity_list":entity_list})


def update_crm(request,id):
    return render(request, path.join('entity', 'update_crm.html'))


def delete_crm(request,id):
    return render(request, path.join('entity', 'delete_crm.html'))


# =====Parent========#

def parent_list(request):
    if request.POST.get('Search')=="Search":
        parentEntity=request.POST.get('parentEntity')
        request.session['parentEntitySearch']=parentEntity
    

    if request.POST.get("parentEntitySize")==None:
       pass
    else:
       request.session['parentEntitySize']=int(request.POST.get('parentEntitySize'))
       request.session['parentEntityFirst']=request.session['parentEntitySize']
       request.session['parentEntityOffset']=0
       request.session['parentEntityCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['parentEntitySearch']=None
      request.session['parentEntitySize']=None
      request.session['parentEntityFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    entityParent(entity_Name_Icontains:$search){
                      edges{
                        node{
                          id
                        
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['parentEntitySearch']})['data']['entityParent']['edges']
    total_records = len(td)
    if total_records%request.session['parentEntityFirst']==0:
      total_pages = total_records//request.session['parentEntityFirst']
    else:
      total_pages = total_records//request.session['parentEntityFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  entityParent(offset:$offset,first:$first,last:$last,entity_Name_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                     entity{
                          id
                          name
                        }
                          baseEntity{
                            id
                            name
                            
                          }
                          parentEntity{
                            id
                            name
                          }
                    }
                  }
                }
                }
                
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['parentEntityOffset']=0
      request.session['parentEntityCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['parentEntityOffset']-=request.session['parentEntityFirst']
      request.session['parentEntityCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['parentEntityOffset']+=request.session['parentEntityFirst']
      request.session['parentEntityCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['parentEntityCurrentPage']=total_pages
      if total_records%int(request.session['parentEntityFirst'])==0:
        last=total_records%int(request.session['parentEntityFirst'])+int(request.session['parentEntityFirst'])
        request.session['parentEntityOffset']=total_records-int(request.session['parentEntityFirst'])
      else:
        last=total_records%int(request.session['parentEntityFirst'])
        request.session['parentEntityOffset']=total_records-total_records%int(request.session['parentEntityFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('entity')=="Entity":
      if request.session['parentEntitySorting']=='entity__name':
        request.session['parentEntitySorting']='-entity__name'
      else:
        request.session['parentEntitySorting']='entity__name'
    if request.POST.get('baseentity')=="Base Entity":
      if request.session['parentEntitySorting']=='base_entity__name':
        request.session['parentEntitySorting']='-base_entity__name'
      else:
        request.session['parentEntitySorting']='base_entity__name'

    if request.POST.get('parententity')=="Parent Entity":
      if request.session['parentEntitySorting']=='parent_entity__name':
        request.session['parentEntitySorting']='-parent_entity__name'
      else:
        request.session['parentEntitySorting']='parent_entity__name'

   
   

    #========End Sorting=========#
    v={
      "offset":request.session['parentEntityOffset'],
      "first":request.session['parentEntityFirst'],
      "search":request.session['parentEntitySearch'],
      "last":last,
      "orderBy":request.session['parentEntitySorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['entityParent']['edges']
    return render(request, path.join('entity', 'parent_list.html'), {"entity_list": entity_list,"max_page":total_pages})
   


def create_parent(request):
  query = """

                 query{
                  entityParent{
                    edges{
                      node{
                    id
                     entity{
                          id
                          name
                        }
                          baseEntity{
                            id
                            name
                            
                          }
                          parentEntity{
                            id
                            name
                          }
                    }
                  }
                }
                }
                
           """
  
  entity_list = client.execute(query=query)['data']['entityParent']['edges']
  if request.method=="POST":
    entity=request.POST.get('entity')
    baseentity=request.POST.get('baseentity')
    parententity=request.POST.get('parententity')
    query1="""
         mutation($entity:Int,$baseEntity:Int,$parentEntity:Int){
            createEntityParent(entity:$entity,baseEntity:$baseEntity,parentEntity:$parentEntity){
              ok
            }
          }
    
    """
    v={
      "entity": entity,
      "baseEntity": baseentity,
      "parentEntity": parententity

    }
    print(entity,baseentity,type(parententity))
    data=client.execute(query=query1,variables=v)

    return redirect("parent_list")
  return render(request, path.join('entity', 'create_parent.html'),{"entity_list":entity_list})


def update_parent(request,id):
    query = """
            query($id:Int!){
              entityParentById(entity:$id){
                
                entity{
                          id
                          name
                        }
                          baseEntity{
                            id
                            name
                            
                          }
                          parentEntity{
                            id
                            name
                          }
              }
            }
        """
    varss = {"id": id}



    data = client.execute(query=query, variables=varss)['data']['entityParentById']
    query2 = """

                 query{
                  entityParent{
                    edges{
                      node{
                    id
                     entity{
                          id
                          name
                        }
                          baseEntity{
                            id
                            name
                            
                          }
                          parentEntity{
                            id
                            name
                          }
                    }
                  }
                }
                }
                
           """
  
    entity_list = client.execute(query=query2)['data']['entityParent']['edges']
    if request.method == 'POST':
        entity=request.POST.get('entity')
        baseentity=request.POST.get('baseentity')
        parententity=request.POST.get('parententity')
        query3 = """
                
                mutation($id:Int,$baseEntity:Int,$entity:Int,$parentEntity:Int){
                  updateEntityParent(currentEntity:$id,baseEntity:$baseEntity,entity:$entity,parentEntity:$parentEntity){
                    ok
                  }
                }
                """
        vars1 = {"id": id, "baseEntity": baseentity, "entity": entity,"parentEntity":parententity}
        client.execute(query=query3, variables=vars1)
        return redirect("parent_list")
    
    return render(request, path.join('entity', 'update_parent.html'),{"data":data,"entity_list":entity_list})


def delete_parent(request,id):
    if request.method == 'POST':
        m = """mutation($id:ID){
                              deleteEntityParent(entity:$id){
                                ok
                              }
                            }"""
        mvars = {
            "id": id
        }
        client.execute(query=m, variables=mvars)
        return redirect('parent_list')




    query = """
            query($id:Int!){
              entityParentById(entity:$id){
                
                entity{
                          id
                          name
                        }
                          baseEntity{
                            id
                            name
                            
                          }
                          parentEntity{
                            id
                            name
                          }
              }
            }
        """
    varss = {"id": id}



    data = client.execute(query=query, variables=varss)['data']['entityParentById']
    return render(request, path.join('entity', 'delete_parent.html'),{'data':data})




#======Entity View=====#
def view_entity(request, id):
     


    query1 = """
    query($id:Int!){
  entityById(id:$id){
    id
    entityType{
        id
      entityType
    }
    entityPurpose{
        id
      entityPurpose
    }
    industry{
        id
      industry
    }
    name
    legalName
    code
    email
    phone
    abbrv
    createdBy{
      email
    }
  }
}
    """
    variables = {"id": id}

    query6 = """
    
            query($entity:Int!){
                  entityParentById(entity:$entity){
                    entity{
                    name
                    id
                    }
                    baseEntity{
                    name
                    id
                    }
                    parentEntity{
                    name
                    id
                    }
                  }
                }
            
            """
    var6 = {"entity": id}

    data = client.execute(query=query1, variables=variables)['data']['entityById']
    parent_base = client.execute(query=query6, variables=var6)['data']['entityParentById']

    return render(request, path.join('entity', 'view_entity.html'), {"data": data, "parent_base": parent_base})
    
    
#=======Address View=============


def view_address(request, id):
     

    query1 = """
                query($id:Int!){
          entityAddressById(id:$id){
            id
            line1
            line2
            country
            state
            district
            city
            locality
            lat
            long
            gst
            pincode
            entity{
              id
              name
            }
            addressLabel
          }
        }
        """

    variables = {"id": id}

    query2 = """
            query{
               entity{
                edges{
                  node{
                id
                name
                }
                }
              }
            }
    """

    details = client.execute(query=query1, variables=variables)['data']['entityAddressById']
    entities = client.execute(query=query2)['data']['entity']['edges']
    return render(request, path.join('entity', 'view_address.html'), {"details": details, "entities": entities})
  
#========View Brand Name=====

def view_brand(request, id):
     

    query = """
            query($id:Int!){
              entityBrandById(id:$id){
                id
                brand
                entity{
                  id
                  name
                }
              }
            }
        """
    varss = {"id": id}

    query1 = """query{
                  entity{
                    id
                    name
                  }
                }"""
    
    data = client.execute(query=query, variables=varss)['data']['entityBrandById']
    entities = client.execute(query=query1)['data']['entity']
    return render(request, path.join('entity', 'view_brand.html'), {"data": data, "e": entities})



 # ======Pricing List=======

def pricing_list(request):
 
    if request.POST.get('Search')=="Search":
        entityPricing=request.POST.get('entityPricing')
        request.session['entityPricingSearch']=entityPricing
    

    if request.POST.get("entityPricingSize")==None:
       pass
    else:
       request.session['entityPricingSize']=int(request.POST.get('entityPricingSize'))
       request.session['entityPricingFirst']=request.session['entityPricingSize']
       request.session['entityPricingOffset']=0
       request.session['entityPricingCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['entityPricingSearch']=None
      request.session['entityPricingSize']=None
      request.session['entityPricingFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                  entityPricing(entity_Name:$search){
                    edges{
                      node{
                        id
                        price
                      }
                    }
                  }
                }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['entityPricingSearch']})['data']['entityPricing']['edges']
    total_records = len(td)
    if total_records%request.session['entityPricingFirst']==0:
      total_pages = total_records//request.session['entityPricingFirst']
    else:
      total_pages = total_records//request.session['entityPricingFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  entityPricing(offset:$offset,first:$first,last:$last,entity_Name:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                        price
                        priceType
                        pricingType 
                        product
                        
                        {
                          id
                          name
                        }
                        subcategory{
                          id
                          name
                        }
                        validFrom
                        validUpto
                        category{
                          id
                          name
                        }
                        entity{
                          id
                          name
                        }
                    
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['entityPricingOffset']=0
      request.session['entityPricingCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['entityPricingOffset']-=request.session['entityPricingFirst']
      request.session['entityPricingCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['entityPricingOffset']+=request.session['entityPricingFirst']
      request.session['entityPricingCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['entityPricingCurrentPage']=total_pages
      if total_records%int(request.session['entityPricingFirst'])==0:
        last=total_records%int(request.session['entityPricingFirst'])+int(request.session['entityPricingFirst'])
        request.session['entityPricingOffset']=total_records-int(request.session['entityPricingFirst'])
      else:
        last=total_records%int(request.session['entityPricingFirst'])
        request.session['entityPricingOffset']=total_records-total_records%int(request.session['entityPricingFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    # if request.POST.get('productcompo')=="Product":
    #   if request.session['productCompositionSorting']=='product__name':
    #     request.session['productCompositionSorting']='-product__name'
    #   else:
    #     request.session['productCompositionSorting']='product__name'

    # if request.POST.get('baseProduct')=="Base Product":
    #   if request.session['productCompositionSorting']=='base_product__name':
    #     request.session['productCompositionSorting']='-base_product__name'
    #   else:
    #     request.session['productCompositionSorting']='base_product__name'

    # if request.POST.get('compositionunit')=="Composition Unit":
    #   if request.session['productCompositionSorting']=='composition_unit__unit':
    #     request.session['productCompositionSorting']='-composition_unit__unit'
    #   else:
    #     request.session['productCompositionSorting']='composition_unit__unit'

    # if request.POST.get('compositioncon')=="Composition Conversion":
    #   if request.session['productCompositionSorting']=='composition_conversion':
    #     request.session['productCompositionSorting']='-composition_conversion'
    #   else:
    #     request.session['productCompositionSorting']='composition_conversion'

    # if request.POST.get('compositionvalue')=="Composition Value":
    #   if request.session['productCompositionSorting']=='composition_value':
    #     request.session['productCompositionSorting']='-composition_value'
    #   else:
    #     request.session['productCompositionSorting']='composition_value'

    # if request.POST.get('calories')=="Calorie":
    #   if request.session['productCompositionSorting']=='calorie':
    #     request.session['productCompositionSorting']='-calorie'
    #   else:
    #     request.session['productCompositionSorting']='calorie'
    
   

    #========End Sorting=========#
    v={
      "offset":request.session['entityPricingOffset'],
      "first":request.session['entityPricingFirst'],
      "search":request.session['entityPricingSearch'],
      "last":last,
      "orderBy":request.session['entityPricingSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['entityPricing']['edges']
    return render(request, path.join('entity', 'pricing_list.html'), {"entity_list": entity_list,"max_page":total_pages})











def create_pricing(request):
  entity_query="""
      query{
            entity{
              edges{
                node{
                  id
                  name
                  entityType{
                    entityType
                  }
                }
              }
            }
          }
  """
  product_category="""
               query{
                    masterProductCategory{
                      edges{
                        node{
                          id
                          name
                        }
                      }
                    }
                  }
  """
  product_subcategory="""
               query{
                    masterProductSubcategory{
                      edges{
                        node{
                          id
                          name
                        }
                      }
                    }
                  }
  """
  product="""
               query{
                    product{
                      edges{
                        node{
                          id
                          name
                        }
                      }
                    }
                  }
  """
  entity_data=client.execute(query=entity_query)['data']['entity']['edges']
  product_category=client.execute(query=product_category)['data']['masterProductCategory']['edges']
  product_subcategory=client.execute(query=product_subcategory)['data']['masterProductSubcategory']['edges']
  product=client.execute(query=product)['data']['product']['edges']
  if request.method=="POST":
    entity=request.POST.get('entity')
    category=request.POST.get('category')
    subcategory=request.POST.get('subcategory')
    products=request.POST.get('product')
    price=request.POST.get('price')
    pricetype=request.POST.get('pricetype')
    validfrom=request.POST.get('validfrom')
    validupto=request.POST.get('validupto')
    pricingtypes=request.POST.get('pricingtypes')
    gstinclusive=request.POST.get('gstinclusive')
    pickup_freq=request.POST.get('pickup_freq')
    pickup_duration=request.POST.get('pickup_duration')
    estimated_qty_kg_in_duration=request.POST.get('estimated_qty_kg_in_duration')
    # print("entity"*10)
    # print(entity,"\n",category,"\n",subcategory,"\n",products,price,pricingtype,validfrom,validupto,pricingtypes)
    entity_pricing="""
              mutation($entity:Int,$category:Int,$price:Int,$priceType:Boolean,$pricingType:String,$product:Int,$subcategory:Int,$validFrom:Date,$validUpto:Date,$estimatedQtyKgInDuration:Float,$gstInclusive:Boolean,$pickupDuration:Int,$pickupFreq:Int){
              createEntityPricing(category:$category,entity:$entity,price:$price,priceType:$priceType,pricingType:$pricingType,product:$product,subcategory:$subcategory,validFrom:$validFrom,validUpto:$validUpto,
                estimatedQtyKgInDuration:$estimatedQtyKgInDuration,gstInclusive:$gstInclusive,pickupDuration:$pickupDuration,pickupFreq:$pickupFreq){
                ok 
              }
              }
    
    """
    variabl={
  
      "entity":53,
      "category":category,
      "price":price,
      "priceType":pricetype,
      "pricingType":pricingtypes,
      "product":products,
      "subcategory":subcategory,
      "estimatedQtyKgInDuration":estimated_qty_kg_in_duration,
      "gstInclusive": gstinclusive,
      "pickupDuration": pickup_duration,
      "pickupFreq": pickup_freq,
      "validFrom":"20211111",
      "validUpto":"20211111"
    }
    client.execute(query=entity_pricing,variables=variabl)
    return redirect("create_auditor")

  return render(request,path.join('entity','create_pricing.html'),{"entity_data":entity_data,"product_category":product_category,"product_subcategory":product_subcategory,"product":product})








def contact_list(request):

    if request.POST.get('Search')=="Search":
        entityContact=request.POST.get('entityContact')
        request.session['entityContactSearch']=entityContact
    

    if request.POST.get("entityContactSize")==None:
       pass
    else:
       request.session['entityContactSize']=int(request.POST.get('entityContactSize'))
       request.session['entityContactFirst']=request.session['entityContactSize']
       request.session['entityContactOffset']=0
       request.session['entityContactCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['entityContactSearch']=None
      request.session['entityContactSize']=None
      request.session['entityContactFirst']=3 #defalt Value 
    query2="""
                query($search:String){
                    entityContact(entity_Name_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['entityContactSearch']})['data']['entityContact']['edges']
    total_records = len(td)
    if total_records%request.session['entityContactFirst']==0:
      total_pages = total_records//request.session['entityContactFirst']
    else:
      total_pages = total_records//request.session['entityContactFirst']+1
    query = """

                query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  entityContact(offset:$offset,first:$first,last:$last,entity_Name_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                      id
                      address
                        {
                          id
                          addressLabel
                        }
                      altEmail
                      altMobile
                      bccEmail
                      ccEmail
                      designation
                        {
                          id 
                          designation
                        }
                      email
                      entity
                        {
                          id
                          name
                        }
                      firstName
                      gender
                      lastName
                      middleName
                      mobile
                      salutation
                        {
                          salutation
                          id
                        }
                       user{
                        id
                        username
                      }
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['entityContactOffset']=0
      request.session['entityContactCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['entityContactOffset']-=request.session['entityContactFirst']
      request.session['entityContactCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['entityContactOffset']+=request.session['entityContactFirst']
      request.session['entityContactCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['entityContactCurrentPage']=total_pages
      if total_records%int(request.session['entityContactFirst'])==0:
        last=total_records%int(request.session['entityContactFirst'])+int(request.session['entityContactFirst'])
        request.session['entityContactOffset']=total_records-int(request.session['entityContactFirst'])
      else:
        last=total_records%int(request.session['entityContactFirst'])
        request.session['entityContactOffset']=total_records-total_records%int(request.session['entityContactFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    # if request.POST.get('productcompo')=="Product":
    #   if request.session['productCompositionSorting']=='product__name':
    #     request.session['productCompositionSorting']='-product__name'
    #   else:
    #     request.session['productCompositionSorting']='product__name'

    # if request.POST.get('baseProduct')=="Base Product":
    #   if request.session['productCompositionSorting']=='base_product__name':
    #     request.session['productCompositionSorting']='-base_product__name'
    #   else:
    #     request.session['productCompositionSorting']='base_product__name'

    # if request.POST.get('compositionunit')=="Composition Unit":
    #   if request.session['productCompositionSorting']=='composition_unit__unit':
    #     request.session['productCompositionSorting']='-composition_unit__unit'
    #   else:
    #     request.session['productCompositionSorting']='composition_unit__unit'

    # if request.POST.get('compositioncon')=="Composition Conversion":
    #   if request.session['productCompositionSorting']=='composition_conversion':
    #     request.session['productCompositionSorting']='-composition_conversion'
    #   else:
    #     request.session['productCompositionSorting']='composition_conversion'

    # if request.POST.get('compositionvalue')=="Composition Value":
    #   if request.session['productCompositionSorting']=='composition_value':
    #     request.session['productCompositionSorting']='-composition_value'
    #   else:
    #     request.session['productCompositionSorting']='composition_value'

    # if request.POST.get('calories')=="Calorie":
    #   if request.session['productCompositionSorting']=='calorie':
    #     request.session['productCompositionSorting']='-calorie'
    #   else:
    #     request.session['productCompositionSorting']='calorie'
    
   

    #========End Sorting=========#
    v={
      "offset":request.session['entityContactOffset'],
      "first":request.session['entityContactFirst'],
      "search":request.session['entityContactSearch'],
      "last":last,
      "orderBy":request.session['entityContactSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['entityContact']['edges']
   
    return render(request, path.join('entity', 'contact_list.html'), {"entity_list": entity_list,"max_page":total_pages})
  


 # ======Category Entity=======
def category_entity_list(request):
    # if request.POST.get('Search')=="Search":
    #     categoryEntity=request.POST.get('categoryEntity')
    #     request.session['categoryEntitySearch']=categoryEntity
    

    # if request.POST.get("categoryEntitySize")==None:
    #    pass
    # else:
    #    request.session['categoryEntitySize']=int(request.POST.get('categoryEntitySize'))
    #    request.session['categoryEntityFirst']=request.session['categoryEntitySize']
    #    request.session['categoryEntityOffset']=0
    #    request.session['categoryEntityCurrentPage']=1


    # #=======Clear=========
    # if request.POST.get('Clear')=="Clear":
    #   request.session['productCompositionSearch']=None
    #   request.session['productCompositionSize']=None
    #   request.session['productCompositionFirst']=3 #defalt Value 
    # query2="""
    #             query($search:String){
    #                 productComposition(product_Name_Icontains:$search){
    #                   edges{
    #                     node{
    #                   id
    #                   }
    #                   }
    #                   }
    #                  }
     
    # """
    # td=client.execute(query=query2,variables={"search": request.session['productCompositionSearch']})['data']['productComposition']['edges']
    # total_records = len(td)
    # if total_records%request.session['productCompositionFirst']==0:
    #   total_pages = total_records//request.session['productCompositionFirst']
    # else:
    #   total_pages = total_records//request.session['productCompositionFirst']+1
    # query = """

    #              query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
    #               productComposition(offset:$offset,first:$first,last:$last,product_Name_Icontains:$search,orderBy:$orderBy){
    #                 edges{
    #                   node{
    #                 id
    #                 product {
    #                   id
    #                   name
    #                 }
    #                 baseProduct {
    #                   id
    #                   name
    #                 }
    #                 compositionUnit {
    #                   id
    #                   unit
    #                 }
    #                 compositionValue
    #                 compositionConversion
    #                 calorie
    #                 compositionUnit{
    #                   id
    #                   unit
    #                 }
    #               }
    #             }
    #             }
    #             }
    #        """
    # # Starting Paginator====
    # if request.POST.get('First')=="First":
    #   request.session['productCompositionOffset']=0
    #   request.session['productCompositionCurrentPage']=1
    # if request.POST.get('Previous')=="Previous":
    #   request.session['productCompositionOffset']-=request.session['productCompositionFirst']
    #   request.session['productCompositionCurrentPage']-=1
    # if request.POST.get('Next')=="Next":
    #   request.session['productCompositionOffset']+=request.session['productCompositionFirst']
    #   request.session['productCompositionCurrentPage']+=1
    # last=None
    # if request.POST.get('Last')=="Last":
    #   request.session['productCompositionCurrentPage']=total_pages
    #   if total_records%int(request.session['productCompositionFirst'])==0:
    #     last=total_records%int(request.session['productCompositionFirst'])+int(request.session['productCompositionFirst'])
    #     request.session['productCompositionOffset']=total_records-int(request.session['productCompositionFirst'])
    #   else:
    #     last=total_records%int(request.session['productCompositionFirst'])
    #     request.session['productCompositionOffset']=total_records-total_records%int(request.session['productCompositionFirst'])
    # #====End Paginator====


    # #========Start Sorting=======#
    
    # if request.POST.get('productcompo')=="Product":
    #   if request.session['productCompositionSorting']=='product__name':
    #     request.session['productCompositionSorting']='-product__name'
    #   else:
    #     request.session['productCompositionSorting']='product__name'

    # if request.POST.get('baseProduct')=="Base Product":
    #   if request.session['productCompositionSorting']=='base_product__name':
    #     request.session['productCompositionSorting']='-base_product__name'
    #   else:
    #     request.session['productCompositionSorting']='base_product__name'

    # if request.POST.get('compositionunit')=="Composition Unit":
    #   if request.session['productCompositionSorting']=='composition_unit__unit':
    #     request.session['productCompositionSorting']='-composition_unit__unit'
    #   else:
    #     request.session['productCompositionSorting']='composition_unit__unit'

    # if request.POST.get('compositioncon')=="Composition Conversion":
    #   if request.session['productCompositionSorting']=='composition_conversion':
    #     request.session['productCompositionSorting']='-composition_conversion'
    #   else:
    #     request.session['productCompositionSorting']='composition_conversion'

    # if request.POST.get('compositionvalue')=="Composition Value":
    #   if request.session['productCompositionSorting']=='composition_value':
    #     request.session['productCompositionSorting']='-composition_value'
    #   else:
    #     request.session['productCompositionSorting']='composition_value'

    # if request.POST.get('calories')=="Calorie":
    #   if request.session['productCompositionSorting']=='calorie':
    #     request.session['productCompositionSorting']='-calorie'
    #   else:
    #     request.session['productCompositionSorting']='calorie'
    
   

    # #========End Sorting=========#
    # v={
    #   "offset":request.session['productCompositionOffset'],
    #   "first":request.session['productCompositionFirst'],
    #   "search":request.session['productCompositionSearch'],
    #   "last":last,
    #   "orderBy":request.session['productCompositionSorting']
    # }
    # entity_list = client.execute(query=query,variables=v)['data']['productComposition']['edges']
    # return render(request, path.join('product', 'composition_list.html'), {"entity_list": entity_list,"max_page":total_pages})









   return render(request, path.join('entity', 'category_entity_list.html'))






def create_contact(request):

  salutations="""
                query{
              masterSalutation{
                edges{
                  node{
                    id
                    salutation
                  }
                }
              }
              }
  
  """

  designations="""
                query{
              masterDesignation{
                edges{
                  node{
                    id
                    designation
                  }
                }
              }
              }
  
  """
  entity_list="""
  query{
              entity{
                edges{
                  node{
                    id
                    name
                  }
                }
              }
              }
  
  """
  # query_lastentity="""
  #                   query{
  #                   lastEntity{
  #                     id
  #                   }
  #                 }
  # """
  salutation=client.execute(query=salutations)['data']['masterSalutation']['edges']
  designation=client.execute(query=designations)['data']['masterDesignation']['edges']
  entity=client.execute(query=entity_list)['data']['entity']['edges']
  # lastentity=client.execute(query=query_lastentity)
  if request.method=="POST":
    salutationss=request.POST.get('salutation')
    firstname=request.POST.get('firstname')
    middlename=request.POST.get('middlename')
    lastname=request.POST.get('lastname')
    designationss=request.POST.get('designation')
    email=request.POST.get('email')
    altemail=request.POST.get('altemail')
    ccemail=request.POST.get('ccemail')
    bbcemail=request.POST.get('bbcemail')
    mobile=request.POST.get('mobile')
    altmobile=request.POST.get('altmobile')
    g=request.POST.get('g')
    

    querys="""
    mutation($address: Int,$altEmail: String,$altMobile: Int,$bccEmail: String,$ccEmail: String,$designation: Int,$email: String,$entity: Int,$firstName: String,$gender: String,$lastName: String,$middleName: String,$mobile: Int,$salutation: Int,$user: Int){
      createEntityContact(address:$address,altEmail:$altEmail,altMobile:$altMobile,bccEmail:$bccEmail,ccEmail:$ccEmail,designation:$designation,email:$email,entity:$entity,firstName:$firstName,gender:$gender,lastName:$lastName,middleName:$middleName,mobile:$mobile,salutation:$salutation,user:$user){
        ok
      
    }
    }
    """
    v={
    "address":2,
    "altEmail":altemail,
    "altMobile":altmobile,
    "bccEmail":bbcemail,
    "ccEmail":ccemail,
    "designation":designationss,
    "email":email,
    "entity":53,
    "firstName":firstname,
    "gender":g,
    "lastName":lastname,
    "middleName":middlename,
    "mobile":mobile,
    "salutation":salutationss,
    "user":2
    }

    # client.execute(query=querys,variables=v)
    data=client.execute(query=querys, variables=v)['data']
    return redirect('create_pricing')
  return render(request,path.join('entity','create_contact.html'),{"salutation":salutation,"desig":designation,"entity":entity})

#=========Auditor=======

def auditor_list(request):
  query="""
        query{
      entityAuditor{
        edges{
          node{
            id
            isActive
            validFrom
            validUpto
            auditor{
              id
              name
            }
            entity{
              id
              name
            }
            auditorContact{
              id
              firstName
            }
            auditorUser{
              id
              username
              
            }
            
          }
        }
      }
      }
  
  """
  auditor_list=client.execute(query=query)['data']['entityAuditor']['edges']
  return render(request,path.join('entity','auditor_list.html'),{"auditor_list":auditor_list})
def create_auditor(request):
  query="""
      query{
      entity{
        edges{
          node{
            id
            name
          }
        }
      }
    }
  """
  entity_list=client.execute(query=query)['data']['entity']['edges']
  query_contact="""
   query{
        entityContact(entity_Id:39){
            edges{
            node{
                user{
                id
                username
                }
            }
            }
        }
        }
  
  """
  entity_contact=client.execute(query=query_contact)['data']['entityContact']['edges']
  return render(request,path.join('entity','create_auditor.html'),{'entity_list':entity_list,"entity_contact":entity_contact})
  
def udpate_auditor(request):
  return render(request,path.join('entity','udpate_auditor.html'))
def delete_auditor(request):
  return render(request,path.join('entity','delete_auditor.html'))
#=========End Auditor=====
#=========Transporter=====
def transporter_list(request):
  query="""
            query{
            entityTransporter{
              edges{
                node{
                  id
                  entity
                  {
                    id
                    name
                  }
                  rate
                  transporter{
                    id
                    name
                  }
                  isActive
                }
              }
            }
          }
  
  """
  entity_transapoter=client.execute(query=query)['data']['entityTransporter']['edges']
  return render(request,path.join('entity','transporter_list.html'),{"entity_transapoter":entity_transapoter})

def create_transporter(request):
  if request.method=="POST":
    entitys=request.POST.get("entity")
    transporter=request.POST.get("transporter")
    isactive=request.POST.get("isactive")
    rate=request.POST.get("rate")
    
    querys="""
    
      mutation($entity: Int,$isActive: Boolean,$rate: Float,$transporter: Int){
      createEntityTransporter (entity: $entity,isActive: $isActive,rate: $rate,transporter: $transporter){
        ok
      
      }
    }
    
    """
    v={
      "entity": entitys,
      "isActive":isactive,
      "rate":rate,
      "transporter":transporter  
      }
    
    client.execute(query=querys,variables=v)
    return redirect('create_brand')

  entity_list="""
                        query{
                        entity{
                        edges{
                          node{
                          
                          id
                            name
                        }
                      }
                      }
                      }
   """
  entity=client.execute(query=entity_list)['data']['entity']['edges']

  
  return render(request,path.join('entity','create_transporter.html'),{'entity_list':entity})
def update_transporter(request):
  return render(request,path.join('entity','update_transporter.html'))
def delete_transporter(request):
  return render(request,path.join('entity','delete_transporter.html'))
#========End Transporter====