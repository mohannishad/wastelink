from django.contrib import messages
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from wastelink.settings import client
import pandas as pd
def entity_file(request):
  if request.method == 'POST':
    if request.POST.get('uploadexcel')=='Upload': 
      excel_file = request.FILES["excel_files"]
      extension = excel_file.name.split('.')[1]
      if extension != 'xlsx':
        messages.info(request, 'Invalid extension. Please upload valid .xlsx extension file.')
        return redirect('entity_list')
      else:
        dataframe = pd.read_excel(excel_file)
        # print(dataframe)
        
        
        query = """
                query fetchEntityName{
                    entity{
                      edges{
                        node{
                          name
                          code
                        }
                      }
                      }
                    }
            """
        ename = client.execute(query=query)['data']['entity']['edges']
        entity_names_list = []
        entity_codes_list = []
        entityname_column_name = ''
        code_column_name = ''
        for k in ename:
            entity_names_list.append(k['node']['name'].lower())
            entity_codes_list.append(k['node']['code'].lower())

        for i in list(dataframe.columns.values):
         
          if i.lower() in 'entity name':
            entityname_column_name += i
            col_values = dataframe[i].tolist()
            for j in col_values:
                    if j.lower() in entity_names_list:
                        dataframe.drop(dataframe.loc[dataframe[entityname_column_name] == j].index, inplace=True)
                        messages.info(request, f'Entity name- {j} already exists')
          # if i.lower() in 'code':
          #       code_column_name += i
          #       col_values = dataframe[i].tolist()
          #       for j in col_values:
          #           if j.lower() in entity_codes_list:
          #               dataframe.drop(dataframe.loc[dataframe[code_column_name] == j].index, inplace=True)
          #               messages.info(request, f'Code {j} already exists')

          
        return redirect("entity_list")
   

def upload_file_brand(request):
  if request.method=="POST":
    if request.POST.get('uploadexcel')=="Upload":
      
      filename=request.FILES['excel_files']
      extension=filename.name.split('.')[1]
      if extension != 'xlsx':
        messages.info(request, 'Invalid extension. Please upload valid .xlsx extension file.')
        return redirect('brand_list')
      else:
        query="""
             query{
              entityBrand{
              entity {
             name
             id
                }
                Brand
                 id
                }
                }
              """
        brand_list=client.execute(query=query)['data']['entityBrand']
        print('e'*10)
        print(brand_list)
        df=pd.read_excel(filename)
        data_frame=pd.DataFrame(df)
        print(data_frame)
        # for i in list(data_frame.columns.values):
        #   print
    return redirect("brand_list")
  return redirect("brand_list")

def upload_file_address(request):
  if request.method=="POST":
    if request.POST.get('uploadexcel')=="Upload":
      
      filename=request.FILES['excel_files']
      extension=filename.name.split('.')[1]
      if extension != 'xlsx':
        messages.info(request, 'Invalid extension. Please upload valid .xlsx extension file.')
        return redirect('address_list')
      else:
        print('Address'*10)
       
        df=pd.read_excel(filename)
        data_frame=pd.DataFrame(df)
        print(data_frame)
  return redirect('address_list')