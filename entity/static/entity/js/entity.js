function validate_entity(){
  var name=document.forms['entity']['name'].value;
  //var legal_name=document.forms['entity']['legal_name'].value;
  var entity_purpose=document.forms['entity']['entity_purpose'].value;
  var entity_type=document.forms['entity']['entity_type'].value;
  var industry=document.forms['entity']['industry'].value;
  

  if(name==""){
      document.getElementById('name-m').innerHTML="This field cannot be blank";
      return false;
  }
//   if(legal_name==""){
//       document.getElementById('legal_name-m').innerHTML="This field cannot be blank";
//      return false;
//   }
  if(entity_purpose=="Please Select Entity Purpose"){
      document.getElementById('entity_purpose-m').innerHTML="This field is required";
      return false;
  }
  if(entity_type=="Please Select Entity Type"){
      document.getElementById('entity_type-m').innerHTML='This field is required';
      return false;
  }
  if(industry=="Please Select Industry"){
      document.getElementById('industry-m').innerHTML='This field is required';
      return false;
  }
}