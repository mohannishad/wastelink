function validate_address(){
  address1=document.forms['address']['address1'].value;
  country=document.forms['address']['country'].value;
  state=document.forms['address']['state'].value;
  district=document.forms['address']['district'].value;
  pincode=document.forms['address']['pincode'].value;
  entityname=document.forms['address']['entityname'].value;
  if(address1==""){
      document.getElementById('address1-m').innerHTML="This field cannot be blank";
      return false;
  }
  if(country=="Please Select Country"){
      document.getElementById('country-m').innerHTML="This field is required";
      return false;
  }
  if(state=="Please Select State"){
      document.getElementById('state-m').innerHTML="This field is required";
      return false;
  }
  if(district=="Please Select District"){
      document.getElementById('district-m').innerHTML="This field is required";
      return false;
  }
  if(pincode=="Please Select Pincode"){
      document.getElementById('pincode-m').innerHTML="This field is required";
      return false;
  }
  if(entityname=="Please Select Entity Name"){
      document.getElementById('entityname-m').innerHTML="This field is required";
      return false;
  }

}