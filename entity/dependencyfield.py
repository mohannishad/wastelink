from django.http import JsonResponse, response
from django.views.decorators.csrf import csrf_exempt
import json
from wastelink.settings import client


@csrf_exempt
def tranporter(request):
    if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        
      
        query9="""
        
            query($id: Int!){
                entityById(id:$id){
                id
                name
            }
            }
        
        """
        var= {"id": int(search)}
        entityid=client.execute(query=query9,variables=var)['data']['entityById']
        enti = []
        enti.append(entityid)
        return JsonResponse(enti, safe=False)

@csrf_exempt
def auditor_auditor_user(request):
    if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        
      
        query9="""
        
           query($id: Float){
                userss(entity_Id:$id,userType_Type:"Auditor"){
                    edges{
                    node{
                        username
                        id
                    }
                    }           
                }
                }
        
        """
        var= {"id": int(search)}
        auditorid=client.execute(query=query9,variables=var)['data']['userss']['edges']
        
        return JsonResponse(auditorid, safe=False)
# @csrf_exempt
# def auditor_contact(request):
#     if request.method == 'POST':
#         search = int(json.loads(request.body).get('search'))
        
      
#         query9="""
        
#            query{
#         entityContact(entity_Id:39){
#             edges{
#             node{
#                 user{
#                 id
#                 username
#                 }
#             }
#             }
#         }
#         }
        
#         """
#         var= {"id": int(search)}
#         auditorid=client.execute(query=query9,variables=var)['data']['userss']['edges']
        
        # return JsonResponse(auditorid, safe=False)
@csrf_exempt
def productTocategory(request):
    if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        query9="""
        
            query($id:Int!){
                productById(id:$id){
                    category {
                    id
                    name
                    }
                      subCategory {
                    id
                    name
                    }
                                            
                }
                }
        
        """
        var= {"id": int(search)}
        entityid=client.execute(query=query9,variables=var)['data']['productById']
        enti = []
        enti.append(entityid)
        return JsonResponse(enti, safe=False)