from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from wastelink.settings import client


@csrf_exempt
def fetchstate_to(request):
     

    if request.method == 'POST':
        search = json.loads(request.body).get('search')

        q = """
                    query($country:String!){
                          masterPincodeStateByCountry(country:$country){
                            state
                          }
                        }

        """

        v = {"country": search}

        query = client.execute(query=q, variables=v)['data']['masterPincodeStateByCountry']
        return JsonResponse(query, safe=False)


@csrf_exempt
def fetchdistrict_to(request):
     

    if request.method == 'POST':
        search = json.loads(request.body).get('search')

        q = """
                    query($state:String!){
                          masterPincodeDistrictByState(state:$state){
                            district
                          }
                        }
            
        """

        v = {"state": search}

        query = client.execute(query=q, variables=v)['data']['masterPincodeDistrictByState']
        return JsonResponse(query, safe=False)


@csrf_exempt
def fatch_pincode_to(request):
     

    if request.method == 'POST':
        search = json.loads(request.body).get('search')

        q = """
                    query($district:String!){
                          masterPincodePincodeByDistrict(district:$district){
                            pincode
                          }
                        }
            
        """

        v = {"district": search}

        query = client.execute(query=q, variables=v)['data']['masterPincodePincodeByDistrict']
        return JsonResponse(query, safe=False)


@csrf_exempt
def fatch_locality_to(request):
     

    if request.method == 'POST':
        search = json.loads(request.body).get('search')

        q = """
                    query($pincode:String!){
                          masterPincodeLocalityByPincode(pincode:$pincode){
                            locality
                          }
                        }
            
        """

        v = {"pincode": search}

        query = client.execute(query=q, variables=v)['data']['masterPincodeLocalityByPincode']
        return JsonResponse(query, safe=False)
