import csv
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from entity.views import brand_list
from wastelink.settings import client

def export_data_entity(request):
  
  query="""
       query{
 entity{
  edges{
      node{
        id
        name
        legalName
        code
        entityType{
          entityType
        }
        entityPurpose{
          entityPurpose
        }
        industry{
          industry
        }
        email
        phone
        abbrv
      }
    }
  }
  }
  
  """
 
  entity_list = client.execute(query=query)['data']['entity']['edges']
  entity_names = []
  entity_codes = []
  entity_id=[]
  entity_legal=[]
  entity_type=[]
  entity_purpose=[]
  entity_industry=[]
  entity_email=[]
  entity_phone=[]
  entity_abbrv=[]

  for i in entity_list:
    entity_id.append(i['node']['id'])
    entity_names.append(i['node']['name'])
    entity_legal.append(i['node']['legalName'])
    entity_codes.append(i['node']['code'])
    entity_type.append(i['node']['entityType'])
    entity_purpose.append(i['node']['entityPurpose'])
    entity_industry.append(i['node']['industry'])
    entity_email.append(i['node']['email'])
    entity_phone.append(i['node']['phone'])
    entity_abbrv.append(i['node']['abbrv'])
    
  

  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Enity.csv"'},
    )
  writer = csv.writer(response)
  writer.writerow(['Id', 'Name', 'Legal Name', 'Code','Entity Type','Entity Purpose','Industry','Email','Phone','Abbrv'])
  # writer.writerow([m])
  for i in range(len(entity_id)):
    writer.writerow([entity_id[i], entity_names[i], entity_legal[i], entity_codes[i], entity_type[i]['entityType'], entity_purpose[i]['entityPurpose'], entity_industry[i]['industry'], entity_email[i], entity_phone[i], entity_abbrv[i]])
  
  return response





def export_data_brand(request):
  query="""
          query{
          entityBrand{
            edges{
              node{
              entity {
                 name
                     }
                   brand
                   id
                   }
                   }
                  }
                          }
  """
  brand_list = client.execute(query=query)['data']['entityBrand']['edges']
  brand_name=[]
  entity_name=[]
  brand_id=[]

  for i in brand_list:
   brand_name.append(i['node']['brand'])
   brand_id.append(i['node']['id'])
   entity_name.append(i['node']['entity']['name'])
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Enity_Brand.csv"'},
    )
  writer = csv.writer(response)
  writer.writerow(['Id', 'Entity Name', 'Brand Name'])
  for i in range(len(brand_id)):
    writer.writerow([brand_id[i],entity_name[i],brand_name[i]])
  return response

def export_data_address(request):
  query="""query{
   entityAddress{
     edges{
       node{
   
    entity{
      name
    }
    addressLabel
    line1
    line2
    id
    country
    state
    district
    city
    locality
    pincode
    lat
    long
    gst
          }
         }
               
       }
     }
         """
  entity_name=[]
  address_label=[]
  address_id=[]
  line1=[]
  line2=[]
  country=[]
  state=[]
  district=[]
  city=[]
  locality=[]
  pincode=[]
  lat=[]
  long=[]
  gst=[]
  address_list=client.execute(query=query)['data']['entityAddress']['edges']
  for i in address_list:
    entity_name.append(i['node']['entity']['name'])
    address_label.append(i['node']['addressLabel'])
    address_id.append(i['node']['id'])
    line1.append(i['node']['line1'])
    line2.append(i['node']['line2'])
    country.append(i['node']['country'])
    state.append(i['node']['state'])
    district.append(i['node']['district'])
    city.append(i['node']['city'])
    locality.append(i['node']['locality'])
    pincode.append(i['node']['pincode'])
    lat.append(i['node']['lat'])
    long.append(i['node']['long'])
    gst.append(i['node']['gst'])
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Entity_Address.csv"'},
    )
  writer = csv.writer(response)
  writer.writerow(['Id', 'Entity Name', 'Address Label',"Line1","Line2","Country","State","District","City","Locality","Pincode","Latitude","Longitude","GST"])
  for i in range(len(address_id)):
    writer.writerow([address_id[i],entity_name[i],address_label[i],line1[i],line2[i],country[i],state[i],district[i],city[i],locality[i],pincode[i],lat[i],long[i],gst[i]])
  return response


def entity_parent(request):
  query = """

                 query{
                  entityParent{
                    edges{
                      node{
                    id
                     entity{
                          id
                          name
                        }
                          baseEntity{
                            id
                            name
                            
                          }
                          parentEntity{
                            id
                            name
                          }
                    }
                  }
                }
                }
                
           """
  id=[]
  entity=[]
  baseentity=[]
  parententity=[]
  entityparent=client.execute(query=query)['data']['entityParent']['edges']
  for i in entityparent:
    id.append(i['node']['id'])
    entity.append(i['node']['entity']['name'])
    baseentity.append(i['node']['baseEntity']['name'])
    parententity.append(i['node']['parentEntity']['name'])
    
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Entity_parent.csv"'},
    )
  writer = csv.writer(response)
  writer.writerow(['Id', 'Entity Name', 'Base Entity',"Parent  Entity"])
  for i in range(len(id)):
    writer.writerow([id[i],entity[i],baseentity[i],parententity[i]])
  return response
def entity_crm(request):
  query = """

                query{
                  entityCrm{
                    edges{
                      node{
                    id
                        owner
                     entity{
                          id
                          name
                        }
                         
                        source{
                          id
                          source
                        }
                        status{
                          id
                          status
                        }
                        
                    }
                  }
                }
                }
                
           """
  
  entity=[]
  source=[]
  status=[]
  owner=[]
  
  entityCrm=client.execute(query=query)['data']['entityCrm']['edges']
  for i in entityCrm:
   
    entity.append(i['node']['entity']['name'])
    source.append(i['node']['source']['source'])
    status.append(i['node']['status']['status'])
    owner.append(i['node']['owner'])
    
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="Entity_CRM.csv"'},
    )
  writer = csv.writer(response)
  writer.writerow([ 'Entity ', 'Source',"Status","Owner"])
  for i in range(len(entity)):
    writer.writerow([entity[i],source[i],status[i],owner[i]])
  return response
