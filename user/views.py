import csv
import datetime

from django.contrib import messages
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from os import path
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.hashers import make_password
from python_graphql_client import GraphqlClient

from core.utility_functions import page_user
from wastelink.settings import client
from .models import User
import random as r
def userlist(request):
  #=======Search==============
  if request.POST.get('Search')=="Search":
    userSearch=request.POST.get('searchUser')
    request.session['userSearch']=userSearch
    request.session['userCurrentPage']=1
    request.session['userOffset']=0
    
  #======End Search========
  #=======Filter==========
  if request.POST.get("userTypeFilter")==None:
    pass
  else:
    usertypefilter=request.POST.get('userTypeFilter')
    request.session['userTypeFilter']=usertypefilter
    request.session['userOffset']=0
    request.session['userCurrentPage']=1
  #---------Entity Filter
  if request.POST.get("userEntityFilter")==None:
    pass
  else:
    userentityfilter=request.POST.get('userEntityFilter')
    request.session['userEntityFilter']=userentityfilter
    request.session['userOffset']=0
    request.session['userCurrentPage']=1
    
  #=======End Filter======

   #=======Is Active Filter======
  if request.POST.get("userisActive")==None:
    pass
  elif request.POST.get("userisActive")=="True":
    userisActive=request.POST.get('userisActive')
    request.session['userisActive']=userisActive
    request.session['userOffset']=0
    request.session['userCurrentPage']=1
  else:
    # userisActive=request.POST.get('userisActive')
    request.session['userisActive']=False
    request.session['userOffset']=0
    request.session['userCurrentPage']=1
  #======Page Size======
  if request.POST.get("userSize")==None:
    pass
  else:
    request.session['userSize']=int(request.POST.get('userSize'))
    request.session['userFirst']=request.session['userSize']
    request.session['userOffset']=0
    request.session['userCurrentPage']=1

 
  #=======Clear=========
  if request.POST.get('Clear')=="Clear":
    request.session['userSearch']=None
    request.session['userSize']=None
    request.session['userTypeFilter']=None
    request.session['userEntityFilter']=None
    request.session['userisActive']=None
    request.session['userOffset']=0
    request.session['userFirst']=10 #defalt Value 
    
  #==========Start Filter =============
  query3="""
           query{
             masterUserTypes{
               edges{
                 node{
                   code
                   type
                 }
               }
             }
                       }
  
  """

  query4 = """

        query{
          entity{
            edges{
              node{
                id
                name
              }
            }
          }
        }
  
  """
  filterdata=client.execute(query=query3)['data']['masterUserTypes']['edges']
  entity_filter=client.execute(query=query4)['data']['entity']['edges']
  #==========End Filter================
  query2="""
    query($search:String,$isActive:Boolean,$userType_Type:String,$entity_Name:String){
                    userss(username_Icontains:$search,isActive:$isActive,userType_Type:$userType_Type,entity_Name:$entity_Name){
                      edges{
                        node{
                    
                      id
                    }
                  }
                  }
                  
                  }
  
  """
  vm={
    "isActive":request.session['userisActive'],
    "search": request.session['userSearch'],
    "userType_Type": request.session['userTypeFilter'],
    "entity_Name":request.session['userEntityFilter']
  }
  td=client.execute(query=query2,variables=vm)['data']['userss']['edges']
  total_records=len(td)
  query = """
             query($offset:Int,$first:Int,$last:Int,$search:String,$isActive:Boolean,$userType_Type:String,$entity_Name:String){
             userss(offset:$offset,first:$first,last:$last,username_Icontains:$search,isActive:$isActive,userType_Type:$userType_Type,entity_Name:$entity_Name){
              edges{
              node{
                  id
                  username
                firstName
                lastName
                role
                entity{
                  id
                  name
                }
                
                  email
                  mobile
                  userType 
                  {
                    code
                    type
                  }  
                isActive    
        }
        }
        }
       }
      """

  
 
  if total_records%request.session['userFirst']==0:
      total_pages = total_records//request.session['userFirst']
  else:
      total_pages = total_records//request.session['userFirst']+1
  
  if request.POST.get('First')=="First":
      request.session['userOffset']=0
      request.session['userCurrentPage']=1
  if request.POST.get('Previous')=="Previous":
      request.session['userOffset']-=request.session['userFirst']
      request.session['userCurrentPage']-=1
  if request.POST.get('Next')=="Next":
      request.session['userOffset']+=request.session['userFirst']
      request.session['userCurrentPage']+=1
  
  last=None
  if request.POST.get('Last')=="Last":
    request.session['userCurrentPage']=total_pages
    if total_records%int(request.session['userFirst'])==0:
      last=total_records%int(request.session['userFirst'])+int(request.session['userFirst'])
      request.session['userOffset']=total_records-int(request.session['userFirst'])
    else:
      last=total_records%int(request.session['userFirst'])
      request.session['userOffset']=total_records-total_records%int(request.session['userFirst'])

 
    #====End Paginator====

  v={
     "offset":request.session['userOffset'],
     "first":request.session['userFirst'],
     "search":request.session['userSearch'],
     "last":last,
     "isActive":request.session['userisActive'],
     "userType_Type": request.session['userTypeFilter'],
     "entity_Name":request.session['userEntityFilter']
  }
  entity_list = client.execute(query=query,variables=v)['data']['userss']['edges']
  query33="""
      query{
        usersCount

      }
  """
  max_rows=client.execute(query=query33)['data']['usersCount']
 
  return render(request, path.join('user', 'user_list.html'), {"entity_list": entity_list,"max_page":total_pages,"filterdata":filterdata,"entity_filter":entity_filter,"t_data":max_rows})




def create_user(request):
      
    query = """query{
              masterUserTypes{
                edges{
                  node{
                    code
                    type
                  }
                }
              }
            }
            """

    userTypes = client.execute(query=query)['data']['masterUserTypes']['edges']

    query1 = """query{
                  entity{
                    edges{
                      node{
                        id
                        name
                      }
                    }
                  }
                }"""

    entity = client.execute(query=query1)['data']['entity']['edges']

    if request.method == 'POST':
        fname = request.POST.get('fname')
        lname = request.POST.get('lname')
        email = request.POST.get('email')
        password = request.POST.get('password')
        role = request.POST.get('role')
        entitys = request.POST.get('entity')
        
        # password = make_password(request.POST.get('password'))
        try:
            if request.FILES['profile']:
                myprofile = request.FILES['profile']
        except Exception as e:
            myprofile = None
        profile = request.POST.get('profile')
        phone = request.POST.get('phone')
        altphone = request.POST.get('altphone')
        if altphone == '':
            altphone = 0

        # belongs = request.POST.get('belongs_to')
        user_type = request.POST.get('user_type')
        admin = request.POST.get('admin')
        if admin == None:
            admin = False
        active = request.POST.get('active')
        if active == None:
            active = False
        super = request.POST.get('super')
        if super == None:
            super = False
        staff = request.POST.get('staff')
        if staff == None:
            staff = False

        q = """mutation($entity:Int!,$role:String,$pp:String,$mob:String,$altmob:String,$utype:String,$em:String!,
				$uname:String!,$fname:String!,$lname:String!,$belongs:String!,$isadmin:Boolean!,
				$isactive:Boolean!,$isstaff:Boolean!,$issuperuser:Boolean!,$p1:String!,$p2:String!){
                  register(entity:$entity,role:$role,email:$em,username:$uname, password1:$p1,
                                password2:$p2, belongsTo:$belongs, isAdmin:$isadmin,isStaff:$isstaff, isActive:$isactive,
                                isSuperuser:$issuperuser, firstName:$fname, lastName:$lname,
                                    profilePhoto:$pp, mobile:$mob, altMobile:$altmob, userType:$utype){
                    token
                    refreshToken
                    errors
                    success
                  }
                }"""

        v = {"fname": fname,
             "lname": lname,
             "uname": fname + lname+str(r.randint(1,99)),
             "p1": password,
             "p2": password,
             "em": email,
             "mob": phone,
             "altmob": altphone,
             "pp": 'demoprofile',
             "isactive": active,
             "isstaff": staff,
             "isadmin": admin,
             "issuperuser": super,
             "utype": user_type,
             "belongs": "Belongs",
             "entity":entitys,
             "role":role
             }

        client.execute(query=q, variables=v)
        

        return redirect('userlist')
  
    return render(request, path.join('user', 'create_user.html'),{'userTypes': userTypes, "entity": entity})


def update_user(request, pk):
      
    query = """query{
              masterUserTypes{
                edges{
                  node{
                    code
                    type
                  }
                }
              }
            }"""

    userTypes = client.execute(query=query)['data']['masterUserTypes']['edges']

    query1 = """query{
                  entity{
                    edges{
                      node{
                        id
                        name
                      }
                    }
                  }
                }"""

    entity = client.execute(query=query1)['data']['entity']['edges']
  

    query2 = '''  query($id:Int){
                        userById(id:$id){
                        id
                        username
                        firstName
                        lastName
                        altMobile
                        email
                        role
                        mobile
                        entity{
                          id
                          name
                        }
                        userType {
                          code
                          type
                        }
                        belongsTo
                      }
                    }
                    '''
    v2 = {"id": pk}
    data = client.execute(query=query2, variables=v2)['data']['userById']
    
    if request.method == 'POST':
        fname = request.POST.get('fname')
        lname = request.POST.get('lname')
        email = request.POST.get('email')
        password = request.POST.get('password')
        # password = make_password(request.POST.get('password'))
        try:
            if request.FILES['profile']:
                myprofile = request.FILES['profile']
        except Exception as e:
            myprofile = None
        profile = request.POST.get('profile')
        phone = request.POST.get('phone')
        altphone = request.POST.get('altphone')
        if altphone == '':
            altphone = 0

        entitys = request.POST.get('entity')
        role = request.POST.get('role')
        user_type = request.POST.get('user_type')
        admin = request.POST.get('admin')

        if admin == None:
            admin = False
        active = request.POST.get('active')
        if active == None:
            active = False
        super = request.POST.get('super')
        if super == None:
            super = False
        staff = request.POST.get('staff')
        if staff == None:
            staff = False

        q = """mutation($id:ID!, $fname:String,$lname:String,$email:String,$mob:Int,$altmob:Int,
    				$pp:String,$isactive:Boolean,$isstaff:Boolean,$isadmin:Boolean,$issuperuser:Boolean,$entity:Int
    				$usertype:String,$belongsto:String,$role:String){
                  updateUser(id:$id, firstName:$fname, lastName:$lname, email:$email, mobile:$mob,role:$role,entity:$entity,
                                    altMobile:$altmob, profilePhoto:$pp, isActive:$isactive, isStaff:$isstaff,
                                    isAdmin:$isadmin, isSuperuser:$issuperuser, userType:$usertype, belongsTo:$belongsto){
                    ok

                  }
                }"""

        v = {"fname": fname,
             "lname": lname,
             "role":role,
             "entity":entitys,
             "email": email,
             "mob": phone,
             "altmob": altphone,
             "pp": "pro",
             "isactive": active,
             "isstaff": staff,
             "isadmin": admin,
             "issuperuser": super,
             "usertype": user_type,
             "belongsto": "belongsto",
             "id": pk
             }

        client.execute(query=q, variables=v)
        print("executed")

        return redirect('userlist')
    return render(request, path.join('user', 'update_user.html'),
                  {"data": data, "entity": entity, "userTypes": userTypes})

def view_user(request, pk):
      
    query = """query{
              masterUserTypes{
                edges{
                  node{
                    code
                    type
                  }
                }
              }
            }"""

    userTypes = client.execute(query=query)['data']['masterUserTypes']['edges']

    query1 = """query{
                  entity{
                    edges{
                      node{
                        id
                        name
                      }
                    }
                  }
                }"""

    entity = client.execute(query=query1)['data']['entity']['edges']
  

    query2 = '''  query($id:Int){
                        userById(id:$id){
                        id
                        username
                        firstName
                        lastName
                        altMobile
                        email
                        role
                        mobile
                        entity{
                          id
                          name
                        }
                        userType {
                          code
                          type
                        }
                        belongsTo
                      }
                    }
                    '''
    v2 = {"id": pk}
    data = client.execute(query=query2, variables=v2)['data']['userById']
    
    
    return render(request, path.join('user', 'userview.html'),
                  {"data": data, "entity": entity, "userTypes": userTypes})


def delete_user(request, id):

    if request.method == 'POST':
        mutation = """mutation($id:){
                              deleteUser(pk:$id){
                                    ok    
                              }
                            }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('userlist')

    query = """query($id:Int!)
            {
                userById(userId: $id){
                username
            }
            }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['userById']

    return render(request, path.join('user', 'delete_user.html'), {"data": data})

def user_export(request):
  query = """
             query{
             users{
              edges{
              node{
                pk
                username
                firstName
                lastName
                role
                entity{
                  id
                  name
                }
                
                  email
                  mobile
                  userType 
                  {
                    code
                    type
                  }  
                isActive 
        }
        }
        }
       }

      """
  query=client.execute(query=query)['data']['users']['edges']
  pk=[]
  username=[]
  firstname=[]
  lastname=[]
  role=[]
  entity=[]
 
  email=[]
  mobile=[]
  userType=[]   
  isActive=[]  
                                    
  
  for i in query:
    pk.append(i['node']['pk'])
    username.append(i['node']['username'])
    firstname.append(i['node']['firstName'])
    lastname.append(i['node']['lastName'])
    email.append(i['node']['email'])
    mobile.append(i['node']['mobile'])
    userType.append(i['node']['userType'])                                       
    role.append(i['node']['role'])                                       
    isActive.append(i['node']['isActive'])                                       
    entity.append(i['node']['entity'])
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="User_Details.csv"'},
    )
    
  writer = csv.writer(response)
  writer.writerow(['Id', 'UserName',"First Name","Last Name","Role","Email","isActive","Mobile","UserType","Entity"])
  for i in range(len(pk)):
    writer.writerow([ pk[i],username[i],firstname[i],lastname[i],role[i],email[i],isActive[i],mobile[i],userType[i],entity[i]])

  return response

#=========Active User======
def activeuser(request,id):
  query="""
        mutation($id:ID){
        deactivateUser(id:$id){
          ok
        }
      }
  
  """
  v={
      "id":id
  }
  client.execute(query=query,variables=v)['data']['deactivateUser']
  return redirect('userlist')
#=========InActive User======
def inactiveuser(request,id):
  query="""
        mutation($id:ID){
        activateUser(id:$id){
          ok
        }
      }
  
  """
  v={
      "id":id
  }
  client.execute(query=query,variables=v)['data']['activateUser']
  return redirect('userlist')