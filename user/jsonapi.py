from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from python_graphql_client import GraphqlClient


@csrf_exempt
def check_unique_email(request):
    client = GraphqlClient(endpoint="http://127.0.0.1:9000/graphql/")
    if request.method == 'POST':
        search = json.loads(request.body).get('search').title()

        query4 = """
                    query{
                      users{
   
                            edges{
      
                        node{
                   email
                          }
      
                              }
                          }
                               }
                """

        parent_entities = client.execute(query=query4)['data']['users']['edges']
        print('X'*30)
        # print(parent_entities)
        # print(parent_entities['email'])
        a = ''

        for i in parent_entities:
            if i['node']['email'].title() == search:
                
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)

@csrf_exempt
def check_unique_phone(request):
    client = GraphqlClient(endpoint="http://127.0.0.1:9000/graphql/")
    if request.method == 'POST':
        search = json.loads(request.body).get('search')
        query4 = """
                    query{
                      users{
                        mobile
                      }
                    }
                """

        parent_entities = client.execute(query=query4)['data']['allUsers']
        a = ''
        for i in parent_entities:
            if i['mobile'] == search:
                a = 'This value already exists!'

        return JsonResponse(a, safe=False)
