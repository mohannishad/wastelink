from os import name
from django.urls import path
from .views import activeuser, inactiveuser, userlist, create_user, update_user, delete_user,user_export, view_user
from .jsonapi import check_unique_email,check_unique_phone

urlpatterns = [
    path('list/', userlist, name='userlist'),
    path('register/', create_user, name='register'),
    path('update/<pk>', update_user, name='update_user'),
    path('view/<pk>', view_user, name='view_user'),
    path('delete/<id>', delete_user, name='delete_user'),
    path('user/active/<id>', activeuser, name='activeuser'),
    path('user/inactive/<id>', inactiveuser, name='inactiveuser'),
    path('user/export', user_export, name='user_export'),

    path('checkUniqueEmail/', check_unique_email, name='check_unique_email'),
    path('checkUniquePhone/', check_unique_phone, name='check_unique_phone'),

]
