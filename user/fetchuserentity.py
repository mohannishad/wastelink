from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from python_graphql_client import GraphqlClient


@csrf_exempt
def load_entity(request):
    client = GraphqlClient(endpoint="http://127.0.0.1:9000/graphql/")
    if request.method == 'POST':
        search = json.loads(request.body).get('search')
        query4 = """

            query($id:Int!){
                  entityByPurposeId(id:$id){
                    name
                  }
                }
        """

        variabless = {"id": search}
        parent_entities = client.execute(query=query4, variables=variabless)['data']['parentEntities']
        parent_entities.append({'entity': search})

        parent_en = []

        for i in parent_entities:
            a = int(i['entity'])
            query5 = """query($id: Int!){

                    entityById(id:$id){
                    id
                    name
                }
                }"""

            variables5 = {"id": a}

            parent_name = client.execute(query=query5, variables=variables5)['data']['entityById']
            parent_en.append(parent_name)

        return JsonResponse(parent_en, safe=False)
