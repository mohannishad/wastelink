function validate_user(){
  fname=document.forms['user']['fname'].value;
  lname=document.forms['user']['lname'].value;
  email=document.forms['user']['email'].value;
  phone=document.forms['user']['phone'].value;
  password=document.forms['user']['password'].value;
  cpassword=document.forms['user']['cpassword'].value;
  usertype=document.forms['user']['user_type'].value;
  belongsto=document.forms['user']['belongs_to'].value;
  
  if(fname==''){
      document.getElementById('fname-m').innerHTML="This field cannot be blank";
      return false;
  }
  if(lname==''){
      document.getElementById('lname-m').innerHTML="This field cannot be blank";
      return false;
  }
  if(email==''){
      document.getElementById('email-m').innerHTML="This field cannot be blank";
      return false;
  }
  if(phone==''){
      document.getElementById('phone-m').innerHTML="This field cannot be blank";
      return false;
  }
  if(password==''){
      document.getElementById('password-m').innerHTML="This field cannot be blank";
      return false;
  }
  if(cpassword==''){
      document.getElementById('cpassword-m').innerHTML="This field cannot be blank";
      return false;
  }
  if(password != cpassword){
      document.getElementById('cpassword-m').innerHTML="Passwords do not match";
      return false;
  }
  if(usertype=='Please Select User Type'){
      document.getElementById('usertype-m').innerHTML="This field is required";
      return false;
  }
  if(belongsto=='Please Select Belongs To'){
      document.getElementById('belongsto-m').innerHTML="This field is required";
      return false;
  }
  }