from django.db import models
from django.db.models import Q
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
# Create your models here.

class User(AbstractBaseUser, PermissionsMixin):
 
  username = models.CharField(max_length=100)
  first_name = models.CharField(max_length=150, blank=True)
  last_name = models.CharField(max_length=150, blank=True)
  email = models.EmailField(max_length=255, unique=True)
  profile_photo = models.ImageField(upload_to='profile/', null=True, blank=True)
  mobile = models.CharField(unique=True,max_length=14)
  alt_mobile = models.CharField(max_length=15, null=True, blank=True)
  is_active = models.BooleanField(default=True)
  is_admin = models.BooleanField(default=False)
  is_staff = models.BooleanField(default=False)
  user_type = models.CharField(max_length=50, null=True, blank=True)
  belongs_to = models.CharField(max_length=128)  # change mapped with id
  date_joined = models.DateTimeField(('date joined'), auto_now_add=True)
  # role = models.CharField(max_length=50)
  # role = models.ForeignKey(Group, on_delete=models.CASCADE)
  ac=['mobile','email']
  
  for i in ac:
   USERNAME_FIELD=i
  REQUIRED_FIELDS = ['username',]
  objects = UserManager()
  def get_short_name(self):
    return self.username
  def __unicode__(self):
    return self.email
  
  class Meta:
    ordering = ['-id']