from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
    path('', include('login.urls')),
    path('user/', include('user.urls')),
    path('vendor/', include('vendor.urls')),
    path('supplier/', include('supplier.urls')),
    path('', include('entity.urls')),
    path('', include('product.urls')),
    path('master/', include('master.urls')),
    path('',include('transaction.urls'))
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
    path('', include('login.urls')),
    path('user/', include('user.urls')),
    path('vendor/', include('vendor.urls')),
    path('supplier/', include('supplier.urls')),
    path('', include('entity.urls')),
    path('', include('product.urls')),
    path('master/', include('master.urls')),
    path('',include('transaction.urls'))
)
