from django.http import JsonResponse, response
from django.views.decorators.csrf import csrf_exempt
import json
from wastelink.settings import client

@csrf_exempt
def categorytosubcategory(request):
   if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        
        query9="""
              query($id:Float){
                      
          masterProductSubcategory(categoryId_Id:$id){
            edges{
              node{
                id
                name
              }
            }
          }
        }
        
        """
        var= {"id": int(search)}
        categorys=client.execute(query=query9,variables=var)['data']['masterProductSubcategory']['edges']
        return JsonResponse(categorys, safe=False)
@csrf_exempt
def entitytobrand(request):
   if request.method == 'POST':
        search = int(json.loads(request.body).get('search'))
        
        query9="""
              query($id:Float){
              entityBrand(entity_Id:$id){
                edges{
                  node{
                    id
                    brand
                  }
                }
              }
            }
        
        """
        var= {"id": int(search)}
        entitybrand=client.execute(query=query9,variables=var)['data']['entityBrand']['edges']
        print(entitybrand)
        return JsonResponse(entitybrand, safe=False)