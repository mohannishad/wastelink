from django.urls import path

from product.dependencyfield import categorytosubcategory, entitytobrand
from .views import product_list, create_product, update_product, delete_product
from .views import composition_list, create_composition, update_composition, delete_composition
from .views import unit_list, create_unit, update_unit, delete_unit
from .views import view_unit,view_composition,view_product
from .export_data import product_units_data,product_composition_data
urlpatterns = [
    # ====product===
    path('product/list', product_list, name='product_list'),
    path('product/create', create_product, name='create_product'),
    path('product/update/<id>', update_product, name='update_product'),
    path('product/delete/<id>', delete_product, name='delete_product'),
    # ====composition===
    path('product/composition/list', composition_list, name='composition_list'),
    path('product/composition/create', create_composition, name='create_composition'),
    path('product/composition/update/<id>', update_composition, name='update_composition'),
    path('product/composition/delete/<id>', delete_composition, name='delete_composition'),
    # ====unit===
    path('product/unit/list', unit_list, name='unit_list'),
    path('product/unit/create', create_unit, name='create_unit'),
    path('product/unit/update/<id>', update_unit, name='update_unit'),
    path('product/unit/delete/<id>', delete_unit, name='delete_unit'),


    #====view of all Pages
    path('product/unit/view/<id>',view_unit, name='view_unit'),
    path('product/composition/view/<id>',view_composition, name='view_composition'),
    path('product/view/<id>', view_product, name='view_product'),
    

    #========Export Data 
    path('product/unit/export',product_units_data,name="product_units_data"),
    path('product/composition/export',product_composition_data,name="product_composition_data"),
    
    #=======Dependency=====
    path('product/categorytosubcategory',categorytosubcategory,name="categorytosubcategory"),
    path('product/entitytobrand',entitytobrand,name='entitytobrand'),
]
