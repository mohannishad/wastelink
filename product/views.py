from functools import total_ordering
from django.shortcuts import render, redirect
from os import path
# ======product=======
from python_graphql_client import GraphqlClient
from wastelink.settings import client

def product_list(request):
     
    if request.POST.get('Search')=="Search":
        search=request.POST.get('products')
        request.session['productsSearch']=search
        request.session['productsOffset']=0
        request.session['productsCurrentPage']=1

    if request.POST.get("productsSize")==None:
       pass
    else:
       request.session['productsSize']=int(request.POST.get('productsSize'))
       request.session['productsFirst']=request.session['productsSize']
       request.session['productsOffset']=0
       request.session['productsCurrentPage']=1

    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['productsSearch']=None
      request.session['productsSize']=None
      request.session['productsFirst']=10 #defalt Value 
    
    query2 = """
   query($search: String,$entity_Name: String,$category_Name: String,$subCategory_Name: String,$brand_Brand: String,$taste_Taste: String,$state_State: String,$nutrition_Type: String,$unitType_MeasureType: String,$nextStep_NextStep: String){
  product(
    
    name_Icontains: $search,
entity_Name: $entity_Name,
category_Name: $category_Name,
subCategory_Name: $subCategory_Name,
brand_Brand: $brand_Brand,
taste_Taste: $taste_Taste,
state_State: $state_State,
nutrition_Type:$nutrition_Type,
unitType_MeasureType:$unitType_MeasureType,
nextStep_NextStep:$nextStep_NextStep
  ){
    edges{
      node{
        id
      }
    }
  }
}
              

                  """
    td=client.execute(query=query2,variables={"search": request.session['productsSearch']})['data']['product']['edges']
    total_records = len(td)
    if total_records%request.session['productsFirst']==0:
      total_pages = total_records//request.session['productsFirst']
    else:
      total_pages = total_records//request.session['productsFirst']+1
    
    query = """
                    query($offset:Int,$first:Int,$orderBy:String,$search: String,$entity_Name: String,$category_Name: String,$subCategory_Name: String,$brand_Brand:String){
          product(offset:$offset,first:$first,orderBy:$orderBy,name_Icontains: $search,entity_Name: $entity_Name,category_Name: $category_Name,subCategory_Name: $subCategory_Name,brand_Brand:$brand_Brand){
                      edges{
                        node{
                          id
                          name
                          code
                          brand{
                            id
                            brand
                          }
                          entity{
                            id
                            name
                          }
                          category{
                            id
                            name
                          }
                          subCategory{
                            id
                            name
                          }
                                }
                              }
                            }
                          }            

                  """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['productsOffset']=0
      request.session['productsCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['productsOffset']-=request.session['productsFirst']
      request.session['productsCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['productsOffset']+=request.session['productsFirst']
      request.session['productsCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['productsCurrentPage']=total_pages
      if total_records%int(request.session['productsFirst'])==0:
        last=total_records%int(request.session['productsFirst'])+int(request.session['productsFirst'])
        request.session['productsOffset']=total_records-int(request.session['productsFirst'])
      else:
        last=total_records%int(request.session['productsFirst'])
        request.session['productsOffset']=total_records-total_records%int(request.session['productsFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    # if request.POST.get('id')=="Id":
    #   if request.session['productsSorting']=='id':
    #     request.session['productsSorting']='-id'
    #   else:
    #     request.session['productsSorting']='id'

    # if request.POST.get('status')=="Status":
    #   if request.session['productsSorting']=='status':
    #     request.session['productsSorting']='-status'
    #   else:
    #     request.session['productsSorting']='status'
    #========End Sorting=========#
    v2={
      "offset":request.session['productsOffset'],
      "first":request.session['productsFirst'],
      "search":request.session['productsSearch'],
      "last":last,
      "orderBy":request.session['productsSorting']
    }

    entity_list = client.execute(query=query,variables=v2)['data']['product']['edges']

    maxrows="""
                  query{
                      productCount
                    }
                """
    maxrowslist=client.execute(query=maxrows)['data']['productCount']
    return render(request, path.join('product', 'product_list.html'), {"entity_list": entity_list,"max_page":total_pages,"maxrowslist":maxrowslist})


def create_product(request):
     
    productpacking="""
                    query{
                  masterProductPackaging{
                    edges{
                      node{
                        id
                        packagingType
                      }
                    }
                  }
                }
    
    """

    packinglist=client.execute(query=productpacking)['data']['masterProductPackaging']['edges']
    categoryquery = """
            query{
          masterProductCategory{
            edges{
              node{
            id
            name
          }
          }
          }
        }
    """
    subcategoryquery = """
            query{
          masterProductSubcategory{
            edges{
              node{
            id
            name
          }
          }
          }
        }
    """

    entityquery = """
    
        query{
        entity{
          edges{
            node{
        id
        name
        }
        }
        }
        }
        
    """

    brandquery = """query{
          entityBrand{
            edges{
              node{
            id
            brand
            }
            }
          }
        }"""

    statequery = """query{
          masterProductState{
            edges{
              node{
            id
            state
            }
            }
          }
        }"""

    tastequery = """query{
          masterProductTaste{
            edges{
              node{
            id
            taste
            }
            }
          }
        }"""
    nutritionquery = """query{
          masterProductNutritionType{
            edges{
              node{
            id
            type
            }
            }
          }
        }"""

    unittypequery = """query{
          masterProductMeasureType{
            edges{
              node{
            id
            measureType
            }
            }
          }
        }"""

    unitquery = """query{
          masterProductMeasureUnit{
            edges{
              node{
            id
            unit
          }
          }
          }
        }"""

    nextstepquery = """query{
          masterProductNextStep{
            edges{
              node{
            id
            nextStep
            }
            }
          }
        }"""

    categorydata = client.execute(query=categoryquery)['data']['masterProductCategory']['edges']
    subcategorydata = client.execute(query=subcategoryquery)['data']['masterProductSubcategory']['edges']
    entitydata = client.execute(query=entityquery)['data']['entity']['edges']
    branddata = client.execute(query=brandquery)['data']['entityBrand']['edges']
    statedata = client.execute(query=statequery)['data']['masterProductState']['edges']
    tastedata = client.execute(query=tastequery)['data']['masterProductTaste']['edges']
    nutritiondata = client.execute(query=nutritionquery)['data']['masterProductNutritionType']['edges']
    unittypedata = client.execute(query=unittypequery)['data']['masterProductMeasureType']['edges']
    unitdata = client.execute(query=unitquery)['data']['masterProductMeasureUnit']['edges']
    nextstepdata = client.execute(query=nextstepquery)['data']['masterProductNextStep']['edges']

    if request.method == 'POST':
        category = request.POST.get('category')
        subcategory = request.POST.get('subcategory')
        entity = request.POST.get('entity')
        brand = request.POST.get('brand')
        name = request.POST.get('name')
        code = request.POST.get('code')
        buysell = request.POST.get('buysell')
        state = request.POST.get('state')
        taste = request.POST.get('taste')
        nutrition = request.POST.get('nutrition')
        unittype = request.POST.get('unittype')
        unit = request.POST.get('unit')
        unitvalue = request.POST.get('unitvalue')
        prod_cgst=request.POST.get('prod_cgst')
        prod_sgst=request.POST.get('prod_sgst')
        prod_igst=request.POST.get('prod_igst')
        prohsn=request.POST.get('prohsn')
        packagingtype=request.POST.get('packagingtype')



        if unitvalue == '':
            unitvalue = 0.0
        denominator = request.POST.get('denominator')
        if denominator == '':
            denominator = 0.0
        aunit = request.POST.get('aunit')
        numerator = request.POST.get('numerator')
        if numerator == '':
            numerator = 0.0
        bunit = request.POST.get('bunit')
        itemvalue = request.POST.get('itemvalue')
        if itemvalue == '':
            itemvalue = 0.0
        mrpinm = request.POST.get('mrpinm')
        if mrpinm == '':
            mrpinm = 0.0
        unitinsidepacs = request.POST.get('unitinsidepacs')
        if unitinsidepacs == '':
            unitinsidepacs = 0.0
        metricsystem = request.POST.get('metricsystem')
        length = request.POST.get('length')
        if length == '':
            length = 0.0
        metricunittype = request.POST.get('metricunittype')
        grossweight = request.POST.get('grossweight')
        if grossweight == '':
            grossweight = 0.0
        moisture = request.POST.get('moisture')
        if moisture == '':
            moisture = 0.0
        y = request.POST.get('yield')
        if y == '':
            y = 0.0
        buyprice = request.POST.get('buyprice')
        if buyprice == '':
            buyprice = 0.0
        sellprice = request.POST.get('sellprice')
        if sellprice == '':
            sellprice = 0.0
        nextstep = request.POST.get('nextstep')
        isbase = request.POST.get('isbase')
        if isbase == None:
            isbase = False
        isveg = request.POST.get('isveg')
        if isveg == None:
            isveg = False
        unitcalorie = request.POST.get('unitcalorie')
        if unitcalorie == '':
            unitcalorie = 0.0
        barcode = request.POST.get('barcode')

         

        query = """

         mutation(
         $prodCgst: Float,$prodHsn: String
          $prodIgst: Float,
          $prodSgst: Float, $packagingType:Int,$category:Int,$subCategory:Int,$entity:Int,$brand:Int,$name:String,$code:String,$buysell:String,$state:Int,$taste:Int,$nutrition:Int,
          $unitType:Int,$unit:Int,$unitValue:Float,$denominator:Float,$aunit:String,$numerator:Float,$bunit:String,$itemValue:Float,$mrpInM:Float,
            $unitInsidePacs:Float,$metricSystem:String,$length:Float,$grossWeight:Float,$metricUnitType:String,$moisture:Float,$y:Float,$buyPrice:Float,
            $sellPrice:Float,$nextStep:Int,$isBase:Boolean,$isVeg:Boolean,$unitCalorie:Float,$barcode:String){
          createProduct(
            prodHsn:$prodHsn,
            prodCgst: $prodCgst,
              prodIgst:$prodIgst,
              prodSgst: $prodSgst,
            packagingType:$packagingType,
            category:$category,subCategory:$subCategory,entity:$entity,brand:$brand,name:$name,code:$code,buysell:$buysell,state:$state,taste:$taste,
            nutrition:$nutrition,unitType:$unitType,unit:$unit,unitValue:$unitValue,denominator:$denominator,aunit:$aunit,numerator:$numerator,bunit:$bunit,
            itemValue:$itemValue,mrpInM:$mrpInM,unitInsidePacs:$unitInsidePacs,metricSystem:$metricSystem,length:$length,grossWeight:$grossWeight,
            metricUnitType:$metricUnitType,moisture:$moisture,yieldField:$y,buyPrice:$buyPrice,sellPrice:$sellPrice,nextStep:$nextStep,isBase:$isBase,isVeg:$isVeg,
            unitCalorie:$unitCalorie,barcode:$barcode){
            ok
          }
        }

        """

        variables = {"category": category, "subCategory": subcategory, "entity": entity, "brand": brand, "name": name,
                     "code": code, "buysell": buysell, "state": state, "taste": taste, "nutrition": nutrition,
                     "unitType": unittype, "unit": unit, "unitValue": unitvalue, "denominator": denominator,
                     "aunit": aunit,
                     "numerator": numerator, "bunit": bunit, "itemValue": itemvalue, "mrpInM": mrpinm,
                     "unitInsidePacs": unitinsidepacs,
                     "metricSystem": metricsystem, "length": length, "grossWeight": grossweight,
                     "metricUnitType": metricunittype, "moisture": moisture, "y": y, "buyPrice": buyprice,
                     "sellPrice": sellprice, "nextStep": nextstep,
                     "isBase": isbase, "isVeg": isveg, "unitCalorie": unitcalorie, "barcode": barcode,
                     "prodCgst":prod_cgst,
                     "prodSgst":prod_sgst,
                     "prodIgst":prod_igst,
                     "prodHsn":prohsn,
                     "packagingType":packagingtype
                     
                     }

        client.execute(query=query, variables=variables)

        return redirect('product_list')
    return render(request, path.join('product', 'create_product.html'), {"category": categorydata,
                                                                         "subcategory": subcategorydata,
                                                                         "entity": entitydata, "brand": branddata,
                                                                         "state": statedata, "taste": tastedata,
                                                                         "nutrition": nutritiondata,
                                                                         "unittype": unittypedata,
                                                                         "unit": unitdata, "nextstep": nextstepdata,"packinglist": packinglist})


def update_product(request, id):
    
    productquery = """
      query($id:Int!){
      productById(id:$id){
              id
              prodHsn
              prodCgst
              prodSgst
              prodIgst
              packagingType{
                  
                id
                packagingType
              }
              category{
                  id
                  name
                }
              subCategory{
                    id
                    name
                    }
              entity{id
              name}
              brand{
              id
              brand}
              name
              code
              buysell
              state{
              id
              state}
              taste{
              id
              taste}
              nutrition{
              id
              type}
              unitType{
              id
              measureType}
              unit{
              id
              unit}
              unitValue
              denominator
              aunit
              numerator
              bunit
              itemValue
              mrpInM
              unitInsidePacs
              metricSystem
              grossWeight
              metricUnitType
              length
              moisture
              yieldField
              buyPrice
              sellPrice
              nextStep{
              id
              nextStep}
              isBase
              isVeg
              unitCalorie
              barcode
            }
          }
          """

    varss1 = {"id": id}
    categoryquery = """
            query{
              masterProductCategory{
                edges{
                  node{
                id
                name
                }
                }
          }
        }
    """
    subcategoryquery = """
            query{
          masterProductSubcategory{
            edges{
              node{
            id
            name
            }
            }
          }
        }
    """

    entityquery = """
    
        query{
        entity{
          edges{
            node{
        id
        name
        }
        }
        }
        }
        
    """

    brandquery = """query{
          entityBrand{
            edges{
              node{
            id
            brand
            }
            }
          }
        }"""

    statequery = """query{
          masterProductState{
            edges{
              node{
            id
            state
            }
            }
          }
        }"""

    tastequery = """query{
          masterProductTaste{
            edges{
              node{
            id
            taste
            }
            }
          }
        }"""
    nutritionquery = """query{
          masterProductNutritionType{
            edges{
              node{
            id
            type
            }
            }
          }
        }"""

    unittypequery = """query{
          masterProductMeasureType{
            edges{
              node{
            id
            measureType
            }
            }
          }
        }"""

    unitquery = """query{
          masterProductMeasureUnit{
            edges{
              node{
            id
            unit
            }
            }
          }
        }"""

    nextstepquery = """query{
          masterProductNextStep{
            edges{
              node{
            id
            nextStep
            }
            }
          }
        }"""
    productpacking="""
                    query{
                  masterProductPackaging{
                    edges{
                      node{
                        id
                        packagingType
                      }
                    }
                  }
                }
    
                    """

    packinglist=client.execute(query=productpacking)['data']['masterProductPackaging']['edges']
    categorydata = client.execute(query=categoryquery)['data']['masterProductCategory']['edges']
    subcategorydata = client.execute(query=subcategoryquery)['data']['masterProductSubcategory']['edges']
    entitydata = client.execute(query=entityquery)['data']['entity']['edges']
    branddata = client.execute(query=brandquery)['data']['entityBrand']['edges']
    statedata = client.execute(query=statequery)['data']['masterProductState']['edges']
    tastedata = client.execute(query=tastequery)['data']['masterProductTaste']['edges']
    nutritiondata = client.execute(query=nutritionquery)['data']['masterProductNutritionType']['edges']
    unittypedata = client.execute(query=unittypequery)['data']['masterProductMeasureType']['edges']
    unitdata = client.execute(query=unitquery)['data']['masterProductMeasureUnit']['edges']
    nextstepdata = client.execute(query=nextstepquery)['data']['masterProductNextStep']['edges']
    productdata = client.execute(query=productquery, variables=varss1)['data']['productById']
  
    if request.method == 'POST':
        category = request.POST.get('category')
        subcategory = request.POST.get('subcategory')
        entity = request.POST.get('entity')
        brand = request.POST.get('brand')
        name = request.POST.get('name')
        code = request.POST.get('code')
        buysell = request.POST.get('buysell')
        state = request.POST.get('state')
        taste = request.POST.get('taste')
        nutrition = request.POST.get('nutrition')
        unittype = request.POST.get('unittype')
        unit = request.POST.get('unit')
        unitvalue = request.POST.get('unitvalue')
        prod_cgst=request.POST.get('prod_cgst')
        prod_sgst=request.POST.get('prod_sgst')
        prod_igst=request.POST.get('prod_igst')
        prohsn=request.POST.get('prohsn')
        packagingtype=request.POST.get('packagingtype')
        if unitvalue == '':
            unitvalue = 0.0
        denominator = request.POST.get('denominator')
        if denominator == '':
            denominator = 0.0
        aunit = request.POST.get('aunit')
        numerator = request.POST.get('numerator')
        if numerator == '':
            numerator = 0.0
        bunit = request.POST.get('bunit')
        itemvalue = request.POST.get('itemvalue')
        if itemvalue == '':
            itemvalue = 0.0
        mrpinm = request.POST.get('mrpinm')
        if mrpinm == '':
            mrpinm = 0.0
        unitinsidepacs = request.POST.get('unitinsidepacs')
        if unitinsidepacs == '':
            unitinsidepacs = 0.0
        metricsystem = request.POST.get('metricsystem')
        length = request.POST.get('length')
        if length == '':
            length = 0.0
        metricunittype = request.POST.get('metricunittype')
        grossweight = request.POST.get('grossweight')
        if grossweight == '':
            grossweight = 0.0
        moisture = request.POST.get('moisture')
        if moisture == '':
            moisture = 0.0
        y = request.POST.get('yield')
        if y == '':
            y = 0.0
        buyprice = request.POST.get('buyprice')
        if buyprice == '':
            buyprice = 0.0
        sellprice = request.POST.get('sellprice')
        if sellprice == '':
            sellprice = 0.0
        nextstep = request.POST.get('nextstep')
        isbase = request.POST.get('isbase')
        if isbase == None:
            isbase = False
        isveg = request.POST.get('isveg')
        if isveg == None:
            isveg = False
        unitcalorie = request.POST.get('unitcalorie')
        if unitcalorie == '':
            unitcalorie = 0.0
        barcode = request.POST.get('barcode')

         

        query = """

       mutation($id:ID,
            $prodCgst: Float,$prodHsn: String
            $prodIgst: Float,
            $prodSgst: Float, $packagingType:Int
            $category:Int,$subCategory:Int,$entity:Int,$brand:Int,$name:String,$code:String,$buysell:String,$state:Int,$taste:Int,$nutrition:Int,
            $unitType:Int,$unit:Int,$unitValue:Float,$denominator:Float,$aunit:String,$numerator:Float,$bunit:String,$itemValue:Float,$mrpInM:Float,
            $unitInsidePacs:Float,$metricSystem:String,$length:Float,$grossWeight:Float,$metricUnitType:String,$moisture:Float,$y:Float,$buyPrice:Float,
            $sellPrice:Float,$nextStep:Int,$isBase:Boolean,$isVeg:Boolean,$unitCalorie:Float,$barcode:String){
            updateProduct(id:$id,
            prodHsn:$prodHsn,
            prodCgst: $prodCgst,
            prodIgst:$prodIgst,
            prodSgst: $prodSgst,
            packagingType:$packagingType,
            category:$category,subCategory:$subCategory,entity:$entity,brand:$brand,name:$name,code:$code,buysell:$buysell,state:$state,taste:$taste,
            nutrition:$nutrition,unitType:$unitType,unit:$unit,unitValue:$unitValue,denominator:$denominator,aunit:$aunit,numerator:$numerator,bunit:$bunit,
            itemValue:$itemValue,mrpInM:$mrpInM,unitInsidePacs:$unitInsidePacs,metricSystem:$metricSystem,length:$length,grossWeight:$grossWeight,
            metricUnitType:$metricUnitType,moisture:$moisture,yieldField:$y,buyPrice:$buyPrice,sellPrice:$sellPrice,nextStep:$nextStep,isBase:$isBase,isVeg:$isVeg,
            unitCalorie:$unitCalorie,barcode:$barcode){
            ok
          }
        }

        """

        variables = {"id": id, "category": category, "subCategory": subcategory, "entity": entity, "brand": brand,
                     "name": name,
                     "code": code, "buysell": buysell, "state": state, "taste": taste, "nutrition": nutrition,
                     "unitType": unittype, "unit": unit, "unitValue": unitvalue, "denominator": denominator,
                     "aunit": aunit,
                     "numerator": numerator, "bunit": bunit, "itemValue": itemvalue, "mrpInM": mrpinm,
                     "unitInsidePacs": unitinsidepacs,
                     "metricSystem": metricsystem, "length": length, "grossWeight": grossweight,
                     "metricUnitType": metricunittype, "moisture": moisture, "y": y, "buyPrice": buyprice,
                     "sellPrice": sellprice, "nextStep": nextstep,
                     "isBase": isbase, "isVeg": isveg, "unitCalorie": unitcalorie, "barcode": barcode,
                     
                     "prodCgst":prod_cgst,
                     "prodSgst":prod_sgst,
                     "prodIgst":prod_igst,
                     "prodHsn":prohsn,
                     "packagingType":packagingtype
                     
                     }

        client.execute(query=query, variables=variables)

        return redirect('product_list')
    return render(request, path.join('product', 'update_product.html'), {"category": categorydata,
                                                                         "subcategory": subcategorydata,
                                                                         "entity": entitydata, "brand": branddata,
                                                                         "state": statedata, "taste": tastedata,
                                                                         "nutrition": nutritiondata,
                                                                         "unittype": unittypedata,
                                                                         "unit": unitdata, "nextstep": nextstepdata,
                                                                         "product": productdata,"packinglist":packinglist})


def delete_product(request, id):
     

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                              deleteProduct(id:$id){
                                ok
                              }
                            }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('product_list')

    query = """query($id:Int!)
        {
            productById(id: $id){
            name
        id
        }
        }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['productById']

    return render(request, path.join('product', 'delete_product.html'), {"data": data})


# ======composition=======
def composition_list(request):
    if request.POST.get('Search')=="Search":
        searchProductComposition=request.POST.get('searchProductComposition')
        request.session['productCompositionSearch']=searchProductComposition
        request.session['productCompositionOffset']=0
        request.session['productCompositionCurrentPage']=1

    if request.POST.get("productCompostionSize")==None:
       pass
    else:
       request.session['productCompositionSize']=int(request.POST.get('productCompostionSize'))
       request.session['productCompositionFirst']=request.session['productCompositionSize']
       request.session['productCompositionOffset']=0
       request.session['productCompositionCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['productCompositionSearch']=None
      request.session['productCompositionSize']=None
      request.session['productCompositionFirst']=10 #defalt Value 
    query2="""
                query($search:String){
                    productComposition(product_Name_Icontains:$search){
                      edges{
                        node{
                      id
                      }
                      }
                      }
                     }
     
    """
    td=client.execute(query=query2,variables={"search": request.session['productCompositionSearch']})['data']['productComposition']['edges']
    total_records = len(td)
    if total_records%request.session['productCompositionFirst']==0:
      total_pages = total_records//request.session['productCompositionFirst']
    else:
      total_pages = total_records//request.session['productCompositionFirst']+1
    query = """

                 query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                  productComposition(offset:$offset,first:$first,last:$last,product_Name_Icontains:$search,orderBy:$orderBy){
                    edges{
                      node{
                    id
                    product {
                      id
                      name
                    }
                    baseProduct {
                      id
                      name
                    }
                    compositionUnit {
                      id
                      unit
                    }
                    compositionValue
                    compositionConversion
                    calorie
                    compositionUnit{
                      id
                      unit
                    }
                  }
                }
                }
                }
           """
    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['productCompositionOffset']=0
      request.session['productCompositionCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['productCompositionOffset']-=request.session['productCompositionFirst']
      request.session['productCompositionCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['productCompositionOffset']+=request.session['productCompositionFirst']
      request.session['productCompositionCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['productCompositionCurrentPage']=total_pages
      if total_records%int(request.session['productCompositionFirst'])==0:
        last=total_records%int(request.session['productCompositionFirst'])+int(request.session['productCompositionFirst'])
        request.session['productCompositionOffset']=total_records-int(request.session['productCompositionFirst'])
      else:
        last=total_records%int(request.session['productCompositionFirst'])
        request.session['productCompositionOffset']=total_records-total_records%int(request.session['productCompositionFirst'])
    #====End Paginator====


    #========Start Sorting=======#
    
    if request.POST.get('productcompo')=="Product":
      if request.session['productCompositionSorting']=='product__name':
        request.session['productCompositionSorting']='-product__name'
      else:
        request.session['productCompositionSorting']='product__name'

    if request.POST.get('baseProduct')=="Base Product":
      if request.session['productCompositionSorting']=='base_product__name':
        request.session['productCompositionSorting']='-base_product__name'
      else:
        request.session['productCompositionSorting']='base_product__name'

    if request.POST.get('compositionunit')=="Composition Unit":
      if request.session['productCompositionSorting']=='composition_unit__unit':
        request.session['productCompositionSorting']='-composition_unit__unit'
      else:
        request.session['productCompositionSorting']='composition_unit__unit'

    if request.POST.get('compositioncon')=="Composition Conversion":
      if request.session['productCompositionSorting']=='composition_conversion':
        request.session['productCompositionSorting']='-composition_conversion'
      else:
        request.session['productCompositionSorting']='composition_conversion'

    if request.POST.get('compositionvalue')=="Composition Value":
      if request.session['productCompositionSorting']=='composition_value':
        request.session['productCompositionSorting']='-composition_value'
      else:
        request.session['productCompositionSorting']='composition_value'

    if request.POST.get('calories')=="Calorie":
      if request.session['productCompositionSorting']=='calorie':
        request.session['productCompositionSorting']='-calorie'
      else:
        request.session['productCompositionSorting']='calorie'
    
   

    #========End Sorting=========#
    v={
      "offset":request.session['productCompositionOffset'],
      "first":request.session['productCompositionFirst'],
      "search":request.session['productCompositionSearch'],
      "last":last,
      "orderBy":request.session['productCompositionSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['productComposition']['edges']
    return render(request, path.join('product', 'composition_list.html'), {"entity_list": entity_list,"max_page":total_pages})


def create_composition(request):
     

    q1 = """
        query{
        product{
          edges{
            node{
        id
        name
        }
        }
        }
        }
    """

    q2 = """
        query{
            masterProductMeasureUnit{
              edges{
                node{
        id
        unit
        }
        }
        }
        }
    """
    products = client.execute(query=q1)['data']['product']['edges']
    units = client.execute(query=q2)['data']['masterProductMeasureUnit']['edges']
    if request.method == 'POST':
        product = request.POST.get('product')
        baseproduct = request.POST.get('baseproduct')
        compositionunit = request.POST.get('compositionunit')
        compositionconversion = request.POST.get('compositionconversion')
        compositionvalue = request.POST.get('compositionvalue')
        calorie = request.POST.get('calorie')

         

        query = """

       mutation($product:Int,$baseProduct:Int,$compositionUnit:Int,$compositionConversion:Float,$compositionValue:Float,$calorie:Float){
          createProductComposition(product:$product, baseProduct:$baseProduct, compositionUnit:$compositionUnit,
                                                        compositionConversion:$compositionConversion, compositionValue:$compositionValue, calorie:$calorie){
            ok
          }
        }

        """

        variables = {
            "product": product,
            "baseProduct": baseproduct,
            "compositionUnit": compositionunit,
            "compositionValue": compositionvalue,
            "compositionConversion": compositionconversion,
            "calorie": calorie
        }

        client.execute(query=query, variables=variables)

        return redirect('composition_list')
    return render(request, path.join('product', 'create_composition.html'), {"products": products, "units": units})


def update_composition(request, id):
     

    q1 = """
        query{
        product{
          edges{
            node{
        id
        name
        }
        }
        }
        }
    """

    q2 = """
        query{
            masterProductMeasureUnit{
              edges{
                node{
        id
        unit
        }
        }
        }
        }
    """
    products = client.execute(query=q1)['data']['product']['edges']
    units = client.execute(query=q2)['data']['masterProductMeasureUnit']['edges']
    query = """
                        query($id:Int!){
                          productCompositionById(id:$id){
                            id
                            product{
                              id
                              name
                            }
                            baseProduct{
                              id
                              name
                            }
                            compositionUnit{
                              id
                              unit
                            }
                            compositionValue
                            compositionConversion
                            calorie
                            
                          }
                          }
                        """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['productCompositionById']

    if request.method == 'POST':
        product = request.POST.get('product')
        baseproduct = request.POST.get('baseproduct')
        compositionunit = request.POST.get('compositionunit')
        compositionconversion = request.POST.get('compositionconversion')
        compositionvalue = request.POST.get('compositionvalue')
        calorie = request.POST.get('calorie')

        q = """

               mutation($id:ID,$product:Int,$baseProduct:Int,$compositionUnit:Int,$compositionConversion:Float,$compositionValue:Float,$calorie:Float){
                  updateProductComposition(id:$id,product:$product, baseProduct:$baseProduct, compositionUnit:$compositionUnit,
                                                                compositionConversion:$compositionConversion, compositionValue:$compositionValue, calorie:$calorie){
                    ok
                  }
                }

                """

        v = {
            "id": id,
            "product": product,
            "baseProduct": baseproduct,
            "compositionUnit": compositionunit,
            "compositionValue": compositionvalue,
            "compositionConversion": compositionconversion,
            "calorie": calorie
        }
        client.execute(query=q, variables=v)
        return redirect("composition_list")

    return render(request, path.join('product', 'update_composition.html'), {"data": data, "p": products,
                                                                             "u": units})


def delete_composition(request, id):
     

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                        deleteProductComposition(id:$id){
                                              ok    
                                            }
                                    }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('composition_list')

    query = """

                    query($id:Int!){
                          productCompositionById(id:$id){
                            id
                            product{
                            id
                            name
                            }
                          }
                        }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['productCompositionById']
    
    return render(request, path.join('product', 'delete_composition.html'), {"data": data})


# ======Unit=======
def unit_list(request):
    if request.POST.get('Search')=="Search":
        searchproductunit=request.POST.get('searchproductunit')
        request.session['productUnitSearch']=searchproductunit
        request.session['productUnitOffset']=0
        request.session['productUnitCurrentPage']=1

    if request.POST.get("productUnitSize")==None:
       pass
    else:
       request.session['productUnitSize']=int(request.POST.get('productUnitSize'))
       request.session['productUnitFirst']=request.session['productUnitSize']
       request.session['productUnitOffset']=0
       request.session['productUnitCurrentPage']=1


    #=======Clear=========
    if request.POST.get('Clear')=="Clear":
      request.session['productUnitSearch']=None
      request.session['productUnitSize']=None
      request.session['productUnitFirst']=3 #defalt Value


    query2="""
    
              query($search:String){
                      productUnits(product_Name_Icontains:$search){
                        edges{
                          node{
                                id
                            }
                            }
                            }
                            }
    """

    td=client.execute(query=query2,variables={"search":request.session['productUnitSearch']})['data']['productUnits']['edges']

    total_records = len(td)
    
    if total_records%request.session['productUnitFirst']==0:
      total_pages = total_records//request.session['productUnitFirst']
    else:
      total_pages = total_records//request.session['productUnitFirst']+1
    query = """

                     query($offset:Int,$first:Int,$last:Int,$search:String,$orderBy:String){
                      productUnits(offset:$offset,first:$first,last:$last,product_Name_Icontains:$search,orderBy:$orderBy){
                        edges{
                          node{
                        id
                        product{
                          id
                          name
                        }
                        secUnitType{
                          id
                          measureType
                        }
                        secUnit{
                          id
                          unit
                        }
                        secConversion
                        secValue
                      }
                    }
                    }
                    }
          """


    # Starting Paginator====
    if request.POST.get('First')=="First":
      request.session['productUnitOffset']=0
      request.session['productUnitCurrentPage']=1
    if request.POST.get('Previous')=="Previous":
      request.session['productUnitOffset']-=request.session['productUnitFirst']
      request.session['productUnitCurrentPage']-=1
    if request.POST.get('Next')=="Next":
      request.session['productUnitOffset']+=request.session['productUnitFirst']
      request.session['productUnitCurrentPage']+=1
    last=None
    if request.POST.get('Last')=="Last":
      request.session['productUnitCurrentPage']=total_pages
      if total_records%int(request.session['productUnitFirst'])==0:
        last=total_records%int(request.session['productUnitFirst'])+int(request.session['productUnitFirst'])
        request.session['productUnitOffset']=total_records-int(request.session['productUnitFirst'])
      else:
        last=total_records%int(request.session['productUnitFirst'])
        request.session['productUnitOffset']=total_records-total_records%int(request.session['productUnitFirst'])
    
   
    #====End Paginator==== 
    #========Start Sorting=======#
    
    if request.POST.get('productUnit')=="Product":
      
      if request.session['productUnitSorting']=='product__name':
        request.session['productUnitSorting']='-product__name'
      else:
        request.session['productUnitSorting']='product__name'
    
    if request.POST.get('secUnitType')=="Sec Unit Type":
      if request.session['productUnitSorting']=='sec_unit_type__measure_type':
        request.session['productUnitSorting']='-sec_unit_type__measure_type'
      else:
        request.session['productUnitSorting']='sec_unit_type__measure_type'

    if request.POST.get('secUnit')=="Sec Unit":
      if request.session['productUnitSorting']=='sec_unit__unit':
        request.session['productUnitSorting']='-sec_unit__unit'
      else:
        request.session['productUnitSorting']='sec_unit__unit'

    if request.POST.get('secConversion')=="Sec Conversion":
      if request.session['productUnitSorting']=='sec_conversion':
        request.session['productUnitSorting']='-sec_conversion'
      else:
        request.session['productUnitSorting']='sec_conversion'

    if request.POST.get('secValue')=="Sec Value":
      if request.session['productUnitSorting']=='sec_value':
        request.session['productUnitSorting']='-sec_value'
      else:
        request.session['productUnitSorting']='sec_value'

    #========End Sorting=========#     
    v={
      "offset":request.session['productUnitOffset'],
      "first": request.session['productUnitFirst'],
      "last":last,
      "search":request.session['productUnitSearch'],
      "orderBy":request.session['productUnitSorting']
    }
    entity_list = client.execute(query=query,variables=v)['data']['productUnits']['edges']

    return render(request, path.join('product', 'unit_list.html'), {"entity_list": entity_list,"max_page":total_pages})


def create_unit(request):
     

    q1 = """
        query{
        product{
          edges{
            node{
        id
        name
        }
        }
        }
        }
    """

    q2 = """
        query{
        masterProductMeasureUnit{
          edges{
            node{
        id
        unit
        }
        }
        }
        }
    """

    q3 = """
        query{
        masterProductMeasureType{
          edges{
            node{
        id
        measureType
        }
        }
        }
        }
    """
    products = client.execute(query=q1)['data']['product']['edges']
    units = client.execute(query=q2)['data']['masterProductMeasureUnit']['edges']
    unittype = client.execute(query=q3)['data']['masterProductMeasureType']['edges']

    if request.method == 'POST':
        product = request.POST.get('product')
        s_unittype = request.POST.get('s_unittype')
        s_unit = request.POST.get('s_unit')
        secconversion = request.POST.get('secconversion')
        secvalue = request.POST.get('secvalue')

         

        query = """

       mutation($product:Int,$secValue:Float,$secConversion:Float,$secUnitType:Int,$secUnit:Int){
          createProductUnits(product:$product, secUnit:$secUnit,secValue:$secValue,secConversion:$secConversion,secUnitType:$secUnitType){
            ok
          }
        }

        """

        variables = {
            "product": product,
            "secValue": secvalue,
            "secConversion": secconversion,
            "secUnitType": s_unittype,
            "secUnit": s_unit
        }

        client.execute(query=query, variables=variables)

        return redirect('unit_list')
    return render(request, path.join('product', 'create_unit.html'), {"products": products, "units": units,
                                                                      "unittype": unittype})


def update_unit(request, id):
     

    qu = """query($id:Int!){
            productUnitsById(id:$id){
            id
            product{
              id
              name
            }
            secUnitType{
              id
              measureType
            }
            secUnit{
              id
              unit
            }
            secConversion
            secValue
            }
            }"""

    q1 = """
        query{
        product{
          edges{
            node{
        id
        name
        }
        }
        }
        }
    """

    q2 = """
        query{
        masterProductMeasureUnit{
          edges{
            node{
        id
        unit
        }
        }
        }
        }
    """

    q3 = """
        query{
        masterProductMeasureType{
          edges{
            node{
        id
        measureType
        }
        }
        }
        }
    """
    products = client.execute(query=q1)['data']['product']['edges']
    units = client.execute(query=q2)['data']['masterProductMeasureUnit']['edges']
    unittype = client.execute(query=q3)['data']['masterProductMeasureType']['edges']
    data = client.execute(query=qu, variables={"id": id})['data']['productUnitsById']

    if request.method == 'POST':
        product = request.POST.get('product')
        s_unittype = request.POST.get('s_unittype')
        s_unit = request.POST.get('s_unit')
        secconversion = request.POST.get('secconversion')
        secvalue = request.POST.get('secvalue')

         

        query = """
               mutation($id:ID,$product:Int,$secValue:Float,$secConversion:Float,$secUnitType:Int,$secUnit:Int){
                  updateProductUnits(id:$id,product:$product, secUnit:$secUnit,secValue:$secValue,secConversion:$secConversion,secUnitType:$secUnitType){
                    ok
                  }
                }

                """
        variables = {
            "id": id,
            "product": product,
            "secValue": secvalue,
            "secConversion": secconversion,
            "secUnitType": s_unittype,
            "secUnit": s_unit,
        }

        client.execute(query=query, variables=variables)
        return redirect("unit_list")

    return render(request, path.join('product', 'update_unit.html'), {"products": products, "unittype": unittype,
                                                                      "units": units, "data": data})


def delete_unit(request, id):

     

    if request.method == 'POST':
        mutation = """mutation($id:ID){
                                        deleteProductUnits(id:$id){
                                              ok    
                                            }
                                    }"""
        mvars = {
            "id": id
        }
        client.execute(query=mutation, variables=mvars)
        return redirect('unit_list')

    query = """

                    query($id:Int!){
                      productUnitsById(id:$id){ 
                        product{
                          id
                          name
                        }
                    }
                    }"""

    variables = {
        "id": id
    }
    data = client.execute(query=query, variables=variables)['data']['productUnitsById']

    return render(request, path.join('product', 'delete_unit.html'), {"data": data})



#======Create View of All Pages======




def view_unit(request, id):
     

    qu = """query($id:Int!){
            productUnitsById(id:$id){
            id
            product{
              id
              name
            }
            secUnitType{
              id
              measureType
            }
            secUnit{
              id
              unit
            }
            secConversion
            secValue
            }
            }"""

    q1 = """
        query{
        product{
          edges{
            node{
        id
        name
        }
        }
        }
        }
    """

    q2 = """
        query{
        masterProductMeasureUnit{
          edges{
            node{
        id
        unit
        }
        }
        }
        }
    """

    q3 = """
        query{
        masterProductMeasureType{
          edges{
            node{
        id
        measureType
        }
        }
        }
        }
    """
    products = client.execute(query=q1)['data']['product']['edges']
    units = client.execute(query=q2)['data']['masterProductMeasureUnit']['edges']
    unittype = client.execute(query=q3)['data']['masterProductMeasureType']['edges']
    data = client.execute(query=qu, variables={"id": id})['data']['productUnitsById']
    return render(request, path.join('product', 'view_unit.html'), {"products": products, "unittype": unittype,"units": units, "data": data})



def view_composition(request, id):
     

    q1 = """
        query{
        product{
          edges{
            node{
        id
        name
        }
        }
        }
        }
    """

    q2 = """
        query{
            masterProductMeasureUnit{
              edges{
                node{
        id
        unit
        }
        }
        }
        }
    """
    products = client.execute(query=q1)['data']['product']['edges']
    units = client.execute(query=q2)['data']['masterProductMeasureUnit']['edges']
    query = """
                        query($id:Int!){
                          productCompositionById(id:$id){
                            id
                            product{
                              id
                              name
                            }
                            baseProduct{
                              id
                              name
                            }
                            compositionUnit{
                              id
                              unit
                            }
                            compositionValue
                            compositionConversion
                            calorie
                            
                          }
                          }
                        """
    varss = {"id": id}
    data = client.execute(query=query, variables=varss)['data']['productCompositionById']

    return render(request, path.join('product', 'view_composition.html'), {"data": data, "p": products,
                                                                             "u": units})




def view_product(request, id):
     
    productquery = """query($id:Int!){
                                    productById(id:$id){
                                            id
                                            prodHsn
                                            prodCgst
                                            prodSgst
                                            prodIgst
                                            packagingType{
                                                
                                             id
                                              packagingType
                                            }
                                            category{
                                                id
                                                name
                                              }
                                            subCategory{
                                                  id
                                                  name
                                                  }
                                            entity{id
                                            name}
                                            brand{
                                            id
                                            brand}
                                            name
                                            code
                                            buysell
                                            state{
                                            id
                                            state}
                                            taste{
                                            id
                                            taste}
                                            nutrition{
                                            id
                                            type}
                                            unitType{
                                            id
                                            measureType}
                                            unit{
                                            id
                                            unit}
                                            unitValue
                                            denominator
                                            aunit
                                            numerator
                                            bunit
                                            itemValue
                                            mrpInM
                                            unitInsidePacs
                                            metricSystem
                                            grossWeight
                                            metricUnitType
                                            length
                                            moisture
                                            yieldField
                                            buyPrice
                                            sellPrice
                                            nextStep{
                                            id
                                            nextStep}
                                            isBase
                                            isVeg
                                            unitCalorie
                                            barcode
                                          }
                                        }
"""

    varss1 = {"id": id}
    categoryquery = """
            query{
          masterProductCategory{
            edges{
              node{
            id
            name
          }
          }
          }
        }
    """
    subcategoryquery = """
            query{
          masterProductSubcategory{
            edges{
              node{
            id
            name
            }
            }
          }
        }
    """

    entityquery = """
    
        query{
        entity{
          edges{
            node{
        id
        name
        }
        }
        }
        }
        
    """

    brandquery = """query{
          entityBrand{
            edges{
              node{
            id
            brand
            }
            }
          }
        }"""

    statequery = """query{
          masterProductState{
            edges{
              node{
            id
            state
            }
            }
          }
        }"""

    tastequery = """query{
          masterProductTaste{
            edges{
              node{
            id
            taste
            }
            }
          }
        }"""
    nutritionquery = """query{
          masterProductNutritionType{
            edges{
              node{
            id
            type
            }
            }
          }
        }"""

    unittypequery = """query{
          masterProductMeasureType{
            edges{
              node{
            id
            measureType
            }
            }
          }
        }"""

    unitquery = """query{
          masterProductMeasureUnit{
            edges{
              node{
            id
            unit
            }
            }
          }
        }"""

    nextstepquery = """query{
          masterProductNextStep{
            edges{
              node{
            id
            nextStep
          }
          }
          }
        }"""

    categorydata = client.execute(query=categoryquery)['data']['masterProductCategory']['edges']
    subcategorydata = client.execute(query=subcategoryquery)['data']['masterProductSubcategory']['edges']
    entitydata = client.execute(query=entityquery)['data']['entity']['edges']
    branddata = client.execute(query=brandquery)['data']['entityBrand']['edges']
    statedata = client.execute(query=statequery)['data']['masterProductState']['edges']
    tastedata = client.execute(query=tastequery)['data']['masterProductTaste']['edges']
    nutritiondata = client.execute(query=nutritionquery)['data']['masterProductNutritionType']['edges']
    unittypedata = client.execute(query=unittypequery)['data']['masterProductMeasureType']['edges']
    unitdata = client.execute(query=unitquery)['data']['masterProductMeasureUnit']['edges']
    nextstepdata = client.execute(query=nextstepquery)['data']['masterProductNextStep']['edges']
    productdata = client.execute(query=productquery, variables=varss1)['data']['productById']

    return render(request, path.join('product', 'view_product.html'), {"category": categorydata,
                                                                         "subcategory": subcategorydata,
                                                                         "entity": entitydata, "brand": branddata,
                                                                         "state": statedata, "taste": tastedata,
                                                                         "nutrition": nutritiondata,
                                                                         "unittype": unittypedata,
                                                                         "unit": unitdata, "nextstep": nextstepdata,
                                                                         "product": productdata})