function validate_composition(){
    
  product=document.forms['composition']['product'].value;
  baseproduct=document.forms['composition']['baseproduct'].value;
  compositionunit=document.forms['composition']['compositionunit'].value;

  if(product=="Please Select Product"){
      document.getElementById('product-m').innerHTML="This field is required";
      return false;
  }
  if(baseproduct=="Please Select Base Product"){
      document.getElementById('baseproduct-m').innerHTML="This field is required";
      return false;
  }
  if(compositionunit=="Please Select Composition Unit"){
      document.getElementById('compositionunit-m').innerHTML="This field is required";
      return false;
  }
}