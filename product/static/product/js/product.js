function validate_product(){
  category=document.forms['product']['category'].value;
  subcategory=document.forms['product']['subcategory'].value;
  entity=document.forms['product']['entity'].value;
  brand=document.forms['product']['brand'].value;
  name=document.forms['product']['name'].value;
  code=document.forms['product']['code'].value;
  buysell=document.forms['product']['buysell'].value;
  state=document.forms['product']['state'].value;
  taste=document.forms['product']['taste'].value;
  nutrition=document.forms['product']['nutrition'].value;
  unittype=document.forms['product']['unittype'].value;
  unit=document.forms['product']['unit'].value;
  aunit=document.forms['product']['aunit'].value;
  bunit=document.forms['product']['bunit'].value;
  nextstep=document.forms['product']['nextstep'].value;

  if(category=="Please Select Category"){
      document.getElementById('category-m').innerHTML="This field is required";
   return false;
  }
  if(subcategory=="Please Select SubCategory"){
      document.getElementById('subcategory-m').innerHTML="This field is required";
   return false;
  }
  if(entity=="Please Select Entity"){
      document.getElementById('entity-m').innerHTML="This field is required";
   return false;
  }
  if(brand=="Please Select Brand"){
      document.getElementById('brand-m').innerHTML="This field is required";
   return false;
  }
  if(name==""){
      document.getElementById('name-m').innerHTML="This field cannot be blank";
   return false;
  }
  if(code==""){
      document.getElementById('code-m').innerHTML="This field cannot be blank";
   return false;
  }
  if(buysell==""){
      document.getElementById('buysell-m').innerHTML="This field cannot be blank";
   return false;
  }
  if(state=="Please Select State"){
      document.getElementById('state-m').innerHTML="This field is required";
   return false;
  }
  if(taste=="Please Select Taste"){
      document.getElementById('taste-m').innerHTML="This field is required";
   return false;
  }
  if(nutrition=="Please Select Nutrition"){
      document.getElementById('nutrition-m').innerHTML="This field is required";
   return false;
  }
  if(unittype=="Please Select Unit Type"){
      document.getElementById('unittype-m').innerHTML="This field is required";
   return false;
  }
  if(unit=="Please Select Unit"){
      document.getElementById('unit-m').innerHTML="This field is required";
   return false;
  }
  
  if(aunit==""){
      document.getElementById('aunit-m').innerHTML="This field cannot be blank";
   return false;
  }
  if(bunit==""){
      document.getElementById('bunit-m').innerHTML="This field cannot be blank";
   return false;
  }
  if(nextstep=="Please Select Next Step"){
      document.getElementById('nextstep-m').innerHTML="This field is required";
   return false;
  }
}