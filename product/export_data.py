from django.http.response import HttpResponse
import csv
from wastelink.settings import client
def product_composition_data(request):
  query="""query{
                 productComposition{
                    edges{
                      node{
                    id
                    product {
                      id
                      name
                    }
                    baseProduct {
                      id
                      name
                    }
                    compositionUnit {
                      id
                      unit
                    }
                    compositionValue
                    compositionConversion
                    calorie
                    compositionUnit{
                      id
                      unit
                    }
                  }
                }
                }
                }
  
  """
  composition_data=client.execute(query=query)['data']['productComposition']['edges']
  product_compo_Id=[]
  product=[]
  baseProduct=[]
  compositionUnit=[]
  compositionValue=[]
  compositionConversion=[]
  calorie=[]
  for i in composition_data:
    product_compo_Id.append(i['node']['id'])
    product.append(i['node']['product']['name'])
    baseProduct.append(i['node']['baseProduct']['name'])
    compositionUnit.append(i['node']['compositionUnit']['unit'])
    compositionConversion.append(i['node']['compositionConversion'])
    compositionValue.append(i['node']['compositionValue'])
    calorie.append(i['node']['calorie'])
    
    
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="product_composition.csv"'},
    )

  writer = csv.writer(response)
  writer.writerow(['ID', 'PRODUCT', 'bASE PRODUCT', 'COMPOSITION UNIT','COMPOSITION CONVERSION','COMPOSITION VALUE','CALORIE'])
  
  for i in range(len(product_compo_Id)):
    writer.writerow([product_compo_Id[i],product[i],baseProduct[i],compositionUnit[i],compositionConversion[i],compositionValue[i],calorie[i]])
    
  return response

def product_units_data(request):
  

  query = """

                     query{
                      productUnits{
                        edges{
                          node{
                        id
                        product{
                          id
                          name
                        }
                        secUnitType{
                          id
                          measureType
                        }
                        secUnit{
                          id
                          unit
                        }
                        secConversion
                        secValue
                      }
                    }
                    }
                    }
          """
  product_unit_list=client.execute(query=query)['data']['productUnits']['edges']
  product_untiId=[]
  product=[]
  setUnitType=[]
  secUnit=[]
  seccon=[]
  secvalue=[]
  for i in product_unit_list:
    product_untiId.append(i['node']['id'])
    product.append(i['node']['product'])
    setUnitType.append(i['node']['secUnitType']['measureType'])
    secUnit.append(i['node']['secUnit'])
    seccon.append(i['node']['secConversion'])
    secvalue.append(i['node']['secValue'])
  
  response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename="product_units.csv"'},
    )

  writer = csv.writer(response)
  writer.writerow(['Id', 'Product', 'Sec Unit Type', 'Sec Unit','Sec Conversion','Sec Value'])
  
  for i in range(len(product_untiId)):
    writer.writerow([product_untiId[i],product[i]['name'],setUnitType[i],secUnit[i]['unit'],seccon[i],secvalue[i]])
    
  return response